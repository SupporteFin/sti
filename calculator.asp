﻿<!DOCTYPE html>

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->
<%
session("cur_page")="IR " &  listed_share & " Investment Calculator"
session("page_asp")="calculator.asp"
page_name="Investment Calculator"

if  session("lang")="E"  then
   messageError="Please input only number."
else
   messageError="กรุณากรอกข้อมูลเป็นตัวเลข"
end if
%>

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<TITLE><%=message_title%> :: <%=page_name%></TITLE>
	<META name="Title" content="<%=message_metatitle%>">
	<META name="Description" content="<%=message_description%>">
	<META name="Keywords" content="<%=message_keyword%>">	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="author" content="">
	<link rel="shortcut icon" href="<%=img_icon%>">
	<link href="style_listed.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../function2007_utf8.js"></script>
	<!-- CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/flexslider.css" rel="stylesheet" type="text/css">
	<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
	<!-- ADD CSS -->
	<link href="css/component.css" rel="stylesheet" type="text/css">
	<link href="css/custom.css" rel="stylesheet" type="text/css">
	<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">
	<!-- FONTS -->
	<link href="css/font_add.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- Mouse Hover CSS -->
	<script src="scripts/jquery.min.js" type="text/javascript"></script>
	<script src="scripts/jquery.nicescroll.min.js"></script>
	<!-- SCRIPTS -->
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<!--[if IE]><html class="ie" lang="en"> <![endif]-->
	<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
	<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
	<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
	<script src="scripts/animate.js" type="text/javascript"></script>
	<script src="scripts/myscript.js" type="text/javascript"></script>
	<script src="scripts/owl.carousel.js" type="text/javascript"></script>
	<!-- NEW -->
	<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
	<link href="css/site.css" rel="stylesheet" type="text/css" />
	<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
	<!-- scroll-hint -->
	<link rel="stylesheet" href="css/scroll-hint.css">
	
	<script language="javascript" src="../../function2007_utf8.js"></script>
	<script language="javascript" src="scripts/function.js"></script>
	
	<style>
		input[type=text] {
			text-align: right;
			border-radius: 0px;
			border: 1px solid #006c9d;
			line-height: 20px;
			font-size: 15px;
			font-weight: normal;
			margin-bottom: 4px;
			height: 30px !important;
		}
		input[type=text]:focus {
			border: 1px solid #006c9d;
		}
		
	</style>
</head>

<body class="main" style="font-size: 15px;">

	<!-- PRELOADER -->
	<img id="preloader" src="images/preloader.gif" alt="">
	<!-- //PRELOADER -->
	<div class="preloader_hide">
		<div id="page" class="single_page">

			<!--================================ Menu Top ==================================-->
				<!--#include file='i_menu_top.asp'-->
			<!--=================================== End ====================================-->

			<!--=============================== Menu Left ==================================-->
				<!--#include file='i_menu_left.asp'-->
			<!--================================== End =====================================-->

			<!--============================ Title Menu =============================-->
			<section class="mbr-section article">
				<div class="container">
					<div class="bigTopic">
						<h1 class="bigTopicText"><%=arr_menu_desc(56)%></h1>
					</div>
				</div>
			</section>
			<!--============================== End ==================================-->

			<section class="mbr-section article sectionSub">
			<!--===================================================== Content ====================================================-->
			
				<div class="container">
					<table width="75%" border="0" cellspacing="0" cellpadding="0" align="center">
						<tr>
							<td align="center" valign="top">    
								<form name="calculator" METHOD="POST" ACTION="<%=session("page_asp")%>">
								<%if session("lang")="E" then%>
									<tr>
										<td align="center" valign="top" style="padding-left: 15px;"> 
											<table width="100%" cellspacing="1" cellpadding="3" border="0 class="ir_table" align="center" style="margin-top: -40px;">
												<tr align="left">
													  <td colspan="3" >
														<br><b>[</font> Instructions: ]</font></b> To estimate your profit and loss, please fill up the following 3 columns,  “<strong>Price Purchase</strong>”, “<strong>Share Held</strong>” and “<strong>Price Sold</strong>”.
													  </td>
												</tr>          
												<tr align="left">
													<td colspan="3" class="font_calculator"><br><b>Profit & Loss Calculation</b></td>
												</tr>
												<tr align="left">
													  <td width="35%">Price Purchased (THB)</td>
													  <td width="25%"><input type="text" name="price_purchase_enter" id="price_purchase_enter"   onKeyPress="return BannerKey(this);"  class="cellinput"></td>
													  <td valign="bottom"><div id='message_price_purchase' class="noMessageError" style="display:none"><%=messageError%></div></td>
												</tr>
												<tr align="left">
													  <td>Share Held</td>
													  <td><input type="text" name="shares_held_enter" id="shares_held_enter" onKeyPress="return BannerKey(this);" class="cellinput"></td>
													  <td valign="bottom"><div id='message_shares_held' class="noMessageError" style="display:none"><%=messageError%></div></td>
												</tr>
												<tr align="left">
													  <td>Commission (%)</td>
													  <td><input type="text" name="commission_enter" id="commission_enter"  value="0.25"  onKeyPress="return BannerKey(this);"  class="cellinput"></td>
													  <td valign="bottom"><div id='message_commission' class="noMessageError" style="display:none"><%=messageError%></div></td>
												</tr>
												<tr align="left">
													  <td>Minimum Commission (THB)</td>
													  <td><input type="text" name="min_commission_enter" id="min_commission_enter" value="50"  onKeyPress="return BannerKey(this);"  class="cellinput"></td>
													  <td valign="bottom"><div id='message_min_commission' class="noMessageError" style="display:none"><%=messageError%></div></td>
												</tr>
												<tr align="left">
													  <td>VAT (%)</td>
													  <td><input type="text" name="vat_enter" id="vat_enter" value="7"  onKeyPress="return BannerKey(this);"  class="cellinput"></td>
													  <td valign="bottom"><div id='message_vat' class="noMessageError" style="display:none"><%=messageError%></div></td>
												</tr>
												<tr align="left">
													  <td>Price Sold (THB)</td>
													  <td><input type="text" name="price_sold_enter" id="price_sold_enter" value=""  onKeyPress="return BannerKey(this);"  class="cellinput"></td>
													  <td valign="bottom"><div id='message_price_sold' class="noMessageError" style="display:none"><%=messageError%></div></td>
												</tr>                                                       
												<tr>
													<td colspan="3"><div class="ir_textDivider"></div></td>
												</tr>
												<tr align="left">
													<td colspan="3" class="font_calculator"><br><b>Gross Dividend Per Share</b></td>
												</tr>
												<tr align="left">
													  <td  width="35%">Taxed (THB)</td>
													  <td  width="25%"><input type="text" name= "dividend_taxed_enter" id= "dividend_taxed_enter"  onKeyPress="return BannerKey(this);"  class="cellinput"></td>
													  <td valign="bottom"><div id='message_dividend_taxed' class="noMessageError" style="display:none"><%=messageError%></div></td>
												</tr>
												<tr align="left">
													  <td>Tax Exempt (THB)</td>
													  <td><input type="text" name="dividend_tax_exempt_enter" id="dividend_tax_exempt_enter"  onKeyPress="return BannerKey(this);"  class="cellinput"></td>
													  <td valign="bottom"><div id='message_dividend_tax_exempt' class="noMessageError" style="display:none"><%=messageError%></div></td>
												</tr> </br>
												<tr>
													<td  align="right">&nbsp;</td>
													<td colspan=2 align="left"><br><input class="style_button" type="button" value="Calculate" onClick="javascript:CheckTextInput();" style="padding: 5px 15px; background: #006c9d; color: #FFFFFF;"></td>
												</tr>
											  </table>
										</td>
									</tr>
									 <tr>
										  <td align="center" valign="top" style="padding-left: 15px;"><br>                 
											  <table width="100%" cellspacing="1" cellpadding="3" border="0" class="ir_table ir_tableBorder1">
												<tr class="row2">
													  <td align="left">Share Held</td>
													  <td width="30%" align="left"><input type="text" name="shares_held" onFocus="document.calculator.shares_held_enter.select();" class="ir_investmentCalculator"></td>
												</tr>
												<tr class="row2">
													  <td align="left">Price Purchased Per Share (THB)</td>
													  <td align="left"><input type="text" name="price_purchase" onFocus="document.calculator.price_purchase_enter.select();" class="ir_investmentCalculator"></td>
												</tr>
												<tr class="row2">
													  <td align="left">Price Sold (THB)</td>
													  <td align="left"><input type="text" name="price_sold" onFocus="document.calculator.price_sold_enter.select();" class="ir_investmentCalculator"></td>
												</tr>
												<tr class="row2">
													  <td align="left">Total Gross Profit(Loss) On These Shares (THB)</td>
													  <td align="left"><input type="text" name="profit" onFocus="document.calculator.shares_held_enter.select();" class="ir_investmentCalculator"></td>
												</tr>
												<tr class="row2">
													  <td align="left">Less Buying And Selling Commission (THB)</td>
													  <td align="left"><input type="text" name="commission" onFocus="document.calculator.commission_enter.select();" class="ir_investmentCalculator"></td>
												</tr>
												<tr class="row2">
													  <td align="left">Less VAT (THB)</td>
													  <td align="left"><input type="text" name="vat" onFocus="document.calculator.vat_enter.select();" class="ir_investmentCalculator"></td>
												</tr>
												<tr class="row2">
													  <td align="left">Net Profit(Loss) (THB)</td>
													  <td align="left"><input type="text" name="net_profit" onFocus="document.calculator.shares_held_enter.select();" class="ir_investmentCalculator"></td>
												</tr>
												<tr class="row2">
													  <td align="left">As A Percentage, Your Investment Has Changed (%)</td>
													  <td align="left"><input type="text" name="invest_change_percent" onFocus="document.calculator.shares_held_enter.select();" class="ir_investmentCalculator"></td>
												</tr>
											  </table>
										</td>
									 </tr>                      
									<tr>
										<td align="center" valign="top" style="padding-left: 15px;"><br>
											  <table width="100%" cellspacing="3" cellpadding="3" border="0" class="ir_table ir_tableBorder" style="background-color: #badeef;">
												<tr class="row1">
												  <td class="styleTotal" align="left">Net Dividend (THB)</td>
												  <td width="30%" class="style39" align="left"><input type="text" name="net_dividend" onFocus="document.calculator.dividend_taxed_enter.select();" class="ir_investmentCalculator styleTotal"></td>
												</tr>
											  </table>       
										 </td>
									 </tr> 
								<%else%>
									<tr>
										<td align="center" valign="top" style="padding-left: 15px;"> 
											<table width="100%" cellspacing="1" cellpadding="3" border="0" class="ir_table" align="center" style="margin-top: -40px;">
												<tr align="left">
													  <td colspan="3" ><b>[</font> วิธีการใช้งาน ]</b>
													  ในการคำนวณมูลค่ากำไร หรือ ขาดทุน กรุณากรอกข้อมูลใน 3 ช่องดังต่อไปนี้ :  “<strong>ราคาซื้อ</strong>”, “<strong>จำนวนหุ้นที่ถือ</strong>” และ “<strong>ราคาขาย</strong>”</td>
												</tr>          
												<tr align="left">
													<td colspan="3" class="font_calculator"><br><b>เครื่องคำนวณการลงทุน</b></td>
												</tr>
												<tr align="left">
													  <td width="35%">ราคาซื้อ (บาท)</td>
													  <td width="25%"><input type="text" name="price_purchase_enter" id="price_purchase_enter" onKeyPress="return BannerKey(this);"  class="cellinput"></td>
													  <td valign="bottom"><div id='message_price_purchase' class="noMessageError" style="display:none"><%=messageError%></div></td>
												</tr>
												<tr align="left">
													  <td>จำนวนหุ้นที่ถือ</td>
													  <td><input type="text" name="shares_held_enter"  id="shares_held_enter" onKeyPress="return BannerKey(this);"  class="cellinput"></td>
													  <td valign="bottom"><div id='message_shares_held' class="noMessageError" style="display:none"><%=messageError%></div></td>
												</tr>
												<tr align="left">
													  <td>ค่าคอมมิสชั่น (%)</td>
													  <td><input type="text" name="commission_enter" id="commission_enter"  value="0.25"  onKeyPress="return BannerKey(this);"  class="cellinput"></td>
													  <td valign="bottom"><div id='message_commission' class="noMessageError" style="display:none"><%=messageError%></div></td>
												</tr>
												<tr align="left">
													  <td>คอมมิสชั่นขั้นต่ำ (บาท)</td>
													  <td><input type="text" name="min_commission_enter" id="min_commission_enter" value="50"  onKeyPress="return BannerKey(this);"  class="cellinput"></td>
													  <td valign="bottom"><div id='message_min_commission' class="noMessageError" style="display:none"><%=messageError%></div></td>
												</tr>
												<tr align="left">
													  <td>ภาษีมูลค่าเพิ่ม (%)</td>
													  <td><input type="text" name="vat_enter" id="vat_enter"  value="7"  onKeyPress="return BannerKey(this);" class="cellinput"></td>
													  <td valign="bottom"><div id='message_vat' class="noMessageError" style="display:none"><%=messageError%></div></td>
												</tr>
												<tr align="left">
													  <td>ราคาขาย (บาท)</td>
													  <td><input type="text" name="price_sold_enter" id="price_sold_enter"  value=""  onKeyPress="return BannerKey(this);"  class="cellinput"></td>
													  <td valign="bottom"><div id='message_price_sold' class="noMessageError" style="display:none"><%=messageError%></div></td>
												</tr>                                                       
												<tr>
													<td colspan="3"><div class="ir_textDivider"></div></td>
												</tr>
												<tr align="left">
													<td colspan="3" class="font_calculator"><br><b>เงินปันผลต่อหุ้น</b></td>
												</tr>
												<tr align="left">
													  <td  width="35%" align="left">หักภาษี (บาท)</td>
													  <td width="25%"><input type="text" name="dividend_taxed_enter" id="dividend_taxed_enter"  onKeyPress="return BannerKey(this);"  class="cellinput"></td>
													  <td valign="bottom"><div id='message_dividend_taxed' class="noMessageError" style="display:none"><%=messageError%></div></td>
												</tr>
												<tr align="left">
													  <td align="left">ยกเว้นภาษี (บาท)</td></br>
													  <td><input type="text" name="dividend_tax_exempt_enter" id="dividend_tax_exempt_enter"  onKeyPress="return BannerKey(this);"  class="cellinput"></td>
													  <td valign="bottom"><div id='message_dividend_tax_exempt' class="noMessageError" style="display:none"><%=messageError%></div></td>
												</tr>
												<tr>
													<td >&nbsp;</td>
													<td colspan=2 align="left"><br><input class="btn btn-primary" type="button" value="ทำการคำนวณ" onClick="javascript:CheckTextInput();" style="padding: 5px 15px; background: #006c9d; color: #FFFFFF;">
													</td>
												</tr>
											</table>
										</td>
									</tr>
									 <tr>
										  <td align="center" valign="top" style="padding-left: 15px;"><br>                 
											  <table width="100%" cellspacing="1" cellpadding="3" border="0" class="ir_table ir_tableBorder1">
												<tr class="row2">
													  <td align="left">จำนวนหุ้นที่ถือ</td>
													  <td width="30%" align="left"><input type="text" name="shares_held" onFocus="document.calculator.shares_held_enter.select();" class="ir_investmentCalculator"></td>
												</tr>
												<tr class="row2">
													  <td align="left">ราคาที่ซื้อต่อหุ้น (บาท)</td>
													  <td align="left"><input type="text" name="price_purchase" onFocus="document.calculator.price_purchase_enter.select();" class="ir_investmentCalculator"></td>
												</tr>
												<tr class="row2">
													  <td align="left">ราคาที่ขาย (บาท)</td>
													  <td align="left"><input type="text" name="price_sold" onFocus="document.calculator.price_sold_enter.select();" class="ir_investmentCalculator"></td>
												</tr>
												<tr class="row2">
													  <td align="left">กำไร(ขาดทุน) ขั้นต้น ก่อนหักค่าคอมมิชชั่น (บาท)</td>
													  <td align="left"><input type="text" name="profit" onFocus="document.calculator.shares_held_enter.select();" class="ir_investmentCalculator"></td>
												</tr>
												<tr class="row2">
													  <td align="left">จำนวนค่าคอมมิชชั่น (บาท)</td>
													  <td align="left"><input type="text" name="commission" onFocus="document.calculator.commission_enter.select();" class="ir_investmentCalculator"></td>
												</tr>
												<tr class="row2">
													  <td align="left">ภาษีมูลค่าเพิ่มขั้นต่ำ (บาท)</td>
													  <td align="left"><input type="text" name="vat" onFocus="document.calculator.vat_enter.select();" class="ir_investmentCalculator"></td>
												</tr>
												<tr class="row2">
													  <td align="left">กำไร(ขาดทุน)สุทธิ (บาท)</td>
													  <td align="left"><input type="text" name="net_profit" onFocus="document.calculator.shares_held_enter.select();" class="ir_investmentCalculator"></td>
												</tr>
												<tr class="row2">
													  <td align="left">คิดเป็นร้อยละของหน่วยลงทุน (%)</td>
													  <td align="left"><input type="text" name="invest_change_percent" onFocus="document.calculator.shares_held_enter.select();" class="ir_investmentCalculator"></td>
												</tr>
											</table>
										</td>
									 </tr>                      
									<tr>
										<td align="center" valign="top" style="padding-left: 15px;"><br>
											  <table width="100%" cellspacing="3" cellpadding="3" border="0" class="ir_table ir_tableBorder" style="background-color: #badeef;">
												<tr class="row1">
													  <td class="styleTotal" align="left">เงินปันผลขั้นต้น (บาท)</td>
													  <td width="30%" class="style39" align="left"><input type="text" name="net_dividend" onFocus="document.calculator.dividend_taxed_enter.select();" class="ir_investmentCalculator styleTotal" style="    margin-bottom: 0px !important;"></td>
												</tr>
											  </table>
										 </td>
									 </tr> 
								<%end if%>
								</form>
							</td>
						  </tr> 
					</table> 
					<br>
					<br>
					<br>
				</div>
			<!--======================================================= End ======================================================-->
			</section>
			
		</div>

		<!--====================================== Footer ======================================-->
			<!--#include file='i_footer.asp'-->
		<!--======================================== End =======================================-->

		<script type="text/javascript" src="scripts/modernizr.custom.js"></script>
		<%if session("lang")="E" then%><script type="text/javascript" src="scripts/jquery.dlmenu-en.js"></script><%else%><script type="text/javascript" src="scripts/jquery.dlmenu.js"></script><%end if%>
		<script>
			$(function() {
				$('#dl-menu').dlmenu();
			});
		</script>
		<!-- js/scroll-hint -->
		<script src="scripts/scroll-hint.js"></script>
		<!-- js/scroll-hint -->
		<script>
			new ScrollHint('.js-scrollable', {
			});
		</script>
	</div>
	 <!--#include file="i_googleAnalytics.asp"-->	  
</body>
</html>