﻿<!DOCTYPE html>

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->
<!--#include file = "../../i_conir.asp" --> 
<!--#include file = "../../function_asp2007.asp" -->

<%
session("cur_page")="IR " &  listed_share & " Inquiry Form"
session("page_asp")="inquiry_detail.asp"
page_name="Inquiry Form"

if session("lang")="" or request("lang")="T"  then  
	session("lang")="T"
elseif request("lang")="E" then
	session("lang")="E"
end if
%>
 <!--#include file="../../constpage2007.asp"-->

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
		<title><%=message_title%> :: <%=page_name%></title>
		<meta content="<%=message_keyword%>" name="keywords">
		<meta content="<%=message_description%>" name="description">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="robots" content="index,follow">
		<link href="style_listed.css" rel="stylesheet" type="text/css">
		<script language="javascript" src="../../function2007_utf8.js"></script>
		<!-- CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="css/flexslider.css" rel="stylesheet" type="text/css">
		<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
		<link href="css/style.css" rel="stylesheet" type="text/css">
		<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
		<!-- ADD CSS -->
		<link href="css/component.css" rel="stylesheet" type="text/css">
		<link href="css/custom.css" rel="stylesheet" type="text/css">
		<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">
		<!-- FONTS -->
		<link href="css/font_add.css" rel="stylesheet" type="text/css">
		<link href="css/font-awesome.css" rel="stylesheet">
		<!-- Mouse Hover CSS -->
		<script src="scripts/jquery.min.js" type="text/javascript"></script>
		<script src="scripts/jquery.nicescroll.min.js"></script>
		<!-- SCRIPTS -->
		<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
		<!--[if IE]><html class="ie" lang="en"> <![endif]-->
		<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
		<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
		<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
		<script src="scripts/animate.js" type="text/javascript"></script>
		<script src="scripts/myscript.js" type="text/javascript"></script>
		<script src="scripts/owl.carousel.js" type="text/javascript"></script>
		<!-- NEW -->
		<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
		<link href="css/site.css" rel="stylesheet" type="text/css" />
		<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
		<!-- scroll-hint -->
		<link rel="stylesheet" href="css/scroll-hint.css">
</head>
<body>
<div class="header-wrap">
	<div class="container">
	<!-- ........................................ Top ........................................... -->	
		<!--#include file = "i_top_logo_detail.asp" -->
	<!-- ...................................... End Top ......................................... -->		    	
	</div>
</div>	
<div class="container" style="padding-top: 60px;">
	  <div class="row">	
		<div class="col-md-2"></div>		
		<div class="col-md-8 content-ir">
			<div class="vc_row wpb_row vc_row-fluid">
			
			<div class="col-lg-12">
				<h2 style="text-align: left" class="vc_custom_heading title"><%=arr_menu_desc(64)%></h2>						
			</div>
		<!---------------------------------- Content --------------------------------------->
				<form name="frm1" method="post" action="<%=session("page_asp")%>">						
					<table cellSpacing=0 cellPadding=0 width="100%" border=0  align="center" >
						  <tr>
							 <td align="center" >
								   <table width="90%"  border="0" cellspacing="8" cellpadding="0">
										<tr>
											   <td align="left" valign="top" colspan="2"><br>
												  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
														   <tr align="right" valign="top">
															 <td>
															 <a  class="web" href="request_inquiry.asp">
															<h4><%if session("lang")="T" or session("lang")=""  then%>คำถามอื่น ๆ / ฝากคำถาม<%else%>Other / Questions to Management<%end if%></h4></a></td>
														   </tr>
												  </table>
												  <br>
												  </td>
											  </tr>
											<!------------------------------------------------------------------------------------->
											<%
											strsql=""
											strsql=sql_inquiryform(0,0, request("id"))
									  
											set rs=conir.execute(strsql)
											if not rs.eof and not rs.bof then
											%>										
											  <tr>
												  <td align="left" valign="top" width="10%"><font class="font_faq_Q">Q : </font></td>
												  <td align="center" valign="top">
													 <table width="100%"  border="0" cellpadding="5" cellspacing="1" class="bg_in_quiry3">
														<tr>
															  <td align="left" valign="top" class="bg_in_quiry4">
															  <font class="font_inquiry2"><%=replace(replace(rs("title"),vbcrlf,"<br>"),space(1)," ")%></font></td>
														</tr>
														<tr>
														   <td align="left" valign="top" class="bg_in_quiry5"><font class="font_datetime"><%=DisplayDate(cdate(left(rs("timestamp"),8))) & " "	& right(rs("timestamp"),8)%></font></td>
														</tr>
												  </table>
												  </td>
											  </tr>																	  
											  <%
												  strsql=""
												  strsql=sql_answer(request("id") )
												  
												  set rs1=conir.execute(strsql)
												  if not rs1.eof and not rs1.bof then
													 i=1
													 do while not rs1.eof 
											  %>
												<tr><td height="30px"></td></tr>
											  <tr>
													<%if i=1 then%>
														<td align="left" valign="top"><font class="font_faq_A">A : </font></td>			
													<%else%>
														<td align="left" valign="top">&nbsp;</td>			
													<%end if%>
												  <td align="center" valign="top">
													  <table width="100%"  border="0" cellpadding="3" cellspacing="1"   class="bg_in_quiry9">
														   <tr>
															  <td align="left" valign="top" class="bg_in_quiry7">
															   <font class="font_inquiry3"> <%=replace(replace(rs1("answer"),vbcrlf,"<br>"),space(1)," ")%></font></td>
														</tr>
														<tr>            
														   <td align="left" valign="top" class="bg_in_quiry8"><font class="font_datetime"><%=DisplayDate(cdate(left(rs("timestamp"),8))) & " "	& right(rs("timestamp"),8)%></font></td>
														</tr>
													 </table>
											   </td>
											</tr>
											<%
												  i=i+1
												  rs1.movenext
												  loop
											end if
											rs1.close
											%>     
											<%end if%>
										   <!------------------------------------------------------------------------------------->
									  </table>		
								<p>&nbsp;</p>                                    
							 </td>
						  </tr>	
					  </table>
				</form>
				
		<!---------------------------------End Content ------------------------------------->
			</div>
		</div><!-- / .row -->
		<div class="col-md-2"></div>	
	</div>
</div>
<div class="container">
	<!-- ............................................ Footer ............................................	-->
				<!--#include file = "i_footer.asp" -->
	<!-- ............................................ End Footer .................................... -->
</div>
 <!--#include file="i_googleAnalytics.asp"-->	
</body>
</html>