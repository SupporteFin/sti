﻿<!DOCTYPE html>

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->
<%
session("cur_page")="IR " &  listed_share & " Financial Statement"
session("page_asp")="finance_statement.asp"
page_name="Financial Statement"
%>

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<TITLE><%=message_title%> :: <%=page_name%></TITLE>
	<META name="Title" content="<%=message_metatitle%>">
	<META name="Description" content="<%=message_description%>">
	<META name="Keywords" content="<%=message_keyword%>">	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="author" content="">
	<link rel="shortcut icon" href="<%=img_icon%>">
	<link href="style_listed.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../function2007_utf8.js"></script>
	<!-- CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/flexslider.css" rel="stylesheet" type="text/css">
	<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
	<!-- ADD CSS -->
	<link href="css/component.css" rel="stylesheet" type="text/css">
	<link href="css/custom.css" rel="stylesheet" type="text/css">
	<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">
	<!-- FONTS -->
	<link href="css/font_add.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- Mouse Hover CSS -->
	<script src="scripts/jquery.min.js" type="text/javascript"></script>
	<script src="scripts/jquery.nicescroll.min.js"></script>
	<!-- SCRIPTS -->
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<!--[if IE]><html class="ie" lang="en"> <![endif]-->
	<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
	<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
	<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
	<script src="scripts/animate.js" type="text/javascript"></script>
	<script src="scripts/myscript.js" type="text/javascript"></script>
	<script src="scripts/owl.carousel.js" type="text/javascript"></script>
	<!-- NEW -->
	<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
	<link href="css/site.css" rel="stylesheet" type="text/css" />
	<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
	<!-- scroll-hint -->
	<link rel="stylesheet" href="css/scroll-hint.css">
	
	<style>
		.style_button {
			padding: 0px 15px; 
			background: #006c9d; 
			color: #FFFFFF;
		}
	</style>
</head>

<body class="main">

	<!-- PRELOADER -->
	<img id="preloader" src="images/preloader.gif" alt="">
	<!-- //PRELOADER -->
	<div class="preloader_hide">
		<div id="page" class="single_page">

			<!--================================ Menu Top ==================================-->
				<!--#include file='i_menu_top.asp'-->
			<!--=================================== End ====================================-->

			<!--=============================== Menu Left ==================================-->
				<!--#include file='i_menu_left.asp'-->
			<!--================================== End =====================================-->

			<!--============================ Title Menu =============================-->
			<section class="mbr-section article">
				<div class="container">
					<div class="bigTopic">
						<h1 class="bigTopicText"><%=arr_menu_desc(30)%></h1>
					</div>
				</div>
			</section>
			<!--============================== End ==================================-->

			<section class="mbr-section article sectionSub">
			<!--===================================================== Content ====================================================-->
				<!--include file="i_finance_statement.asp"-->
				<%if session("lang")="E" then%>
					<div class="container">
						  <div class="table-data js-scrollable scroll-hint" style="position: relative; overflow: auto;">
							<table border="0" cellspacing="0" cellpadding="0" class="table-width">
							  <tbody style="font-size: 15.5px;">
								<tr>
								  <th width="80%" align="center" class="topicTeble" style="">Finance Statement</th>   
								  <th width="20%" align="center" class="topicTeble"> Download</th>
								</tr>
								<!-- Table Header -->
								<tr class="even">
									  <td align="left" class="topicTeble2">Financial Statement Quarter 1/2019</td>
									  <td align="center" class="topicTeble2">
										<a href="pdf/finance_statement_Q1_2562_e.zip" target="_blank" download>
										<img src="images/icon_download.png"></a>
									  </td>
								</tr>
								<tr class="even">
									  <td align="left" class="topicTeble2">Finance Statement 2561</td>
									  <td align="center" class="topicTeble2">
										<a href="pdf/finance_statement_2561_e.zip" target="_blank" download>
										<img src="images/icon_download.png"></a>
									  </td>
								</tr>
								<tr class="even">
									  <td align="left" class="topicTeble2">Financial Statement Quarter 3/2018</td>
									  <td align="center" class="topicTeble2">
										<a href="pdf/finance_statement_q3-2561_e.zip" target="_blank" download>
										<img src="images/icon_download.png"></a>
									  </td>
								</tr>
								<tr class="even">
									  <td align="left" class="topicTeble2">Financial Statement Quarter 2/2018</td>
									  <td align="center" class="topicTeble2">
										<a href="pdf/finance_statement_q2-2561_e.zip" target="_blank" download>
										<img src="images/icon_download.png"></a>
									  </td>
								</tr>
								<tr class="even">
									  <td align="left" class="topicTeble2">Financial Statement Quarter 1/2018</td>
									  <td align="center" class="topicTeble2">
										<a href="pdf/finance_statement_q1-2561_e.zip" target="_blank" download>
										<img src="images/icon_download.png"></a>
									  </td>
								</tr>

							  </tbody>
							</table>
						  </div>
						  <div style="height: 215px;"></div>
					</div>
				<%else%>
					<div class="container">
						  <div class="table-data js-scrollable scroll-hint" style="position: relative; overflow: auto;">
							<table border="0" cellspacing="0" cellpadding="0" class="table-width">
							  <tbody style="font-size: 15.5px;">
								<tr>
								  <th width="80%" align="center" class="topicTeble" style="">งบการเงิน</th>   
								  <th width="20%" align="center" class="topicTeble"> ดาวน์โหลด</th>
								</tr>
								<!-- Table Header -->
								<tr class="even">
									  <td align="left" class="topicTeble2">ไตรมาส1/2562</td>
									  <td align="center" class="topicTeble2">
										<a href="pdf/finance_statement_Q1_2562_t.zip" target="_blank" download>
										<img src="images/icon_download.png"></a>
									  </td>
								</tr>
								<tr class="even">
									  <td align="left" class="topicTeble2">ประจำปี 2561</td>
									  <td align="center" class="topicTeble2">
										<a href="pdf/finance_statement_2561_t.zip" target="_blank" download>
										<img src="images/icon_download.png"></a>
									  </td>
								</tr>
								<tr class="even">
									  <td align="left" class="topicTeble2">ไตรมาส3/2561</td>
									  <td align="center" class="topicTeble2">
										<a href="pdf/finance_statement_Q3-2561_t.zip" target="_blank" download>
										<img src="images/icon_download.png"></a>
									  </td>
								</tr>
								<tr class="even">
									  <td align="left" class="topicTeble2">ไตรมาส2/2561</td>
									  <td align="center" class="topicTeble2">
										<a href="pdf/finance_statement_Q2-2561_t.zip" target="_blank" download>
										<img src="images/icon_download.png"></a>
									  </td>
								</tr>
								<tr class="even">
									  <td align="left" class="topicTeble2">ไตรมาส1/2561</td>
									  <td align="center" class="topicTeble2">
										<a href="pdf/finance_statement_Q1-2561_t.zip" target="_blank" download>
										<img src="images/icon_download.png"></a>
									  </td>
								</tr>

							  </tbody>
							</table>
						  </div>
						  <div style="height: 215px;"></div>
					</div>
				<%end if%>
			<!--======================================================= End ======================================================-->
			</section>
			
		</div>

		<!--====================================== Footer ======================================-->
			<!--#include file='i_footer.asp'-->
		<!--======================================== End =======================================-->

		<script type="text/javascript" src="scripts/modernizr.custom.js"></script>
		<%if session("lang")="E" then%><script type="text/javascript" src="scripts/jquery.dlmenu-en.js"></script><%else%><script type="text/javascript" src="scripts/jquery.dlmenu.js"></script><%end if%>
		<script>
			$(function() {
				$('#dl-menu').dlmenu();
			});
		</script>
		<!-- js/scroll-hint -->
		<script src="scripts/scroll-hint.js"></script>
		<!-- js/scroll-hint -->
		<script>
			new ScrollHint('.js-scrollable', {
			});
		</script>
	</div>
	 <!--#include file="i_googleAnalytics.asp"-->	  
</body>
</html>