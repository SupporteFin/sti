﻿<!--#include file = "../../formatdate_utf8.asp" -->
<%
function DisplayDate(vdate)

		vformateDate = "DD MMMM YYYY"	
		if (Instr(vdate,"AM") > 0) or (Instr(vdate,"PM") > 0) then
			vdate = left(vdate,8)		
		end if
		if session("lang") = "E" then
			strDate = DateToString(vdate,vformateDate)
		else
			strDate = DateToStringTH(vdate,vformateDate)
		end if
		if Instr(strDate,"Type mismatch") > 0 then
			DisplayDate = vdate
		else
			DisplayDate = strDate
		end if
      
end function 

function DisplayDateNum(vdate)

		vformateDate = "DD-MM-YYYY"	
		if (Instr(vdate,"AM") > 0) or (Instr(vdate,"PM") > 0) then
			vdate = left(vdate,8)		
		end if
		if session("lang") = "E" then
			strDate = DateToString(vdate,vformateDate)
		else
			strDate = DateToStringTH(vdate,vformateDate)
		end if
		if Instr(strDate,"Type mismatch") > 0 then
			DisplayDateNum = vdate
		else
			DisplayDateNum = strDate
		end if
      
end function 

function DisplayDateTime(vdate)

		strtime =  Replace(Mid(vdate,10,5),".",":")	
		if session("lang") = "E" then
			if timevalue(strtime) >timevalue("12:00:00") then
			
					if hours=12 then
						strtime=strtime & " PM"
					else
						vtime = Replace(vdate,".",":")	
						if len(hour(vtime)) <>2 then 
							hours="0" & hour(vtime)
						else
							hours=hour(vtime)
						end if
					
						if len(minute(vtime)) <>2 then 
							minutes="0" & minute(vtime)
						else
							minutes=minute(vtime)
						end if
					
					if len(hours-12)=2 then  
						strtime=(hours-12) & ":" & minutes & " PM"
					else
						strtime= "0" & (hours-12) & ":" & minutes & " PM"
					end if
				end if
			 else
					strtime= strtime & " AM"
			 end if
		else
			strtime =  Mid(vdate,10,5) & " น."
		end if
	 
		vformateDate = "DD MMMM YYYY"	
		if (Instr(vdate,"AM") > 0) or (Instr(vdate,"PM") > 0) then
			vdate = left(vdate,8)		
		end if
		if session("lang") = "E" then
			strDate = DateToString(vdate,vformateDate)
		else
			strDate = DateToStringTH(vdate,vformateDate)
		end if
		if Instr(strDate,"Type mismatch") > 0 then
			DisplayDateTime = vdate
		else
			DisplayDateTime = strDate & " " & strtime
		end if
		
	DisplayDateTime = strtime
		
end function 

function DisplayDateShort(vdate)

		vformateDate = "DD MMM YYYY"	
		if (Instr(vdate,"AM") > 0) or (Instr(vdate,"PM") > 0) then
			vdate = left(vdate,8)		
		end if
		if session("lang") = "E" then
			strDate = DateToString(vdate,vformateDate)
		else
			strDate = DateToStringTH(vdate,vformateDate)
		end if
		if Instr(strDate,"Type mismatch") > 0 then
			DisplayDateShort = vdate
		else
			DisplayDateShort = strDate
		end if
      
end function 

function DisplayDateDay(vdate)

		vformateDate = "DD"	
		if (Instr(vdate,"AM") > 0) or (Instr(vdate,"PM") > 0) then
			vdate = left(vdate,8)		
		end if
		if session("lang") = "E" then
			strDate = DateToString(vdate,vformateDate)
		else
			strDate = DateToStringTH(vdate,vformateDate)
		end if
		if Instr(strDate,"Type mismatch") > 0 then
			DisplayDateDay = vdate
		else
			DisplayDateDay = strDate
		end if
      
end function 

function DisplayDateMonth(vdate)

		vformateDate = "MMM"	
		if (Instr(vdate,"AM") > 0) or (Instr(vdate,"PM") > 0) then
			vdate = left(vdate,8)		
		end if
		if session("lang") = "E" then
			strDate = DateToString(vdate,vformateDate)
		else
			strDate = DateToStringTH(vdate,vformateDate)
		end if
		if Instr(strDate,"Type mismatch") > 0 then
			DisplayDateMonth = vdate
		else
			DisplayDateMonth = strDate
		end if
      
end function 

function GetFileSizeType(file)

	arrpath=split(Server.MapPath("home.asp"),"\")	
	for inti=0 to ubound(arrpath)-intstep
			tmppath=tmppath & arrpath(inti) & "\"
	next	
	tmppath = replace(tmppath,"home.asp\","")
	pathFileName = tmppath & file
			
	set objFSO=Server.CreateObject("Scripting.FileSystemObject")
	If objFSO.FileExists(pathFileName) Then
		set objFile = objFSO.GetFile(pathFileName)
		intSizeK = Int((objFile.Size/1024) + .5)
		if intSizeK >= 1000 then
			intSizeK = (intSizeK/1000)  
			intSizeK = formatnumber(intSizeK,2,-1)
		end if
		if intSizeK = 0 then intSizeK = 1
		'GetFileSizeType = "("&UCase(objFSO.GetExtensionName(pathFileName))&", "&intSizeK&"KB)"				
		GetFileSizeType	= intSizeK&"KB"
	Else
		GetFileSizeType = "0KB"		
	End If
	set objFSO=nothing
	set objFile=nothing

end function

function YearText(varyear)
	if session("lang") = "E" then 
		strYear = varyear
	else
		strYear = varyear + 543
	end if
	YearText = strYear
end function


function DisplayDateNow(vdate,flag,intpastyear)

			if len(day(now)) <>2 then 
				days="0" & day(now)
			else
				days=day(now)
			end if
			
			if len(month(now)) <>2 then 
				months="0" & month(now)
			else
				months=month(now)
			end if

			if len(year(now)) >=4 then
					if year(now) >2500 then
								years=year(now)-543
					else
								years=year(now)
					end if
			else
					years="20" & year(now)
			end if
			
			'flag = 1 (Start Date) , 2 (End Date)
			if flag = 1 then
				years = years - intpastyear
			else
				years = years
			end if
			
			DisplayDateNow = days & "/" & months & "/" & years
      
end function 
%>