﻿<!DOCTYPE html>

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->
<%
session("cur_page")="IR " &  listed_share & " CSR Activities"
session("page_asp")="csr.asp"
page_name="CSR Activities"
%>

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<TITLE><%=message_title%> :: <%=page_name%></TITLE>
	<META name="Title" content="<%=message_metatitle%>">
	<META name="Description" content="<%=message_description%>">
	<META name="Keywords" content="<%=message_keyword%>">	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="author" content="">
	<link rel="shortcut icon" href="<%=img_icon%>">
	<link href="style_listed.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../function2007_utf8.js"></script>
	<!-- CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/flexslider.css" rel="stylesheet" type="text/css">
	<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
	<!-- ADD CSS -->
	<link href="css/component.css" rel="stylesheet" type="text/css">
	<link href="css/custom.css" rel="stylesheet" type="text/css">
	<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">
	<!-- FONTS -->
	<link href="css/font_add.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- Mouse Hover CSS -->
	<script src="scripts/jquery.min.js" type="text/javascript"></script>
	<script src="scripts/jquery.nicescroll.min.js"></script>
	<!-- SCRIPTS -->
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<!--[if IE]><html class="ie" lang="en"> <![endif]-->
	<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
	<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
	<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
	<script src="scripts/animate.js" type="text/javascript"></script>
	<script src="scripts/myscript.js" type="text/javascript"></script>
	<script src="scripts/owl.carousel.js" type="text/javascript"></script>
	<!-- NEW -->
	<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
	<link href="css/site.css" rel="stylesheet" type="text/css" />
	<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
	<!-- scroll-hint -->
	<link rel="stylesheet" href="css/scroll-hint.css">
	
	<style type="text/css">
		/* --------- Global ----------*/
		.transition{
		    transition: all 0.5s ease;
		    -webkit-transition: all 0.5s ease;
		    -moz-transition: all 0.5s ease;
		    -o-transition: all 0.5s ease;
		    -ms-transition: all 0.5s ease;
		}
		.img-responsive{
		    width:100%;
		}

		/* --------- Simple Zoom Effects ----------*/
		.product-div1{
		    position:relative;
		    /*overflow:hidden;*/
        margin-bottom: 65px;
		}
		.product-div1:hover img{
		    transform: scale(1.1);
		}
		img{
		    transform: scale(1);
		}

	</style>
	
	 <!-- imagepreviewer -->
	  <script src="scripts/jquery-imagepreviewer.min.js" type="text/javascript"></script>
	  <script type="text/javascript">
		window.onload = function() {
		  $(".article-box").imagePreviewer();
		};
	  </script>
	  <!-- END imagepreviewer -->
</head>

<body class="main">

	<!-- PRELOADER -->
	<img id="preloader" src="images/preloader.gif" alt="">
	<!-- //PRELOADER -->
	<div class="preloader_hide">
		<div id="page" class="single_page">

			<!--================================ Menu Top ==================================-->
				<!--#include file='i_menu_top.asp'-->
			<!--=================================== End ====================================-->

			<!--=============================== Menu Left ==================================-->
				<!--#include file='i_menu_left.asp'-->
			<!--================================== End =====================================-->

			<!--============================ Title Menu =============================-->
			<section class="mbr-section article">
				<div class="container">
					<div class="bigTopic">
						<h1 class="bigTopicText"><%=arr_menu_desc(48)%></h1>
					</div>
				</div>
			</section>
			<!--============================== End ==================================-->

			<section class="mbr-section article sectionSub article-box">
			<!--===================================================== Content ====================================================-->
			<%if session("lang")="E" then%>
				<div class="container">
					<div class="jquery-script-ads">
						<script type="text/javascript">
							google_ad_client = "ca-pub-2783044520727903";
							google_ad_slot = "2780937993";
							google_ad_width = 728;
							google_ad_height = 90;
						</script>
					</div>
					<img src="images/HistoryBG-1.jpg" style="margin-bottom: 15px;">
					<p class="bigTopicSub">แนวทางภาพรวม</p>
					<p class="bigTopicSubText" style="margin-bottom: 35px;">บริษัทฯ และบริษัทย่อยตระหนักถึงความสำคัญของการดำเนินธุรกิจด้วยความรับผิดชอบต่อสังคม ชุมชน และสิ่งแวดล้อม ซึ่งเป็นปัจจัยสำคัญหนึ่งที่จะช่วยให้การดำเนินธุรกิจของกลุ่มบริษัทฯ ได้รับการยอมรับและได้รับการสนับสนุนจากผู้มีส่วนเกี่ยวข้องทุกฝ่าย ส่งผลให้ธุรกิจของกลุ่มบริษัทฯ สามารถเติบโตอย่างมั่นคงยั่งยืนต่อไปในระยะยาว ด้วยเหตุนี้ บริษัทฯจึงมีแนวทางที่จะดำเนินธุรกิจภายใต้กรอบของธรรมาภิบาลที่ดี ด้วยความซื่อสัตย์สุจริต มีความโปร่งใส ตรวจสอบได้ ควบคู่กับการมุ่งให้ความสำคัญและคำนึงถึงประโยชน์และ/หรือผลกระทบจากการดำเนินธุรกิจของกลุ่มบริษัทฯ ที่อาจจะเกิดขึ้นต่อระบบเศรษฐกิจ สิ่งแวดล้อม และผู้มีส่วนได้เสียทุกฝ่ายที่เกี่ยวข้อง อันได้แก่ ผู้ถือหุ้น ผู้ลงทุน พนักงาน ลูกค้า คู่ค้า ชุมชน และสังคม หน่วยงานต่างๆ เป็นต้น อยู่เสมอ รวมทั้งดูแลและส่งเสริมให้การปฏิบัติงานของกรรมการ ผู้บริหาร และพนักงานของกลุ่มบริษัทฯ เป็นไปในทิศทางเดียวกันตามแนวทางความรับผิดชอบต่อสังคมที่ได้กำหนดไว้</p>
					<p class="bigTopicSub">กิจกรรมเพื่อประโยชน์ต่อสังคมและสิ่งแวดล้อม</p>
					<p class="bigTopicSubText" style="margin-bottom: 35px;">กลุ่มบริษัทฯ ได้ให้การสนับสนุนและ/หรือเป็นผู้ริเริ่มในการดำเนินโครงการและ/หรือกิจกรรมเพื่อประโยชน์ต่อสังคมและสิ่งแวดล้อมอย่างต่อเนื่อง ภายใต้ความร่วมมือระหว่างผู้บริหารและพนักงานของกลุ่มบริษัทฯ ดังนี้</p>

					<p style="font-size: 20px;font-weight: 400;color: #006c9d;">(1) ด้านศาสนาและวัฒนธรรม เช่น</p>
					<ul class="CSRActivities-ul">
						<li>- บริจาคเงินเพื่อบูรณะซ่อมแซมวัดต่างๆ เช่น ศาลาการเปรียญ วัดเครือมิตรสันติธรรม อำเภอไทรโยค จังหวัดกาญจนบุรี</li>
						<li>- ออกแบบกุฏิ ศาลาการเปรียญ ศาลาปฏิบัติธรรม ให้กับมหาวิทยาลัยจุฬาลงกรณ์ราชวิทยาลัย อำเภอวังน้อย จังหวัดอยุธยา วัดบันดาลใจ อำเภอวังน้อย จังหวัดอยุธยา และวัดบ่อไร่ จังหวัดตราด</li>
						<li>- ร่วมทำบุญถวายผ้ากฐินพระราชทาน ณ วัดจองคำ จังหวัดแม่ฮ่องสอน</li>
						<li>- ร่วมทำบุญทอดผ้าป่าสามัคคี วัดพระพุทธบาทบัวคำ จ.อุดรธานี</li>
						<li>- มอบเงินสนับสนุนกิจกรรมของสภาการโบราณสถานระหว่างประเทศ (International Council on Monuments and Sites) ปี 2561</li>
					</ul>

					<p style="font-size: 20px;font-weight: 400;color: #006c9d;margin-top: 35px;">(2) ด้านการศึกษาและกิจกรรมของเยาวชน เช่น</p>
					<ul class="CSRActivities-ul">
						<li>- ส่งผู้บริหารหรือผู้แทนของกลุ่มบริษัทฯ ร่วมเป็นวิทยากรบรรยายให้ความรู้และประสบการณ์ด้านวิศวกรรมในเรื่องต่างๆ เช่น การตรวจสอบอาคาร การต่อเติมอาคาร เสาเข็ม ให้แก่นักศึกษาของสถาบันการศึกษา สมาชิกของสมาคมวิชาชีพด้านวิศวกรรม และบุคลากรของหน่วยงานภาครัฐภาคเอกชนต่างๆ เช่น มหาวิทยาลัยเทคโนโลยีพระจอมเกล้าธนบุรี มหาวิทยาลัยศรีปทุม วิศวกรรมสถานแห่งประเทศไทย สภาวิศวกร กรมป้องกันและบรรเทาสาธารณภัย กระทรวงมหาดไทย เป็นต้น</li>
						<li>- เข้าร่วมโครงการเตรียมความพร้อมสู่ “วิชาชีพวิศวกร” กับทางมูลนิธิเพื่อการพัฒนามหาวิทยาลัยเทคโนโลยีพระจอมเกล้าธนบุรีซึ่งเป็นองค์กรแม่ข่ายของสภาวิศวกร ร่วมกับภาควิชาวิศวกรรมโยธา มหาวิทยาลัยเทคโนโลยีพระจอมเกล้าธนบุรี และชมรมนักศึกษาเก่าโยธาบางมด ได้จัดทำ โครงการเตรียมความพร้อมสู่ “วิชาชีพวิศวกร” ขึ้น เพื่อเป็นการเตรียมความพร้อมให้แก่นักศึกษาปีสุดท้าย ของนักศึกษามหาวิทยาลัยเทคโนโลยีพระจอมเกล้าธนบุรีก่อนก้าวสู่สายอาชีพ ได้มีโอกาสศึกษาแนวทางการเลือกสายอาชีพด้านวิศวกรรม และมีโอกาสเริ่มต้นทำงานในสายอาชีพที่เหมาะสมกับทักษะ ความสามารถและตามความต้องการของตนเองได้อย่างถูกต้องอันจะนำมาซึ่งความสำเร็จในการประกอบวิชาชีพด้านวิศวกรรมต่อไป </li>
						<li>- มอบทุนการศึกษาและลงนามบันทึกข้อตกลงความร่วมมือทางวิชาการ กับมหาวิทยาลัยเทคโนโลยีราชมงคลรัตนโกสินทร์ ภายใต้วัตถุประสงค์เพื่อการสนับสนุนกิจกรรมสหกิจของนักศึกษา หลักสูตรผลิตบัณฑิต การฝึกอบรม ทุนการศึกษา ศึกษาดูงาน ทุนวิจัย โครงการเพิ่มพูนความรู้ของอาจารย์และทักษะด้านที่ปรึกษาเพื่อบริหารและควบคุมงานก่อสร้าง ตามกรอบและแนวทางข้อตกลงความร่วมมือทางวิชาการ ทั้งนี้บริษัทฯได้มอบทุนการศึกษาให้กับนักศึกษาที่เรียนดีแต่ขาดแคลนทุนทรัพย์ จำนวน 200,000 บาท เมื่อวันอังคาร ที่ 20 พฤศจิกายน 2561 ณ ห้องประชุม 1 ชั้น 2 สำนักงานอธิการบดี มหาวิทยาลัยเทคโนโลยีราชมงคลรัตนโกสินทร์ พื้นที่ศาลายา</li>
						<li class="CSRActivities-img">
							<img src="images/CSR/img1-1.jpg">
							<img src="images/CSR/img1-2.jpg">
							<img src="images/CSR/img1-3.jpg">
							<img src="images/CSR/img1-4.jpg">
							<p style="font-size: 18px;font-weight: 600;color: #006c9d;margin-top: -5px;">ภาพการมอบทุนการศึกษาและลงนามบันทึกข้อตกลงความร่วมมือทางวิชาการ
								<br>กับมหาวิทยาลัยเทคโนโลยีราชมงคลรัตนโกสินทร์</p>
						</li>
						<li>- ร่วมทำบุญทอดผ้าป่าสามัคคีเพื่อการศึกษา ณ โรงเรียนบางเลนวิทยา จังหวัดนครปฐม</li>
						<li>- มอบเงินสนับสนุนชมรมบาสเกตบอลชาย โรงเรียนจารุวัฒนานุกูล</li>
						<li>- มอบเงินสนับสนุนการศึกษา โครงการรวมพลคนใจบุญ มหาวิทยาลัยขอนแก่น</li>
						<li>- มอบเงินสนับสนุนการจัดการประชุมวิชาการด้านวิศวกรรมอาเซียน (ASEAN Federation of Engineering Organization)</li>
					</ul>

					<p style="font-size: 20px;font-weight: 400;color: #006c9d;margin-top: 35px;">(3) ด้านคุณภาพชีวิตและชุมชน เช่น</p>
					<ul class="CSRActivities-ul">
						<li>- ส่งผู้บริหารหรือผู้แทนของกลุ่มบริษัทฯ ร่วมเป็นวิศวกรอาสาของสมาคมวิชาชีพด้านวิศวกรรมต่างๆ เช่น วิศวกรรมสถานแห่งประเทศไทย เพื่อดำเนินการหรือดำเนินกิจกรรมต่างๆ ที่เป็นประโยชน์ต่อสาธารณชน เช่น</li>
						<ul class="CSRActivities-Sub-ul">
							<li>ร่วมเป็นวิศวกรอาสาของวิศวกรรมสถานแห่งประเทศไทย ในการตรวจสอบอาคารและให้คำแนะนำในการออกแบบเพื่อแก้ไขซ่อมแซมอาคาร เมื่อเกิดเหตุภัยพิบัติต่างๆ เช่น เหตุการณ์แผ่นดินไหว จังหวัดเชียงราย เหตุการณ์อุทกภัย จังหวัดสกลนคร และจังหวัดสุราษฎร์ธานี</li>
							<li>ร่วมเป็นวิศวกรอาสาในกิจกรรม “คลินิกช่าง” ของวิศวกรรมสถานแห่งประเทศไทย ซึ่งเป็นศูนย์ให้คำแนะนำแก่ประชาชนที่มีปัญหาด้านวิศวกรรมโดยไม่คิดค่าใช้จ่าย</li>
							<li class="CSRActivities-img" style="list-style-type: none;margin: 0 0 0px 45px;">
								<img src="images/CSR/img2-1.jpg">
								<img src="images/CSR/img2-2.jpg">
								<img src="images/CSR/img2-3.jpg">
								<img src="images/CSR/img2-4.jpg">
								<p style="font-size: 18px;font-weight: 600;color: #006c9d;margin-top: -5px;">ภาพกิจกรรม “คลินิกช่าง” ของวิศวกรรมสถานแห่งประเทศไทย</p>
							</li>
							<li>ร่วมเป็นวิทยากรในการตอบคำถามด้านวิศวกรรมแก่ประชาชนทั่วไปในรายการวิทยุของวิศวกรรมสถานแห่งประเทศไทยที่จัดขึ้นกับวิทยุกระจายเสียงแห่งประเทศไทย คลินิกช่าง FM 105” เผยแพร่เสียงทางสถานีวิทยุ FM 105 MHz ทุกวันเสาร์และอาทิตย์ เวลา 05.00 - 06.00 น.</li>
							<li class="CSRActivities-img" style="list-style-type: none;margin: 0 0 0px 45px;">
								<img src="images/CSR/img3-1.jpg" style="width: 199.5px;">
								<img src="images/CSR/img3-2.jpg">
								<p style="font-size: 18px;font-weight: 600;color: #006c9d;margin-top: -5px;">ภาพการเป็นวิทยากรในการตอบคำถามด้านวิศวกรรมแก่ประชาชนทั่วไปในรายการวิทยุของคลินิกช่าง FM 105 MHz</p>
							</li>
						</ul>
						<li>- วันที่ 23 พฤษภาคม พ.ศ. 2561 บริษัทฯ จัดกิจกรรมร่วมกับสถาบันปลูกป่า ณ ป่าวังจันทร์ จ.ระยอง โดยกิจกรรม มีการเพาะพันธุ์ต้นกล้าและอนุบาลต้นไม้ที่เพิ่งปลูก เพื่อเป็นการขยายพันธุ์ป่าไม้หายาก</li>
						<li class="CSRActivities-img" style="list-style-type: none;margin: 0 0 0px 45px;">
							<img src="images/CSR/img4-1.jpg">
							<img src="images/CSR/img4-2.jpg">
							<p style="font-size: 18px;font-weight: 600;color: #006c9d;margin-top: -5px;">ภาพการเพาะพันธุ์ต้นกล้าและอนุบาลต้นไม้ กับสถาบันปลูกป่า ณ ป่าวังจันทร์ จ.ระยอง</p>
						</li>
						<li>- มอบเงินสนับสนุนสลากการกุศลกาชาด จังหวัดนนทบุรี</li>
						<li>- มอบเงินสนับสนุนสลากบำรุงสภากาชาดไทย สมาคมนักศึกษาเก่า มหาวิทยาลัยเทคโนโลยีพระจอมเกล้าธนบุรี</li>
					</ul>

				</div>
			<%else%>
				<div class="container">
					<div class="jquery-script-ads">
						<script type="text/javascript">
							google_ad_client = "ca-pub-2783044520727903";
							google_ad_slot = "2780937993";
							google_ad_width = 728;
							google_ad_height = 90;
						</script>
					</div>
					<img src="images/HistoryBG-1.jpg" style="margin-bottom: 15px;">
					<p class="bigTopicSub">แนวทางภาพรวม</p>
					<p class="bigTopicSubText" style="margin-bottom: 35px;">บริษัทฯ และบริษัทย่อยตระหนักถึงความสำคัญของการดำเนินธุรกิจด้วยความรับผิดชอบต่อสังคม ชุมชน และสิ่งแวดล้อม ซึ่งเป็นปัจจัยสำคัญหนึ่งที่จะช่วยให้การดำเนินธุรกิจของกลุ่มบริษัทฯ ได้รับการยอมรับและได้รับการสนับสนุนจากผู้มีส่วนเกี่ยวข้องทุกฝ่าย ส่งผลให้ธุรกิจของกลุ่มบริษัทฯ สามารถเติบโตอย่างมั่นคงยั่งยืนต่อไปในระยะยาว ด้วยเหตุนี้ บริษัทฯจึงมีแนวทางที่จะดำเนินธุรกิจภายใต้กรอบของธรรมาภิบาลที่ดี ด้วยความซื่อสัตย์สุจริต มีความโปร่งใส ตรวจสอบได้ ควบคู่กับการมุ่งให้ความสำคัญและคำนึงถึงประโยชน์และ/หรือผลกระทบจากการดำเนินธุรกิจของกลุ่มบริษัทฯ ที่อาจจะเกิดขึ้นต่อระบบเศรษฐกิจ สิ่งแวดล้อม และผู้มีส่วนได้เสียทุกฝ่ายที่เกี่ยวข้อง อันได้แก่ ผู้ถือหุ้น ผู้ลงทุน พนักงาน ลูกค้า คู่ค้า ชุมชน และสังคม หน่วยงานต่างๆ เป็นต้น อยู่เสมอ รวมทั้งดูแลและส่งเสริมให้การปฏิบัติงานของกรรมการ ผู้บริหาร และพนักงานของกลุ่มบริษัทฯ เป็นไปในทิศทางเดียวกันตามแนวทางความรับผิดชอบต่อสังคมที่ได้กำหนดไว้</p>
					<p class="bigTopicSub">กิจกรรมเพื่อประโยชน์ต่อสังคมและสิ่งแวดล้อม</p>
					<p class="bigTopicSubText" style="margin-bottom: 35px;">กลุ่มบริษัทฯ ได้ให้การสนับสนุนและ/หรือเป็นผู้ริเริ่มในการดำเนินโครงการและ/หรือกิจกรรมเพื่อประโยชน์ต่อสังคมและสิ่งแวดล้อมอย่างต่อเนื่อง ภายใต้ความร่วมมือระหว่างผู้บริหารและพนักงานของกลุ่มบริษัทฯ ดังนี้</p>

					<p style="font-size: 20px;font-weight: 400;color: #006c9d;">(1) ด้านศาสนาและวัฒนธรรม เช่น</p>
					<ul class="CSRActivities-ul">
						<li>- บริจาคเงินเพื่อบูรณะซ่อมแซมวัดต่างๆ เช่น ศาลาการเปรียญ วัดเครือมิตรสันติธรรม อำเภอไทรโยค จังหวัดกาญจนบุรี</li>
						<li>- ออกแบบกุฏิ ศาลาการเปรียญ ศาลาปฏิบัติธรรม ให้กับมหาวิทยาลัยจุฬาลงกรณ์ราชวิทยาลัย อำเภอวังน้อย จังหวัดอยุธยา วัดบันดาลใจ อำเภอวังน้อย จังหวัดอยุธยา และวัดบ่อไร่ จังหวัดตราด</li>
						<li>- ร่วมทำบุญถวายผ้ากฐินพระราชทาน ณ วัดจองคำ จังหวัดแม่ฮ่องสอน</li>
						<li>- ร่วมทำบุญทอดผ้าป่าสามัคคี วัดพระพุทธบาทบัวคำ จ.อุดรธานี</li>
						<li>- มอบเงินสนับสนุนกิจกรรมของสภาการโบราณสถานระหว่างประเทศ (International Council on Monuments and Sites) ปี 2561</li>
					</ul>

					<p style="font-size: 20px;font-weight: 400;color: #006c9d;margin-top: 35px;">(2) ด้านการศึกษาและกิจกรรมของเยาวชน เช่น</p>
					<ul class="CSRActivities-ul">
						<li>- ส่งผู้บริหารหรือผู้แทนของกลุ่มบริษัทฯ ร่วมเป็นวิทยากรบรรยายให้ความรู้และประสบการณ์ด้านวิศวกรรมในเรื่องต่างๆ เช่น การตรวจสอบอาคาร การต่อเติมอาคาร เสาเข็ม ให้แก่นักศึกษาของสถาบันการศึกษา สมาชิกของสมาคมวิชาชีพด้านวิศวกรรม และบุคลากรของหน่วยงานภาครัฐภาคเอกชนต่างๆ เช่น มหาวิทยาลัยเทคโนโลยีพระจอมเกล้าธนบุรี มหาวิทยาลัยศรีปทุม วิศวกรรมสถานแห่งประเทศไทย สภาวิศวกร กรมป้องกันและบรรเทาสาธารณภัย กระทรวงมหาดไทย เป็นต้น</li>
						<li>- เข้าร่วมโครงการเตรียมความพร้อมสู่ “วิชาชีพวิศวกร” กับทางมูลนิธิเพื่อการพัฒนามหาวิทยาลัยเทคโนโลยีพระจอมเกล้าธนบุรีซึ่งเป็นองค์กรแม่ข่ายของสภาวิศวกร ร่วมกับภาควิชาวิศวกรรมโยธา มหาวิทยาลัยเทคโนโลยีพระจอมเกล้าธนบุรี และชมรมนักศึกษาเก่าโยธาบางมด ได้จัดทำ โครงการเตรียมความพร้อมสู่ “วิชาชีพวิศวกร” ขึ้น เพื่อเป็นการเตรียมความพร้อมให้แก่นักศึกษาปีสุดท้าย ของนักศึกษามหาวิทยาลัยเทคโนโลยีพระจอมเกล้าธนบุรีก่อนก้าวสู่สายอาชีพ ได้มีโอกาสศึกษาแนวทางการเลือกสายอาชีพด้านวิศวกรรม และมีโอกาสเริ่มต้นทำงานในสายอาชีพที่เหมาะสมกับทักษะ ความสามารถและตามความต้องการของตนเองได้อย่างถูกต้องอันจะนำมาซึ่งความสำเร็จในการประกอบวิชาชีพด้านวิศวกรรมต่อไป </li>
						<li>- มอบทุนการศึกษาและลงนามบันทึกข้อตกลงความร่วมมือทางวิชาการ กับมหาวิทยาลัยเทคโนโลยีราชมงคลรัตนโกสินทร์ ภายใต้วัตถุประสงค์เพื่อการสนับสนุนกิจกรรมสหกิจของนักศึกษา หลักสูตรผลิตบัณฑิต การฝึกอบรม ทุนการศึกษา ศึกษาดูงาน ทุนวิจัย โครงการเพิ่มพูนความรู้ของอาจารย์และทักษะด้านที่ปรึกษาเพื่อบริหารและควบคุมงานก่อสร้าง ตามกรอบและแนวทางข้อตกลงความร่วมมือทางวิชาการ ทั้งนี้บริษัทฯได้มอบทุนการศึกษาให้กับนักศึกษาที่เรียนดีแต่ขาดแคลนทุนทรัพย์ จำนวน 200,000 บาท เมื่อวันอังคาร ที่ 20 พฤศจิกายน 2561 ณ ห้องประชุม 1 ชั้น 2 สำนักงานอธิการบดี มหาวิทยาลัยเทคโนโลยีราชมงคลรัตนโกสินทร์ พื้นที่ศาลายา</li>
						<li class="CSRActivities-img">
							<img src="images/CSR/img1-1.jpg">
							<img src="images/CSR/img1-2.jpg">
							<img src="images/CSR/img1-3.jpg">
							<img src="images/CSR/img1-4.jpg">
							<p style="font-size: 18px;font-weight: 600;color: #006c9d;margin-top: -5px;">ภาพการมอบทุนการศึกษาและลงนามบันทึกข้อตกลงความร่วมมือทางวิชาการ
								<br>กับมหาวิทยาลัยเทคโนโลยีราชมงคลรัตนโกสินทร์</p>
						</li>
						<li>- ร่วมทำบุญทอดผ้าป่าสามัคคีเพื่อการศึกษา ณ โรงเรียนบางเลนวิทยา จังหวัดนครปฐม</li>
						<li>- มอบเงินสนับสนุนชมรมบาสเกตบอลชาย โรงเรียนจารุวัฒนานุกูล</li>
						<li>- มอบเงินสนับสนุนการศึกษา โครงการรวมพลคนใจบุญ มหาวิทยาลัยขอนแก่น</li>
						<li>- มอบเงินสนับสนุนการจัดการประชุมวิชาการด้านวิศวกรรมอาเซียน (ASEAN Federation of Engineering Organization)</li>
					</ul>

					<p style="font-size: 20px;font-weight: 400;color: #006c9d;margin-top: 35px;">(3) ด้านคุณภาพชีวิตและชุมชน เช่น</p>
					<ul class="CSRActivities-ul">
						<li>- ส่งผู้บริหารหรือผู้แทนของกลุ่มบริษัทฯ ร่วมเป็นวิศวกรอาสาของสมาคมวิชาชีพด้านวิศวกรรมต่างๆ เช่น วิศวกรรมสถานแห่งประเทศไทย เพื่อดำเนินการหรือดำเนินกิจกรรมต่างๆ ที่เป็นประโยชน์ต่อสาธารณชน เช่น</li>
						<ul class="CSRActivities-Sub-ul">
							<li>ร่วมเป็นวิศวกรอาสาของวิศวกรรมสถานแห่งประเทศไทย ในการตรวจสอบอาคารและให้คำแนะนำในการออกแบบเพื่อแก้ไขซ่อมแซมอาคาร เมื่อเกิดเหตุภัยพิบัติต่างๆ เช่น เหตุการณ์แผ่นดินไหว จังหวัดเชียงราย เหตุการณ์อุทกภัย จังหวัดสกลนคร และจังหวัดสุราษฎร์ธานี</li>
							<li>ร่วมเป็นวิศวกรอาสาในกิจกรรม “คลินิกช่าง” ของวิศวกรรมสถานแห่งประเทศไทย ซึ่งเป็นศูนย์ให้คำแนะนำแก่ประชาชนที่มีปัญหาด้านวิศวกรรมโดยไม่คิดค่าใช้จ่าย</li>
							<li class="CSRActivities-img" style="list-style-type: none;margin: 0 0 0px 45px;">
								<img src="images/CSR/img2-1.jpg">
								<img src="images/CSR/img2-2.jpg">
								<img src="images/CSR/img2-3.jpg">
								<img src="images/CSR/img2-4.jpg">
								<p style="font-size: 18px;font-weight: 600;color: #006c9d;margin-top: -5px;">ภาพกิจกรรม “คลินิกช่าง” ของวิศวกรรมสถานแห่งประเทศไทย</p>
							</li>
							<li>ร่วมเป็นวิทยากรในการตอบคำถามด้านวิศวกรรมแก่ประชาชนทั่วไปในรายการวิทยุของวิศวกรรมสถานแห่งประเทศไทยที่จัดขึ้นกับวิทยุกระจายเสียงแห่งประเทศไทย คลินิกช่าง FM 105” เผยแพร่เสียงทางสถานีวิทยุ FM 105 MHz ทุกวันเสาร์และอาทิตย์ เวลา 05.00 - 06.00 น.</li>
							<li class="CSRActivities-img" style="list-style-type: none;margin: 0 0 0px 45px;">
								<img src="images/CSR/img3-1.jpg" style="width: 199.5px;">
								<img src="images/CSR/img3-2.jpg">
								<p style="font-size: 18px;font-weight: 600;color: #006c9d;margin-top: -5px;">ภาพการเป็นวิทยากรในการตอบคำถามด้านวิศวกรรมแก่ประชาชนทั่วไปในรายการวิทยุของคลินิกช่าง FM 105 MHz</p>
							</li>
						</ul>
						<li>- วันที่ 23 พฤษภาคม พ.ศ. 2561 บริษัทฯ จัดกิจกรรมร่วมกับสถาบันปลูกป่า ณ ป่าวังจันทร์ จ.ระยอง โดยกิจกรรม มีการเพาะพันธุ์ต้นกล้าและอนุบาลต้นไม้ที่เพิ่งปลูก เพื่อเป็นการขยายพันธุ์ป่าไม้หายาก</li>
						<li class="CSRActivities-img" style="list-style-type: none;margin: 0 0 0px 45px;">
							<img src="images/CSR/img4-1.jpg">
							<img src="images/CSR/img4-2.jpg">
							<p style="font-size: 18px;font-weight: 600;color: #006c9d;margin-top: -5px;">ภาพการเพาะพันธุ์ต้นกล้าและอนุบาลต้นไม้ กับสถาบันปลูกป่า ณ ป่าวังจันทร์ จ.ระยอง</p>
						</li>
						<li>- มอบเงินสนับสนุนสลากการกุศลกาชาด จังหวัดนนทบุรี</li>
						<li>- มอบเงินสนับสนุนสลากบำรุงสภากาชาดไทย สมาคมนักศึกษาเก่า มหาวิทยาลัยเทคโนโลยีพระจอมเกล้าธนบุรี</li>
					</ul>

				</div>
			<%end if%>
			<!--======================================================= End ======================================================-->
			</section>
			
		</div>

		<!--====================================== Footer ======================================-->
			<!--#include file='i_footer.asp'-->
		<!--======================================== End =======================================-->

		<script type="text/javascript" src="scripts/modernizr.custom.js"></script>
		<%if session("lang")="E" then%><script type="text/javascript" src="scripts/jquery.dlmenu-en.js"></script><%else%><script type="text/javascript" src="scripts/jquery.dlmenu.js"></script><%end if%>
		<script>
			$(function() {
				$('#dl-menu').dlmenu();
			});
		</script>
		<!-- js/scroll-hint -->
		<script src="scripts/scroll-hint.js"></script>
		<!-- js/scroll-hint -->
		<script>
			new ScrollHint('.js-scrollable', {
			});
		</script>
	</div>
	 <!--#include file="i_googleAnalytics.asp"-->	  
</body>
</html>