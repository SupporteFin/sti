<section class="mbr-section article section-menu section-menu-res">
	<div class="container padding0 fontMenu">
		<div id="dl-menu" class="dl-menuwrapper col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<button class="dl-trigger" style="animation-name: fadeInLeft !important;">Open Menu</button>
			<p><span><%=arr_menu_desc(18)%></span></p>
			<ul class="dl-menu">
				<li class="active"><a href="home.asp"><%=arr_menu_desc(19)%></a></li>
				<li><a href="#"><%=arr_menu_desc(20)%></a>
					<!-- Corporate Information -->
					<ul class="dl-submenu">
						<li><a href="history.asp"><%=arr_menu_desc(21)%></a></li>
						<!--  History -->
						<li><a href="vision.asp"><%=arr_menu_desc(22)%></a></li>
						<!-- Vision -->
						<!-- <li><a href="ContextOfOrganization.asp">บริบทองค์กร</a></li>  Context of Organization  -->
						<li><a href="chairman.asp"><%=arr_menu_desc(23)%></a></li>
						<!-- Chairman’s Statement -->
						<li><a href="board.asp"><%=arr_menu_desc(24)%></a></li>
						<!-- Board of Director -->
						<li><a href="organization.asp"><%=arr_menu_desc(25)%></a></li>
						<!-- Organization Chart -->
						<li><a href="structure.asp"><%=arr_menu_desc(26)%> </a></li>
						<!-- Shareholder Structure -->
						<li><a href="award.asp"><%=arr_menu_desc(27)%></a></li>
						<!-- Awards and Certificates -->
						<li><a href="pdf/5-7-CompanyRegulation.pdf" target="_blank"><%=arr_menu_desc(28)%></a></li>
						<!-- Company Regulation -->
					</ul>
				</li>
				<li><a href="#"><%=arr_menu_desc(29)%></a>
					<!-- Financial Information -->
					<ul class="dl-submenu">
						<li><a href="finance_statement.asp"><%=arr_menu_desc(30)%></a></li>
						<!-- Financial Statement -->
						<li><a href="finance_highlights.asp"><%=arr_menu_desc(31)%></a></li>
						<!-- Financial Highlight -->
						<li><a href="mda.asp"><%=arr_menu_desc(32)%></a></li>
						<!-- MD&A -->
					</ul>
				</li>
				<li><a href="#"><%=arr_menu_desc(33)%></a>
					<!-- Shareholder Information -->
					<ul class="dl-submenu">
						<li><a href="general_meeting.asp"><%=arr_menu_desc(34)%></a></li>
						<!-- General Meeting -->
						<li><a href="major.asp"><%=arr_menu_desc(35)%></a></li>
						<!-- Major Shareholder -->
						<li><a href="dividend.asp"><%=arr_menu_desc(36)%></a></li>
						<!-- Dividend Payment -->
						<li><a href="ir_calendar.asp"><%=arr_menu_desc(37)%></a></li>
						<!-- IR Calendar -->
						<!--<li><a href="company.asp"><%=arr_menu_desc(38)%></a></li>-->
						<li><a href="javascript:void(0);"><%=arr_menu_desc(38)%></a></li>
						<!-- Company Snapshot -->
						<li><a href="factsheet.asp"><%=arr_menu_desc(39)%></a></li>
						<!-- Fact Sheet -->
						<%if session("lang")="E" then%>
							<li><a href="https://market.sec.or.th/public/ipos/IPOSEQ01.aspx?TransID=209823&lang=en" target="_blank"><%=arr_menu_desc(40)%></a></li>
						<%else%>
							<li><a href="https://market.sec.or.th/public/ipos/IPOSEQ01.aspx?TransID=209823&lang=th" target="_blank"><%=arr_menu_desc(40)%></a></li>
						<%end if%>
						
						<!-- Filing -->
					</ul>
				</li>
				<li><a href="#"><%=arr_menu_desc(41)%></a>
					<!-- Annual Report / Form 56-1 -->
					<ul class="dl-submenu">
						<li><a href="annual_report.asp"><%=arr_menu_desc(42)%></a></li>
						<!-- Annual Report -->
						<li><a href="finance_56.asp"><%=arr_menu_desc(43)%></a></li>
						<!-- Form 56-1 -->
					</ul>
				</li>
				<li><a href="#"><%=arr_menu_desc(44)%></a>
					<!-- Sustainability -->
					<ul class="dl-submenu">
						<li><a href="corporate.asp"><%=arr_menu_desc(45)%></a></li>
						<!-- Corporate Governance -->
						<li><a href="anticorruption.asp"><%=arr_menu_desc(46)%></a></li>
						<!-- Anti-corruption -->
						<li><a href="ethics.asp"><%=arr_menu_desc(47)%></a></li>
						<!-- Business Ethics -->
						<!--<li><a href="csr.asp"><%=arr_menu_desc(48)%></a></li>-->
						<li><a href="https://www.sti.co.th/th/blog-cat.php?cid=9" target="_blank"><%=arr_menu_desc(48)%></a></li>
						<!-- CSR & Activities -->
						<li><a href="boardcharter.asp"><%=arr_menu_desc(49)%></a></li>
						<!-- Board Charter -->
						<li><a href="policy.asp"><%=arr_menu_desc(50)%></a></li>
						<!-- Company policy -->
					</ul>
				</li>
				<li><a href="webcasts.asp"><%=arr_menu_desc(51)%></a></li>
				<!-- Webcast & Presentation -->
				<li><a href="https://www.sti.co.th/th/investor/analystdata.php" target="_blank"><%=arr_menu_desc(52)%></a></li>
				<!-- Analyst Report -->
				<li><a href="#"><%=arr_menu_desc(53)%></a>
					<!-- Stock Information -->
					<ul class="dl-submenu">
						<li><a href="stock_quote.asp"><%=arr_menu_desc(54)%></a></li>
						<!-- Stock Price -->
						<%if session("lang")="E" then%>
							<li><a href="https://www.set.or.th/set/historicaltrading.do?symbol=STI&language=en&country=EN" target="_blank"><%=arr_menu_desc(55)%></a></li>
						<%else%>
							<li><a href="https://www.set.or.th/set/historicaltrading.do?symbol=STI&language=th&country=TH" target="_blank"><%=arr_menu_desc(55)%></a></li>
						<%end if%>
						<!-- Historical Price -->
						<li><a href="calculator.asp"><%=arr_menu_desc(56)%></a></li>
						<!-- Investment Calculator -->
					</ul>
				</li>
				<li><a href="#"><%=arr_menu_desc(57)%></a>
					<!-- News Room -->
					<ul class="dl-submenu">
						<li><a href="set_announcement.asp"><%=arr_menu_desc(58)%></a></li>
						<!-- SET Announcement -->
						<li><a href="news_update.asp"><%=arr_menu_desc(59)%></a></li>
						<!-- News Update -->
						<li><a href="public.asp"><%=arr_menu_desc(60)%></a></li>
						<!-- Public Relation -->
						<li><a href="news_clipping.asp"><%=arr_menu_desc(61)%></a></li>
						<!-- News Clipping -->
						<li><a href="press.asp"><%=arr_menu_desc(62)%></a></li>
						<!-- Press Release -->
					</ul>
				</li>
				<li><a href="#"><%=arr_menu_desc(63)%></a>
					<!-- Information Request System -->
					<ul class="dl-submenu">
						<li><a href="request_inquiry.asp"><%=arr_menu_desc(64)%></a></li>
						<!-- Inquiry From -->
						<li><a href="complaints.asp"><%=arr_menu_desc(65)%></a></li>
						<!-- Complaint Channel -->
						<li><a href="request_alerts.asp"><%=arr_menu_desc(66)%></a></li>
						<!-- E-mail Alert -->
						<!--<li><a href="FAQs.asp">คำถามที่ถูกถามบ่อย</a></li>  FAQs -->
					</ul>
				</li>
				<li><a href="ir_contact.asp"><%=arr_menu_desc(67)%></a>
					<!-- IR Contact -->
				</li>
			</ul>
		</div>

		<div class="col-lg-2 col-md-2 offset-menu-th width-short-th IEmenu1">
			<div class="coverOffset-menu">
				<a href="ir_calendar.asp" class="displayFlex">
					<img class="img-short" src="images/hl_icn.png">
					<p class="text-short"><%=arr_menu_desc(68)%></p>
				</a>
			</div>
		</div>

		<div class="col-lg-2 col-md-2 width-short-th IEmenu2">
			<div class="coverOffset-menu">
				<a href="webcasts.asp" class="displayFlex">
					<img class="img-short" src="images/Multimedia.png">
					<p class="text-short"><%=arr_menu_desc(69)%></p>
				</a>
			</div>
		</div>

		<div class="col-lg-2 col-md-2 width-short-th IEmenu3">
			<div class="coverOffset-menu">
				<a href="ir_contact.asp" class="displayFlex">
					<img class="img-short" src="images/contact.png">
					<p class="text-short IEcon" style="margin: 22px 40px 0 0px;text-transform:uppercase;"><%=arr_menu_desc(67)%></p>
				</a>
			</div>
		</div>

		<!--=================== Language =====================-->
			<!--#include file='i_lang.asp'-->
		<!--====================== End =======================-->
	</div>
</section>