﻿<!DOCTYPE html>

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->
<!--#include file = "../../function_asp2007.asp" -->
<!--#include file = "../../i_conir.asp" --> 

<%
session("cur_page")="IR " &  listed_share & " Annual Report"
session("page_asp")="annual_report.asp"
page_name="Annual Report"
%>

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<TITLE><%=message_title%> :: <%=page_name%></TITLE>
	<META name="Title" content="<%=message_metatitle%>">
	<META name="Description" content="<%=message_description%>">
	<META name="Keywords" content="<%=message_keyword%>">	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="author" content="">
	<link rel="shortcut icon" href="<%=img_icon%>">
	<link href="style_listed.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../function2007_utf8.js"></script>
	<!-- CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/flexslider.css" rel="stylesheet" type="text/css">
	<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
	<!-- ADD CSS -->
	<link href="css/component.css" rel="stylesheet" type="text/css">
	<link href="css/custom.css" rel="stylesheet" type="text/css">
	<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">
	<!-- FONTS -->
	<link href="css/font_add.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- Mouse Hover CSS -->
	<script src="scripts/jquery.min.js" type="text/javascript"></script>
	<script src="scripts/jquery.nicescroll.min.js"></script>
	<!-- SCRIPTS -->
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<!--[if IE]><html class="ie" lang="en"> <![endif]-->
	<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
	<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
	<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
	<script src="scripts/animate.js" type="text/javascript"></script>
	<script src="scripts/myscript.js" type="text/javascript"></script>
	<script src="scripts/owl.carousel.js" type="text/javascript"></script>
	<!-- NEW -->
	<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
	<link href="css/site.css" rel="stylesheet" type="text/css" />
	<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
	<!-- scroll-hint -->
	<link rel="stylesheet" href="css/scroll-hint.css">
</head>

<body class="main">

	<!-- PRELOADER -->
	<img id="preloader" src="images/preloader.gif" alt="">
	<!-- //PRELOADER -->
	<div class="preloader_hide">
		<div id="page" class="single_page">

			<!--================================ Menu Top ==================================-->
				<!--#include file='i_menu_top.asp'-->
			<!--=================================== End ====================================-->

			<!--=============================== Menu Left ==================================-->
				<!--#include file='i_menu_left.asp'-->
			<!--================================== End =====================================-->

			<!--============================ Title Menu =============================-->
			<section class="mbr-section article">
				<div class="container">
					<div class="bigTopic">
						<h1 class="bigTopicText"><%=arr_menu_desc(42)%></h1>
					</div>
				</div>
			</section>
			<!--============================== End ==================================-->

			<section class="mbr-section article sectionSub">
			<!--===================================================== Content ====================================================-->
			<%if session("lang")="E" then%>
			
			<%else%>
			
			<%end if%>
			
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="row">
							<div class="clearfix"></div>
							<%
								 dim totalrec
								 totalrec=0
								 i = 0
								 strsql=""
								strsql = sql_annual_main("'"&listed_share&"'",session("lang"),0)					'=========== Revise Support TH/EN  
								 set rs = conir.execute(strsql)
								 if not  rs.eof and not  rs.bof then 
									totalrec=rs("sumrec")
								 end if														
								 rs.close
								 
								 session("num_count")=totalrec
								 strsql=""
								strsql = sql_annual_main("'"&listed_share&"'",session("lang"),100)				'=========== Revise Support TH/EN
								set rs = conir.execute(strsql)
								 if not  rs.eof and not  rs.bof then
									do while not rs.eof
										if i = 0 then
										%>
											<div class="col-md-6">
												<div class="annual-wrap">
													 <div class="cover-annual">
														<p class="font_home_irhead6"><%if session("lang")="E" then%>ANNUAL REPORT <%=rs("annualyear")-543%><%else%>รายงานประจำปี <%=rs("annualyear")%><%end if%></p>
														<%if session("lang")="E" then%>
															<p class="font_text_irhome5">นำเสนอผลการดำเนินงานตามแผนปฏิบัติการของบริษัท สโตนเฮ้นจ์ อินเตอร์ จำกัด (มหาชน) เพื่อให้นักลงทุนได้รับข้อมูลนโยบาย และภาพรวมของ ธุรกิจ และข้อมูลอื่นๆ</p>
														<%else%>
															<p class="font_text_irhome5">นำเสนอผลการดำเนินงานตามแผนปฏิบัติการของบริษัท สโตนเฮ้นจ์ อินเตอร์ จำกัด (มหาชน) เพื่อให้นักลงทุนได้รับข้อมูลนโยบาย และภาพรวมของ ธุรกิจ และข้อมูลอื่นๆ</p>
														<%end if%>
													</div>
													
													<div class="cover">
														<!--<img src="images/an.jpg" alt="" class="shadow-an img-responsive">-->
														<img src="images/<%=rs("picture_annual")%>" alt="" class=" img-responsive">
													</div>
													
													<div class="button-wrapper">
														<!--<a href="pdf/annual_report_2018.pdf" target="_blank" class="a-btnAn">-->
														<a href="annual/<%=rs("filename_annual")%>" target="_blank" class="a-btnAn">
															<!-- <span class="a-btn-symbol"></span> -->
															<span class="a-btn-slide-text"><%if session("lang")="E" then%>Files Download (Lastest)<%else%>ดาวน์โหลด (ฉบับล่าสุด)<%end if%></span>
															<span class="a-btn-slide-icon"></span>
														</a>
													</div>
												  </div>
											</div>
										<%else%>
											<%if i = 1 then%>
												<div class="col-md-6">
											<%end if%>
											<div class="Annual-button">
												<!--<a href="javascript:void(0);" target="_blank">-->
												<a href="annual/<%=rs("filename_annual")%>" target="_blank">
													<div class="cover-Annual-psd">
														<div class="Annual-psd"></div>
													 </div>
													 <p><%if session("lang")="E" then%>ANNUAL REPORT <%=rs("annualyear")-543%><%else%>รายงานประจำปี <%=rs("annualyear")%><%end if%></p>
												 </a>
											</div>
										<%end if
										i = i+1
										rs.movenext	
									loop
									
									if i > 1 then%>
										</div>
									<%end if
								else%>
									<div class="col-md-12" style="text-align: center;">
										<font color="#ff0000" class="no_information">
										<%
										if  session("lang")="E"  then 
											response.write "No Information Now !! "
										else																				
											response.write "ไม่มีข้อมูล ณ ขณะนี้ "
										end if%>
										</font>
									</div>
								<%end if
								rs.close
								 conir.close
								 set rs=nothing
								 set conir=nothing
								%>
						</div>
					</div>
				</div>
			</div>
			<!--======================================================= End ======================================================-->
			</section>
			
		</div>

		<!--====================================== Footer ======================================-->
			<!--#include file='i_footer.asp'-->
		<!--======================================== End =======================================-->

		<script type="text/javascript" src="scripts/modernizr.custom.js"></script>
		<%if session("lang")="E" then%><script type="text/javascript" src="scripts/jquery.dlmenu-en.js"></script><%else%><script type="text/javascript" src="scripts/jquery.dlmenu.js"></script><%end if%>
		<script>
			$(function() {
				$('#dl-menu').dlmenu();
			});
		</script>
		<!-- js/scroll-hint -->
		<script src="scripts/scroll-hint.js"></script>
		<!-- js/scroll-hint -->
		<script>
			new ScrollHint('.js-scrollable', {
			});
		</script>
	</div>
	 <!--#include file="i_googleAnalytics.asp"-->	  
</body>
</html>