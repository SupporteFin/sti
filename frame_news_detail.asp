﻿<!DOCTYPE html>

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->

<%
if request("subtitle")=1  then
	session("cur_page")="IR " & listed_share & " News Update" 
   page_name="News Update"
   title=arr_menu_desc(59)
elseif request("subtitle")=2  then
	session("cur_page")="IR " & listed_share & " Set Announcement" 
   page_name="Set Announcement"
   title=arr_menu_desc(58)
end if
%>

<html class="js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths desktop landscape">
<head>
<meta charset="utf-8">
<title><%=message_title%> :: <%=page_name%></title>
<meta name="keywords" content="<%=message_keyword%>">
<meta name="description" content="<%=message_description%>"> 
<META name="GENERATOR" content="MSHTML 8.00.7600.16385">
<LINK rel="shortcut icon" href="favicon.ico"></LINK>

<link href="style_listed.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../function2007_utf8.js"></script>
<!-- CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/flexslider.css" rel="stylesheet" type="text/css">
<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
<!-- ADD CSS -->
<link href="css/component.css" rel="stylesheet" type="text/css">
<link href="css/custom.css" rel="stylesheet" type="text/css">
<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">
<!-- FONTS -->
<link href="css/font_add.css" rel="stylesheet" type="text/css">
<link href="css/font-awesome.css" rel="stylesheet">
<!-- Mouse Hover CSS -->
<script src="scripts/jquery.min.js" type="text/javascript"></script>
<script src="scripts/jquery.nicescroll.min.js"></script>
<!-- SCRIPTS -->
<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if IE]><html class="ie" lang="en"> <![endif]-->
<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
<script src="scripts/animate.js" type="text/javascript"></script>
<script src="scripts/myscript.js" type="text/javascript"></script>
<script src="scripts/owl.carousel.js" type="text/javascript"></script>
<!-- NEW -->
<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="css/site.css" rel="stylesheet" type="text/css" />
<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
<!-- scroll-hint -->
<link rel="stylesheet" href="css/scroll-hint.css">

</head>
	<frameset ROWS="135,*,103"   frameborder="0" framespacing="0">
		<frame NAME="head_det" SRC="frame_top_logo.asp?title=<%=request("title")%>&subtitle=<%=request("subtitle")%>"  noresize frameborder="0" SCROLLING="NO">
		<frameset cols="*,1000,*" frameborder="0" framespacing="0" class="bgcolor_news2">
				<frame name="left"   src="frame_left.asp"   noresize SCROLLING="NO">
				<frame align="center"  NAME="det" SRC="../../read_detail/news/news.asp?newsid=<%=request("newsid")%>&lang=<%=request("lang")%>"  SCROLLING="auto"   noresize  frameborder="0">	
				<frame name="right"   src="frame_right.asp"   noresize SCROLLING="NO">
		</frameset>	
		<frame name="footer" src="frame_footer.asp"   noresize SCROLLING="NO">
	</frameset>	
      <!--#include file="i_googleAnalytics.asp"-->	
</body>
</html>