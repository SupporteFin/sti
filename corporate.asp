﻿<!DOCTYPE html>

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->
<%
session("cur_page")="IR " &  listed_share & " Corporate Governance"
session("page_asp")="corporate.asp"
page_name="Corporate Governance"
%>

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<TITLE><%=message_title%> :: <%=page_name%></TITLE>
	<META name="Title" content="<%=message_metatitle%>">
	<META name="Description" content="<%=message_description%>">
	<META name="Keywords" content="<%=message_keyword%>">	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="author" content="">
	<link rel="shortcut icon" href="<%=img_icon%>">
	<link href="style_listed.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../function2007_utf8.js"></script>
	<!-- CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/flexslider.css" rel="stylesheet" type="text/css">
	<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
	<!-- ADD CSS -->
	<link href="css/component.css" rel="stylesheet" type="text/css">
	<link href="css/custom.css" rel="stylesheet" type="text/css">
	<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">
	<!-- FONTS -->
	<link href="css/font_add.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- Mouse Hover CSS -->
	<script src="scripts/jquery.min.js" type="text/javascript"></script>
	<script src="scripts/jquery.nicescroll.min.js"></script>
	<!-- SCRIPTS -->
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<!--[if IE]><html class="ie" lang="en"> <![endif]-->
	<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
	<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
	<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
	<script src="scripts/animate.js" type="text/javascript"></script>
	<script src="scripts/myscript.js" type="text/javascript"></script>
	<script src="scripts/owl.carousel.js" type="text/javascript"></script>
	<!-- NEW -->
	<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
	<link href="css/site.css" rel="stylesheet" type="text/css" />
	<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
	<!-- scroll-hint -->
	<link rel="stylesheet" href="css/scroll-hint.css">
</head>

<body class="main">

	<!-- PRELOADER -->
	<img id="preloader" src="images/preloader.gif" alt="">
	<!-- //PRELOADER -->
	<div class="preloader_hide">
		<div id="page" class="single_page">

			<!--================================ Menu Top ==================================-->
				<!--#include file='i_menu_top.asp'-->
			<!--=================================== End ====================================-->

			<!--=============================== Menu Left ==================================-->
				<!--#include file='i_menu_left.asp'-->
			<!--================================== End =====================================-->

			<!--============================ Title Menu =============================-->
			<section class="mbr-section article">
				<div class="container">
					<div class="bigTopic">
						<h1 class="bigTopicText"><%=arr_menu_desc(45)%></h1>
					</div>
				</div>
			</section>
			<!--============================== End ==================================-->

			<section class="mbr-section article sectionSub">
			<!--===================================================== Content ====================================================-->
			<%if session("lang")="E" then%>
				<div class="container">
					<p style="font-size: 22px;font-weight: 500;margin-right: 15px;margin-top: 3px;color: #006c9d;text-align: left;">Message from the Chairman of the Board of Directors</p>
					<p class="bigTopicSubText">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					The Board of Directors of Stonehenge Inter Public Company Limited gives importance to corporate governance system and procedure that leads to success in operating business with 
					highest benefits to shareholders and fairness to all stakeholders. In this regard, it has always adhered corporate governance as guidelines for performing its duty and responsibility in its
					business operations.</p>
					
					<p class="bigTopicSubText">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					The Board of Directors is committed and aware on its responsibility to all stakeholders to comply and give importance to good corporate governance policy.</p>
					
					<p class="bigTopicSubText">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					The Company has considered various essences by evaluating vision, mission, core values of the organization, and Code of Conduct that directors, executives, and employees adhere to 
					and has added extension to cover international practice guidelines to be up-to-date and suitable for changing period and circumstance in order to align with expectation of shareholders, 
					investors, and stakeholders of the Company.</p>
					
					<p class="bigTopicSubText">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					The Board of Directors has given its best effort to ensure strict compliance to Code of Conduct and aims to develop the Company’s corporate governance in order to achieve its goal of 
					creating stability and sustainable and continuous growth to the Company and its shareholders.</p>
					
					<p class="bigTopicSubText">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					This Corporate Governance Policy has been approved in the meeting of the Board of Directors No. 2/2018 on March 27, 2018 and is effective from March 28, 2018 onwards.</p>

					<div>
						<p class="signatureName">Mr.Jumpol Sumpaopol</p>
						<p class="signaturePosition">Chairman of the Board of Directors</p>
					</div>

					<div class="table-data js-scrollable scroll-hint" style="position: relative; overflow: auto;">
						<table border="0" cellspacing="0" cellpadding="0" class="table-width">
							<tbody style="font-size: 15.5px;">
								<tr>
									<th width="60%" align="center" class="topicTeble" style="">Document name</th>
									<th width="15%" align="center" class="topicTeble">File size</th>
									<th width="15%" align="center" class="topicTeble">File type</th>
									<th width="10%" align="center" class="topicTeble">Download</th>
								</tr>
								<!-- Table Header -->
								<tr class="even">
									<td align="center" class="topicTeble2">Corporate Governance by Stonehenge Inter Public Company Limited</td>
									<td align="center" class="topicTeble2">223 KB</td>
									<td align="center" class="topicTeble2">PDF</td>
									<td align="center" class="topicTeble2">
										<a href="pdf/5-1_CorporateGovernance_270361_EN.pdf" target="_blank" download>
											<img src="images/dw.png"></a>
									</td>
								</tr>

							</tbody>
						</table>
					</div>

				</div>
			<%else%>
				<div class="container">
					<p style="font-size: 22px;font-weight: 500;margin-right: 15px;margin-top: 3px;color: #006c9d;text-align: left;">สารจากประธานกรรมการ</p>
					<p class="bigTopicSubText">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;คณะกรรมการ บริษัท สโตนเฮ้นจ์ อินเตอร์ จำกัด (มหาชน) ได้ให้ความสำคัญต่อระบบและกระบวนการกำกับดูแลกิจการที่ดีในการนำมาซึ่งความสำเร็จในการดำเนินธุรกิจให้เกิดประโยชน์สูงสุดแก่ผู้ถือหุ้นและเป็นธรรมแก่ผู้มีส่วนได้เสียทุกฝ่าย จึงได้ยึดถือเป็นแนวทางปฏิบัติหน้าที่ตามความรับผิดชอบในการดำเนินธุรกิจตลอดมา</p>
					<p class="bigTopicSubText">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;คณะกรรมการบริษัท มีความมุ่งมั่นและตระหนักถึงความรับผิดชอบที่พึงมีต่อผู้มีส่วนได้ส่วนเสียทุกฝ่าย ในการที่จะปฏิบัติและให้ความสำคัญกับนโยบายการกำกับดูแลกิจการที่ดี</p>
					<p class="bigTopicSubText">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;บริษัทฯ ได้พิจารณาสาระสำคัญต่างๆ ด้วยการประมวล วิสัยทัศน์ พันธกิจ คุณค่าหลักขององค์กร หลักการนโยบาย และแนวทางปฏิบัติที่ดีของกรรมการ ผู้บริหาร และพนักงาน ยึดถือหน้าที่ตามความรับผิดชอบและเพิ่มเติมให้ครอบคลุมแนวปฏิบัติต่างๆ ที่เป็นสากล มีความทันสมัยเหมาะสมกับเวลาและสถานการณ์ที่เปลี่ยนแปลงไป เพื่อให้สอดคล้องกับความคาดหวังของผู้ถือหุ้น นักลงทุน และผู้มีส่วนได้เสียที่มีต่อบริษัทฯ</p>
					<p class="bigTopicSubText">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;คณะกรรมการบริษัท มีความพยายามอย่างที่สุดที่จะดูแลให้มีการปฏิบัติตามนโยบายและแนวปฏิบัติที่ระบุไว้อย่างเคร่งครัดและมีความมุ่งมั่นที่จะพัฒนาการกำกับดูแลกิจการของบริษัทฯเพื่อให้บรรลุเป้าหมายในการสร้างความมั่นคงและความเจริญเติบโตอย่างต่อเนื่องและยั่งยืนให้กับบริษัทฯ และผู้ถือหุ้นต่อไป</p>
					<p class="bigTopicSubText">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;นโยบายการกำกับดูแลกิจการที่ดีฉบับนี้ได้รับการอนุมัติจากที่ประชุมคณะกรรมการบริษัทครั้งที่ 2/2561 เมื่อวันที่ 27 มีนาคม 2561 โดยให้มีผลตั้งแต่วันที่ 28 มีนาคม 2561 เป็นต้นไป</p>

					<div>
						<p class="signatureName">นายจุมพล สำเภาพล</p>
						<p class="signaturePosition">ประธานกรรมการ</p>
					</div>

					<div class="table-data js-scrollable scroll-hint" style="position: relative; overflow: auto;">
						<table border="0" cellspacing="0" cellpadding="0" class="table-width">
							<tbody style="font-size: 15.5px;">
								<tr>
									<th width="60%" align="center" class="topicTeble" style="">ชื่อเอกสาร</th>
									<th width="15%" align="center" class="topicTeble">ขนาดไฟล์</th>
									<th width="15%" align="center" class="topicTeble">ชนิดไฟล์</th>
									<th width="10%" align="center" class="topicTeble">ดาวน์โหลด</th>
								</tr>
								<!-- Table Header -->
								<tr class="even">
									<td align="center" class="topicTeble2">นโยบายเกี่ยวกับการกำกับดูแลกิจการที่ดี ของ บริษัท สโตนเฮ้นจ์ อินเตอร์ จำกัด (มหาชน)</td>
									<td align="center" class="topicTeble2">414 KB</td>
									<td align="center" class="topicTeble2">PDF</td>
									<td align="center" class="topicTeble2">
										<a href="pdf/5-1_CorporateGovernance_270361_THAI.pdf" target="_blank" download>
											<img src="images/dw.png"></a>
									</td>
								</tr>

							</tbody>
						</table>
					</div>

				</div>
			<%end if%>
				
			<!--======================================================= End ======================================================-->
			</section>
			
		</div>

		<!--====================================== Footer ======================================-->
			<!--#include file='i_footer.asp'-->
		<!--======================================== End =======================================-->

		<script type="text/javascript" src="scripts/modernizr.custom.js"></script>
		<%if session("lang")="E" then%><script type="text/javascript" src="scripts/jquery.dlmenu-en.js"></script><%else%><script type="text/javascript" src="scripts/jquery.dlmenu.js"></script><%end if%>
		<script>
			$(function() {
				$('#dl-menu').dlmenu();
			});
		</script>
		<!-- js/scroll-hint -->
		<script src="scripts/scroll-hint.js"></script>
		<!-- js/scroll-hint -->
		<script>
			new ScrollHint('.js-scrollable', {
			});
		</script>
	</div>
	 <!--#include file="i_googleAnalytics.asp"-->	  
</body>
</html>