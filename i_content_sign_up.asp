﻿<style>
	input[type=text], textarea, select {
		border-radius: 0px;
		border: 1px solid #006c9d;
		line-height: 20px;
		font-size: 13px;
		font-weight: normal;
		margin-bottom: 4px;
		height: 30px !important;
	}
	input[type=text]:focus, textarea:focus {
		border: 1px solid #006c9d;
	}
</style>

						<table width="100%"  border="0" cellpadding="5" cellspacing="0" align="left">
						<tr>
									<td align="left" valign="top"><br>
									<%if session("lang")="T" or session("lang")=""  then%>
									กรุณากรอกรายละเอียดด้านล่างเพื่อทำการสมัครสมาชิก IR Services
									<%else%>
									Please provide your information to subscribe our E-mail Alert Services			
									<%end if%>	
							  </td>
							</tr>
							<tr>
									<td align="left" valign="top">
										<table  border="0" cellspacing="0" cellpadding="3">
												<tr  valign="middle" style="height: 32px;">                 
														<td width="130"><font color="red"><b>*</b></font>&nbsp;<%if session("lang")="T" or session("lang")=""  then%>อีเมล<%else%>E-mail<%end if%> : </td>
														<td><input name="e_mail" id="e_mail" type="text" size="30" maxlength="40"  class="style_textbox"  value="">
														</td>
												</tr>
												<tr  valign="middle" style="height: 32px;">
														<td><font color="red"><b>*</b></font>&nbsp;<%if session("lang")="T" or session("lang")=""  then%>ชื่อ<%else%>First Name<%end if%> : </td>
														<td><input name="fname" id="fname"  type="text" size="30" maxlength="40"  class="style_textbox" value="<%=fname%>">
														</td>
												</tr>	
												<tr valign="middle" style="height: 32px;">
														<td><font color="red"><b>*</b></font>&nbsp;<%if session("lang")="T" or session("lang")=""  then%>นามสกุล<%else%>Last Name<%end if%> : </td>
														<td><input name="lname" id="lname"  type="text" size="30" maxlength="40"  class="style_textbox" value="<%=lname%>" >
														</td>
												</tr>
												<tr valign="middle" style="height: 100px;">
														<td><font color="red"><b>*</b></font>&nbsp;<%if session("lang")="T" or session("lang")=""  then%>ที่อยู่<%else%>Address<%end if%> : </td>
														<td><textarea name="address" id="address"  type="textarea" cols="30" rows="3" class="style_text_area" WRAP="virtual" value="<%=address%>" ></textarea>
														</td>
												</tr>
												<tr valign="middle" style="height: 32px;">
														<td><font color="red"><b>*</b></font>&nbsp;<%if session("lang")="T" or session("lang")=""  then%>เบอร์โทร<%else%>Telphone<%end if%> : </td>
														<td><input name="telphone" id="telphone" type="text" size="30" maxlength="20"  class="style_textbox" value="<%=telphone%>" >
														</td>
												</tr>
												<tr  valign="middle" style="height: 32px;">
														<td><font color="red"><b>*</b></font>&nbsp;<%if session("lang")="T" or session("lang")=""  then%>บริษัท<%else%>Company<%end if%> : </td>
														<td><input name="company" id="company"  type="text" size="30" maxlength="40"  class="style_textbox" value="<%=company%>"  >
														</td>
												</tr>
												<tr  valign="middle" style="height: 32px;">
														<td><font color="red"><b>*</b></font>&nbsp;<%if session("lang")="T" or session("lang")=""  then%>สถานะ<%else%>Profile<%end if%> : </td>
														<td>
															<select name="profile" id="profile"  size="1"  style="cursor:hand;" class="style_combo">
															 <option selected value="0"><%if session("lang")="T" or session("lang")=""  then%>เลือกสถานะ<%else%>Select Profile<%end if%></option>
															<%			
																strsql="SELECT profile_id,profile FROM member_profile order by profile"
																set rs=conirauthen.execute(strsql)
																
																if not rs.eof and not rs.bof then
																	do while not rs.eof %>
																				<option value="<%=rs("profile_id")%>" <%if profile=trim(rs("profile_id")) then%> selected<%end if%>><%=rs("profile")%></option>
																<%			rs.movenext	
																	loop		
																end if
																rs.close
																set rs=nothing
																conirauthen.close
																set conirauthen=nothing																	
																%>											
														</select>
														</td>
												</tr>      
												<tr  valign="middle" style="height: 60px;">                 
															<td><font color="red"><b>*</b></font>&nbsp;<%if session("lang")="T" or session("lang")=""  then%>Captcha<%else%>Captcha<%end if%> : </td>
															<td align="left"><iframe id="iframe_content_captcha" name="iframe_content_captcha" src="i_content_captcha.asp" width="240px" height="40px" frameborder="0" framespacing="0" framepadding="0"  scrolling="no"></iframe>
															</td>
													</tr>
										</table>
									</td>
							</tr>
					</table>
