﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->
<!-- include virtual = "../../i_conpsims.asp" -->
<!-- include virtual = "../../financial/i_month.asp" -->
<!--#include file = "../../i_aboutbrowser.asp" -->

<%
session("cur_page")="IR " &  listed_share & " Financial Statement Yearly"
session("page_asp")="finance_yearly.asp"
page_name="Financial Statement Yearly"
%>

<html>
<head>
	<!-- Fix Default -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><%=message_title%> :: <%=page_name%></title>
	<meta content="<%=message_keyword%>" name="keywords">
	<meta content="<%=message_description%>" name="description">
	<link rel="shortcut icon" href="<%=shortcut_icon%>" type="image/x-icon">
	<link rel="icon" href="<%=shortcut_icon%>" type="image/x-icon">
	<script language="javascript" src="scripts/framemanager.js"></script>
	<link href="style_listed.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../function2007_utf8.js"></script>
	<!-- CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/flexslider.css" rel="stylesheet" type="text/css">
	<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
	<!-- ADD CSS -->
	<link href="css/component.css" rel="stylesheet" type="text/css">
	<link href="css/custom.css" rel="stylesheet" type="text/css">
	<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">
	<!-- FONTS -->
	<link href="css/font_add.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- Mouse Hover CSS -->
	<script src="scripts/jquery.min.js" type="text/javascript"></script>
	<script src="scripts/jquery.nicescroll.min.js"></script>
	<!-- SCRIPTS -->
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<!--[if IE]><html class="ie" lang="en"> <![endif]-->
	<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
	<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
	<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
	<script src="scripts/animate.js" type="text/javascript"></script>
	<script src="scripts/owl.carousel.js" type="text/javascript"></script>
	<!-- NEW -->
	<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
	<link href="css/site.css" rel="stylesheet" type="text/css" />
	<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
	<!-- scroll-hint -->
	<link rel="stylesheet" href="css/scroll-hint.css">
	
	<style>
		.font_header_title {
			font-size: 24px;
			font-weight: 600;
		}
		
		.font_th_header3{
			font-size: 14px;
			text-align: center;
			background-color: #006c9d;
			color: #ffffff;
			padding: 10px;
		}
	</style>
</head>
<body>

<main class="cd-main-content">
	<section id="main">
		<!--#include file="i_top_detail.asp"-->
	</section>
	
	<div class="container paddingmain" style="min-height: 836px;">
		<div class="row">
			<div class="col-lg-12" style="padding-bottom: 50px;">
				<div class="row">
					<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
						<div class="row">
							<div class="col-lg-12" style="padding-bottom: 30px;padding-top: 30px;">		
								<p class="font_header_title"><%if session("lang")="E" then %>Financial Statement<%else%>งบการเงินปี<%end if%>  <%=Request("years")%></p>							
								<div class="hr_header_title"></div>	
							</div>  
							<br>
						</div>
						
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" valign="top">
									<div class="main_text">
									<!-- .............................................................................. Content .............................................................................. -->	
										<table width="100%" border="1" cellspacing="0" cellpadding="3">
											 <tr style="height: 35px;">  
												<td valign="middle" align="left">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td align="left">
																<!--#include file="../file_customize/i_financial_yearly_utf8.asp"-->	
															</td>
														</tr>
													</table>
												</td>
											  </tr>
											  <tr style="height: 35px;">  
												<td valign="middle" align="center">
													<table width="100%"  border="0" cellpadding="3" cellspacing="2"  class="box">										
														<tr>  
															<td align="center"> <%if browsertype="IE" and  cint(browserversion) <=6 then%>
															   <IFRAME  id="ifrSample1"   src="<%=srciframe%>" frameBorder="0"  width="100%" height="600"  scrolling ="Auto"  ></IFRAME>
															 <%else%>
															<IFRAME  id="ifrSample1"   src="<%=srciframe%>" frameBorder="0"  width="100%" onload="FrameManager.registerFrame(this)"  scrolling ="No"></IFRAME>
															 <%end if%></td>
														</tr>
													</table>									
												</td>																
											  </tr>	                 
											</table>    
									 <!-- .............................................................................. End Content .............................................................................. -->		
									</div>
								</td>
							</tr>
							<tr>
							  <td align="left">&nbsp;</td>
							</tr>
							<tr>
							  <td></td>
							</tr>
				  </table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
	<!-- ..................... footer ..................... -->
	<!--include file="i_footer_detail.asp"-->
	<!-- ..................... footer ..................... -->
	</div>
</main>
 <!--#include file="i_googleAnalytics.asp"-->	
</body>
</html>