﻿<!DOCTYPE html>

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->
<%
session("cur_page")="IR " &  listed_share & " Financial Highlight"
session("page_asp")="finance_highlights.asp"
page_name="Financial Highlight"
%>

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<TITLE><%=message_title%> :: <%=page_name%></TITLE>
	<META name="Title" content="<%=message_metatitle%>">
	<META name="Description" content="<%=message_description%>">
	<META name="Keywords" content="<%=message_keyword%>">	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="author" content="">
	<link rel="shortcut icon" href="<%=img_icon%>">
	<link href="style_listed.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../function2007_utf8.js"></script>
	<!-- CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/flexslider.css" rel="stylesheet" type="text/css">
	<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
	<!-- ADD CSS -->
	<link href="css/component.css" rel="stylesheet" type="text/css">
	<link href="css/custom.css" rel="stylesheet" type="text/css">
	<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">
	<!-- FONTS -->
	<link href="css/font_add.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- Mouse Hover CSS -->
	<script src="scripts/jquery.min.js" type="text/javascript"></script>
	<script src="scripts/jquery.nicescroll.min.js"></script>
	<!-- SCRIPTS -->
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<!--[if IE]><html class="ie" lang="en"> <![endif]-->
	<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
	<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
	<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
	<script src="scripts/animate.js" type="text/javascript"></script>
	<script src="scripts/myscript.js" type="text/javascript"></script>
	<script src="scripts/owl.carousel.js" type="text/javascript"></script>
	<!-- NEW -->
	<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
	<link href="css/site.css" rel="stylesheet" type="text/css" />
	<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
	<!-- scroll-hint -->
	<link rel="stylesheet" href="css/scroll-hint.css">
</head>

<body class="main">

	<!-- PRELOADER -->
	<img id="preloader" src="images/preloader.gif" alt="">
	<!-- //PRELOADER -->
	<div class="preloader_hide">
		<div id="page" class="single_page">

			<!--================================ Menu Top ==================================-->
				<!--#include file='i_menu_top.asp'-->
			<!--=================================== End ====================================-->

			<!--=============================== Menu Left ==================================-->
				<!--#include file='i_menu_left.asp'-->
			<!--================================== End =====================================-->

			<!--============================ Title Menu =============================-->
			<section class="mbr-section article">
				<div class="container">
					<div class="bigTopic">
						<h1 class="bigTopicText"><%=arr_menu_desc(31)%></h1>
					</div>
				</div>
			</section>
			<!--============================== End ==================================-->

			<section class="mbr-section article sectionSub">
			<!--===================================================== Content ====================================================-->
			<%if session("lang")="E" then%>
				<div class="container">
					<div class="table-data js-scrollable scroll-hint" style="position: relative; overflow: auto;">
						<table border="0" cellspacing="0" cellpadding="0" class="table-width table-MajorShareholder">
							  <tbody style="font-size: 15.5px;">
								<tr>
									  <th width="60%" align="center" class="topicTeble">Period</th>   
									  <th width="20%" align="center" class="topicTeble">Y/E '18</th>
									  <th width="20%" align="center" class="topicTeble">Q1 '19</th>
								</tr>
								<tr>
									  <th width="60%" align="center" class="topicTeble">as of</th>   
									  <th width="20%" align="center" class="topicTeble">31/12/2018</th>
									  <th width="20%" align="center" class="topicTeble">31/03/2019</th>
								</tr>
								<tr class="even">
									  <td colspan="3" align="left"><b>Financial Data</b></td>
								</tr>
								<tr class="even">
									  <td align="left">Assets</td>
									  <td align="right">716.82</td>
									  <td align="right">729.38</td>
								</tr>
								<tr class="even">
									  <td align="left">Liabilities</td>
									  <td align="right">144.63</td>
									  <td align="right">138.94</td>
								</tr>
								<tr class="even">
									  <td align="left">Equity</td>
									  <td align="right">572.19</td>
									  <td align="right">590.44</td>
								</tr>
								<tr class="even">
									  <td align="left">Paid-up Capital</td>
									  <td align="right">134.00</td>
									  <td align="right">134.00</td>
								</tr>
								<tr class="even">
									  <td align="left">Revenue</td>
									  <td align="right">635.10</td>
									  <td align="right">155.26</td>
								</tr>
								<tr class="even">
									  <td align="left">Net Profit</td>
									  <td align="right">73.03</td>
									  <td align="right">18.25</td>
								</tr>
								<tr class="even">
									  <td align="left">EPS (Baht)</td>
									  <td align="right">0.36</td>
									  <td align="right">0.07</td>
								</tr>
								<tr class="even">
									  <td colspan="3" align="left"><b>Financial Ratio</b></td>
								</tr>
								<tr class="even">
									  <td align="left">ROA(%)</td>
									  <td align="right">17.91</td>
									  <td align="right">12.26</td>
								</tr>
								<tr class="even">
									  <td align="left">ROE(%)</td>
									  <td align="right">20.03</td>
									  <td align="right">12.41</td>
								</tr>
								<tr class="even">
									  <td align="left">Net Profit Margin(%)</td>
									  <td align="right">11.50</td>
									  <td align="right">11.76</td>
								</tr>
								<tr>
									  <th width="60%" align="center" class="topicTeble">Statistics as of</th>   
									  <th width="20%" align="center" valign="top" class="topicTeble">28/12/2018</th>
									  <th width="20%" align="center" valign="top" class="topicTeble">06/06/2019</th>
								</tr>
								<tr class="even">
									  <td align="left">Last Price(Baht)</td>
									  <td align="right">5.30</td>
									  <td align="right">4.98</td>
								</tr>
								<tr class="even">
									  <td align="left">Market Cap.</td>
									  <td align="right">1,420.40</td>
									  <td align="right">1,334.64</td>
								</tr>
								<tr class="even">
									  <td align="left">F/S Period (As of date)</td>
									  <td align="right"></td>
									  <td align="right">31/03/2562</td>
								</tr>
								<tr class="even">
									  <td align="left">P/E</td>
									  <td align="right">25.10</td>
									  <td align="right">14.91</td>
								</tr>
								<tr class="even">
									  <td align="left">P/BV</td>
									  <td align="right"> - </td>
									  <td align="right">2.26</td>
								</tr>
								<tr class="even">
									  <td align="left">Book Value per share (Baht)</td>
									  <td align="right">N.A.</td>
									  <td align="right">2.20</td>
								</tr>
								<tr class="even">
									  <td align="left">Dvd. Yield(%)</td>
									  <td align="right">N/A</td>
									  <td align="right">2.01</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			<%else%>
				<div class="container">
					<div class="table-data js-scrollable scroll-hint" style="position: relative; overflow: auto;">
						<table border="0" cellspacing="0" cellpadding="0" class="table-width table-MajorShareholder">
							  <tbody style="font-size: 15.5px;">
								<tr>
									  <th width="60%" align="center" class="topicTeble">งวดงบการเงิน</th>   
									  <th width="20%" align="center" class="topicTeble">งบปี 61</th>
									  <th width="20%" align="center" class="topicTeble">ไตรมาส1/62</th>
								</tr>
								<tr>
									  <th width="60%" align="center" class="topicTeble">ณ วันที่</th>   
									  <th width="20%" align="center" class="topicTeble">31/12/2561</th>
									  <th width="20%" align="center" class="topicTeble">31/03/2562</th>
								</tr>
								<tr class="even">
									  <td colspan="3" align="left"><b>บัญชีทางการเงินที่สำคัญ</b></td>
								</tr>
								<tr class="even">
									  <td align="left">สินทรัพย์รวม</td>
									  <td align="right">716.82</td>
									  <td align="right">729.38</td>
								</tr>
								<tr class="even">
									  <td align="left">หนี้สินรวม</td>
									  <td align="right">144.63</td>
									  <td align="right">138.94</td>
								</tr>
								<tr class="even">
									  <td align="left">ส่วนของผู้ถือหุ้น</td>
									  <td align="right">572.19</td>
									  <td align="right">590.44</td>
								</tr>
								<tr class="even">
									  <td align="left">มูลค่าหุ้นที่เรียกชำระแล้ว</td>
									  <td align="right">134.00</td>
									  <td align="right">134.00</td>
								</tr>
								<tr class="even">
									  <td align="left">รายได้รวม</td>
									  <td align="right">635.10</td>
									  <td align="right">155.26</td>
								</tr>
								<tr class="even">
									  <td align="left">กำไรสุทธิ</td>
									  <td align="right">73.03</td>
									  <td align="right">18.25</td>
								</tr>
								<tr class="even">
									  <td align="left">กำไรต่อหุ้น (บาท)</td>
									  <td align="right">0.36</td>
									  <td align="right">0.07</td>
								</tr>
								<tr class="even">
									  <td colspan="3" align="left"><b>อัตราส่วนทางการเงินที่สำคัญ</b></td>
								</tr>
								<tr class="even">
									  <td align="left">ROA(%)</td>
									  <td align="right">17.91</td>
									  <td align="right">12.26</td>
								</tr>
								<tr class="even">
									  <td align="left">ROE(%)</td>
									  <td align="right">20.03</td>
									  <td align="right">12.41</td>
								</tr>
								<tr class="even">
									  <td align="left">อัตรากำไรสุทธิ(%)</td>
									  <td align="right">11.50</td>
									  <td align="right">11.76</td>
								</tr>
								<tr>
									  <th width="60%" align="center" class="topicTeble">ค่าสถิติสำคัญ</th>   
									  <th rowspan="2" width="20%" align="center" valign="top" class="topicTeble">28/12/2561</th>
									  <th rowspan="2" width="20%" align="center" valign="top" class="topicTeble">06/06/2562</th>
								</tr>
								<tr>
									  <th width="60%" align="center" class="topicTeble">ณ วันที่</th>   
								</tr>
								<tr class="even">
									  <td align="left">ราคาล่าสุด(บาท)</td>
									  <td align="right">5.30</td>
									  <td align="right">4.98</td>
								</tr>
								<tr class="even">
									  <td align="left">มูลค่าหลักทรัพย์ตามราคาตลาด</td>
									  <td align="right">1,420.40</td>
									  <td align="right">1,334.64</td>
								</tr>
								<tr class="even">
									  <td align="left">วันที่ของงบการเงินที่ใช้คำนวณค่าสถิติ</td>
									  <td align="right"></td>
									  <td align="right">31/03/2562</td>
								</tr>
								<tr class="even">
									  <td align="left">P/E (เท่า)</td>
									  <td align="right">25.10</td>
									  <td align="right">14.91</td>
								</tr>
								<tr class="even">
									  <td align="left">P/BV (เท่า)</td>
									  <td align="right"> - </td>
									  <td align="right">2.26</td>
								</tr>
								<tr class="even">
									  <td align="left">มูลค่าหุ้นทางบัญชีต่อหุ้น (บาท)</td>
									  <td align="right">N.A.</td>
									  <td align="right">2.20</td>
								</tr>
								<tr class="even">
									  <td align="left">อัตราส่วนเงินปันผลตอบแทน(%)</td>
									  <td align="right">N/A</td>
									  <td align="right">2.01</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			<%end if%>
			<!--======================================================= End ======================================================-->
			
			</section>
			
		</div>

		<!--====================================== Footer ======================================-->
			<!--#include file='i_footer.asp'-->
		<!--======================================== End =======================================-->

		<script type="text/javascript" src="scripts/modernizr.custom.js"></script>
		<%if session("lang")="E" then%><script type="text/javascript" src="scripts/jquery.dlmenu-en.js"></script><%else%><script type="text/javascript" src="scripts/jquery.dlmenu.js"></script><%end if%>
		<script>
			$(function() {
				$('#dl-menu').dlmenu();
			});
		</script>
		<!-- js/scroll-hint -->
		<script src="scripts/scroll-hint.js"></script>
		<!-- js/scroll-hint -->
		<script>
			new ScrollHint('.js-scrollable', {
			});
		</script>
	</div>
	 <!--#include file="i_googleAnalytics.asp"-->	  
</body>
</html>