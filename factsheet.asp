﻿<!DOCTYPE html>

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->
<!--#include file = "../../i_aboutbrowser.asp" -->
<!--#include file="../file_customize/i_factsheet.asp"-->

<%
session("cur_page")="IR " &  listed_share & " Fact Sheet"
session("page_asp")="factsheet.asp"
page_name="Fact Sheet"
%>

<html xmlns="http://www.w3.org/1999/xhtml">
	
<head>
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<title><%=message_title%> :: <%=page_name%> </title>
	<meta name="description" content="<%=message_Description%>">
	<meta name="keywords" content="<%=message_keyword%>">
	<link rel="shortcut icon" href="<%=shortcut_icon%>" />
	
	<script type="text/javascript" src="Scripts/FrameManager.js"></script>
	<!-- CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="css/flexslider.css" rel="stylesheet" type="text/css">
		<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
		<link href="css/style.css" rel="stylesheet" type="text/css">
		<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
		<!-- <link href="css/colors/" rel="stylesheet" type="text/css" id="colors"> -->

		<!-- ADD CSS -->
		<link href="css/component.css" rel="stylesheet" type="text/css">
		<link href="css/custom.css" rel="stylesheet" type="text/css">
		<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">

		<!-- FONTS -->
		<link href="css/font-awesome.css" rel="stylesheet">

		<script src="scripts/jquery.min.js" type="text/javascript"></script>
		<script src="scripts/jquery.nicescroll.min.js"></script>

		<!-- SCRIPTS -->
		<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
		<!--[if IE]><html class="ie" lang="en"> <![endif]-->
		
		
		<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
		<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
		
		<!-- <script src="scripts/superfish.min.js" type="text/javascript"></script> -->
		<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
		<script src="scripts/animate.js" type="text/javascript"></script>
		<script src="scripts/myscript.js" type="text/javascript"></script>
		<script src="scripts/cc_502.js" type="text/javascript"></script>
		<script src="scripts/js15_as.js" type="text/javascript"></script>
		<script src="scripts/owl.carousel.js" type="text/javascript"></script>

		<!-- NEW -->
		<!-- <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" /> -->
		<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
		<link href="css/site.css" rel="stylesheet" type="text/css" />
		<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
		<!-- END NEW -->
</head>
<body>
<div id="templatemo_wrapper">
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
   <tr>
		<td>
			 	<!--#include file = "i_top_logo_detail.asp" -->
		</div> 		
		</td>
	</tr>
   <tr>
		<td align="center">
				<table border="0" cellspacing="0" cellpadding="0" class="bg_content">
				   <tr>
					 <td align="center" valign="top">
					   <table  border="0" cellspacing="0" cellpadding="0" class="tb_maincontent">
						 <tr>
						   <td align="left"><p><br><br><strong style="font-weight: 600; font-size: 20px;"><%=arr_menu_desc(39)%></strong><br><br></p></td>
						 </tr>
						 <tr>
						   <td align="center">
						   <!-- ++++++++++++++++++++++++  content ++++++++++++++++++++++++-->	
								<table width="920" border="0" cellspacing="0" cellpadding="0">	
								  <tr>  
									<td valign="middle" align="left">
										<table width="100%"  border="0" cellpadding="3" cellspacing="2" >										
											<tr>  
												<td> <%if browsertype="IE" and  cint(browserversion) <=6 then%>
												   <IFRAME  id="ifrSample1"   src="<%=srciframe%>" frameBorder="0"  width="100%" height="600"  scrolling ="Auto"  ></IFRAME>
												 <%else%>
												<!--<IFRAME  id="ifrSample1"   src="<%=srciframe%>" frameBorder="0"  width="100%" onload="FrameManager.registerFrame(this)"  scrolling ="No"></IFRAME>-->
												<IFRAME  id="ifrSample1"   src="<%=srciframe%>" frameBorder="0"  width="100%" height="3400" scrolling ="No"></IFRAME>
												 <%end if%></td>
											</tr>
										</table>									
									</td>																
								  </tr>	                 
								</table>   					

							<!-- +++++++++++++++++++++ End  content ++++++++++++++++++++++++-->	
						   </td>
						 </tr>
						  <tr>
								<td>&nbsp;</td>
							</tr>
						</table>
					 </td>
				   </tr>
				</table>
		</td>
	</tr>		
   <tr>
		<td>		
				 <table width="100%" border="0" cellspacing="0" cellpadding="0" >
				  <tr>
						<td>&nbsp;</td>
					</tr>
				</table> 
		</td>
	</tr>
</table> 
</div>			
<!-- ++++++++++++++++++++++++  footer ++++++++++++++++++++++++-->	
		<!--include file = "i_footer_detail.asp" -->
<!-- +++++++++++++++++++++ End footer ++++++++++++++++++++++++-->		
 <!--#include file="i_googleAnalytics.asp"-->	
</body>
</html>