<section class="mbr-section article section-menu section-menu-res">
	<div class="container padding0 fontMenu">
		<div id="dl-menu" class="dl-menuwrapper col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<button class="dl-trigger" style="animation-name: fadeInLeft !important;">Open Menu</button>
			<p><span><%=arr_menu_desc(18)%></span></p>
			<ul class="dl-menu">
				<li class="active"><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?home';"><%=arr_menu_desc(19)%></a></li>
				<li><a href="#"><%=arr_menu_desc(20)%></a>
					<!-- Corporate Information -->
					<ul class="dl-submenu">
						<li><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?history';"><%=arr_menu_desc(21)%></a></li>
						<!--  History -->
						<li><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?vision';"><%=arr_menu_desc(22)%></a></li>
						<!-- Vision -->
						<!-- <li><a href="ContextOfOrganization.asp">บริบทองค์กร</a></li>  Context of Organization  -->
						<li><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?chairman';"><%=arr_menu_desc(23)%></a></li>
						<!-- Chairman’s Statement -->
						<li><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?board';"><%=arr_menu_desc(24)%></a></li>
						<!-- Board of Director -->
						<li><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?boardcharter';"><%=arr_menu_desc(49)%></a></li>
						<!-- Board Charter -->
						<li><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?organization';"><%=arr_menu_desc(25)%></a></li>
						<!-- Organization Chart -->
						<li><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?structure';"><%=arr_menu_desc(26)%> </a></li>
						<!-- Shareholder Structure -->
						<li><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?award';"><%=arr_menu_desc(27)%></a></li>
						<!-- Awards and Certificates -->
						<li><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?documents';"><%=arr_menu_desc(28)%></a></li>
						<!-- Company Regulation -->
					</ul>
				</li>
				<li><a href="#"><%=arr_menu_desc(29)%></a>
					<!-- Financial Information -->
					<ul class="dl-submenu">
						<li><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?finance_statement';"><%=arr_menu_desc(30)%></a></li>
						<!-- Financial Statement -->
						<li><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?finance_highlights';"><%=arr_menu_desc(31)%></a></li>
						<!-- Financial Highlight -->
						<li><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?mda';"><%=arr_menu_desc(32)%></a></li>
						<!-- MD&A -->
					</ul>
				</li>
				<li><a href="#"><%=arr_menu_desc(33)%></a>
					<!-- Shareholder Information -->
					<ul class="dl-submenu">
						<li><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?general_meeting';"><%=arr_menu_desc(34)%></a></li>
						<!-- General Meeting -->
						<li><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?major';"><%=arr_menu_desc(35)%></a></li>
						<!-- Major Shareholder -->
						<li><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?dividend';"><%=arr_menu_desc(36)%></a></li>
						<!-- Dividend Payment -->
						<li><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?ir_calendar';"><%=arr_menu_desc(37)%></a></li>
						<!-- IR Calendar -->
						<!--<li><a href="company.asp"><%=arr_menu_desc(38)%></a></li>-->
						<li><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?company_snapshot';"><%=arr_menu_desc(38)%></a></li>
						<!-- Company Snapshot -->
						<!--<li><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?factsheet';"><%=arr_menu_desc(39)%></a></li>-->
						<%if session("lang")="E" then%>
							<li><a href="https://www.set.or.th/set/factsheet.do?symbol=STI&ssoPageId=3&language=en&country=EN" target="_blank"><%=arr_menu_desc(39)%></a></li>
						<%else%>
							<li><a href="https://www.set.or.th/set/factsheet.do?symbol=STI&ssoPageId=3&language=th&country=TH" target="_blank"><%=arr_menu_desc(39)%></a></li>
						<%end if%>
						<!-- Fact Sheet -->
						<%if session("lang")="E" then%>
							<li><a href="https://market.sec.or.th/public/ipos/IPOSEQ01.aspx?TransID=209823&lang=en" target="_blank"><%=arr_menu_desc(40)%></a></li>
						<%else%>
							<li><a href="https://market.sec.or.th/public/ipos/IPOSEQ01.aspx?TransID=209823&lang=th" target="_blank"><%=arr_menu_desc(40)%></a></li>
						<%end if%>
						
						<!-- Filing -->
					</ul>
				</li>
				<li><a href="#"><%=arr_menu_desc(41)%></a>
					<!-- Annual Report / Form 56-1 -->
					<ul class="dl-submenu">
						<li><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?annual_report';"><%=arr_menu_desc(42)%></a></li>
						<!-- Annual Report -->
						<li><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?finance_56';"><%=arr_menu_desc(43)%></a></li>
						<!-- Form 56-1 -->
					</ul>
				</li>
				<li><a href="#"><%=arr_menu_desc(44)%></a>
					<!-- Sustainability -->
					<ul class="dl-submenu">
						<li><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?corporate';"><%=arr_menu_desc(45)%></a></li>
						<!-- Corporate Governance -->
						<li><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?anticorruption';"><%=arr_menu_desc(46)%></a></li>
						<!-- Anti-corruption -->
						<li><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?ethics';"><%=arr_menu_desc(47)%></a></li>
						<!-- Business Ethics -->
						<!--<li><a href="csr.asp"><%=arr_menu_desc(48)%></a></li>-->
						<li><a href="https://www.sti.co.th/th/blog-cat.php?cid=9" target="_blank"><%=arr_menu_desc(48)%></a></li>
						<!-- CSR & Activities -->
						<li><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?policy';"><%=arr_menu_desc(50)%></a></li>
						<!-- Company policy -->
					</ul>
				</li>
				<li><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?webcasts';"><%=arr_menu_desc(51)%></a></li>
				<!-- Webcast & Presentation -->
				<li><a href="https://www.sti.co.th/th/investor/analystdata.php" target="_blank"><%=arr_menu_desc(52)%></a></li>
				<!-- Analyst Report -->
				<li><a href="#"><%=arr_menu_desc(53)%></a>
					<!-- Stock Information -->
					<ul class="dl-submenu">
						<li><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?stock_quote';"><%=arr_menu_desc(54)%></a></li>
						<!-- Stock Price -->
						<%if session("lang")="E" then%>
							<li><a href="https://www.set.or.th/set/historicaltrading.do?symbol=STI&language=en&country=EN" target="_blank"><%=arr_menu_desc(55)%></a></li>
						<%else%>
							<li><a href="https://www.set.or.th/set/historicaltrading.do?symbol=STI&language=th&country=TH" target="_blank"><%=arr_menu_desc(55)%></a></li>
						<%end if%>
						<!-- Historical Price -->
						<li><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?calculator';"><%=arr_menu_desc(56)%></a></li>
						<!-- Investment Calculator -->
					</ul>
				</li>
				<li><a href="#"><%=arr_menu_desc(57)%></a>
					<!-- News Room -->
					<ul class="dl-submenu">
						<li><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?set_announcement';"><%=arr_menu_desc(58)%></a></li>
						<!-- SET Announcement -->
						<li><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?news_update';"><%=arr_menu_desc(59)%></a></li>
						<!-- News Update -->
						<!--<li><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?public';"><%=arr_menu_desc(60)%></a></li>-->
						<li><a href="https://www.sti.co.th/th/blog-cat.php?cid=1" target="_blank"><%=arr_menu_desc(60)%></a></li>
						<!-- Public Relation -->
						<li><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?news_clipping';"><%=arr_menu_desc(61)%></a></li>
						<!-- News Clipping -->
						<!--<li><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?press';"><%=arr_menu_desc(62)%></a></li>-->
						<!-- Press Release -->
					</ul>
				</li>
				<li><a href="#"><%=arr_menu_desc(63)%></a>
					<!-- Information Request System -->
					<ul class="dl-submenu">
						<li><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?request_inquiry';"><%=arr_menu_desc(64)%></a></li>
						<!-- Inquiry From -->
						<li><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?complaints';"><%=arr_menu_desc(65)%></a></li>
						<!-- Complaint Channel -->
						<li><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?request_alerts';"><%=arr_menu_desc(66)%></a></li>
						<!-- E-mail Alert -->
						<!--<li><a href="FAQs.asp">คำถามที่ถูกถามบ่อย</a></li>  FAQs -->
					</ul>
				</li>
				<li><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?ir_contact';"><%=arr_menu_desc(67)%></a>
					<!-- IR Contact -->
				</li>
			</ul>
		</div>

		<div class="col-lg-2 col-md-2 offset-menu-th width-short-th IEmenu1">
			<div class="coverOffset-menu">
				<a href="javascript:void(0);" onclick="parent.location.href='popup.asp?ir_calendar';" class="displayFlex">
					<img class="img-short" src="images/hl_icn.png">
					<p class="text-short"><%=arr_menu_desc(68)%></p>
				</a>
			</div>
		</div>

		<div class="col-lg-2 col-md-2 width-short-th IEmenu2">
			<div class="coverOffset-menu">
				<a href="javascript:void(0);" onclick="parent.location.href='popup.asp?webcasts';" class="displayFlex">
					<img class="img-short" src="images/Multimedia.png">
					<p class="text-short"><%=arr_menu_desc(69)%></p>
				</a>
			</div>
		</div>

		<div class="col-lg-2 col-md-2 width-short-th IEmenu3">
			<div class="coverOffset-menu">
				<a href="javascript:void(0);" onclick="parent.location.href='popup.asp?ir_contact';" class="displayFlex">
					<img class="img-short" src="images/contact.png">
					<p class="text-short IEcon" style="margin: 22px 40px 0 0px;text-transform:uppercase;"><%=arr_menu_desc(67)%></p>
				</a>
			</div>
		</div>

		<!--=================== Language =====================-->
			<!--#include file='i_lang.asp'-->
		<!--====================== End =======================-->
	</div>
</section>