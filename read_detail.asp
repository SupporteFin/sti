﻿<!DOCTYPE html>

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->

<%
pagetitle = title
title=""
if request("topic")="calendar" then
         session("cur_page")="IR " & listed_share &" IR Calendar Detail" 
         title=  arr_menu_desc(68)
         page_name="IR Calendar Detail"
 elseif request("topic")="clipping" then
         session("cur_page")="IR " & listed_share &" News Clipping Detail"
         if session("lang")="E" then 
		 title= "News Clipping" 
	else 
		title= "ข่าวจากหนังสือพิมพ์" 
	end if
        page_name="News Clipping Detail"
elseif request("topic")="analyst" then
       session("cur_page")="IR " & listed_share & "  Analyst Report Detail"
         title= arr_menu_desc(52)
        page_name="Analyst Report Detail"
else
	session("cur_page")=""
end if 
%>

<html lang="<%if session("lang")="E" then%>en<%else%>th<%end if%>">
		
	  <head>
		<title><%=message_title%> :: <%=page_name%> </title>
		<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="description" content="<%=message_Description%>">
		<meta name="keywords" content="<%=message_keyword%>">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
		  
		<!-- CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="css/flexslider.css" rel="stylesheet" type="text/css">
		<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
		<link href="css/style.css" rel="stylesheet" type="text/css">
		<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
		<!-- <link href="css/colors/" rel="stylesheet" type="text/css" id="colors"> -->

		<!-- ADD CSS -->
		<link href="css/component.css" rel="stylesheet" type="text/css">
		<link href="css/custom.css" rel="stylesheet" type="text/css">
		<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">

		<!-- FONTS -->
		<link href="css/font-awesome.css" rel="stylesheet">

		<script src="scripts/jquery.min.js" type="text/javascript"></script>
		<script src="scripts/jquery.nicescroll.min.js"></script>

		<!-- SCRIPTS -->
		<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
		<!--[if IE]><html class="ie" lang="en"> <![endif]-->
		
		
		<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
		<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
		
		<!-- <script src="scripts/superfish.min.js" type="text/javascript"></script> -->
		<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
		<script src="scripts/animate.js" type="text/javascript"></script>
		<script src="scripts/myscript.js" type="text/javascript"></script>
		<script src="scripts/cc_502.js" type="text/javascript"></script>
		<script src="scripts/js15_as.js" type="text/javascript"></script>
		<script src="scripts/owl.carousel.js" type="text/javascript"></script>

		<!-- NEW -->
		<!-- <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" /> -->
		<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
		<link href="css/site.css" rel="stylesheet" type="text/css" />
		<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
		<!-- END NEW -->
	  </head>
	  
	<body class="main">
		<style type="text/css">
			.menu_block {
				position: fixed;
				z-index: 9999;
				left: 0;
				top: 0;
				right: 0;
				height: 100px;
				width: 100%;
				background-color: #fff;
				box-shadow: 0 2px 3px rgba(0,0,0,0.1);
			}
			.ReadDetail {
				margin-top: 24px;
				text-align: center;
			}
		</style>
		
		<!-- PRELOADER -->
		<img id="preloader" src="images/preloader.gif" alt="">
		<!-- //PRELOADER -->
		<div class="preloader_hide" style="padding-bottom: 70px;">
			<!-- PAGE -->
			<div id="page" class="single_page">
				<!-- HEADER -->
				<header style="font-family: 'promptextralight';">
					<!-- MENU BLOCK -->
					<div class="menu_block">
						<!-- CONTAINER -->
						<div class="container clearfix">
							<!-- LOGO -->
							<div class="ReadDetail">
								<a href="https://www.sti.co.th/th/index.php"><img src="images/logo005.png" width="180" alt=""></a>
							</div><!-- //LOGO -->
							<!-- MENU -->
						</div><!-- //MENU BLOCK -->
					</div><!-- //CONTAINER -->
				</header><!-- //HEADER -->	
			</div> 	<!-- END id="page" -->
		</div>
	
		<div class="container read-page ir-font">  	
			<div class="col-lg-12" style="padding-bottom: 50px;">
				<div class="row">
					<!-- ########################### content details ################################ -->
					<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
						<div class="row">
							<div class="col-lg-12">
								<p style="color:#324695;font-size: 34px;font-weight:normal;padding-bottom:12px;font-weight:bold;"><%=title%></p>
								<div style="background-color:#00ccff;width:120px;height:2px;"></div><br><br>
							</div>
						</div>
						
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" valign="top">
									<!-- .............................................................................. Content .............................................................................. -->	
									<table cellSpacing=0 cellPadding=0 width="100%" border=0 align="center">
										<%if request("topic")="calendar" then%>																		
												 <IFRAME id="detail" src="calendar/relate_info/<%=request("name")%>" frameBorder="0" width="100%" height="1000" scrolling="yes"></IFRAME>
										<%elseif request("topic")="clipping" then
												  strsrc=""
												 if customer_id= 0 or customer_id=98  then
													strsrc="clipping/" & trim(replace(request("name"),"@","&"))
												else
													strsrc="http://ir.efinancethai.com/listed/" &  listed_share & "/clipping/" & trim(replace(request("name"),"@","&"))
												end if
												'response.write strsrc
										%>
										<IFRAME id="detail" src="<%=strsrc%>" frameBorder="0" width="100%" height="1200" scrolling="yes"></IFRAME>
										<%elseif request("topic")="analyst" then
												  strsrc=""
													if instr(1,request("name"),"/") > 0 then
													strsrc=pathnews & "research/" &  trim(replace(request("name"),"@","&"))
												else
													strsrc="research/" &  trim(replace(request("name"),"@","&"))
												end if											 
											   
												'response.write strsrc
										%>
										 <IFRAME id="detail" src="<%=strsrc%>" frameBorder="0" width="100%" height="1000" scrolling="yes"></IFRAME>
										<%elseif request("topic")="public" then%>
										<IFRAME id="detail" src="public_relation/<%=request("name")%>" frameBorder="0" width="100%" height="1000" scrolling="yes"></IFRAME>
										<%else%>
										<IFRAME id="detail" src="" frameBorder="0" width="100%" height="1000" scrolling="yes"></IFRAME>		
										<%end if%>
									 </table> 

									<input type="text" value="<%=strsrc%>" style="display:none;">  
									<input type="text" value="<%=pathnews%>" style="display:none;">  
									 <!-- .............................................................................. End Content .............................................................................. -->														  
								</td>
							</tr>
							<tr>
							  <td align="left">&nbsp;</td>
							</tr>
							<tr>
							  <td></td>
							</tr>
							  </table>
						
					</div>
					<!-- ########################### content details ################################ -->
					
				</div>
			</div>
		</div> 	  
	  
		<!--#include file="i_googleAnalytics.asp"-->
	</body>
</html>