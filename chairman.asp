﻿<!DOCTYPE html>

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->
<%
session("cur_page")="IR " &  listed_share & " Chairman’s Statement"
session("page_asp")="chairman.asp"
page_name="Chairman’s Statement"
%>

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<TITLE><%=message_title%> :: <%=page_name%></TITLE>
	<META name="Title" content="<%=message_metatitle%>">
	<META name="Description" content="<%=message_description%>">
	<META name="Keywords" content="<%=message_keyword%>">	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="author" content="">
	<link rel="shortcut icon" href="<%=img_icon%>">
	<link href="style_listed.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../function2007_utf8.js"></script>
	<!-- CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/flexslider.css" rel="stylesheet" type="text/css">
	<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
	<!-- ADD CSS -->
	<link href="css/component.css" rel="stylesheet" type="text/css">
	<link href="css/custom.css" rel="stylesheet" type="text/css">
	<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">
	<!-- FONTS -->
	<link href="css/font_add.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- Mouse Hover CSS -->
	<script src="scripts/jquery.min.js" type="text/javascript"></script>
	<script src="scripts/jquery.nicescroll.min.js"></script>
	<!-- SCRIPTS -->
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<!--[if IE]><html class="ie" lang="en"> <![endif]-->
	<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
	<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
	<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
	<script src="scripts/animate.js" type="text/javascript"></script>
	<script src="scripts/myscript.js" type="text/javascript"></script>
	<script src="scripts/owl.carousel.js" type="text/javascript"></script>
	<!-- NEW -->
	<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
	<link href="css/site.css" rel="stylesheet" type="text/css" />
	<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
	<!-- scroll-hint -->
	<link rel="stylesheet" href="css/scroll-hint.css">
</head>

<body class="main">

	<!-- PRELOADER -->
	<img id="preloader" src="images/preloader.gif" alt="">
	<!-- //PRELOADER -->
	<div class="preloader_hide">
		<div id="page" class="single_page">

			<!--================================ Menu Top ==================================-->
				<!--#include file='i_menu_top.asp'-->
			<!--=================================== End ====================================-->

			<!--=============================== Menu Left ==================================-->
				<!--#include file='i_menu_left.asp'-->
			<!--================================== End =====================================-->

			<!--============================ Title Menu =============================-->
			<section class="mbr-section article">
				<div class="container">
					<div class="bigTopic">
						<h1 class="bigTopicText"><%=arr_menu_desc(23)%></h1>
					</div>
				</div>
			</section>
			<!--============================== End ==================================-->

			<section class="mbr-section article sectionSub">
			<!--===================================================== Content ====================================================-->
			<%if session("lang")="E" then%>
				<div class="container">
					  <div class="col-lg-12">       
						<p style="font-size: 22px;font-weight: 500;margin-right: 15px;margin-top: 3px;color: #006c9d;text-align: left;">Stonehenge Inter Public Company Limited</p>
						<img src="images/HistoryBG-1.jpg" style="    margin-bottom: 15px;">
						<p class="bigTopicSubText" style="margin-bottom: 35px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						In 2018 it was the year that the Thai economy continued to recover.  The Office of National Economic and Social Development Council anticipated the Thai economy in 2018 to 
						expand by 4.2-4.7 per cent; as a result from many factors concerning the export and manufacturing sector expansion in many industries, continued expansion trend of 
						private investment, more support granted by government projects. Government spending and investment that is likely to accelerate as the disbursement, the progress of 
						government investment projects, as well as a clear recovery of private investment including the progress of investment in important infrastructure resulted in the overall real 
						estate market conditions to improve from purchasing power and housing demand that began to recover according to the economic conditions especially projects along the BTS line.  
						However, real estate operators have still faced challenges from intense competition both from the opening of new projects of the former major operators and new entrepreneurs as 
						well as strict credit consideration and an upward interest rate trend that may reduce the ability of consumers and real estate entrepreneurs, which may affect investment in the 
						construction of various projects. The company has prepared a continuous support plan for 2-3 years. Nevertheless, the company will introduce new construction management 
						techniques including construction methods that will reduce cost, time and manpower and present them to the companies, both existing and new traders, owning the projects to be
						in line with economic conditions and competition in real estate and large construction businesses. </p>
					  </div>
					  <!-- <div class="col-lg-5"><img src="images/ChairmanStatement_inof.jpg"></div> -->
					  <div class="col-lg-12" style="float: left;">
						<p class="bigTopicSubText" style="margin-bottom: 35px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						So in the year 2018, the company implemented strategies to support the needs of real estate operators by focusing on the development of the knowledge and skills of the professional
						engineering team and architects, developing the potential and expertise of the teams and bringing in the modern technology to help the work of the real estate entrepreneurs regularly.  
						The company also adjusted the strategy of providing consulting services for construction management and control including architectural and engineering design services, interior 
						decoration and the preservation of ancient sites in order to be able to satisfy various types of demands such as mixed-use development, office buildings, shopping centers, 
						educational institutions, multi-purpose buildings, nursing homes, as well as to serve more government sectors.</p>
						
						<p class="bigTopicSubText" style="margin-bottom: 35px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						In 2018, it was also a year of readiness to bring the company into a listed company in the stock market. With the company’s objectives, it is to bring the business of Construction 
						Management services and Architectural Design, Engineering Design, Interior Design, historical conservation and to step up to become a leading international company that 
						creates stability and sustainability. The company first opened the trading on the Stock Exchange of Thailand on 19 December 2018.</p>
						
						<p class="bigTopicSubText" style="margin-bottom: 35px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						Looking forward to the year 2019 it will be a challenging year for the real estate business both in terms of rising interest rate direction, economic and political overview. 
						The company, therefore, focuses on increasing the potential for providing consulting services for construction management and control in order to respond to a variety of demands.
						Yet, the company must strictly comply with the professional ethics and focus on quality work policy in accordance with the international standards by taking into account the purpose 
						of the work as the mainstay which consists of quality, time and budget.</p>
						<p class="bigTopicSubText" style="margin-bottom: 35px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						On behalf of the Chairman of the Board of Directors of Stonehenge Inter Public Company Limited, I would like to thank the customers, business partners, and shareholders who 
						have always trusted and supported the company's operations.  The company by the board of directors, executive committee and all employees are ready to continue to develop
						the business with a dedication to conducting business carefully adhering to good governance principles for long-term returns to all concerned parties in a sustainable manner. 
						In addition, the company has determined that social responsibility is an important goal in business operations. Safety precautions and the impact of environmental problems 
						caused by building and large project construction on people and communities are the vital concern of each project. The company is also determined to support business activities 
						and public interest activities that will benefit society, the environment and all stakeholders.</p>
						
					  <div>
						<p class="signatureName">Mr. Jumpol Sumpaopol</p>
						<p class="signaturePosition">Chairman of the Board</p>
					  </div>
					  </div>

			 


					</div>
			<%else%>
				<div class="container">
					  <div class="col-lg-12">       
						<p style="font-size: 22px;font-weight: 500;margin-right: 15px;margin-top: 3px;color: #006c9d;text-align: left;">บริษัท สโตนเฮ้นจ์  อินเตอร์ จำกัด (มหาชน)</p>
						<img src="images/HistoryBG-1.jpg" style="margin-bottom: 15px;">
						<p class="bigTopicSubText" style="margin-bottom: 35px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ในปี 2561 เป็นปีที่เศรษฐกิจไทยฟื้นตัวอย่างต่อเนื่อง โดยสำนักงานสภาพัฒนาการเศรษฐกิจและสังคมแห่งชาติ คาดการณ์เศรษฐกิจไทยในปี 2561 จะขยายตัวร้อยละ 4.2-4.7 จากปัจจัยในหลายๆด้าน ทั้งการขยายตัวภาคส่งออกและการผลิตในหลายอุตสาหกรรม การลงทุนภาคเอกชนมีแนวโน้มขยายตัวต่อเนื่อง และได้รับแรงสนับสนุนเพิ่มเติมจากโครงการภาครัฐที่ชัดเจนมากขึ้น  การใช้จ่ายและการลงทุนของภาครัฐที่มีแนวโน้มเร่งขึ้นตามการเบิกจ่ายและความคืบหน้าของโครงการลงทุนภาครัฐ ตลอดจนการฟื้นตัวที่ชัดเจนของการลงทุนภาคเอกชน และความคืบหน้าของการลงทุนในโครงสร้างพื้นฐานที่สำคัญ ส่งผลให้สภาวะตลาดอสังหาริมทรัพย์โดยรวมมีแนวโน้มปรับตัวดีขึ้นจากกำลังซื้อและความต้องการที่อยู่อาศัยที่เริ่มฟื้นตัวตามภาวะเศรษฐกิจ โดยเฉพาะโครงการตามแนวรถไฟฟ้า </p>
					  </div>
					  <!-- <div class="col-lg-5"><img src="images/ChairmanStatement_inof.jpg"></div> -->
					  <div class="col-lg-12" style="float: left;">
						<p class="bigTopicSubText" style="margin-bottom: 35px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;อย่างไรก็ดี ผู้ประกอบการอสังหาริมทรัพย์ยังเผชิญความท้าทายจากการแข่งขันที่รุนแรงทั้งจากการเปิดโครงการใหม่ของผู้ประกอบการรายใหญ่เดิม และผู้ประกอบการรายใหม่ ตลอดจนความเข้มงวดในการพิจารณาสินเชื่อและแนวโน้มอัตราดอกเบี้ยขาขึ้นที่อาจลดทอนความสามารถของผู้บริโภค และผู้ประกอบการด้านอสังหาริมทรัพย์   ที่อาจส่งผลกระทบต่อการลงทุนก่อสร้างโครงการต่างๆ ซึ่งบริษัทฯ มีการเตรียมการและแผนรองรับต่อเนื่องมา 2 - 3 ปี  อย่างไรก็ตามบริษัทฯจะได้นำเทคนิคการบริหารจัดการก่อสร้างใหม่ฯ รวมทั้งวิธีการก่อสร้างที่จะทำให้ลดค่าใช้จ่าย เวลา และกำลังคนมานำเสนอให้กับกลุ่มบริษัทที่เป็นเจ้าของโครงการต่างฯทั้งกลุ่มผู้ค้ารายเดิมและผู้ค้ารายใหม่ เพื่อให้สอดคล้องกับภาวะเศรษฐกิจและการแข่งขันในธุระกิจอสังหาริมทรัพย์และการก่อสร้างขนาดใหญ่</p>
						<p class="bigTopicSubText" style="margin-bottom: 35px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ดังนั้นในรอบปี  2561  บริษัทฯได้ดำเนินกลยุทธ์เพื่อรองรับความต้องการของผู้ประกอบการอสังหาริมทรัพย์ ด้วยการมุ่งเน้นพัฒนาการความรู้ความสามารถของทีมวิศวกร และสถาปนิกอย่างมืออาชีพ พัฒนาศักยาภาพของทีมงานให้ความเชี่ยวชาญ และนำเทคโนโลยีสมัยใหม่เข้ามาช่วยในการทำงานให้ผักบผู้ประกอบการอสังหาริมทรัพย์อย่างสม่ำเสมอ ปรับกลยุทธ์ในการให้บริการที่ปรึกษาบริหารและควบคุมงานก่อสร้าง รวมทั้งงานให้บริการออกแบบด้านสถาปัตยกรรมและวิศวกรรม งานตกแต่งภายใน และงานอนุรักษ์โบราณสถาน  ให้สามารถรับงานบริการให้หลากหลายประเภทมากขึ้น เช่น งานโครงการ Mixed-use Development อาคารสำนักงาน ศูนย์การค้า สถานศึกษาและอาคารเอนกประสงค์ และสถานพยาบาล รวมถึงการรับงานบริการหน่วยงานภาครัฐมากขึ้น  เป็นต้น</p>
						<p class="bigTopicSubText" style="margin-bottom: 35px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ในปี 2561 ยังเป็นปีแห่งความพร้อมในการนำบริษัทฯเข้าเป็นบริษัทจดทะเบียนในตลาดหลักทรัพย์ ด้วยบริษัทฯมีวัตถุประสงค์ที่จะนำพาธุรกิจการให้บริการที่ปรึกษาบริหารและควบคุมงานก่อสร้าง และให้บริการออกแบบงานด้านสถาปัตยกรรม และวิศวกรรม งานตกแต่งภายใน ก้าวขึ้นสู่ความเป็นสากลชั้นนำ และสร้างความมั่นคงและยั่งยืน บริษัทฯได้เข้าทำการเปิดการซื้อขายหลักทรัพย์ในตลาดหลักทรัพย์แห่งประเทศไทยเป็นครั้งแรกเมื่อวันที่ 19 ธันวาคม 2561</p>
						<p class="bigTopicSubText" style="margin-bottom: 35px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;เมื่อมองไปข้างหน้าสำหรับปี 2562 จะเป็นปีที่มีความท้าทายของธุรกิจอสังหาริมทรัพย์ ทั้งในปัจจัยของทิศทางอัตราดอกเบี้ยที่ปรับตัวเพิ่มขึ้น ภาพรวมเศรษฐกิจและการเมือง บริษัทฯ จึงเน้นการเพิ่มศักยภาพในการรับให้บริการงานที่ปรึกษาบริหารและควบคุมงานก่อสร้าง ให้มีความหลากหลายด้านมากขึ้น ดำเนินงานตามหลักจรรยาบรรณวิชาชีพอย่างเคร่งครัด เน้นนโยบายการทำงานอย่างมีคุณภาพ มาตรฐานระดับสากล คำนึงวัตถุประสงค์ของงานเป็นหลักหัวใจสำคัญ คือ คุณภาพ เวลา และงบประมาณ </p>
						<p class="bigTopicSubText" style="margin-bottom: 35px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ในนามของประธานกรรมการบริษัท สโตนเฮ้นจ์ อินเตอร์ จำกัด (มหาชน) ผมขอขอบคุณลูกค้า พันธมิตรทางธุรกิจ ผู้ถือหุ้นทุกท่าน ที่ได้ให้ความไว้วางใจและสนับสนุนการดำเนินงานของบริษัทฯด้วยดีมาตลอด และบริษัทฯโดยคณะกรรมการบริษัท  คณะกรรมการบริหารและพนักงานทุกคน มีความพร้อมในการสานต่อพัฒนาธุรกิจด้วยความมุ่งมั่น ทุ่มเท ในการดำเนินธุรกิจอย่างรอบคอบ ยึดมั่นในหลักธรรมมาภิบาลที่ดี เพื่อผลตอบแทนในระยะยาวแก่ผู้ที่เกี่ยวข้องทุกฝ่ายอย่างยั่งยืนต่อไป และบริษัทฯ กำหนดให้ความรับผิดชอบต่อสังคมเป็นเป้าหมายสำคัญในการดำเนินธุรกิจ การให้ความระมัดระวังด้านความปลอดภัย และผลกระทบเรื่องปัญหาสิ่งแวดล้อมต่อประชาชน ชุมชน และประชาคม ของการก่อสร้างอาคารและสิ่งก่อสร้างขนาดใหญ่ เป็นเรื่องสำคัญในการให้บริการในแต่ละโครงการ รวมถึงการสนับสนุนกิจกรรมทางธุรกิจและกิจกรรมสาธารณประโยชน์ที่จะเอื้อประโยชน์ต่อสังคม สิ่งแวดล้อม และกลุ่มผู้มีส่วนได้เสียทุกฝ่าย</p>
					  <div>
						<p class="signatureName">นายจุมพล สำเภาพล</p>
						<p class="signaturePosition">ประธานกรรมการ</p>
					  </div>
					  </div>

			 


					</div>
			<%end if%>
			<!--======================================================= End ======================================================-->
			</section>
			
		</div>

		<!--====================================== Footer ======================================-->
			<!--#include file='i_footer.asp'-->
		<!--======================================== End =======================================-->

		<script type="text/javascript" src="scripts/modernizr.custom.js"></script>
		<%if session("lang")="E" then%><script type="text/javascript" src="scripts/jquery.dlmenu-en.js"></script><%else%><script type="text/javascript" src="scripts/jquery.dlmenu.js"></script><%end if%>
		<script>
			$(function() {
				$('#dl-menu').dlmenu();
			});
		</script>
		<!-- js/scroll-hint -->
		<script src="scripts/scroll-hint.js"></script>
		<!-- js/scroll-hint -->
		<script>
			new ScrollHint('.js-scrollable', {
			});
		</script>
	</div>
	 <!--#include file="i_googleAnalytics.asp"-->	  
</body>
</html>