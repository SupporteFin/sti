﻿<!DOCTYPE html>

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->
<!--#include file = "../../function_asp2007.asp" -->
<!--#include file = "../../i_conir.asp" -->

<%
session("cur_page")="IR " &  listed_share & " IR Calendar"
session("page_asp")="ir_calendar.asp"
page_name="IR Calendar"

pagesize=15
%>
<!--#include file="../../constpage2007.asp"-->

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<TITLE><%=message_title%> :: <%=page_name%></TITLE>
	<META name="Title" content="<%=message_metatitle%>">
	<META name="Description" content="<%=message_description%>">
	<META name="Keywords" content="<%=message_keyword%>">	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="author" content="">
	<link rel="shortcut icon" href="<%=img_icon%>">
	<link href="style_listed.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../function2007_utf8.js"></script>
	<!-- CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/flexslider.css" rel="stylesheet" type="text/css">
	<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
	<!-- ADD CSS -->
	<link href="css/component.css" rel="stylesheet" type="text/css">
	<link href="css/custom.css" rel="stylesheet" type="text/css">
	<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">
	<!-- FONTS -->
	<link href="css/font_add.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- Mouse Hover CSS -->
	<script src="scripts/jquery.min.js" type="text/javascript"></script>
	<script src="scripts/jquery.nicescroll.min.js"></script>
	<!-- SCRIPTS -->
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<!--[if IE]><html class="ie" lang="en"> <![endif]-->
	<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
	<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
	<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
	<script src="scripts/animate.js" type="text/javascript"></script>
	<script src="scripts/myscript.js" type="text/javascript"></script>
	<script src="scripts/owl.carousel.js" type="text/javascript"></script>
	<!-- NEW -->
	<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
	<link href="css/site.css" rel="stylesheet" type="text/css" />
	<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
	<!-- scroll-hint -->
	<link rel="stylesheet" href="css/scroll-hint.css">
</head>

<body class="main">

	<!-- PRELOADER -->
	<img id="preloader" src="images/preloader.gif" alt="">
	<!-- //PRELOADER -->
	<div class="preloader_hide">
		<div id="page" class="single_page">

			<!--================================ Menu Top ==================================-->
				<!--#include file='i_menu_top.asp'-->
			<!--=================================== End ====================================-->

			<!--=============================== Menu Left ==================================-->
				<!--#include file='i_menu_left.asp'-->
			<!--================================== End =====================================-->

			<!--============================ Title Menu =============================-->
			<section class="mbr-section article">
				<div class="container">
					<div class="bigTopic">
						<h1 class="bigTopicText"><%=arr_menu_desc(68)%></h1>
					</div>
				</div>
			</section>
			<!--============================== End ==================================-->

			<section class="mbr-section article sectionSub">
				<div class="container">
					<!--===================================================== Content ====================================================-->
					<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tb_maincontent">
						<tr>
							<td align="center">
								<form name="frm1" METHOD="POST" ACTION="<%=session("page_asp")%>">
									  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
										 <tr>
											<td align="left" valign="top">
												<div class="table-data js-scrollable scroll-hint" style="position: relative; overflow: auto;">
													<table border="0" cellspacing="0" cellpadding="0" class="table-width table-MajorShareholder">
														<tbody style="font-size: 15.5px;">
															<tr class="Head-table-MajorShareholder">
																<th align="center" class="topicTeble" style="width: 15%;"><%if session("lang")="E" then%>Date &amp; Time<%else%>วันและเวลา<%end if%></th>
																<th align="center" class="topicTeble" style="width: 25%;"><%if session("lang")="E" then%>Event<%else%>กิจกรรม<%end if%></th>
																<th align="center" class="topicTeble" style="width: 36%;"><%if session("lang")="E" then%>Location<%else%>สถานที่<%end if%></th>
																<th align="center" class="topicTeble" style="width: 12%;"><%if session("lang")="E" then%>Relate Info<%else%>เอกสารประกอบ<%end if%></th>
																<th align="center" class="topicTeble" style="width: 12%;"><%if session("lang")="E" then%>IR Multimedia<%else%>มัลติมีเดีย<%end if%></th>
															</tr>
															<!--#include file="../../select_page2007.asp"-->
															   <%			   
															  dim totalrec
															  totalrec=0
															  
															  strsql=""
															  strsql= sql_ir_carlendar(listed_share,session("lang"),0)																						
														   
															  set rs = conir.execute(strsql)
															  if not rs.eof and not rs.bof then
																 totalrec=rs("num_count")																						
															  end if
															  rs.close
														   
															  session("num_count")=totalrec
														   
															  %>			
															  
															   <!--#include file="../../calculate_page2007.asp"-->	
															  <%		
																 start_date=request("date")
																 theFrist=request("chkFrist")
																 if start_date <> "" then
																	  if theFrist = "" then
																		  index=ir_calendar_index(listed_share,session("lang"),start_date)
																		  if index <> 0 then
																			 session("pageno") = cint(index/pagesize)
																			 if session("pageno") = 0 then
																				session("pageno") = 1
																			 end if
																		  end if
																		  theFrist = "Y"
																	  end if
																 end if                             
															  
															  strsql=""
															  strsql= sql_ir_carlendar(listed_share,session("lang"),((session("pageno")-1)*session("pagesize")) & "," & session("pagesize") )		
															  'response.write strsql
															  set rs=conir.execute(strsql)
															  if not rs.eof and not rs.bof then
																 i=0
																   do while not rs.eof 	
																	%>
																	<tr class="even">
																		<td align="center">
																		  <%=WriteDateEvent(rs("start_date"),rs("stop_date"))%><br>
																		  <%
																				 response.write rs("detail_time")
																		  %>
																		  </td>
																		  <td align="left"><%=rs("detail")%></td>
																		 <%
																			location = cstr(rs("location_name"))	
																			if location<> "-" and not isnull(location)  then%>
																								
																				<td align="left"><%=rs("location_name")%></td>	
																				
																			<% elseif location= "-" or isnull(location)  then%>
																			
																				<td align="center">-</td>	
																				  
																			<%end if%>
																		 
																		  <td align="center">		
																		  
																		  <%
																			  strsql=""																				
																			 strsql=sql_relate_info(rs("id") ,session("lang"))
																			 set rs1=conir.execute(strsql)
																			 
																			 if not rs1.eof and not rs1.bof then
																			 n=0
																		  %>
																		 <table width="100%"  border="0" cellpadding="0" cellspacing="0" >
																		   <%
																		   do while not rs1.eof 																					      
																				 if n >0 then
																		   %>
																		  <tr align="left" valign="middle"> 
																			 <td valign="middle"><hr size="1"  class="color_hr_calendar" width="100%"></td>
																		  </tr>
																		  <%
																				end if
																		  %>
																		  <tr align="left" valign="top" style="background-color: transparent !important;"> 
																			 <td align="center">	 		
																		  <%if instr(rs1("file_name"),".pdf")>0 then%>
																			<img src="images/bullet_arrow_right.png" align="absmiddle"> <a class="web" href="read_detail.asp?topic=calendar&name=<%=rs1("file_name")%>"  target="_blank"><%=rs1("relate_info")%></a> 
																		  <%else%>
																			 <img src="images/bullet_arrow_right.png" align="absmiddle"> <a class="web" href="calendar/relate_info/<%=rs1("file_name")%>"  target="_blank"><%= rs1("relate_info")%></a> 
																		  <%end if%>
																		  </td>
																		  </tr>	
																		  
																		  <%
																			 n=n+1
																			 rs1.movenext
																		  loop
																		  %>
																		  </table>
																		  <%
																		  else
																			  response.write"<center>-</center>"
																		  end if
																		  rs1.close
																		  
																		  session("countrow")=n
																						 
																			 %>		
																		  </td>
																		  <!-- ......................................................................  START ADD COLUMN IR MULTIMEDIA BY BOWL ...................................................................... -->
																		  <td valign="middle"  align="center">
																		  <%
																			 date_now=date()
																			 time_now=time()
																			 date_event = rs("live_date")
																				
																				 if rs("live_audio")="Y" then
																				   if cdate(date_now) = cdate(date_event) then
																					   if  time_now  >=TimeValue(rs("start_time"))  and   time_now <= TimeValue(rs("stop_time")) then
																						 %><img src="../../images2007/audio.gif"  onClick="window.open('../../IRPlus/LiveAudio.asp','video','width=312,height=165,top=0,left=0');" style="cursor:hand"><%                                 
																					  end if              
																				   end if
																				end if							
																		  %>
																		  </td>
																		  <!-- ......................................................................  END ADD COLUMN IR MULTIMEDIA BY BOWL ...................................................................... -->
																	</tr>
																	<%
																		i=i+1
																		rs.movenext
																		loop	
																	   else%>
																	   <tr>
																		  <td align="center" valign="middle" colspan="4">
																			 <font color="#ff0000" class="no_information">
																			 <%
																			if  session("lang")="E"  then 
																				response.write "No Information Now !! "
																			else																				
																				response.write "ไม่มีข้อมูล ณ ขณะนี้ "
																			end if
																			 %></font>
																		  </td>
																	</tr>
																<%end if%>
																<%
																   rs.close
																   %>	
														</tbody>
													</table>
											</td>
										</tr>	
										<!--+++++++++++++++++++++++++ Page Button+++++++++++++++++++++++--->
										<tr>
											<td align="right" valign="top">
												  <!--#include file="page_button.asp"-->
											</td>
										 </tr>				
									</table>	
								</form>	  
							 </td>
						 </tr>				
					</table>
					<!--======================================================= End ======================================================-->
				</div>
			</section>
			
		</div>

		<!--====================================== Footer ======================================-->
			<!--#include file='i_footer.asp'-->
		<!--======================================== End =======================================-->

		<script type="text/javascript" src="scripts/modernizr.custom.js"></script>
		<%if session("lang")="E" then%><script type="text/javascript" src="scripts/jquery.dlmenu-en.js"></script><%else%><script type="text/javascript" src="scripts/jquery.dlmenu.js"></script><%end if%>
		<script>
			$(function() {
				$('#dl-menu').dlmenu();
			});
		</script>
		<!-- js/scroll-hint -->
		<script src="scripts/scroll-hint.js"></script>
		<!-- js/scroll-hint -->
		<script>
			new ScrollHint('.js-scrollable', {
			});
		</script>
	</div>
	 <!--#include file="i_googleAnalytics.asp"-->	  
</body>
</html>