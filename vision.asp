﻿<!DOCTYPE html>

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->
<%
session("cur_page")="IR " &  listed_share & " Vision & Mission"
session("page_asp")="vision.asp"
page_name="Vision & Mission"
%>

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<TITLE><%=message_title%> :: <%=page_name%></TITLE>
	<META name="Title" content="<%=message_metatitle%>">
	<META name="Description" content="<%=message_description%>">
	<META name="Keywords" content="<%=message_keyword%>">	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="author" content="">
	<link rel="shortcut icon" href="<%=img_icon%>">
	<link href="style_listed.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../function2007_utf8.js"></script>
	<!-- CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/flexslider.css" rel="stylesheet" type="text/css">
	<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
	<!-- ADD CSS -->
	<link href="css/component.css" rel="stylesheet" type="text/css">
	<link href="css/custom.css" rel="stylesheet" type="text/css">
	<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">
	<!-- FONTS -->
	<link href="css/font_add.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- Mouse Hover CSS -->
	<script src="scripts/jquery.min.js" type="text/javascript"></script>
	<script src="scripts/jquery.nicescroll.min.js"></script>
	<!-- SCRIPTS -->
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<!--[if IE]><html class="ie" lang="en"> <![endif]-->
	<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
	<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
	<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
	<script src="scripts/animate.js" type="text/javascript"></script>
	<script src="scripts/myscript.js" type="text/javascript"></script>
	<script src="scripts/owl.carousel.js" type="text/javascript"></script>
	<!-- NEW -->
	<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
	<link href="css/site.css" rel="stylesheet" type="text/css" />
	<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
	<!-- scroll-hint -->
	<link rel="stylesheet" href="css/scroll-hint.css">
</head>

<body class="main">

	<!-- PRELOADER -->
	<img id="preloader" src="images/preloader.gif" alt="">
	<!-- //PRELOADER -->
	<div class="preloader_hide">
		<div id="page" class="single_page">

			<!--================================ Menu Top ==================================-->
				<!--#include file='i_menu_top.asp'-->
			<!--=================================== End ====================================-->

			<!--=============================== Menu Left ==================================-->
				<!--#include file='i_menu_left.asp'-->
			<!--================================== End =====================================-->

			<!--============================ Title Menu =============================-->
			<section class="mbr-section article">
				<div class="container">
					<div class="bigTopic">
						<h1 class="bigTopicText"><%=arr_menu_desc(22)%></h1>
					</div>
				</div>
			</section>
			<!--============================== End ==================================-->

			<section class="mbr-section article sectionSub">
			<!--===================================================== Content ====================================================-->
			<%if session("lang")="E" then%>
				<div class="container" style="padding-right: 15px;padding-left: 15px;">
				  <!-- <div class="coverVisionText">
					<p class="VisionText">บริษัท สโตนเฮ้นจ์ อินเตอร์ จำกัด (มหาชน) (“บริษัทฯ” หรือ “STI”) จัดตั้งขึ้นในปี 2547 โดยผู้ถือหุ้นและผู้บริหารซึ่งเป็นวิศวกรผู้มีประสบการณ์ความเชี่ยวชาญในแวดวงวิศวกรรม อันได้แก่ </p>
					<ul class="cover-ul">
					  <li>(1) นายสมเกียรติ  ศิลวัฒนาวงศ์</li>
					  <li>(2) นายไพรัช  เล้าประเสริฐ</li>
					  <li>(3) นายสมจิตร์ เปี่ยมเปรมสุข</li>
					  <li>(4) นายกิตติศักดิ์  สุภาควัฒน์</li>
					</ul>
					<p class="VisionText" style="padding-left: 10px;">และผู้ถือหุ้นและผู้บริหารซึ่งเป็นนักธุรกิจที่มีประสบการณ์และความเชี่ยวชาญในการบริหาร ได้แก่ </p>
					<ul class="cover-ul">
					  <li>(5) นายอิสรินทร์ สุวัฒโน</li>
					</ul>
					<p class="VisionText">โดยมีวัตถุประสงค์เพื่อดำเนินธุรกิจที่ปรึกษาบริหารและควบคุมงานก่อสร้าง  ตั้งแต่การศึกษาสำรวจ  ให้คำปรึกษา ข้อเสนอแนะ บริหาร และควบคุมงานก่อสร้าง รวมไปถึงการตรวจสอบรับรองงาน เพื่อให้โครงการแล้วเสร็จและสามารถเปิดดำเนินการหรือเข้าใช้ประโยชน์ได้ตามแผนงานของลูกค้า นอกจากนี้ การให้บริการของกลุ่มบริษัทฯ ยังครอบคลุมไปถึงการให้บริการออกแบบงานด้านสถาปัตยกรรมและวิศวกรรม งานตกแต่งภายใน และงานอนุรักษ์โบราณสถาน</p>
					<p class="VisionText">ซึ่งดำเนินการโดยบริษัท สโตนเฮ้นจ์ จำกัด (“บริษัทย่อย” “STH”) ที่เป็นบริษัทย่อยของบริษัทฯ ซึ่งบริษัทฯ มีความมุ่งมั่นที่จะให้บริการแก่ลูกค้าอย่างมีประสิทธิภาพ ทั้งในด้านคุณภาพงานที่ได้มาตรฐานวิชาชีพระดับสากล  ภายใต้การควบคุมกรอบระยะเวลาและงบประมาณของโครงการก่อสร้างให้เป็นไปตามแผนงานของลูกค้า เพื่อสร้างความพึงพอใจสูงสุดให้แก่ลูกค้าที่รับบริการของบริษัทฯ  ทั้งนี้ บริษัทฯ และบริษัทย่อยได้กำหนดวิสัยทัศน์ ภารกิจ และเป้าหมายในการดำเนินธุรกิจดังนี้</p>
				  </div> -->
				  <div class="coverVisionText2">
					<p class="head" style="float: left;font-size: 20px;font-weight: 500;margin-right: 15px;margin-top: 6px;color: #006c9d;">Vision :</p>
					<p class="VisionText" style="font-weight: 400;">The Group determines the vision of becoming the best Construction Management services and Architectural Design, Engineering Design, Interior Design company in Thailand.</p>
					<div class="VisionCoverIconBEST">
					  <!-- <div class="col-lg-6 col-md-6 col-sm-6">
						<img src="images/Icon/B.png" alt="">
					  </div>
					  <div class="col-lg-6 col-md-6 col-sm-6">
						<img src="images/Icon/E.png" alt="">
					  </div>
					  <div class="col-lg-6 col-md-6 col-sm-6">
						<img src="images/Icon/S.png" alt="">
					  </div>
					  <div class="col-lg-6 col-md-6 col-sm-6">
						<img src="images/Icon/T.png" alt="">
					  </div> -->
					  <img src="images/Icon/iconBEST.png">
					</div>
				  </div>
				  <div class="coverVisionText3">
					<p style="float: left;font-size: 20px;font-weight: 500;margin-right: 15px;margin-top: 5px;color: #006c9d;">Mission :</p>
					<p class="VisionText" style="font-weight: 500;">The organization's mission is as follows</p>
					  <div class="col-lg-4 col-md-4" style="text-align: center;">
						<img src="images/Icon/m1.png">
						<p class="VisionText" style="padding: 0px 30px;">To provide services with international professional standards by understanding and recognizing the needs of customers which also giving the priority of project’s budget control in order to maintain the maximum benefit of customers.</p>
					  </div>
					  <div class="col-lg-4 col-md-4" style="text-align: center;">
						<img src="images/Icon/m2.png">
						<p class="VisionText" style="padding: 0px 30px;">To manage the project by experienced and field expertise personnel in order to ensure the success of the project according to the customer's plan.</p>
					  </div>
					  <div class="col-lg-4 col-md-4" style="text-align: center;">
						<img src="images/Icon/m3.png">
						<p class="VisionText" style="padding: 0px 30px;">To ensure a good corporate governance in terms of fairness, transparency, accountability and competitiveness to guarantee the quality of service in all projects.</p>
					  </div>
				  </div>
				  <!-- <div style="text-align: center;"><img src="images/BEST.png" style="margin-top: 30px;"></div> -->
				</div>
			<%else%>
				<div class="container" style="padding-right: 15px;padding-left: 15px;">
				  <!-- <div class="coverVisionText">
					<p class="VisionText">บริษัท สโตนเฮ้นจ์ อินเตอร์ จำกัด (มหาชน) (“บริษัทฯ” หรือ “STI”) จัดตั้งขึ้นในปี 2547 โดยผู้ถือหุ้นและผู้บริหารซึ่งเป็นวิศวกรผู้มีประสบการณ์ความเชี่ยวชาญในแวดวงวิศวกรรม อันได้แก่ </p>
					<ul class="cover-ul">
					  <li>(1) นายสมเกียรติ  ศิลวัฒนาวงศ์</li>
					  <li>(2) นายไพรัช  เล้าประเสริฐ</li>
					  <li>(3) นายสมจิตร์ เปี่ยมเปรมสุข</li>
					  <li>(4) นายกิตติศักดิ์  สุภาควัฒน์</li>
					</ul>
					<p class="VisionText" style="padding-left: 10px;">และผู้ถือหุ้นและผู้บริหารซึ่งเป็นนักธุรกิจที่มีประสบการณ์และความเชี่ยวชาญในการบริหาร ได้แก่ </p>
					<ul class="cover-ul">
					  <li>(5) นายอิสรินทร์ สุวัฒโน</li>
					</ul>
					<p class="VisionText">โดยมีวัตถุประสงค์เพื่อดำเนินธุรกิจที่ปรึกษาบริหารและควบคุมงานก่อสร้าง  ตั้งแต่การศึกษาสำรวจ  ให้คำปรึกษา ข้อเสนอแนะ บริหาร และควบคุมงานก่อสร้าง รวมไปถึงการตรวจสอบรับรองงาน เพื่อให้โครงการแล้วเสร็จและสามารถเปิดดำเนินการหรือเข้าใช้ประโยชน์ได้ตามแผนงานของลูกค้า นอกจากนี้ การให้บริการของกลุ่มบริษัทฯ ยังครอบคลุมไปถึงการให้บริการออกแบบงานด้านสถาปัตยกรรมและวิศวกรรม งานตกแต่งภายใน และงานอนุรักษ์โบราณสถาน</p>
					<p class="VisionText">ซึ่งดำเนินการโดยบริษัท สโตนเฮ้นจ์ จำกัด (“บริษัทย่อย” “STH”) ที่เป็นบริษัทย่อยของบริษัทฯ ซึ่งบริษัทฯ มีความมุ่งมั่นที่จะให้บริการแก่ลูกค้าอย่างมีประสิทธิภาพ ทั้งในด้านคุณภาพงานที่ได้มาตรฐานวิชาชีพระดับสากล  ภายใต้การควบคุมกรอบระยะเวลาและงบประมาณของโครงการก่อสร้างให้เป็นไปตามแผนงานของลูกค้า เพื่อสร้างความพึงพอใจสูงสุดให้แก่ลูกค้าที่รับบริการของบริษัทฯ  ทั้งนี้ บริษัทฯ และบริษัทย่อยได้กำหนดวิสัยทัศน์ ภารกิจ และเป้าหมายในการดำเนินธุรกิจดังนี้</p>
				  </div> -->
				  <div class="coverVisionText2">
					<p class="head" style="float: left;font-size: 20px;font-weight: 500;margin-right: 15px;margin-top: 6px;color: #006c9d;">วิสัยทัศน์ (Vision) :</p>
					<p class="VisionText" style="font-weight: 400;">ก้าวสู่การเป็นบริษัทที่ปรึกษาบริหารและควบคุมงานก่อสร้างแบบครบวงจร และผู้ให้บริการ ออกแบบด้านสถาปัตยกรรม และวิศวกรรมที่ดีที่สุด <span style="font-size: 22px;color: #006c9d;margin-left: 3px;margin-right: 3px;"> (BEST) </span> ในประเทศไทย</p>
					<div class="VisionCoverIconBEST">
					  <!-- <div class="col-lg-6 col-md-6 col-sm-6">
						<img src="images/Icon/B.png" alt="">
					  </div>
					  <div class="col-lg-6 col-md-6 col-sm-6">
						<img src="images/Icon/E.png" alt="">
					  </div>
					  <div class="col-lg-6 col-md-6 col-sm-6">
						<img src="images/Icon/S.png" alt="">
					  </div>
					  <div class="col-lg-6 col-md-6 col-sm-6">
						<img src="images/Icon/T.png" alt="">
					  </div> -->
					  <img src="images/Icon/iconBEST.png">
					</div>
				  </div>
				  <div class="coverVisionText3">
					<p style="float: left;font-size: 20px;font-weight: 500;margin-right: 15px;margin-top: 5px;color: #006c9d;">พันธกิจ (Mission)  :</p>
					<p class="VisionText" style="font-weight: 500;">พันธกิจของกลุ่มบริษัทฯ</p>
					  <div class="col-lg-4 col-md-4" style="text-align: center;">
						<img src="images/Icon/m1.png">
						<p class="VisionText" style="padding: 0px 30px;">มุ่งมั่นให้บริการที่มีคุณภาพ ภายใต้มาตรฐานวิชาชีพระดับสากล โดยเข้าใจและตระหนักถึงความต้องการของลูกค้า รวมถึงให้ความสำคัญต่อการควบคุมงบประมาณโครงการ เพื่อรักษาผลประโยชน์สูงสุดของลูกค้าเป็นสำคัญ</p>
					  </div>
					  <div class="col-lg-4 col-md-4" style="text-align: center;">
						<img src="images/Icon/m2.png">
						<p class="VisionText" style="padding: 0px 30px;">บริหารจัดการโครงการโดยบุคลากรที่มีประสบการณ์ความเชี่ยวชาญ เพื่อรับประกันถึงผลสำเร็จของโครงการตามแผนงานของลูกค้า</p>
					  </div>
					  <div class="col-lg-4 col-md-4" style="text-align: center;">
						<img src="images/Icon/m3.png">
						<p class="VisionText" style="padding: 0px 30px;">ดูแลให้มีการกำกับดูแลกิจการที่ดี (good corporate governance) ทั้งด้านความเป็นธรรม ความโปร่งใส ตรวจสอบได้ และศักยภาพการแข่งขัน เพื่อเป็นหลักประกันถึงคุณภาพงานบริการในทุกโครงการ</p>
					  </div>
				  </div>
				  <!-- <div style="text-align: center;"><img src="images/BEST.png" style="margin-top: 30px;"></div> -->
				</div>
			<%end if%>
			<!--======================================================= End ======================================================-->
			</section>
			
		</div>

		<!--====================================== Footer ======================================-->
			<!--#include file='i_footer.asp'-->
		<!--======================================== End =======================================-->

		<script type="text/javascript" src="scripts/modernizr.custom.js"></script>
		<%if session("lang")="E" then%><script type="text/javascript" src="scripts/jquery.dlmenu-en.js"></script><%else%><script type="text/javascript" src="scripts/jquery.dlmenu.js"></script><%end if%>
		<script>
			$(function() {
				$('#dl-menu').dlmenu();
			});
		</script>
		<!-- js/scroll-hint -->
		<script src="scripts/scroll-hint.js"></script>
		<!-- js/scroll-hint -->
		<script>
			new ScrollHint('.js-scrollable', {
			});
		</script>
	</div>
	 <!--#include file="i_googleAnalytics.asp"-->	  
</body>
</html>