﻿<!DOCTYPE html>

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->
<!--#include file = "../../formatdate_utf8.asp" -->
<!-- #include file = "../../i_conpsims.asp" -->
<!--#include file = "../../i_conir.asp" --> 
<!--#include file = "../../function_asp2007.asp" -->

<%
session("cur_page")="IR " &  listed_share & " Form 56-1"
session("page_asp")="finance_56.asp"
page_name="Form 56-1"
%>

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<TITLE><%=message_title%> :: <%=page_name%></TITLE>
	<META name="Title" content="<%=message_metatitle%>">
	<META name="Description" content="<%=message_description%>">
	<META name="Keywords" content="<%=message_keyword%>">	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="author" content="">
	<link rel="shortcut icon" href="<%=img_icon%>">
	<link href="style_listed.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../function2007_utf8.js"></script>
	<!-- CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/flexslider.css" rel="stylesheet" type="text/css">
	<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
	<!-- ADD CSS -->
	<link href="css/component.css" rel="stylesheet" type="text/css">
	<link href="css/custom.css" rel="stylesheet" type="text/css">
	<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">
	<!-- FONTS -->
	<link href="css/font_add.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- Mouse Hover CSS -->
	<script src="scripts/jquery.min.js" type="text/javascript"></script>
	<script src="scripts/jquery.nicescroll.min.js"></script>
	<!-- SCRIPTS -->
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<!--[if IE]><html class="ie" lang="en"> <![endif]-->
	<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
	<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
	<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
	<script src="scripts/animate.js" type="text/javascript"></script>
	<script src="scripts/myscript.js" type="text/javascript"></script>
	<script src="scripts/owl.carousel.js" type="text/javascript"></script>
	<!-- NEW -->
	<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
	<link href="css/site.css" rel="stylesheet" type="text/css" />
	<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
	<!-- scroll-hint -->
	<link rel="stylesheet" href="css/scroll-hint.css">
</head>

<body class="main">

	<!-- PRELOADER -->
	<img id="preloader" src="images/preloader.gif" alt="">
	<!-- //PRELOADER -->
	<div class="preloader_hide">
		<div id="page" class="single_page">

			<!--================================ Menu Top ==================================-->
				<!--#include file='i_menu_top.asp'-->
			<!--=================================== End ====================================-->

			<!--=============================== Menu Left ==================================-->
				<!--#include file='i_menu_left.asp'-->
			<!--================================== End =====================================-->

			<!--============================ Title Menu =============================-->
			<section class="mbr-section article">
				<div class="container">
					<div class="bigTopic">
						<h1 class="bigTopicText"><%=arr_menu_desc(43)%></h1>
					</div>
				</div>
			</section>
			<!--============================== End ==================================-->

			<section class="mbr-section article sectionSub">
			<!--===================================================== Content ====================================================-->
			<div class="container">
				<div class="table-data js-scrollable scroll-hint" style="position: relative; overflow: auto;">
					<table border="0" cellspacing="0" cellpadding="0" class="table-width table-MajorShareholder">
						<tbody style="font-size: 15.5px;">
							<tr class="Head-table-MajorShareholder">
								<th align="center" class="topicTeble" style="width: 50%;"><%if session("lang")="E" then%>Company Name<%else%>ชื่อบริษัท<%end if%></th>
								<th align="center" class="topicTeble" style="width: 15%;"><%if session("lang")="E" then%>Year<%else%>ประจำปี<%end if%></th>
								<th align="center" class="topicTeble" style="width: 20%;"><%if session("lang")="E" then%>Date &amp; Time<%else%>วันที่ได้รับข้อมูล<%end if%></th>
								<th align="center" class="topicTeble" style="width: 15%;"><%if session("lang")="E" then%>Detail<%else%>รายละเอียด<%end if%></th>
							</tr>
						<%
						   flag_info=false
						   company_name=""
						   strsql=""
						   strsql=sql_company_name(listed_com_id,session("lang"))
						   set rs = conpsims.execute(strsql)
						   if not rs.eof  and  not rs.bof then
							company_name=rs("com_name")
						   end if
						   rs.close
						   
						   if company_name <> "" then
							   strsql=""
							   strsql=sql_f56_info(listed_share)
							   record = 1
							   set rs =conir.execute(strsql)
								  if not rs.eof and not rs.bof then							
									   do while not rs.eof
									   %>
										<tr class="even">
											 <td align="left"><%=company_name%></td>
											 <td align="center">
												<%
												if session("lang") = "E" then
													annualyear = rs("annualyear")
												else
													annualyear = rs("annualyear")+543
												end if
												%>
												<%=annualyear%>
											 </td>
											 <td align="center"><%if session("lang") = "E" then%><% =DateToString(rs("last_update"),"DD/MM/YYYY")%><%else%><% =DateToStringTH(rs("last_update"),"DD/MM/YYYY")%><%end if%></td>
											 <td align="center" ><a href="f56_1/<%=rs("filename")%>" target="_blank" title="Download"><img src="images/icon_download.png" width="16px" border="0"></a></td>
										  </tr> 
										<%
										rs.movenext	
										flag_info=true
									  loop
									  rs.close
									  set rs=nothing
								end if			
							end if%>
							
							<%if flag_info=false then%>
							  <tr>
								  <td align="center" valign="middle" colspan="4">
									 <font color="#ff0000" class="no_information">
									 <%
									if  session("lang")="E"  then 
										response.write "No Information Now !! "
									else																				
										response.write "ไม่มีข้อมูล ณ ขณะนี้ "
									end if
									 %></font>
								  </td>
							</tr>
							<%end if%>
						</<tbody>
					</table>
				</div>
			</div>
			<!--======================================================= End ======================================================-->
			</section>
			
		</div>

		<!--====================================== Footer ======================================-->
			<!--#include file='i_footer.asp'-->
		<!--======================================== End =======================================-->

		<script type="text/javascript" src="scripts/modernizr.custom.js"></script>
		<%if session("lang")="E" then%><script type="text/javascript" src="scripts/jquery.dlmenu-en.js"></script><%else%><script type="text/javascript" src="scripts/jquery.dlmenu.js"></script><%end if%>
		<script>
			$(function() {
				$('#dl-menu').dlmenu();
			});
		</script>
		<!-- js/scroll-hint -->
		<script src="scripts/scroll-hint.js"></script>
		<!-- js/scroll-hint -->
		<script>
			new ScrollHint('.js-scrollable', {
			});
		</script>
	</div>
	 <!--#include file="i_googleAnalytics.asp"-->	  
</body>
</html>