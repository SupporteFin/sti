﻿<div class="panel panel-default" style="margin-bottom: 10px;">
	<div class="panel-heading" style="padding: 26px 15px 0 24px;">
		<p class="headNewtext2"><%if session("lang")="E" then%>SET ANNOUNCEMENT<%else%>ข่าวจากตลาดหลักทรัพย์ฯ<%end if%></p>
	</div>
	<div class="panel-body" style="min-height: 435px;">
		<div class="row">
			<div class="col-xs-12">
				<ul class="demo1"> 
					<%
					  source_id="0100"
					  strsql=""
					  strsql = sql_news(listed_share,session("lang"),8,source_id)
					  'response.write strsql
					  set rs = connews.execute(strsql)
					  last_update= ""
					  if not  rs.eof and not  rs.bof then
						flagData=false
						i=0
						 do while not rs.eof
							 ' +++ For Check Show News!! +++					
							 if last_update = "" then
								last_update = cdate(left(rs("last_date"),8))
							 end if
							 ' +++ For Check Show News!! +++	
							flagData=true
					%>	
					<li class="news-item">
						<table cellpadding="4">
							<tr>
								<p class="news-text1"><%if session("lang")="E" then%>Date : <%else%>โพสต์เมื่อ : <%end if%></p><span class="news-text2-day"><%=DisplayDateShort(rs("last_date"))%></span>
								<p class="news-text1"><%if session("lang")="E" then%>Time : <%else%>เวลา : <%end if%></p><span class="news-text2-time"><%=DisplayDateTime(rs("last_date"))%></span><br>
								<%if source_id="0100" then				  
									if instr(rs("title"),"งบการเงิน")=1   or instr(rs("title"),"Financial Statement ")  then%>
										<a href="<%=pathdownloadfinancial%>?year=<%=year(cdate(left(rs("last_date"),8)))%>&newsid=<%=rs("news_id")%>&lang=<%=rs("lang")%>&page=Announcement&V_server=IR" target="_blank" >
									 <%else%>
										<a href="frame_news_detail.asp?newsid=<%=encrypt(rs("news_id"), "eFinanceThai")%>&lang=<%=rs("lang")%>&title=2&subtitle=2" target="_blank" >
									<% end if 
								else%>
									<a href="frame_news_detail.asp?newsid=<%=encrypt(rs("news_id"), "eFinanceThai")%>&lang=<%=rs("lang")%>&title=1&subtitle=1" target="_blank" >
								<%end if%>
									<p class="news-text3">
										<%=rs("title")%>
										&nbsp;<%if last_update = cdate(left(rs("last_date"),8)) then%> <img src="images/update.gif"><%end if%>
									</p>
								</a>
							</tr>
						</table>
					</li>
					<%														
							 rs.movenext 
							i=i+1			 
						 loop				
					 else
					%> 
					<li class="news-item">
						<table cellpadding="4">
							<tr>
								<a href="javascript:void(0);">
									<p style="color: #ff0000;">
										<%if session("lang")="E" then %>No Information Now !!<%else%>ไม่มีข้อมูล ณ ขณะนี้ <%end if%>
									</p>
								</a>
							</tr>
						</table>
					</li>
					<%
					  end if
					  rs.close
					  %>
				</ul>
			</div>
		</div>
	</div>
	<div class="panel-footer background-panel-footer1">
		<ul class="pagination pull-left" style="margin: 0px;">
			<li>
				<a href="#" class="prev">
					<span class="glyphicon glyphicon-chevron-down"></span>
				</a>
			</li>
			<li>
				<a href="#" class="next">
					<span class="glyphicon glyphicon-chevron-up"></span>
				</a>
			</li>
		</ul>
		<%if session("lang")="E" then%>
			<a href="set_announcement.asp?lang=E" class="readAll">READ ALL</a>
		<%else%>
			<a href="set_announcement.asp" class="readAll">อ่านทั้งหมด</a>
		<%end if%>
		<div class="clearfix"></div>
	</div>
</div>