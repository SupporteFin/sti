﻿<!DOCTYPE html>

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->
<!--#include file = "../../i_conirauthen.asp" --> 
<!--#include file = "../../function_asp2007.asp" -->
<!--#include file = "../../i_conir.asp" --> 
<%
session("cur_page")="IR " &  listed_share & " Press Release"
session("page_asp")="press.asp"
page_name="Press Release"

pagesize=15
%>
<!--#include file="../../constpage2007.asp"-->

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<TITLE><%=message_title%> :: <%=page_name%></TITLE>
	<META name="Title" content="<%=message_metatitle%>">
	<META name="Description" content="<%=message_description%>">
	<META name="Keywords" content="<%=message_keyword%>">	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="author" content="">
	<link rel="shortcut icon" href="<%=img_icon%>">
	<link href="style_listed.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../function2007_utf8.js"></script>
	<!-- CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/flexslider.css" rel="stylesheet" type="text/css">
	<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
	<!-- ADD CSS -->
	<link href="css/component.css" rel="stylesheet" type="text/css">
	<link href="css/custom.css" rel="stylesheet" type="text/css">
	<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">
	<!-- FONTS -->
	<link href="css/font_add.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- Mouse Hover CSS -->
	<script src="scripts/jquery.min.js" type="text/javascript"></script>
	<script src="scripts/jquery.nicescroll.min.js"></script>
	<!-- SCRIPTS -->
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<!--[if IE]><html class="ie" lang="en"> <![endif]-->
	<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
	<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
	<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
	<script src="scripts/animate.js" type="text/javascript"></script>
	<script src="scripts/myscript.js" type="text/javascript"></script>
	<script src="scripts/owl.carousel.js" type="text/javascript"></script>
	<!-- NEW -->
	<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
	<link href="css/site.css" rel="stylesheet" type="text/css" />
	<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
	<!-- scroll-hint -->
	<link rel="stylesheet" href="css/scroll-hint.css">
</head>

<body class="main">

	<!-- PRELOADER -->
	<img id="preloader" src="images/preloader.gif" alt="">
	<!-- //PRELOADER -->
	<div class="preloader_hide">
		<div id="page" class="single_page">

			<!--================================ Menu Top ==================================-->
				<!--#include file='i_menu_top.asp'-->
			<!--=================================== End ====================================-->

			<!--=============================== Menu Left ==================================-->
				<!--#include file='i_menu_left.asp'-->
			<!--================================== End =====================================-->

			<!--============================ Title Menu =============================-->
			<section class="mbr-section article">
				<div class="container">
					<div class="bigTopic">
						<h1 class="bigTopicText"><%=arr_menu_desc(62)%></h1>
					</div>
				</div>
			</section>
			<!--============================== End ==================================-->

			<section class="mbr-section article sectionSub">
			<!--===================================================== Content ====================================================-->
				<div class="container">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td align="left" valign="top">
								<table border="0" cellspacing="0" cellpadding="0" width="100%">
									<form name="frm1" METHOD="POST" ACTION="<%=session("page_asp")%>">
										 <tr>
											<td align="left" valign="top">
												<div class="table-data js-scrollable scroll-hint" style="position: relative; overflow: auto;">
													<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table-width table-MajorShareholder">
														<tbody style="font-size: 15.5px;">
															<!--#include file="../../select_page2007.asp"-->
															<%
															   'dim totalrec
															   totalrec=0
															   
															   strsql=sql_press_release(listed_com_id,session("lang"),0,"")
															   'response.write strsql
															   set rs=conir.execute(strsql)
															   if not rs.eof and not  rs.bof then 
																  totalrec=rs("num_count")
															   end if
															
															   session("num_count")=totalrec
															%>
															<!--#include file="../../calculate_page2007.asp"-->		
															<%
															 '-------------------------------------Get Maxdate for Check Update Image-------------------------------------
															 maxdate=""
															 strmaxdate=sql_press_release(listed_com_id,session("lang"),0,"max")
															 
															 set rs_maxdate = conir.execute(strmaxdate)
															 
															 if not rs_maxdate.eof and not rs_maxdate.bof then
																maxdate = rs_maxdate("maxdate")
															 end if 
															 '-------------------------------------Get Maxdate for Check Update Image-------------------------------------
															 
															 strsql = sql_press_release(listed_com_id,session("lang"),((session("pageno")-1)*session("pagesize"))  & "," & session("pagesize"),"" )
															 'response.write strsql
															 set rs_press=conir.execute(strsql)
															 
															 if not rs_press.eof and not  rs_press.bof then 
															 i = 0
																do while not rs_press.eof
																' +++ For Check Show News!! +++
																if session("pageno") = 1 then                                                       
																	  if last_update = "" then
																		 last_update = cdate(left(rs_press("display_timestamp"),8))
																		  session("last_update") = last_update
																	  end if
																else
																   last_update = session("last_update") 
																end if
															%>	
															<tr class="even">
																<td align="center" width="20%" style="background-color: #006c9d; color: #FFFFFF;">
																	<span><%=DisplayDate(rs_press("display_timestamp"))%></span>
																</td>
																 <%if i mod 2 = 0 then%>
																	<td align="left" width="80%" style="padding-left: 12px; background-color: #f3f3f3;">
																<%else%>
																	<td align="left" width="80%" style="padding-left: 12px;">
																<%end if%>
																	<a class="web" href="<%=pathfileserver%>/Listed/<%=listed_share%>/press_release/<%=rs_press("filename")%>" target="_blank"><%=rs_press("title")%></a>			
																<%	if last_update = cdate(left(rs_press("display_timestamp"),8)) then%>
																	&nbsp;<img src="images/update.gif" border=0>
																<%end if%> 
																</td>
															</tr>
															<%
																  i=i+1
																  rs_press.movenext
																loop
															 else
															 %>
															 <tr>
																<td align="center"  valign="middle" colspan="2"><br><br><br>
																	<font color="#ff0000" class="no_information">
																		<%
																		   if  session("lang")="E"  then 
																			response.write "No Information Now !! "
																		   else																				
																			response.write "ไม่มีข้อมูล ณ ขณะนี้ "
																		   end if
																		 %>
																	</font>
																	<br><br><br>
																</td>
															 </tr>
															 <%
															 end if
															 rs.close
															 rs_maxdate.close
															 rs_press.close		
															 %>
														</tbody>
													</table>
												</div>
											</td>
										</tr>
										<tr>
											<td align="left">&nbsp;</td>
										</tr>
										<tr>
											<td align="right" valign="top">
												<!--#include file="page_button.asp"-->
											</td>
										</tr>
									</form>
								</table>
							</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td></td>
						</tr>
					</table>
				</div>
			<!--======================================================= End ======================================================-->
			</section>
			
		</div>

		<!--====================================== Footer ======================================-->
			<!--#include file='i_footer.asp'-->
		<!--======================================== End =======================================-->

		<script type="text/javascript" src="scripts/modernizr.custom.js"></script>
		<%if session("lang")="E" then%><script type="text/javascript" src="scripts/jquery.dlmenu-en.js"></script><%else%><script type="text/javascript" src="scripts/jquery.dlmenu.js"></script><%end if%>
		<script>
			$(function() {
				$('#dl-menu').dlmenu();
			});
		</script>
		<!-- js/scroll-hint -->
		<script src="scripts/scroll-hint.js"></script>
		<!-- js/scroll-hint -->
		<script>
			new ScrollHint('.js-scrollable', {
			});
		</script>
	</div>
	 <!--#include file="i_googleAnalytics.asp"-->	  
</body>
</html>