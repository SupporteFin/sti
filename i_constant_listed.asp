<%
session("current_path")="STI"   
session("com_id")="1466"   
style="sti"  
website="https://www.sti.co.th/"

listed_share="STI"   '<----[ 1 ]---->
listed_com_id="1466"   '<---[ 2 ]--->
listed_website="https://www.sti.co.th/"
%>
<!--#include file = "../../check_status_of_listed.asp" -->
<%

if session("lang")="E" then
	message_title="STI | Investor Relations"
else 
	message_title="บริษัท สโตนเฮ้นจ์ อินเตอร์ จำกัด (มหาชน)"
end if
message_keyword="ที่ปรึกษาบริหารและควบคุมงานก่อสร้าง, Consulting and Project Management, ให้บริการออกแบบงานด้านสถาปัตยกรรม, Architectural Design, วิศวกรรม, Engineering Design, งานตกแต่งภายใน, Interior Design, งานอนุรักษ์โบราณสถาน, Historical Conservation"
message_Description="ที่ปรึกษาบริหารและควบคุมงานก่อสร้าง, Consulting and Project Management, ให้บริการออกแบบงานด้านสถาปัตยกรรม, Architectural Design, วิศวกรรม, Engineering Design, งานตกแต่งภายใน, Interior Design, งานอนุรักษ์โบราณสถาน, Historical Conservation"
img_icon="images/favicon.ico"
path_website=""

' +++++++++++++++++ config website listed ++++++++++++++++++

menu00="Y"							
menu01="Y"							
menu02="Y"							
menu03="Y"							
menu04="Y"							
menu05="Y"							
menu06="Y"							
menu07="Y"							
menu08="Y"							
menu09="Y"							
menu10="Y"							
menu11="Y"							
menu12="Y"							
menu13="Y"									
menu14="Y"							
menu15="Y"							
menu16="Y"							
menu17="Y"							
menu18="Y"							
menu19="Y"							
menu20="Y"							
menu21="Y"							
menu22="Y"							
menu23="Y"							
menu24="Y"							
menu25="Y"							
menu26="Y"							
menu27="Y"							
menu28="Y"							
menu29="Y"							
menu30="Y"							
menu31="Y"	
menu32="Y"	
menu33="Y"	
menu34="Y"	
menu35="Y"	
menu37="Y"							
menu38="Y"							
menu39="Y"							
menu40="Y"							
menu41="Y"							
menu42="Y"							
menu43="Y"							
menu44="Y"							
menu45="Y"							
menu46="Y"							
menu47="Y"							
menu48="Y"							
menu49="Y"							
menu50="Y"									
menu51="Y"							
menu52="Y"							
menu53="Y"							
menu54="Y"							
menu55="Y"							
menu56="Y"							
menu57="Y"							
menu58="Y"							
menu59="Y"							
menu60="Y"							
menu61="Y"							
menu62="Y"							
menu63="Y"							
menu64="Y"							
menu65="Y"							
menu66="Y"							
menu67="Y"							
menu68="Y"	
menu69="Y"	
menu70="Y"	
menu71="Y"	
menu72="Y"	

' +++++++++++++++++ Config status of menu +++++++++++++++++ 
'Y=link and N=no link


' +++++++++++++++++  language +++++++++++++++++ 
if request("lang")="T"  then  'Language
	session("lang")="T"	
elseif request("lang")="E" then
	session("lang")="E"
elseif session("lang")="" then
	session("lang")="T"	
end if

if session("lang")="E" then

	read_more = "Read More"
	url_stock_quote="https://sti.co.th/th/stock_quote.html"
	url_stock_graph="https://sti.co.th/th/stock_graph.html"
	url_stock_price=""
	message_more = ""
	url_filing=""
   
else 
	
	read_more = ""	
	url_stock_quote="https://sti.co.th/th/stock_quote_th.html"
	url_stock_graph="https://sti.co.th/th/stock_graph.html"
	url_stock_price=""
	message_more = ""	
	url_filing=""
   
end if

' +++++++++++++++++ Config Old +++++++++++++++++ 
 session("bg_in_form")="#FF8400"   
 session("bg_re_alert")="#FF8400"       

 ' +++++++++++++++++  Emergency Case +++++++++++++++++ 
link_web_customer="https://www.sti.co.th/th/ir_index.php"		                                                             'Path Website  **** use ****


%>
<!--#include file = "i_description_menu.asp" -->