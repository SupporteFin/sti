﻿<!DOCTYPE html>

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->
<!-- #include file = "../../i_conir.asp" -->
<!-- #include file = "../../function_asp2007.asp" -->
<%
session("cur_page")="IR " &  listed_share & " Webcast & Presentation"
session("page_asp")="webcasts.asp"
page_name="Webcast & Presentation"

pagesize=15
%>
<!--#include file="../../constpage2007.asp"-->

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<TITLE><%=message_title%> :: <%=page_name%></TITLE>
	<META name="Title" content="<%=message_metatitle%>">
	<META name="Description" content="<%=message_description%>">
	<META name="Keywords" content="<%=message_keyword%>">	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="author" content="">
	<link rel="shortcut icon" href="<%=img_icon%>">
	<link href="style_listed.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../function2007_utf8.js"></script>
	<!-- CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/flexslider.css" rel="stylesheet" type="text/css">
	<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
	<!-- ADD CSS -->
	<link href="css/component.css" rel="stylesheet" type="text/css">
	<link href="css/custom.css" rel="stylesheet" type="text/css">
	<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">
	<!-- FONTS -->
	<link href="css/font_add.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- Mouse Hover CSS -->
	<script src="scripts/jquery.min.js" type="text/javascript"></script>
	<script src="scripts/jquery.nicescroll.min.js"></script>
	<!-- SCRIPTS -->
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<!--[if IE]><html class="ie" lang="en"> <![endif]-->
	<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
	<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
	<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
	<script src="scripts/animate.js" type="text/javascript"></script>
	<script src="scripts/myscript.js" type="text/javascript"></script>
	<script src="scripts/owl.carousel.js" type="text/javascript"></script>
	<!-- NEW -->
	<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
	<link href="css/site.css" rel="stylesheet" type="text/css" />
	<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
	<!-- scroll-hint -->
	<link rel="stylesheet" href="css/scroll-hint.css">
</head>

<body class="main">

	<!-- PRELOADER -->
	<img id="preloader" src="images/preloader.gif" alt="">
	<!-- //PRELOADER -->
	<div class="preloader_hide">
		<div id="page" class="single_page">

			<!--================================ Menu Top ==================================-->
				<!--#include file='i_menu_top.asp'-->
			<!--=================================== End ====================================-->

			<!--=============================== Menu Left ==================================-->
				<!--#include file='i_menu_left.asp'-->
			<!--================================== End =====================================-->

			<!--============================ Title Menu =============================-->
			<section class="mbr-section article">
				<div class="container">
					<div class="bigTopic">
						<h1 class="bigTopicText"><%=arr_menu_desc(51)%></h1>
					</div>
				</div>
			</section>
			<!--============================== End ==================================-->

			<section class="mbr-section article sectionSub">
			<!--===================================================== Content ====================================================-->
				<div class="container">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td align="left" valign="top">
								<div style="width: 100%;color: #252525;line-height:20px;">
									<form action="<%=session("page_asp")%>" method='post' name="frm1">
										<p>
											<table width="100%" align="center"  border="0" cellspacing="0" cellpadding="0" >
												<tr align="left" valign="top">
													<td><strong class="boldo"><%	if  session("lang")="E" then %>Legend<%else%>สัญลักษณ์<%end if%>:</strong></td>
													<td  width="165"><img src="../images2007/pic_present.gif" width="23" height="12"><%	if  session("lang")="E" then %>Presentation (pdf)<%else%>ข้อมูลพรีเซนเทชั่น<%end if%> </td>
													<td  width="155"><img src="../images2007/pic_transcripts.gif" width="23" height="12"><%	if  session("lang")="E" then %>Transcripts (pdf)<%else%>ข้อมูลเอกสาร<%end if%></td>
													<td  width="100"><img src="../images2007/pic_webcast.gif" width="23" height="12"><%	if  session("lang")="E" then %>Webcast<%else%>ไฟล์วีดีโอ<%end if%> </td>
													<td  width="100"><img src="../images2007/pic_audio.gif" width="23" height="12"><%	if  session("lang")="E" then %> Audio<%else%>ไฟล์เสียง<%end if%> </td>
												</tr>
											</table>
											<!--#include file="../../select_page2007.asp"-->
										</p>
										<p>
											<div class="table-data js-scrollable scroll-hint" style="position: relative; overflow: auto;">
												<table width="100%" border="1" cellspacing="0" cellpadding="0" class="table-width table-MajorShareholder">
													<tbody style="font-size: 15.5px;">
														<tr class="Head-table-MajorShareholder">
															<th colspan="6" align="left" valign="middle" class="topicTeble" style="background-color: #006c9d; color: #FFFFFF;">
																<font> <%if session("lang")="E" then%>Current Event<%else%>กิจกรรม<%end if%></font>
															</th>
														</tr>
														<%
															  dim totalrec
															  totalrec=0
															  
															  strsql=""
															  strsql=sql_webcast(listed_com_id,session("lang"),0)
															  
															  set rs=conir.execute(strsql)
															  if not rs.eof and not rs.bof then
															  totalrec=rs("rec_count")
															  end if
															  rs.close				
															  
															  session("num_count")=totalrec	
														   %>
														   <!--#include file="../../calculate_page2007.asp"-->
														<%
															  strsql=""
															  strsql=sql_webcast(listed_com_id,session("lang"),((session("pageno")-1)*session("pagesize")) & "," & session("pagesize") )
															  'response.write strsql
															  set rs=conir.execute(strsql)
																 if not rs.eof and not rs.bof then
																	i=0
																	   do while not rs.eof  %>
																		<%if i mod 2 = 0 then%>
																		<tr align="left" style="padding: 12px; background-color: #f3f3f3;">
																		<%else%>
																		<tr align="left" style="padding: 12px; background-color: #ffffff;">
																		<%end if%>
																			<td style="padding: 12px;"><%=rs("detail")%></td>
																		
																		<%if rs("presentation")=0 then%>
																			<td align="center" width="50">&nbsp;</td>
																		<%else%>
																			<td align="center" width="50"><a class="web" href="<%=pathfileserver%>/Listed/<%=listed_share%>/webcasts_detail.asp?type=present&id=<%=rs("id")%>" target="_blank"><img src="../images2007/pic_present.gif" border="0" width="23" height="12"></a></td>
																		<%end if%>
																	
																		<%if rs("transcript")=0 then%>
																			<td align="center" width="50">&nbsp;</td>
																		<%else%>
																			<td align="center" width="50"><a class="web" href="<%=pathfileserver%>/Listed/<%=listed_share%>/webcasts_detail.asp?type=tscript&id=<%=rs("id")%>" target="_blank"><img src="../images2007/pic_transcripts.gif" width="23" height="12" border="0"></a></td>
																		<%end if%>
																		
																		<%if rs("multimedia")=0 then%>
																			<td align="center" width="35">&nbsp;</td>
																		<%else%>
																			<td align="center" width="35"><a class="web" href="webcasts_player.asp?id=<%=rs("name")%>" target="_blank"><img src="../images2007/pic_webcast.gif" width="23" height="12" border="0" style="cursor:hand;"></a></td>	
																		<%end if%>
																		
																		<%if rs("audio")=0 then%>
																			<td align="center" width="35">&nbsp;</td>
																		<%else%>
																			<td align="center" width="35"><a class="web" href="<%=pathfileserver%>/Listed/<%=listed_share%>/calendar/webcasts/<%=rs("name")%>/index.htm" target="_blank"><img src="../images2007/pic_audio.gif" width="23" height="12" border="0" style="cursor:hand;"></a></td>	
																		<%end if%>
																			<td align="center" width="160" style="padding: 12px;"><span><%=DisplayDate(rs("tmpdate"))%></span></td>
																		</tr>
																		<%
																		i=i+1
																		rs.movenext
																	   loop	
																 else%>
																		  
																		<tr class="bgcolor_no_information">
																			<td align="center" colspan="6"  valign="middle">
																				<div align="center"><font color="ff0000" class="no_information">
																			   <%
																				   if  session("lang")="E"  then 
																					response.write "No Information Now !! "
																				   else																				
																					response.write "ไม่มีข้อมูล ณ ขณะนี้ "
																				   end if
																				   %></font></div>
																			</td>
																		</tr>
															<%
																 end if	 
																 rs.close			
																 conir.close
																 set rs=nothing
																 set conir=nothing
															%>
													</tbody>
												</table>
											</div>
											<div align="center"><br><font style="font-size:12px;"><!--#include file="page_button.asp"--></font></div>
										</p>
										<p style="text-align: center;">
											<%if session("lang")="E" then%>
												<b>" To support the correct display, please use the latest version of Browser and Flash Player. "</b>
											<%else%>
												<b>"เพื่อรองรับการแสดงผลที่ถูกต้อง กรุณาใช้ Browser และ Flash Player เวอร์ชั่น ล่าสุด"</b>
											<%end if%>	
											<br><br>
										</p>
									</form>
								</div>
							</td>
						</tr>
						<tr>
							  <td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td></td>
						</tr>
					</table>
				</div>
			<!--======================================================= End ======================================================-->
			</section>
			
		</div>

		<!--====================================== Footer ======================================-->
			<!--#include file='i_footer.asp'-->
		<!--======================================== End =======================================-->

		<script type="text/javascript" src="scripts/modernizr.custom.js"></script>
		<%if session("lang")="E" then%><script type="text/javascript" src="scripts/jquery.dlmenu-en.js"></script><%else%><script type="text/javascript" src="scripts/jquery.dlmenu.js"></script><%end if%>
		<script>
			$(function() {
				$('#dl-menu').dlmenu();
			});
		</script>
		<!-- js/scroll-hint -->
		<script src="scripts/scroll-hint.js"></script>
		<!-- js/scroll-hint -->
		<script>
			new ScrollHint('.js-scrollable', {
			});
		</script>
	</div>
	 <!--#include file="i_googleAnalytics.asp"-->	  
</body>
</html>