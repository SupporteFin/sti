﻿<!DOCTYPE html>

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->
<%
session("cur_page")="IR " &  listed_share & " Awards and Certificates"
session("page_asp")="award.asp"
page_name="Awards and Certificates"
%>

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<TITLE><%=message_title%> :: <%=page_name%></TITLE>
	<META name="Title" content="<%=message_metatitle%>">
	<META name="Description" content="<%=message_description%>">
	<META name="Keywords" content="<%=message_keyword%>">	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="author" content="">
	<link rel="shortcut icon" href="<%=img_icon%>">
	<link href="style_listed.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../function2007_utf8.js"></script>
	<!-- CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/flexslider.css" rel="stylesheet" type="text/css">
	<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
	<!-- ADD CSS -->
	<link href="css/component.css" rel="stylesheet" type="text/css">
	<link href="css/custom.css" rel="stylesheet" type="text/css">
	<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">
	<!-- FONTS -->
	<link href="css/font_add.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- Mouse Hover CSS -->
	<script src="scripts/jquery.min.js" type="text/javascript"></script>
	<script src="scripts/jquery.nicescroll.min.js"></script>
	<!-- SCRIPTS -->
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<!--[if IE]><html class="ie" lang="en"> <![endif]-->
	<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
	<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
	<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
	<script src="scripts/animate.js" type="text/javascript"></script>
	<script src="scripts/myscript.js" type="text/javascript"></script>
	<script src="scripts/owl.carousel.js" type="text/javascript"></script>
	<!-- NEW -->
	<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
	<link href="css/site.css" rel="stylesheet" type="text/css" />
	<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
	<!-- scroll-hint -->
	<link rel="stylesheet" href="css/scroll-hint.css">
	
	<style type="text/css">
		.thumbnail {
		  border: 0px solid #ddd;
		}
		.thumbnail, .img-thumbnail {
		  -webkit-box-shadow: none;
		  box-shadow: none;
		}
		.modal-body {
		  position: relative;
		  padding: 0px; 
		  overflow: hidden;
		}
      </style>
</head>

<body class="main">

	<!-- PRELOADER -->
	<img id="preloader" src="images/preloader.gif" alt="">
	<!-- //PRELOADER -->
	<div class="preloader_hide">
		<div id="page" class="single_page">

			<!--================================ Menu Top ==================================-->
				<!--#include file='i_menu_top.asp'-->
			<!--=================================== End ====================================-->

			<!--=============================== Menu Left ==================================-->
				<!--#include file='i_menu_left.asp'-->
			<!--================================== End =====================================-->

			<!--============================ Title Menu =============================-->
			<section class="mbr-section article">
				<div class="container">
					<div class="bigTopic">
						<h1 class="bigTopicText"><%=arr_menu_desc(27)%></h1>
					</div>
				</div>
			</section>
			<!--============================== End ==================================-->

			<section class="mbr-section article sectionSub">
			<!--===================================================== Content ====================================================-->
			<%if session("lang")="E" then%>
				<div class="container" style="padding-right: 15px;padding-left: 15px;">
					  <div class="row">
					
						<!-- BLOG BLOCK -->
						<div class="blog_block col-lg-12 col-md-12 padbot50">           
						<!-- BLOG POST -->
						
						<div class="certificate">
						  <img src="images/certificate.jpg">
						  <p class="certificate-text1">Management system as per</p>
						  <p class="certificate-text2">ISO 9001 : 2015</p>
						</div>

						<div class="row">
						  <h1 class="otherAward">Other awards</h1>
						  <div class="col-lg-4 center">
							<div class="product-div1">
							  <div class="img-div1">
								<img src="images/award/1_DSCF4022.png" class="img-responsive thumbnail">
								  <p class="boardAwardOffice">1. สนับสนุนโครงการ มจธ. 55 ปี ร่วมสร้างคนดี สร้างความรู้ คู่แผ่นดินไทย </p>
							  </div>
							</div>
						  </div>  

						  <div class="col-lg-4 center">
							<div class="product-div1">
							  <div class="img-div1">
								<img src="images/award/2_DSCF4024.png" class="img-responsive thumbnail">
								  <p class="boardAwardOffice">2. ผู้สนับสนุนงานวิจัยและพัฒนานักศึกษา </p>
							  </div>
							</div>
						  </div>

						  <div class="col-lg-4 center">
							<div class="product-div1">
							  <div class="img-div1">
								<img src="images/award/3_DSCF4025.png" class="img-responsive thumbnail">
								  <p class="boardAwardOffice">3. ดำเนินการก่อสร้างปรับปรุงอาคารสำนักงานใหญ่หลังเดิม (อาคาร 2) ธนาคารแห่งประเทศไทย </p>
							  </div>
							</div>
						  </div>
						</div>

						<div class="row">
						  <div class="col-lg-4 center">
							<div class="product-div1">
							  <div class="img-div1">
								<img src="images/award/4_DSCF4026.png" class="img-responsive thumbnail">
								  <p class="boardAwardOffice">4. การสนับสนุนและความร่วมมืออันดียิ่งต่อการส่งเสริมการกลับเข้าทำงานของลูกจ้างพิการของสำนักงานประกันสังคม </p>
							  </div>
							</div>
						  </div>  

						  <div class="col-lg-4 center">
							<div class="product-div1">
							  <div class="img-div1">
								<img src="images/award/5_DSCF4027.png" class="img-responsive thumbnail">
								  <p class="boardAwardOffice">5. ที่ปรึกษาบริหารโครงการและควบคุมงานก่อสร้าง งานวิศวกรรมระบบปรับอากาศ, ระบบระบายอากาศ และติดตั้งอุปกรณ์พื้นฐาน โดยไม่เกิดอุบัติเหตุถึงขั้นหยุดงานเป็นจำนวน 350,000 ชั่วโมง </p>
							  </div>
							</div>
						  </div>

						  <div class="col-lg-4 center">
							<div class="product-div1">
							  <div class="img-div1">
								<img src="images/award/6_DSCF4028.png" class="img-responsive thumbnail">
								  <p class="boardAwardOffice">6. ที่ปรึกษาบริหารโครงการและควบคุมงานก่อสร้าง งานก่อสร้างทางเชื่อมระบบสาธารณูปโภค เข้าสู่ที่ตั้งโครงการของ ปตท. ณ อ.วังจันทร์ โดยไม่เกิดอุบัติเหตุถึงขั้นหยุดงานเป็นจำนวน 400,000 ชั่วโมง </p>
							  </div>
							</div>
						  </div>
						</div>
					  
						<div class="row">
						  <div class="col-lg-4 center">
							<div class="product-div1">
							  <div class="img-div1">
								<img src="images/award/7_DSCF4029.png" class="img-responsive thumbnail">
								  <p class="boardAwardOffice">7. ที่ปรึกษาบริหารโครงการและควบคุมงานก่อสร้าง งานก่อสร้างทางเชื่อมระบบสาธารณูปโภค เข้าสู่ที่ตั้งโครงการของ ปตท. ณ อ.วังจันทร์ โดยไม่เกิดอุบัติเหตุถึงขั้นหยุดงานเป็นจำนวน 700,000 ชั่วโมง </p>
							  </div>
							</div>
						  </div>  

						  <div class="col-lg-4 center">
							<div class="product-div1">
							  <div class="img-div1">
								<img src="images/award/8_DSCF4031.png" class="img-responsive thumbnail">
								  <p class="boardAwardOffice">8. ที่ปรึกษาบริหารโครงการและควบคุมงานก่อสร้าง งานโครงสร้างและสถาปัตยกรรม โดยไม่เกิดอุบัติเหตุถึงขั้นหยุดงานเป็นจำนวน 2,000,000 ชั่วโมง </p>
							  </div>
							</div>
						  </div>

						  <div class="col-lg-4 center">
							<div class="product-div1">
							  <div class="img-div1">
								<img src="images/award/9_DSCF4030.png" class="img-responsive thumbnail">
								  <p class="boardAwardOffice">9. ที่ปรึกษาบริหารโครงการและควบคุมงานก่อสร้าง งานโครงสร้างและสถาปัตยกรรม โดยไม่เกิดอุบัติเหตุถึงขั้นหยุดงานเป็นจำนวน 3,400,000 ชั่วโมง </p>
							  </div>
							</div>
						  </div>
						</div>

						<div style="text-align: center;margin-top: 30px;"><img src="images/award-inof.png"></div>


					  <!-- //BLOG POST -->
					  </div><!-- //BLOG BLOCK -->

					  </div>
					</div>
			<%else%>
				<div class="container" style="padding-right: 15px;padding-left: 15px;">
					  <div class="row">
					
						<!-- BLOG BLOCK -->
						<div class="blog_block col-lg-12 col-md-12 padbot50">           
						<!-- BLOG POST -->

						<div class="certificate">
						  <img src="images/certificate.jpg">
						  <p class="certificate-text1">Management system as per</p>
						  <p class="certificate-text2">ISO 9001 : 2015</p>
						</div>

						<div class="row">
						  <h1 class="otherAward">รางวัลอื่นๆ</h1>
						  <div class="col-lg-4 center">
							<div class="product-div1">
							  <div class="img-div1">
								<img src="images/award/1_DSCF4022.png" class="img-responsive thumbnail">
								  <p class="boardAwardOffice">1. สนับสนุนโครงการ มจธ. 55 ปี ร่วมสร้างคนดี สร้างความรู้ คู่แผ่นดินไทย </p>
							  </div>
							</div>
						  </div>  

						  <div class="col-lg-4 center">
							<div class="product-div1">
							  <div class="img-div1">
								<img src="images/award/2_DSCF4024.png" class="img-responsive thumbnail">
								  <p class="boardAwardOffice">2. ผู้สนับสนุนงานวิจัยและพัฒนานักศึกษา </p>
							  </div>
							</div>
						  </div>

						  <div class="col-lg-4 center">
							<div class="product-div1">
							  <div class="img-div1">
								<img src="images/award/3_DSCF4025.png" class="img-responsive thumbnail">
								  <p class="boardAwardOffice">3. ดำเนินการก่อสร้างปรับปรุงอาคารสำนักงานใหญ่หลังเดิม (อาคาร 2) ธนาคารแห่งประเทศไทย </p>
							  </div>
							</div>
						  </div>
						</div>

						<div class="row">
						  <div class="col-lg-4 center">
							<div class="product-div1">
							  <div class="img-div1">
								<img src="images/award/4_DSCF4026.png" class="img-responsive thumbnail">
								  <p class="boardAwardOffice">4. การสนับสนุนและความร่วมมืออันดียิ่งต่อการส่งเสริมการกลับเข้าทำงานของลูกจ้างพิการของสำนักงานประกันสังคม </p>
							  </div>
							</div>
						  </div>  

						  <div class="col-lg-4 center">
							<div class="product-div1">
							  <div class="img-div1">
								<img src="images/award/5_DSCF4027.png" class="img-responsive thumbnail">
								  <p class="boardAwardOffice">5. ที่ปรึกษาบริหารโครงการและควบคุมงานก่อสร้าง งานวิศวกรรมระบบปรับอากาศ, ระบบระบายอากาศ และติดตั้งอุปกรณ์พื้นฐาน โดยไม่เกิดอุบัติเหตุถึงขั้นหยุดงานเป็นจำนวน 350,000 ชั่วโมง </p>
							  </div>
							</div>
						  </div>

						  <div class="col-lg-4 center">
							<div class="product-div1">
							  <div class="img-div1">
								<img src="images/award/6_DSCF4028.png" class="img-responsive thumbnail">
								  <p class="boardAwardOffice">6. ที่ปรึกษาบริหารโครงการและควบคุมงานก่อสร้าง งานก่อสร้างทางเชื่อมระบบสาธารณูปโภค เข้าสู่ที่ตั้งโครงการของ ปตท. ณ อ.วังจันทร์ โดยไม่เกิดอุบัติเหตุถึงขั้นหยุดงานเป็นจำนวน 400,000 ชั่วโมง </p>
							  </div>
							</div>
						  </div>
						</div>
					  
						<div class="row">
						  <div class="col-lg-4 center">
							<div class="product-div1">
							  <div class="img-div1">
								<img src="images/award/7_DSCF4029.png" class="img-responsive thumbnail">
								  <p class="boardAwardOffice">7. ที่ปรึกษาบริหารโครงการและควบคุมงานก่อสร้าง งานก่อสร้างทางเชื่อมระบบสาธารณูปโภค เข้าสู่ที่ตั้งโครงการของ ปตท. ณ อ.วังจันทร์ โดยไม่เกิดอุบัติเหตุถึงขั้นหยุดงานเป็นจำนวน 700,000 ชั่วโมง </p>
							  </div>
							</div>
						  </div>  

						  <div class="col-lg-4 center">
							<div class="product-div1">
							  <div class="img-div1">
								<img src="images/award/8_DSCF4031.png" class="img-responsive thumbnail">
								  <p class="boardAwardOffice">8. ที่ปรึกษาบริหารโครงการและควบคุมงานก่อสร้าง งานโครงสร้างและสถาปัตยกรรม โดยไม่เกิดอุบัติเหตุถึงขั้นหยุดงานเป็นจำนวน 2,000,000 ชั่วโมง </p>
							  </div>
							</div>
						  </div>

						  <div class="col-lg-4 center">
							<div class="product-div1">
							  <div class="img-div1">
								<img src="images/award/9_DSCF4030.png" class="img-responsive thumbnail">
								  <p class="boardAwardOffice">9. ที่ปรึกษาบริหารโครงการและควบคุมงานก่อสร้าง งานโครงสร้างและสถาปัตยกรรม โดยไม่เกิดอุบัติเหตุถึงขั้นหยุดงานเป็นจำนวน 3,400,000 ชั่วโมง </p>
							  </div>
							</div>
						  </div>
						</div>

						<div style="text-align: center;margin-top: 30px;"><img src="images/award-inof.png"></div>

					  <!-- //BLOG POST -->
					  </div><!-- //BLOG BLOCK -->

					  </div>
					</div>
			<%end if%>
			<!--======================================================= End ======================================================-->
			</section>
			
		</div>

		<!--====================================== Footer ======================================-->
			<!--#include file='i_footer.asp'-->
		<!--======================================== End =======================================-->

		<script type="text/javascript" src="scripts/modernizr.custom.js"></script>
		<%if session("lang")="E" then%><script type="text/javascript" src="scripts/jquery.dlmenu-en.js"></script><%else%><script type="text/javascript" src="scripts/jquery.dlmenu.js"></script><%end if%>
		<script>
			$(function() {
				$('#dl-menu').dlmenu();
			});
		</script>
		<!-- js/scroll-hint -->
		<script src="scripts/scroll-hint.js"></script>
		<!-- js/scroll-hint -->
		<script>
			new ScrollHint('.js-scrollable', {
			});
		</script>
	</div>
	 <!--#include file="i_googleAnalytics.asp"-->	  
</body>
</html>