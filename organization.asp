﻿<!DOCTYPE html>

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->
<%
session("cur_page")="IR " &  listed_share & " Organization Chart"
session("page_asp")="organization.asp"
page_name="Organization Chart"
%>

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<TITLE><%=message_title%> :: <%=page_name%></TITLE>
	<META name="Title" content="<%=message_metatitle%>">
	<META name="Description" content="<%=message_description%>">
	<META name="Keywords" content="<%=message_keyword%>">	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="author" content="">
	<link rel="shortcut icon" href="<%=img_icon%>">
	<link href="style_listed.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../function2007_utf8.js"></script>
	<!-- CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/flexslider.css" rel="stylesheet" type="text/css">
	<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
	<!-- ADD CSS -->
	<link href="css/component.css" rel="stylesheet" type="text/css">
	<link href="css/custom.css" rel="stylesheet" type="text/css">
	<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">
	<!-- FONTS -->
	<link href="css/font_add.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- Mouse Hover CSS -->
	<script src="scripts/jquery.min.js" type="text/javascript"></script>
	<script src="scripts/jquery.nicescroll.min.js"></script>
	<!-- SCRIPTS -->
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<!--[if IE]><html class="ie" lang="en"> <![endif]-->
	<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
	<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
	<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
	<script src="scripts/animate.js" type="text/javascript"></script>
	<script src="scripts/myscript.js" type="text/javascript"></script>
	<script src="scripts/owl.carousel.js" type="text/javascript"></script>
	<!-- NEW -->
	<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
	<link href="css/site.css" rel="stylesheet" type="text/css" />
	<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
	<!-- scroll-hint -->
	<link rel="stylesheet" href="css/scroll-hint.css">
</head>

<body class="main">

	<!-- PRELOADER -->
	<img id="preloader" src="images/preloader.gif" alt="">
	<!-- //PRELOADER -->
	<div class="preloader_hide">
		<div id="page" class="single_page">

			<!--================================ Menu Top ==================================-->
				<!--#include file='i_menu_top.asp'-->
			<!--=================================== End ====================================-->

			<!--=============================== Menu Left ==================================-->
				<!--#include file='i_menu_left.asp'-->
			<!--================================== End =====================================-->

			<!--============================ Title Menu =============================-->
			<section class="mbr-section article">
				<div class="container">
					<div class="bigTopic">
						<h1 class="bigTopicText"><%=arr_menu_desc(25)%></h1>
					</div>
				</div>
			</section>
			<!--============================== End ==================================-->

			<section class="mbr-section article sectionSub">
			<!--===================================================== Content ====================================================-->
			<%if session("lang")="E" then%>
				<div class="container" style="padding-right: 15px;padding-left: 15px;">
					  <!-- <div style="margin-bottom: 25px;"> -->
						  <!-- <p class="ContextOfOrganizationText">โครงสร้างการจัดการของบริษัทฯ ประกอบด้วย คณะกรรมการบริษัท และคณะกรรมการชุดย่อยจำนวน 4 คณะ ประกอบด้วย คณะกรรมการตรวจสอบ คณะกรรมการบริหาร คณะกรรมการสรรหาและพิจารณาค่าตอบแทน และคณะกรรมการบริหารความเสี่ยง ทั้งนี้ คณะกรรมการและผู้บริหารของบริษัทฯ มีคุณสมบัติครบถ้วนและไม่มีลักษณะต้องห้ามตามมาตรา 68 แห่งพระราชบัญญัติ มหาชนจำกัด พ.ศ. 2535 (รวมฉบับที่แก้ไข) </p> -->

						  <!-- <p class="ContextOfOrganizationText">รวมทั้งไม่มีลักษณะต้องห้ามและลักษณะที่แสดงถึงการขาดความเหมาะสมที่จะได้รับความไว้วางใจให้บริหารจัดการกิจการที่มีมหาชนเป็นผู้ถือหุ้นตามมาตรา 89/3 และมาตรา 89/6 แห่งพระราชบัญญัติหลักทรัพย์และตลาดหลักทรัพย์ พ.ศ. 2535 (รวมฉบับที่แก้ไข) และตามประกาศคณะกรรมการกำกับตลาดทุนที่เกี่ยวข้อง ทั้งนี้ โครงสร้างการจัดการภายในของบริษัทฯ ซึ่งได้รับการอนุมัติจากที่ประชุมคณะกรรมการบริษัท ครั้งที่ 2/2561 เมื่อวันที่ 27 มีนาคม 2561 มีรายละเอียดดังนี้ </p> -->

						  <!-- <p class="ContextOfOrganizationText"></p> -->
					  <!-- </div>  -->
					  <!-- END container -->

					  <div style="margin-top: 40px;"><img src="images/OrganizationChart_en.png"></div>
					  <div style="text-align: center;margin-bottom: 35px;margin-top: 40px;"><img src="images/OrganizationChart_info.png"></div>
					</div>
			<%else%>
				<div class="container" style="padding-right: 15px;padding-left: 15px;">
						<!-- <div style="margin-bottom: 25px;"> -->
							  <!-- <p class="ContextOfOrganizationText">โครงสร้างการจัดการของบริษัทฯ ประกอบด้วย คณะกรรมการบริษัท และคณะกรรมการชุดย่อยจำนวน 4 คณะ ประกอบด้วย คณะกรรมการตรวจสอบ คณะกรรมการบริหาร คณะกรรมการสรรหาและพิจารณาค่าตอบแทน และคณะกรรมการบริหารความเสี่ยง ทั้งนี้ คณะกรรมการและผู้บริหารของบริษัทฯ มีคุณสมบัติครบถ้วนและไม่มีลักษณะต้องห้ามตามมาตรา 68 แห่งพระราชบัญญัติ มหาชนจำกัด พ.ศ. 2535 (รวมฉบับที่แก้ไข) </p> -->

							  <!-- <p class="ContextOfOrganizationText">รวมทั้งไม่มีลักษณะต้องห้ามและลักษณะที่แสดงถึงการขาดความเหมาะสมที่จะได้รับความไว้วางใจให้บริหารจัดการกิจการที่มีมหาชนเป็นผู้ถือหุ้นตามมาตรา 89/3 และมาตรา 89/6 แห่งพระราชบัญญัติหลักทรัพย์และตลาดหลักทรัพย์ พ.ศ. 2535 (รวมฉบับที่แก้ไข) และตามประกาศคณะกรรมการกำกับตลาดทุนที่เกี่ยวข้อง ทั้งนี้ โครงสร้างการจัดการภายในของบริษัทฯ ซึ่งได้รับการอนุมัติจากที่ประชุมคณะกรรมการบริษัท ครั้งที่ 2/2561 เมื่อวันที่ 27 มีนาคม 2561 มีรายละเอียดดังนี้ </p> -->

							  <!-- <p class="ContextOfOrganizationText"></p> -->
				  <!-- </div>  -->
				  <!-- END container -->

				  <div style="margin-top: 40px;"><img src="images/OrganizationChart.png"></div>
				  <div style="text-align: center;margin-bottom: 35px;margin-top: 40px;"><img src="images/OrganizationChart_info.png"></div>
				</div>
			<%end if%>
			<!--======================================================= End ======================================================-->
			</section>
			
		</div>

		<!--====================================== Footer ======================================-->
			<!--#include file='i_footer.asp'-->
		<!--======================================== End =======================================-->

		<script type="text/javascript" src="scripts/modernizr.custom.js"></script>
		<%if session("lang")="E" then%><script type="text/javascript" src="scripts/jquery.dlmenu-en.js"></script><%else%><script type="text/javascript" src="scripts/jquery.dlmenu.js"></script><%end if%>
		<script>
			$(function() {
				$('#dl-menu').dlmenu();
			});
		</script>
		<!-- js/scroll-hint -->
		<script src="scripts/scroll-hint.js"></script>
		<!-- js/scroll-hint -->
		<script>
			new ScrollHint('.js-scrollable', {
			});
		</script>
	</div>
	 <!--#include file="i_googleAnalytics.asp"-->	  
</body>
</html>