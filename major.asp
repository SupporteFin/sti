﻿<!DOCTYPE html>

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->
<!--#include file = "../../formatdate_utf8.asp" -->
<!-- #include file = "../../i_conpsims.asp" -->
<!--#include file = "../../function_asp2007.asp" -->

<%
session("cur_page")="IR " &  listed_share & " Major Shareholder"
session("page_asp")="major.asp"
page_name="Major Shareholder"
%>

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<TITLE><%=message_title%> :: <%=page_name%></TITLE>
	<META name="Title" content="<%=message_metatitle%>">
	<META name="Description" content="<%=message_description%>">
	<META name="Keywords" content="<%=message_keyword%>">	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="author" content="">
	<link rel="shortcut icon" href="<%=img_icon%>">
	<link href="style_listed.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../function2007_utf8.js"></script>
	<!-- CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/flexslider.css" rel="stylesheet" type="text/css">
	<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
	<!-- ADD CSS -->
	<link href="css/component.css" rel="stylesheet" type="text/css">
	<link href="css/custom.css" rel="stylesheet" type="text/css">
	<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">
	<!-- FONTS -->
	<link href="css/font_add.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- Mouse Hover CSS -->
	<script src="scripts/jquery.min.js" type="text/javascript"></script>
	<script src="scripts/jquery.nicescroll.min.js"></script>
	<!-- SCRIPTS -->
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<!--[if IE]><html class="ie" lang="en"> <![endif]-->
	<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
	<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
	<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
	<script src="scripts/animate.js" type="text/javascript"></script>
	<script src="scripts/myscript.js" type="text/javascript"></script>
	<script src="scripts/owl.carousel.js" type="text/javascript"></script>
	<!-- NEW -->
	<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
	<link href="css/site.css" rel="stylesheet" type="text/css" />
	<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
	<!-- scroll-hint -->
	<link rel="stylesheet" href="css/scroll-hint.css">
</head>

<body class="main">

	<!-- PRELOADER -->
	<img id="preloader" src="images/preloader.gif" alt="">
	<!-- //PRELOADER -->
	<div class="preloader_hide">
		<div id="page" class="single_page">

			<!--================================ Menu Top ==================================-->
				<!--#include file='i_menu_top.asp'-->
			<!--=================================== End ====================================-->

			<!--=============================== Menu Left ==================================-->
				<!--#include file='i_menu_left.asp'-->
			<!--================================== End =====================================-->

			<!--============================ Title Menu =============================-->
			<section class="mbr-section article">
				<div class="container">
					<div class="bigTopic">
						<h1 class="bigTopicText"><%=arr_menu_desc(35)%></h1>
					</div>
				</div>
			</section>
			<!--============================== End ==================================-->

			<section class="mbr-section article sectionSub">
			<!--===================================================== Content ====================================================-->
				<%if session("lang")="E" then%>
				<div class="container">
					<p style="font-size: 22px;font-weight: 500;margin-right: 15px;margin-top: 3px;color: #006c9d;text-align: left;">Major Shareholders :</p>
					
						<p class="bigTopicSubText" style="margin-bottom: 35px;">List of the top 10 major shareholders including the number of share and the percentage of total share as of book-closing date April 30, 2019. </p>
					
					<div class="table-data js-scrollable scroll-hint" style="position: relative; overflow: auto;">
						<table border="0" cellspacing="0" cellpadding="0" class="table-width table-MajorShareholder">
							<tbody style="font-size: 15.5px;">
								<tr class="Head-table-MajorShareholder">
									 <th width="5%" align="center" class="topicTeble"></th>
									 <th width="55%" align="center" class="topicTeble" style="">Shareholders</th> 
									 <th width="20%" align="center" class="topicTeble">Number of shares</th>
									 <th width="20%" align="center" class="topicTeble">%</th>
								</tr>
								
									<tr class="even">
										 <td align="center">1</td>
										 <td align="left">Univentures Capital Co., Ltd.</td>
										 <td align="right">76,551,100.00</td>
										 <td align="right">28.56</td>
									  </tr> 
									  <tr class="even">
										 <td align="center">2</td>
										 <td align="left">Mr. Somkiat Silawatanawong</td>
										 <td align="right">40,000,000.00</td>
										 <td align="right">14.93</td>
									  </tr> 
									  <tr class="even">
										 <td align="center">3</td>
										 <td align="left">Mr. Pairuch Laoprasert</td>
										 <td align="right">31,219,000.00</td>
										 <td align="right">11.65</td>
									  </tr> 
									  <tr class="even">
										 <td align="center">4</td>
										 <td align="left">Mr. Somchit Peumpremsuk</td>
										 <td align="right">30,000,000.00</td>
										 <td align="right">11.19</td>
									  </tr> 
									  <tr class="even">
										 <td align="center">5</td>
										 <td align="left">Mr. Issarin Suwatano</td>
										 <td align="right">12,000,000.00</td>
										 <td align="right">4.48</td>
									  </tr> 
									  <tr class="even">
										 <td align="center">6</td>
										 <td align="left">Mr. Kittisak Suphakawat</td>
										 <td align="right">8,000,000.00</td>
										 <td align="right">2.99</td>
									  </tr> 
									  <tr class="even">
										 <td align="center">7</td>
										 <td align="left">Mr. Rangsan Phatcharakitti</td>
										 <td align="right">6,100,000.00</td>
										 <td align="right">2.28</td>
									  </tr> 
									  <tr class="even">
										 <td align="center">8</td>
										 <td align="left">Mr. Terdsakul Vividworn</td>
										 <td align="right">4,100,000.00</td>
										 <td align="right">1.53</td>
									  </tr> 
									  
									 
									  <tr class="even evenImportant">
										  <td align="center"></td>
										  <td align="left">Total</td>
										  <td align="right">207,970,100.00</td>
										  <td align="right">77.61</td>
									</tr>
									
							</tbody>
						</table>
						<br><br>
						<br><br>
					</div>
				</div>
				<%else%>
				<div class="container">
					<p style="font-size: 22px;font-weight: 500;margin-right: 15px;margin-top: 3px;color: #006c9d;text-align: left;">ผู้ถือหุ้นรายใหญ่ :</p>
					
						<p class="bigTopicSubText" style="margin-bottom: 35px;">รายชื่อผู้ถือหุ้น 10 รายแรกและสัดส่วนการถือหุ้น ณ วันที่ 30 เมษายน 2019</p>
					
					<div class="table-data js-scrollable scroll-hint" style="position: relative; overflow: auto;">
						<table border="0" cellspacing="0" cellpadding="0" class="table-width table-MajorShareholder">
							<tbody style="font-size: 15.5px;">
								<tr class="Head-table-MajorShareholder">
									 <th width="5%" align="center" class="topicTeble"></th>
									 <th width="55%" align="center" class="topicTeble" style="">รายชื่อผู้ถือหุ้น</th> 
									 <th width="20%" align="center" class="topicTeble">จำนวนหุ้น</th>
									 <th width="20%" align="center" class="topicTeble">ร้อยละ</th>
								</tr>
								
									<tr class="even">
										 <td align="center">1</td>
										 <td align="left">บริษัท ยูนิเวนเจอร์ แคปปิตอล จำกัด</td>
										 <td align="right">76,551,100.00</td>
										 <td align="right">28.56</td>
									  </tr> 
									  <tr class="even">
										 <td align="center">2</td>
										 <td align="left">นายสมเกียรติ ศิลวัฒนาวงศ์</td>
										 <td align="right">40,000,000.00</td>
										 <td align="right">14.93</td>
									  </tr> 
									  <tr class="even">
										 <td align="center">3</td>
										 <td align="left">นายไพรัช เล้าประเสริฐ</td>
										 <td align="right">31,219,000.00</td>
										 <td align="right">11.65</td>
									  </tr> 
									  <tr class="even">
										 <td align="center">4</td>
										 <td align="left">นายสมจิตร์ เปี่ยมเปรมสุข</td>
										 <td align="right">30,000,000.00</td>
										 <td align="right">11.19</td>
									  </tr> 
									  <tr class="even">
										 <td align="center">5</td>
										 <td align="left">นายอิสรินทร์ สุวัฒโน</td>
										 <td align="right">12,000,000.00</td>
										 <td align="right">4.48</td>
									  </tr> 
									  <tr class="even">
										 <td align="center">6</td>
										 <td align="left">นายกิตติศักดิ์ สุภาควัฒน์</td>
										 <td align="right">8,000,000.00</td>
										 <td align="right">2.99</td>
									  </tr> 
									  <tr class="even">
										 <td align="center">7</td>
										 <td align="left">นายรังสรรค์ พัชรากิตติ</td>
										 <td align="right">6,100,000.00</td>
										 <td align="right">2.28</td>
									  </tr> 
									  <tr class="even">
										 <td align="center">8</td>
										 <td align="left">นายเทอดสกุล วิวิธวร</td>
										 <td align="right">4,100,000.00</td>
										 <td align="right">1.53</td>
									  </tr> 
									  
									 
									  <tr class="even evenImportant">
										  <td align="center"></td>
										  <td align="left">รวม</td>
										  <td align="right">207,970,100.00</td>
										  <td align="right">77.61</td>
									</tr>
									
							</tbody>
						</table>
						<br><br>
						<br><br>
					</div>
				</div>
				<%end if%>
			<!--======================================================= End ======================================================-->
			</section>
			
		</div>

		<!--====================================== Footer ======================================-->
			<!--#include file='i_footer.asp'-->
		<!--======================================== End =======================================-->

		<script type="text/javascript" src="scripts/modernizr.custom.js"></script>
		<%if session("lang")="E" then%><script type="text/javascript" src="scripts/jquery.dlmenu-en.js"></script><%else%><script type="text/javascript" src="scripts/jquery.dlmenu.js"></script><%end if%>
		<script>
			$(function() {
				$('#dl-menu').dlmenu();
			});
		</script>
		<!-- js/scroll-hint -->
		<script src="scripts/scroll-hint.js"></script>
		<!-- js/scroll-hint -->
		<script>
			new ScrollHint('.js-scrollable', {
			});
		</script>
	</div>
	 <!--#include file="i_googleAnalytics.asp"-->	  
</body>
</html>