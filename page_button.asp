<div align="center">
<div class="paginator">
<div id="paging_top" class="paging" style="padding-top: 20px;font-size: 13px;">

<%	 
     if session("totalpage") <> "" and IsNumeric(session("totalpage"))  then
          if session("totalpage") > 1 then 
	    %><strong>Page: </strong><%=session("pageno")%> of <%=session("totalpage")%> page(s) &nbsp;&nbsp;&nbsp;<%
               const_pagesize = session("totalpage")
               if session("totalpage") >= 1  then
                     if  cint(session("totalpage"))  = 1 then
                     %>
                        <span class="activepage">1</span>
                <%              
                     else

                          if session("pageno_first") = "" or request("pageno_first") = ""  then
                              session("pageno_first") = 1
                          else
                              session("pageno_first") = request("pageno_first")
                          end if
                          
                          if  cint(session("pageno")) = cint(session("totalpage")) then            
                               chk_pageno_first = cint(session("totalpage")) mod const_pagesize
                                if chk_pageno_first  >0  and chk_pageno_first <= const_pagesize-1 then
                                   session("pageno_first") = (cint(session("totalpage"))-chk_pageno_first)+1
                                end if                       
                        end if                              
                        
                          if cint(session("pageno_first")) >= 1 then
                               if  session("pageno") <> 1 then
                                 if session("pageno") <= cint(session("pageno_first")) then 
                                     if cint(session("pageno")) =  cint(session("pageno_first"))  then
                                        if cint(session("pageno_first")) = cint(session("totalpage")) then
                                         %>
                                       <a  class="page"  href="javascript:SelectedPageChangedPrev('<%=session("page_asp")%>','<%=cint(session("pageno"))-1%>','<%=(cint(session("pageno_first"))-const_pagesize)+1%>');">&laquo;</a>       
                                       <%                                            
                                        else
                                          %>
                                       <a  class="page"   href="javascript:SelectedPageChangedPrev('<%=session("page_asp")%>','<%=cint(session("pageno"))-1%>','<%=(cint(session("pageno_first"))-const_pagesize)%>');">&laquo;</a>     
                                       <%                                            
                                        end if
                                   end if
                                 else
                                  %>
                                 <a class="page"  href="javascript:SelectedPageChangedPrev('<%=session("page_asp")%>','<%=cint(session("pageno"))-1%>','<%=cint(session("pageno_first"))%>');">&laquo;</a>
                                 <%                                
                                 end if
                              end if
                             
                             if  cint(session("totalpage")) =  cint(session("pageno_first")) then
                                 if  cint(session("pageno_first")) - const_pagesize < 1 then
                                     session("pageno_first")  = 1
                                 end if
                             end if
                         end if
                         
                                if cint(session("pageno_first")) = cint(session("totalpage")) then
                                    if  cint(session("pageno")) = cint(session("totalpage")) then            
                                        chk_pageno_first = cint(session("totalpage")) mod const_pagesize
                                         if chk_pageno_first  >0  and chk_pageno_first <= const_pagesize-1 then
                                            session("pageno_first") = (cint(session("totalpage"))-chk_pageno_first)+1
                                         end if         
                                    else
                                    session("pageno_first")  = (cint(session("pageno_first") )-const_pagesize)+1                                   
                                   end if                                      
                                else
                                   session("pageno_first") = session("pageno_first")
                                end if 
                                 for i = session("pageno_first") to (cint(session("pageno_first"))+const_pagesize)-1
                                      if i <= session("totalpage") and i >=1  then
                                          if Cint(session("pageno")) = i then					
                                          %>
                                             <span class="activepage"><%=i%></span>
                                          <%
                                          else
                                          %>
                                             <a  class="page"   href="javascript:SelectedPageChanged('<%=session("page_asp")%>','<%=i%>');"><font class="font_page"><%=i%></font></a>
                                          <%
                                          end if
                                      end if
                                 next
                                 if session("pageno") <> session("totalpage")  then
                                                if session("pageno") >= (cint(session("pageno_first"))+const_pagesize)-1 then
                                                %>
                                                <a  class="page"   href="javascript:SelectedPageChangedNext('<%=session("page_asp")%>','<%=cint(session("pageno"))+1%>');">&raquo;</a>
                                                <%                           
                                                else
                                                %>
                                                <a  class="page"   href="javascript:SelectedPageChanged('<%=session("page_asp")%>','<%=cint(session("pageno"))+1%>');">&raquo;</a>
                                                <%                                
                                                end if                                                             
                                 end if
                     end if
               end if
          end if
     end if
     
%>
<input type = "hidden" id = "totalpage" name = "totalpage" value="<%=session("totalpage")%>">	
<input type = "hidden" id = "pageno" name = "pageno"   value="<%=session("pageno")%>">
<input type = "hidden" id = "pageno_first" name = "pageno_first"   value="<%=session("pageno_first")%>">
<input type="hidden" name="date" id="date" value="<%=start_date%>">
<input type="hidden" name="chkFrist" id="chkFrist" value="<%=theFrist%>">
<script language="javascript">
	function SelectedPageChanged(page,selectedPage)
	{
			document.getElementById("pageno").value = selectedPage;
         document.forms["frm1"].submit();                  
	}
   
   	function SelectedPageChangedMore(page,selectedPage)
	{
			document.getElementById("pageno_first").value = selectedPage;
         document.getElementById("pageno").value = selectedPage;
         document.forms["frm1"].submit();        
	}
   
    function SelectedPageChangedNext(page,selectedPage)
	{
			document.getElementById("pageno_first").value = selectedPage;
         document.getElementById("pageno").value = selectedPage;
         document.forms["frm1"].submit();        
	}
   
    function SelectedPageChangedPrev(page,selectedPage,constPageFrist)
	{
        document.getElementById("pageno_first").value = constPageFrist;
         document.getElementById("pageno").value = selectedPage;
         document.forms["frm1"].submit();        
	}
   
</script>
</div></div>
</div>
<br>