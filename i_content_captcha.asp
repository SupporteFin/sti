﻿
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=windows-874">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
	  <LINK type="text/css" rel="stylesheet" href="style_listed.css" >
	  <script language="javascript" src="Scripts/jquery.min.js"></script>
	  
	  <style>
		input[type=text] {
			border-radius: 0px;
			border: 1px solid #006c9d;
			line-height: 20px;
			font-size: 13px;
			font-weight: normal;
			margin-bottom: 4px;
		}
		input[type=text]:focus {
			border: 1px solid #006c9d;
		}
	</style>
</head>
<%if session("page_asp")="request_alerts.asp" then%>
		<body class="bg_email_alert6">
<%elseif session("page_asp")="agenda.asp" or session("page_asp")="complaints.asp" then%>
		<body>
<%else%>
		<body class="bg_email_alert7">
<%end if%>
			<%if session("page_asp")="agenda.asp" or session("page_asp")="complaints.asp" then%>
				<table width="100%"  border="0" cellspacing="0" cellpadding="0" style="background:#FFFFFF;">
					<tr>
						<td align="center" valign="top" style="padding-left:0px;padding-bottom:5px;">
							<img src="i_content_captcha_image.asp" id="imgCaptcha" align="absmiddle" width="150px">
						</td>
						<td align="left" valign="top">
						&nbsp;<input type="hidden" name="valueCaptcha" id="valueCaptcha"  size="10" class="style_textbox" >
						</td>
					</tr>
				</table>
			<%else%>
				<table width="100%"  border="0" cellspacing="0" cellpadding="0">
					<tr>
						   <td align="right" valign="top">
								<img src="i_content_captcha_image.asp" id="imgCaptcha" align="absmiddle">
						   </td>
						   <td align="left" valign="top">
						   &nbsp;<input name="resultCaptcha" id="resultCaptcha" type="text" size="13" style="width:80px;" class="style_textbox" ><input type="hidden" name="valueCaptcha" id="valueCaptcha"  size="15" class="style_textbox" >
						   </td>
					</tr>
				</table>
			<%end if%>
			<script LANGUAGE="JavaScript">
					var currentDate = new Date();
					var sTime = currentDate.getTime(); 
					var url = '../../IRPlus/IC_IRMemberService.asp?key=captcha&type=json&timestamp='+sTime;
					var request = jQuery.post(url,null,function(d){   
						//alert(d.value);
						jQuery("#valueCaptcha").val(d.value);
					},"json"); 	
			</script>
</body>
</html>
