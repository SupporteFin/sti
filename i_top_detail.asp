<style type="text/css">
	.menu_block {
		position: fixed;
		z-index: 9999;
		left: 0;
		top: 0;
		right: 0;
		height: 100px;
		width: 100%;
		background-color: #fff;
		box-shadow: 0 2px 3px rgba(0,0,0,0.1);
	}
	.ReadDetail {
		margin-top: 24px;
		text-align: center;
	}
</style>

<div class="" style="padding-bottom: 70px;">
	<!-- PAGE -->
	<div id="page" class="single_page">
		<!-- HEADER -->
		<header style="font-family: 'promptextralight';">
			<!-- MENU BLOCK -->
			<div class="menu_block">
				<!-- CONTAINER -->
				<div class="container clearfix">
					<!-- LOGO -->
					<div class="ReadDetail">
						<a href="https://www.sti.co.th/th/index.php"><img src="images/logo005.png" width="180" alt=""></a>
					</div><!-- //LOGO -->
					<!-- MENU -->
				</div><!-- //MENU BLOCK -->
			</div><!-- //CONTAINER -->
		</header><!-- //HEADER -->	
	</div> 	<!-- END id="page" -->
</div>