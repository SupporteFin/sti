﻿<!DOCTYPE html>

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->
<%
session("cur_page")="IR " &  listed_share & " Board of Director"
session("page_asp")="board.asp"
page_name="Board of Director"
%>

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<TITLE><%=message_title%> :: <%=page_name%></TITLE>
	<META name="Title" content="<%=message_metatitle%>">
	<META name="Description" content="<%=message_description%>">
	<META name="Keywords" content="<%=message_keyword%>">	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="author" content="">
	<link rel="shortcut icon" href="<%=img_icon%>">
	<link href="style_listed.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../function2007_utf8.js"></script>
	<!-- CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/flexslider.css" rel="stylesheet" type="text/css">
	<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
	<!-- ADD CSS -->
	<link href="css/component.css" rel="stylesheet" type="text/css">
	<link href="css/custom.css" rel="stylesheet" type="text/css">
	<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">
	<!-- FONTS -->
	<link href="css/font_add.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- Mouse Hover CSS -->
	<script src="scripts/jquery.min.js" type="text/javascript"></script>
	<script src="scripts/jquery.nicescroll.min.js"></script>
	<!-- SCRIPTS -->
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<!--[if IE]><html class="ie" lang="en"> <![endif]-->
	<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
	<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
	<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
	<script src="scripts/animate.js" type="text/javascript"></script>
	<script src="scripts/myscript.js" type="text/javascript"></script>
	<script src="scripts/owl.carousel.js" type="text/javascript"></script>
	<!-- NEW -->
	<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
	<link href="css/site.css" rel="stylesheet" type="text/css" />
	<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
	<!-- scroll-hint -->
	<link rel="stylesheet" href="css/scroll-hint.css">
	
	<!-- tooltipster -->
  <link rel="stylesheet" type="text/css" href="css/tooltipster.bundle.min.css" />
  <!-- <script type="text/javascript" src="http://code.jquery.com/jquery-1.10.0.min.js"></script> -->
  <script type="text/javascript" src="scripts/tooltipster.bundle.min.js"></script>
  <style type="text/css">
    .tooltip_templates {
      display: none;
    }
    .modal-container .modal-button {
      width: 34px;
      height: 32px;
      line-height: 30px;
      font-size: 32px;
      border: none;
      border-radius: 50px;
      outline: none;
      background-color: #32abca;
      color: #ffffff;
      opacity: 1;
      font-family: 'psl_display_pro';
    }
    .TooltipButton {
      margin-top: 15px;
    }
    .toolLeft{
      float: left;
      margin-right: 15px;
      position: static;
      opacity: 1;
    }
    .tooltipster-sidetip .tooltipster-box {
      background-color: #32abca;
      border:2px solid #32abca;
    }
    .boardModalText {
      color: #ffffff;
      font-size: 26px;
      margin: 10px 0px 15px 0px;
    }
    .boardInofText {
      font-size: 16.5px;
      line-height: 1.3;
    }
    .tooltipster-content {
      overflow: unset;
      overflow: none;
    }
    .board {
      margin-bottom: 35px;
    }
    .board a {
      color: #ffffff;
      font-size: 16.5px;
      text-decoration: underline;
    }
    .board a:hover {
      color: #054d6d;
    }
    .tooltipster-sidetip.tooltipster-top .tooltipster-arrow-border {
      border-top-color: #32abca;
    }
    .tooltipster-sidetip.tooltipster-top .tooltipster-arrow-background {
      border-top-color: #32abca;
    }
    @media screen and (max-width: 700px) {
      .modal-container .modal-button {
          width: 28px;
          height: 27px;
          font-size: 26px;
      }
    }
    @media screen and (max-width: 600px) {
      .modal-container .modal-button {
        width: 23px;
        height: 22px;
        font-size: 21px;
        line-height: 22px;
      }
      .toolLeft {
        margin-right: 5px;
        margin-bottom: 10px;
      }
      .BoardHead {
        margin-top: 60px;
      }
    }
  </style>
  <script>
    $(document).ready(function() {
      $('.tooltip').tooltipster({
          animation: 'fade',
          delay: 200,
          theme: 'tooltipster-punk',
          trigger: 'click',
          contentCloning: true,
          interactive: true
      });
      $('.tooltip2').tooltipster({
          animation: 'fade',
          delay: 200,
          theme: 'tooltipster-punk',
          trigger: 'click',
          contentCloning: true,
          interactive: true
      });
      $('.tooltip3').tooltipster({
          animation: 'fade',
          delay: 200,
          theme: 'tooltipster-punk',
          trigger: 'click',
          contentCloning: true,
          interactive: true
      });
      $('.tooltip4').tooltipster({
          animation: 'fade',
          delay: 200,
          theme: 'tooltipster-punk',
          trigger: 'click',
          contentCloning: true,
          interactive: true
      });
      $('.tooltip5').tooltipster({
          animation: 'fade',
          delay: 200,
          theme: 'tooltipster-punk',
          trigger: 'click',
          contentCloning: true,
          interactive: true
      });
      $('.tooltip6').tooltipster({
          animation: 'fade',
          delay: 200,
          theme: 'tooltipster-punk',
          trigger: 'click',
          contentCloning: true,
          interactive: true
      });
      $('.tooltip7').tooltipster({
          animation: 'fade',
          delay: 200,
          theme: 'tooltipster-punk',
          trigger: 'click',
          contentCloning: true,
          interactive: true
      });
      $('.tooltip8').tooltipster({
          animation: 'fade',
          delay: 200,
          theme: 'tooltipster-punk',
          trigger: 'click',
          contentCloning: true,
          interactive: true
      });
      $('.tooltip9').tooltipster({
          animation: 'fade',
          delay: 200,
          theme: 'tooltipster-punk',
          trigger: 'click',
          contentCloning: true,
          interactive: true
      });
      $('.tooltip10').tooltipster({
          animation: 'fade',
          delay: 200,
          theme: 'tooltipster-punk',
          trigger: 'click',
          contentCloning: true,
          interactive: true
      });
      $('.tooltip11').tooltipster({
          animation: 'fade',
          delay: 200,
          theme: 'tooltipster-punk',
          trigger: 'click',
          contentCloning: true,
          interactive: true
      });
      $('.tooltip12').tooltipster({
          animation: 'fade',
          delay: 200,
          theme: 'tooltipster-punk',
          trigger: 'click',
          contentCloning: true,
          interactive: true
      });
    });
  </script>
<!-- End tooltipster -->
</head>

<body class="main">

	<!-- PRELOADER -->
	<img id="preloader" src="images/preloader.gif" alt="">
	<!-- //PRELOADER -->
	<div class="preloader_hide">
		<div id="page" class="single_page">

			<!--================================ Menu Top ==================================-->
				<!--#include file='i_menu_top.asp'-->
			<!--=================================== End ====================================-->

			<!--=============================== Menu Left ==================================-->
				<!--#include file='i_menu_left.asp'-->
			<!--================================== End =====================================-->

			<!--============================ Title Menu =============================-->
			<section class="mbr-section article">
				<div class="container">
					<div class="bigTopic">
						<h1 class="bigTopicText"><%=arr_menu_desc(24)%></h1>
					</div>
				</div>
			</section>
			<!--============================== End ==================================-->

			<!--===================================================== Content ====================================================-->
			<%if session("lang")="E" then%>
				<section class="mbr-section article">
					<div class="container" style="padding-right: 15px;padding-left: 15px;">
						  <div class="row">
						
							<!-- BLOG BLOCK -->
						  <div class="blog_block col-lg-12 col-md-12 padbot50" style="transform: matrix(1, 0, 0, 1, 0, 0);">           
							<!-- BLOG POST -->
							<img src="images/BoardOfDirector1.png">

							<!-- Tooltip button -->
							<div class="TooltipButton ButtonBod1">
							  <!-- 9 -->
							  <span class="tooltip9 toolLeft" data-tooltip-content="#tooltip_content9">
								<div class="modal-container">
								  <button class="modal-button">9</button>
								</div>
							  </span>
							  <!-- 8 -->
							  <span class="tooltip8 toolLeft" data-tooltip-content="#tooltip_content8">
								<div class="modal-container">
								  <button class="modal-button">8</button>
								</div>
							  </span>
							  <!-- 7 -->
							  <span class="tooltip7 toolLeft" data-tooltip-content="#tooltip_content7">
								<div class="modal-container">
								  <button class="modal-button">7</button>
								</div>
							  </span>
							  <!-- 6 -->
							  <span class="tooltip6 toolLeft" data-tooltip-content="#tooltip_content6">
								<div class="modal-container">
								  <button class="modal-button">6</button>
								</div>
							  </span>
							  <!-- 5 -->
							  <span class="tooltip5 toolLeft" data-tooltip-content="#tooltip_content5">
								<div class="modal-container">
								  <button class="modal-button">5</button>
								</div>
							  </span>
							  <!-- 3 -->
							  <span class="tooltip3 toolLeft" data-tooltip-content="#tooltip_content3">
								<div class="modal-container">
								  <button class="modal-button">3</button>
								</div>
							  </span>
							  <!-- 1 -->
							  <span class="tooltip toolLeft" data-tooltip-content="#tooltip_content">
								<div class="modal-container">
								  <button class="modal-button">1</button>
								</div>
							  </span>
							  <!-- 2 -->
							  <span class="tooltip2 toolLeft" data-tooltip-content="#tooltip_content2">
								<div class="modal-container">
								  <button class="modal-button">2</button>
								</div>
							  </span>
							  <!-- 4 -->
							  <span class="tooltip4 toolLeft" data-tooltip-content="#tooltip_content4">
								<div class="modal-container">
								  <button class="modal-button">4</button>
								</div>
							  </span>
							  <!-- 10 -->
							  <span class="tooltip10 toolLeft" data-tooltip-content="#tooltip_content10">
								<div class="modal-container">
								  <button class="modal-button">10</button>
								</div>
							  </span>
							  <!-- 11 -->
							  <span class="tooltip11 toolLeft" data-tooltip-content="#tooltip_content11">
								<div class="modal-container">
								  <button class="modal-button">11</button>
								</div>
							  </span>
							  <!-- 12 -->
							  <span class="tooltip12 toolLeft" data-tooltip-content="#tooltip_content12">
								<div class="modal-container">
								  <button class="modal-button">12</button>
								</div>
							  </span>
							</div> 
							<!-- END Tooltip button -->

							<!-- Tooltip Content -->
							<div class="TooltipContent">
							  <!-- 9 -->
							  <div class="tooltip_templates">
								<span id="tooltip_content9">
								  <div class="position">
									<h3 class="boardModalText">Mr. Khumpol Poonsonee</h3>
									<p class="boardInofText">Director/ Executive Committee</p>
									<div class="board"><a href="pdf\Board\Khumpol_EN.pdf" target="_blank">More</a></div>
								  </div>
								</span>
							  </div>
							  <!-- 8 -->
							  <div class="tooltip_templates">
								<span id="tooltip_content8">
								  <div class="position">
									<h3 class="boardModalText">Mr. Bundit Muangsornkeaw</h3>
									<p class="boardInofText">Director/ Executive Director</p>
									<div class="board"><a href="pdf\Board\Bundit_EN.pdf" target="_blank">More</a></div>
								  </div>
								</span>
							  </div>
							  <!-- 7 -->
							  <div class="tooltip_templates">
								<span id="tooltip_content7">
								  <div class="position">
									<h3 class="boardModalText">Mr. Worawat Srisa-an</h3>
									<p class="boardInofText">Director/ Executive Director/ <br>Member of Nomination and Remuneration Committee</p>
									<div class="board"><a href="pdf\Board\Worawat_EN.pdf" target="_blank">More</a></div>
								  </div>
								</span>
							  </div>
							  <!-- 6 -->
							  <div class="tooltip_templates">
								<span id="tooltip_content6">
								  <div class="position">
									<h3 class="boardModalText">Mr. Kittisak Suphakawat</h3>
									<p class="boardInofText">Director/ Executive Director/ <br>Chairman of Risk Management Committee</p>
									<div class="board"><a href="pdf\Board\Kittisak_EN.pdf" target="_blank">More</a></div>
								  </div>
								</span>
							  </div>
							  <!-- 5 -->
							  <div class="tooltip_templates">
								<span id="tooltip_content5">
								  <div class="position">
									<h3 class="boardModalText">Mr. Issarin Suwatano</h3>
									<p class="boardInofText">Director/ Executive Director/ <br>Chairman of Risk Management Committee</p>
									<div class="board"><a href="pdf\Board\Issarin_EN.pdf" target="_blank">More</a></div>
								  </div>
								</span>
							  </div>
							  <!-- 3 -->
							  <div class="tooltip_templates">
								<span id="tooltip_content3">
								  <div class="position">
									<h3 class="boardModalText">Mr. Somkiat Silawatanawong</h3>
									<p class="boardInofText">Executive Director/ Chief Executive Officer/ <br>Director of Nomination and Remuneration Committee</p>
									<div class="board"><a href="pdf\Board\Somkia_EN.pdf" target="_blank">More</a></div>
								  </div>
								</span>
							  </div>
							  <!-- 1 -->
							  <div class="tooltip_templates">
								<span id="tooltip_content">
								  <div class="position">
									<h3 class="boardModalText">Mr. Jumpol Sumpaopol</h3>
									<p class="boardInofText">Chairman/ Independent Director/ <br>Chairman of Nomination and Remuneration Committee</p>
									<div class="board"><a href="pdf\Board\Jumpol_EN.pdf" target="_blank">More</a></div>
								  </div>
								</span>
							  </div>
							  <!-- 2 -->
							  <div class="tooltip_templates">
								<span id="tooltip_content2">
								  <div class="position">
									<h3 class="boardModalText">Mr. Pairuch Laoprasert</h3>
									<p class="boardInofText">Executive Director/ Chief Executive Officer/ <br>Director of Nomination and Remuneration Committee</p>
									<div class="board"><a href="pdf\Board\Pairuch_EN.pdf" target="_blank">More</a></div>
								  </div>
								</span>
							  </div>
							  <!-- 4 -->
							  <div class="tooltip_templates">
								<span id="tooltip_content4">
								  <div class="position">
									<h3 class="boardModalText">Mr. Somchit Peumpremsuk</h3>
									<p class="boardInofText">Director/ Executive Director/ <br>Chairman of Risk Management Committee</p>
									<div class="board"><a href="pdf\Board\Somchit_EN.pdf" target="_blank">More</a></div>
								  </div>
								</span>
							  </div>
							  <!-- 10 -->
							  <div class="tooltip_templates">
								<span id="tooltip_content10">
								  <div class="position">
									<h3 class="boardModalText">Assistant Professor Dr. Sarayut Nathaphan</h3>
									<p class="boardInofText">Independent Director and Chairman of The Audit Committee</p>
									<div class="board"><a href="pdf\Board\Sarayut_EN.pdf" target="_blank">More</a></div>
								  </div>
								</span>
							  </div>
							  <!-- 11 -->
							  <div class="tooltip_templates">
								<span id="tooltip_content11">
								  <div class="position">
									<h3 class="boardModalText">Miss Chawaluck Sivayathorn</h3>
									<p class="boardInofText">Independent Director and Member of The Audit Committee</p>
									<div class="board"><a href="pdf\Board\Chawaluck_EN.pdf" target="_blank">More</a></div>
								  </div>
								</span>
							  </div>
							  <!-- 12 -->
							  <div class="tooltip_templates">
								<span id="tooltip_content12">
								  <div class="position">
									<h3 class="boardModalText">Mrs. Suparanan Tantawirat</h3>
									<p class="boardInofText">Independent Director and Member of The Audit Committee</p>
									<div class="board"><a href="pdf\Board\Suparanan_EN.pdf" target="_blank">More</a></div>
								  </div>
								</span>
							  </div>
							</div> 
							<!-- END Tooltip Content -->

						  <!-- //BLOG POST -->
						  </div><!-- //BLOG BLOCK -->

						  </div>
						</div>
					  </section>

					  <!-- คณะกรรมการตรวจสอบ -->
					  <section class="mbr-section article">
						<div class="container" style="padding-right: 15px;padding-left: 15px;">
						  <div class="coverBoard2">
							<p class="BoardHead">Audit Committee</p>
						  </div>
							<!-- BLOG BLOCK -->
							<div class="blog_block col-lg-12 col-md-12 padbot50" style="transform: matrix(1, 0, 0, 1, 0, 0);">           
							<!-- BLOG POST -->
							<img src="images/BoardOfDirector2.png">

							<!-- Tooltip button -->
							<div class="TooltipButton ButtonBod2">
							  <!-- 11 -->
							  <span class="tooltip11 toolLeft" data-tooltip-content="#tooltip_content11">
								<div class="modal-container">
								  <button class="modal-button">2</button>
								</div>
							  </span>
							  <!-- 10 -->
							  <span class="tooltip10 toolLeft" data-tooltip-content="#tooltip_content10">
								<div class="modal-container">
								  <button class="modal-button">1</button>
								</div>
							  </span>
							  <!-- 12 -->
							  <span class="tooltip12 toolLeft" data-tooltip-content="#tooltip_content12">
								<div class="modal-container">
								  <button class="modal-button">3</button>
								</div>
							  </span>
							</div> 
							<!-- END Tooltip button -->

						  <!-- //BLOG POST -->
						  </div><!-- //BLOG BLOCK -->
						</div>
					  </section>

					  <!-- คณะกรรมการสรรหาและพิจารณาค่าตอบแทน -->
					  <section class="mbr-section article">
						<div class="container" style="padding-right: 15px;padding-left: 15px;">
						  <div class="coverBoard2">
							<p class="BoardHead">Nomination and Remuneration Committee</p>
						  </div>
							<!-- BLOG BLOCK -->
							<div class="blog_block col-lg-12 col-md-12 padbot50" style="transform: matrix(1, 0, 0, 1, 0, 0);">           
							<!-- BLOG POST -->
							<img src="images/BoardOfDirector3.png">

							<!-- Tooltip button -->
							<div class="TooltipButton ButtonBod3">
							  <!-- 3 -->
							  <span class="tooltip3 toolLeft" data-tooltip-content="#tooltip_content3">
								<div class="modal-container">
								  <button class="modal-button">3</button>
								</div>
							  </span>
							  <!-- 1 -->
							  <span class="tooltip toolLeft" data-tooltip-content="#tooltip_content">
								<div class="modal-container">
								  <button class="modal-button">1</button>
								</div>
							  </span>
							  <!-- 7 -->
							  <span class="tooltip7 toolLeft" data-tooltip-content="#tooltip_content7">
								<div class="modal-container">
								  <button class="modal-button">2</button>
								</div>
							  </span>
							</div> 
							<!-- END Tooltip button -->

						  <!-- //BLOG POST -->
						  </div><!-- //BLOG BLOCK -->
						</div>
					  </section>

					  <!-- คณะกรรมการบริหาร -->
					  <section class="mbr-section article sectionSub">
						<div class="container" style="padding-right: 15px;padding-left: 15px;">
						  <div class="coverBoard2">
							<p class="BoardHead">Executive Committee</p>
						  </div>
							<!-- BLOG BLOCK -->
							<div class="blog_block col-lg-12 col-md-12 padbot50" style="transform: matrix(1, 0, 0, 1, 0, 0);">           
							<!-- BLOG POST -->
							<img src="images/BoardOfDirector4.png">

							<!-- Tooltip button -->
							<div class="TooltipButton ButtonBod4">
							  <!-- 9 -->
							  <span class="tooltip9 toolLeft" data-tooltip-content="#tooltip_content9">
								<div class="modal-container">
								  <button class="modal-button">8</button>
								</div>
							  </span>
							  <!-- 8 -->
							  <span class="tooltip8 toolLeft" data-tooltip-content="#tooltip_content8">
								<div class="modal-container">
								  <button class="modal-button">7</button>
								</div>
							  </span>
							  <!-- 7 -->
							  <span class="tooltip7 toolLeft" data-tooltip-content="#tooltip_content7">
								<div class="modal-container">
								  <button class="modal-button">6</button>
								</div>
							  </span>
							  <!-- 3 -->
							  <span class="tooltip3 toolLeft" data-tooltip-content="#tooltip_content3">
								<div class="modal-container">
								  <button class="modal-button">2</button>
								</div>
							  </span>
							  <!-- 2 -->
							  <span class="tooltip2 toolLeft" data-tooltip-content="#tooltip_content2">
								<div class="modal-container">
								  <button class="modal-button">1</button>
								</div>
							  </span>
							  <!-- 4 -->
							  <span class="tooltip4 toolLeft" data-tooltip-content="#tooltip_content4">
								<div class="modal-container">
								  <button class="modal-button">3</button>
								</div>
							  </span>
							  <!-- 5 -->
							  <span class="tooltip5 toolLeft" data-tooltip-content="#tooltip_content5">
								<div class="modal-container">
								  <button class="modal-button">4</button>
								</div>
							  </span>
							  <!-- 6 -->
							  <span class="tooltip6 toolLeft" data-tooltip-content="#tooltip_content6">
								<div class="modal-container">
								  <button class="modal-button">5</button>
								</div>
							  </span>
							</div> 
							<!-- END Tooltip button -->

						  <!-- //BLOG POST -->
						  </div><!-- //BLOG BLOCK -->
						</div>
					</section>
			<%else%>
				 <section class="mbr-section article">
					<div class="container" style="padding-right: 15px;padding-left: 15px;">
						  <div class="row">
						
							<!-- BLOG BLOCK -->
						  <div class="blog_block col-lg-12 col-md-12 padbot50" style="transform: matrix(1, 0, 0, 1, 0, 0);">           
							<!-- BLOG POST -->
							<img src="images/BoardOfDirector1.png">

							<!-- Tooltip button -->
							<div class="TooltipButton ButtonBod1">
							  <!-- 9 -->
							  <span class="tooltip9 toolLeft" data-tooltip-content="#tooltip_content9">
								<div class="modal-container">
								  <button class="modal-button">9</button>
								</div>
							  </span>
							  <!-- 8 -->
							  <span class="tooltip8 toolLeft" data-tooltip-content="#tooltip_content8">
								<div class="modal-container">
								  <button class="modal-button">8</button>
								</div>
							  </span>
							  <!-- 7 -->
							  <span class="tooltip7 toolLeft" data-tooltip-content="#tooltip_content7">
								<div class="modal-container">
								  <button class="modal-button">7</button>
								</div>
							  </span>
							  <!-- 6 -->
							  <span class="tooltip6 toolLeft" data-tooltip-content="#tooltip_content6">
								<div class="modal-container">
								  <button class="modal-button">6</button>
								</div>
							  </span>
							  <!-- 5 -->
							  <span class="tooltip5 toolLeft" data-tooltip-content="#tooltip_content5">
								<div class="modal-container">
								  <button class="modal-button">5</button>
								</div>
							  </span>
							  <!-- 3 -->
							  <span class="tooltip3 toolLeft" data-tooltip-content="#tooltip_content3">
								<div class="modal-container">
								  <button class="modal-button">3</button>
								</div>
							  </span>
							  <!-- 1 -->
							  <span class="tooltip toolLeft" data-tooltip-content="#tooltip_content">
								<div class="modal-container">
								  <button class="modal-button">1</button>
								</div>
							  </span>
							  <!-- 2 -->
							  <span class="tooltip2 toolLeft" data-tooltip-content="#tooltip_content2">
								<div class="modal-container">
								  <button class="modal-button">2</button>
								</div>
							  </span>
							  <!-- 4 -->
							  <span class="tooltip4 toolLeft" data-tooltip-content="#tooltip_content4">
								<div class="modal-container">
								  <button class="modal-button">4</button>
								</div>
							  </span>
							  <!-- 10 -->
							  <span class="tooltip10 toolLeft" data-tooltip-content="#tooltip_content10">
								<div class="modal-container">
								  <button class="modal-button">10</button>
								</div>
							  </span>
							  <!-- 11 -->
							  <span class="tooltip11 toolLeft" data-tooltip-content="#tooltip_content11">
								<div class="modal-container">
								  <button class="modal-button">11</button>
								</div>
							  </span>
							  <!-- 12 -->
							  <span class="tooltip12 toolLeft" data-tooltip-content="#tooltip_content12">
								<div class="modal-container">
								  <button class="modal-button">12</button>
								</div>
							  </span>
							</div> 
							<!-- END Tooltip button -->

							<!-- Tooltip Content -->
							<div class="TooltipContent">
							  <!-- 9 -->
							  <div class="tooltip_templates">
								<span id="tooltip_content9">
								  <div class="position">
									<h3 class="boardModalText">นายกำพล ปุญโสณี</h3>
									<p class="boardInofText">กรรมการ / กรรมการบริหาร</p>
									<div class="board"><a href="pdf\Board\Khumpol.pdf" target="_blank">ดูข้อมูลเพิ่มเติม</a></div>
								  </div>
								</span>
							  </div>
							  <!-- 8 -->
							  <div class="tooltip_templates">
								<span id="tooltip_content8">
								  <div class="position">
									<h3 class="boardModalText">นายบัณฑิต ม่วงสอนเขียว</h3>
									<p class="boardInofText">กรรมการ / กรรมการบริหาร</p>
									<div class="board"><a href="pdf\Board\Bundit.pdf" target="_blank">ดูข้อมูลเพิ่มเติม</a></div>
								  </div>
								</span>
							  </div>
							  <!-- 7 -->
							  <div class="tooltip_templates">
								<span id="tooltip_content7">
								  <div class="position">
									<h3 class="boardModalText">นายวรวรรต ศรีสอ้าน</h3>
									<p class="boardInofText">กรรมการ / กรรมการบริหาร / <br>กรรมการสรรหาและพิจารณาค่าตอบแทน</p>
									<div class="board"><a href="pdf\Board\Worawat.pdf" target="_blank">ดูข้อมูลเพิ่มเติม</a></div>
								  </div>
								</span>
							  </div>
							  <!-- 6 -->
							  <div class="tooltip_templates">
								<span id="tooltip_content6">
								  <div class="position">
									<h3 class="boardModalText">นายกิตติศักดิ์ สุภาควัฒน์</h3>
									<p class="boardInofText">กรรมการ / กรรมการบริหาร / กรรมการบริหารความเสี่ยง / <br>รองประธานเจ้าหน้าที่บริหาร ฝ่ายบริหารและควบคุมงานก่อสร้าง</p>
									<div class="board"><a href="pdf\Board\Kittisak.pdf" target="_blank">ดูข้อมูลเพิ่มเติม</a></div>
								  </div>
								</span>
							  </div>
							  <!-- 5 -->
							  <div class="tooltip_templates">
								<span id="tooltip_content5">
								  <div class="position">
									<h3 class="boardModalText">นายอิสรินทร์ สุวัฒโน</h3>
									<p class="boardInofText">กรรมการ / กรรมการบริหาร / กรรมการบริหารความเสี่ยง / <br>รองประธานเจ้าหน้าที่บริหาร ฝ่ายธุรการสำนักงาน</p>
									<div class="board"><a href="pdf\Board\Issarin.pdf" target="_blank">ดูข้อมูลเพิ่มเติม</a></div>
								  </div>
								</span>
							  </div>
							  <!-- 3 -->
							  <div class="tooltip_templates">
								<span id="tooltip_content3">
								  <div class="position">
									<h3 class="boardModalText">นายสมเกียรติ ศิลวัฒนาวงศ์</h3>
									<p class="boardInofText">รองประธานกรรมการ / รองประธานกรรมการบริหาร / <br>กรรมการสรรหาและพิจารณาค่าตอบแทน / ประธานเจ้าหน้าที่บริหาร</p>
									<div class="board"><a href="pdf\Board\Somkia.pdf" target="_blank">ดูข้อมูลเพิ่มเติม</a></div>
								  </div>
								</span>
							  </div>
							  <!-- 1 -->
							  <div class="tooltip_templates">
								<span id="tooltip_content">
								  <div class="position">
									<h3 class="boardModalText">นายจุมพล สำเภาพล</h3>
									<p class="boardInofText">ประธานกรรมการ / กรรมการอิสระ / <br>ประธานคณะกรรมการสรรหาและพิจารณาค่าตอบแทน</p>
									<div class="board"><a href="pdf\Board\Jumpol.pdf" target="_blank">ดูข้อมูลเพิ่มเติม</a></div>
								  </div>
								</span>
							  </div>
							  <!-- 2 -->
							  <div class="tooltip_templates">
								<span id="tooltip_content2">
								  <div class="position">
									<h3 class="boardModalText">นายไพรัช เล้าประเสริฐ</h3>
									<p class="boardInofText">รองประธานกรรมการ / ประธานกรรมการบริหาร</p>
									<div class="board"><a href="pdf\Board\Pairuch.pdf" target="_blank">ดูข้อมูลเพิ่มเติม</a></div>
								  </div>
								</span>
							  </div>
							  <!-- 4 -->
							  <div class="tooltip_templates">
								<span id="tooltip_content4">
								  <div class="position">
									<h3 class="boardModalText">นายสมจิตร์ เปี่ยมเปรมสุข</h3>
									<p class="boardInofText">กรรมการ / กรรมการบริหาร / <br>ประธานกรรมการบริหารความเสี่ยง</p>
									<div class="board"><a href="pdf\Board\Somchit.pdf" target="_blank">ดูข้อมูลเพิ่มเติม</a></div>
								  </div>
								</span>
							  </div>
							  <!-- 10 -->
							  <div class="tooltip_templates">
								<span id="tooltip_content10">
								  <div class="position">
									<h3 class="boardModalText">ผศ.ดร.สรายุทธ์ นาทะพันธ์</h3>
									<p class="boardInofText">กรรมการอิสระ / ประธานกรรมการตรวจสอบ</p>
									<div class="board"><a href="pdf\Board\Sarayut.pdf" target="_blank">ดูข้อมูลเพิ่มเติม</a></div>
								  </div>
								</span>
							  </div>
							  <!-- 11 -->
							  <div class="tooltip_templates">
								<span id="tooltip_content11">
								  <div class="position">
									<h3 class="boardModalText">นางสาวชวลรรค ศิวยาธร</h3>
									<p class="boardInofText">กรรมการอิสระ / กรรมการตรวจสอบ</p>
									<div class="board"><a href="pdf\Board\Chawaluck.pdf" target="_blank">ดูข้อมูลเพิ่มเติม</a></div>
								  </div>
								</span>
							  </div>
							  <!-- 12 -->
							  <div class="tooltip_templates">
								<span id="tooltip_content12">
								  <div class="position">
									<h3 class="boardModalText">นางศุภรานันท์ ตันวิรัช</h3>
									<p class="boardInofText">กรรมการอิสระ / กรรมการตรวจสอบ</p>
									<div class="board"><a href="pdf\Board\Suparanan.pdf" target="_blank">ดูข้อมูลเพิ่มเติม</a></div>
								  </div>
								</span>
							  </div>
							</div> 
							<!-- END Tooltip Content -->

						  <!-- //BLOG POST -->
						  </div><!-- //BLOG BLOCK -->

						  </div>
						</div>
					  </section>

					  <!-- คณะกรรมการตรวจสอบ -->
					  <section class="mbr-section article">
						<div class="container" style="padding-right: 15px;padding-left: 15px;">
						  <div class="coverBoard2">
							<p class="BoardHead">คณะกรรมการตรวจสอบ</p>
						  </div>
							<!-- BLOG BLOCK -->
							<div class="blog_block col-lg-12 col-md-12 padbot50" style="transform: matrix(1, 0, 0, 1, 0, 0);">           
							<!-- BLOG POST -->
							<img src="images/BoardOfDirector2.png">

							<!-- Tooltip button -->
							<div class="TooltipButton ButtonBod2">
							  <!-- 11 -->
							  <span class="tooltip11 toolLeft" data-tooltip-content="#tooltip_content11">
								<div class="modal-container">
								  <button class="modal-button">2</button>
								</div>
							  </span>
							  <!-- 10 -->
							  <span class="tooltip10 toolLeft" data-tooltip-content="#tooltip_content10">
								<div class="modal-container">
								  <button class="modal-button">1</button>
								</div>
							  </span>
							  <!-- 12 -->
							  <span class="tooltip12 toolLeft" data-tooltip-content="#tooltip_content12">
								<div class="modal-container">
								  <button class="modal-button">3</button>
								</div>
							  </span>
							</div> 
							<!-- END Tooltip button -->
						  <!-- //BLOG POST -->
						  </div><!-- //BLOG BLOCK -->
						</div>
					  </section>

					  <!-- คณะกรรมการสรรหาและพิจารณาค่าตอบแทน -->
					  <section class="mbr-section article">
						<div class="container" style="padding-right: 15px;padding-left: 15px;">
						  <div class="coverBoard2">
							<p class="BoardHead">คณะกรรมการสรรหาและพิจารณาค่าตอบแทน</p>
						  </div>
							<!-- BLOG BLOCK -->
							<div class="blog_block col-lg-12 col-md-12 padbot50" style="transform: matrix(1, 0, 0, 1, 0, 0);">           
							<!-- BLOG POST -->
							<img src="images/BoardOfDirector3.png">

							<!-- Tooltip button -->
							<div class="TooltipButton ButtonBod3">
							  <!-- 3 -->
							  <span class="tooltip3 toolLeft" data-tooltip-content="#tooltip_content3">
								<div class="modal-container">
								  <button class="modal-button">3</button>
								</div>
							  </span>
							  <!-- 1 -->
							  <span class="tooltip toolLeft" data-tooltip-content="#tooltip_content">
								<div class="modal-container">
								  <button class="modal-button">1</button>
								</div>
							  </span>
							  <!-- 7 -->
							  <span class="tooltip7 toolLeft" data-tooltip-content="#tooltip_content7">
								<div class="modal-container">
								  <button class="modal-button">2</button>
								</div>
							  </span>
							</div> 
							<!-- END Tooltip button -->
						  <!-- //BLOG POST -->
						  </div><!-- //BLOG BLOCK -->
						</div>
					  </section>

					  <!-- คณะกรรมการบริหาร -->
					  <section class="mbr-section article sectionSub">
						<div class="container" style="padding-right: 15px;padding-left: 15px;">
						  <div class="coverBoard2">
							<p class="BoardHead">คณะกรรมการบริหาร</p>
						  </div>
							<!-- BLOG BLOCK -->
							<div class="blog_block col-lg-12 col-md-12 padbot50" style="transform: matrix(1, 0, 0, 1, 0, 0);">           
							<!-- BLOG POST -->
							<img src="images/BoardOfDirector4.png">

							<!-- Tooltip button -->
							<div class="TooltipButton ButtonBod4">
							  <!-- 9 -->
							  <span class="tooltip9 toolLeft" data-tooltip-content="#tooltip_content9">
								<div class="modal-container">
								  <button class="modal-button">8</button>
								</div>
							  </span>
							  <!-- 8 -->
							  <span class="tooltip8 toolLeft" data-tooltip-content="#tooltip_content8">
								<div class="modal-container">
								  <button class="modal-button">7</button>
								</div>
							  </span>
							  <!-- 7 -->
							  <span class="tooltip7 toolLeft" data-tooltip-content="#tooltip_content7">
								<div class="modal-container">
								  <button class="modal-button">6</button>
								</div>
							  </span>
							  <!-- 3 -->
							  <span class="tooltip3 toolLeft" data-tooltip-content="#tooltip_content3">
								<div class="modal-container">
								  <button class="modal-button">2</button>
								</div>
							  </span>
							  <!-- 2 -->
							  <span class="tooltip2 toolLeft" data-tooltip-content="#tooltip_content2">
								<div class="modal-container">
								  <button class="modal-button">1</button>
								</div>
							  </span>
							  <!-- 4 -->
							  <span class="tooltip4 toolLeft" data-tooltip-content="#tooltip_content4">
								<div class="modal-container">
								  <button class="modal-button">3</button>
								</div>
							  </span>
							  <!-- 5 -->
							  <span class="tooltip5 toolLeft" data-tooltip-content="#tooltip_content5">
								<div class="modal-container">
								  <button class="modal-button">4</button>
								</div>
							  </span>
							  <!-- 6 -->
							  <span class="tooltip6 toolLeft" data-tooltip-content="#tooltip_content6">
								<div class="modal-container">
								  <button class="modal-button">5</button>
								</div>
							  </span>
							</div> 
							<!-- END Tooltip button -->
						  <!-- //BLOG POST -->
						  </div><!-- //BLOG BLOCK -->
						</div>
					</section>
			<%end if%>
			<!--======================================================= End ======================================================-->
			
		</div>

		<!--====================================== Footer ======================================-->
			<!--#include file='i_footer.asp'-->
		<!--======================================== End =======================================-->

		<script type="text/javascript" src="scripts/modernizr.custom.js"></script>
		<%if session("lang")="E" then%><script type="text/javascript" src="scripts/jquery.dlmenu-en.js"></script><%else%><script type="text/javascript" src="scripts/jquery.dlmenu.js"></script><%end if%>
		<script>
			$(function() {
				$('#dl-menu').dlmenu();
			});
		</script>
		<!-- js/scroll-hint -->
		<script src="scripts/scroll-hint.js"></script>
		<!-- js/scroll-hint -->
		<script>
			new ScrollHint('.js-scrollable', {
			});
		</script>
	</div>
	 <!--#include file="i_googleAnalytics.asp"-->	  
</body>
</html>