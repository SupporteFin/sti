<!-- HEADER -->
<header style="font-family: 'promptextralight';">
	<!-- MENU BLOCK -->
	<div class="menu_block">
		<!-- CONTAINER -->
		<div class="container clearfix">
			<!-- LOGO -->
			<div class="logo pull-left" style="margin-top: 15px">
				<a href="https://www.sti.co.th/th/index.php"><img src="https://www.sti.co.th/th/images/logo/logo005.png" width="150" alt=""></a>
			</div>
			<!-- //LOGO -->
			<!-- MENU -->
			<div class="main-menu pull-right">
				<nav class="navmenu center">
					<div class="navbar-header">
						<button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#id_nav1">
							<span class="fa fa-align-justify" style="line-height: 41px;"></span>
						</button>
					</div>
					<div class="collapse navbar-collapse" id="id_nav1">
						<ul class="navbar-nav">
							<li class="first active scroll_btn"><a href="https://www.sti.co.th/th/index.php"><%=arr_menu_desc(0)%></a></li>
							<li class="scroll_btn"><a href="https://www.sti.co.th/th/investor/vision.php"><%=arr_menu_desc(1)%></a></li>
							<li class="scroll_btn"><a href="javascript:void(0);" onclick="parent.location.href='popup.asp?home';"><%=arr_menu_desc(2)%></a></li>
							<li class="dropdown">
								<a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);"><%=arr_menu_desc(3)%></a>
								<ul class="dropdown-content">
									<li><a href="https://www.sti.co.th/th/residential.php"><%=arr_menu_desc(4)%></a></li>
									<li><a href="https://www.sti.co.th/th/commercial.php"><%=arr_menu_desc(5)%></a></li>
									<li><a href="https://www.sti.co.th/th/education.php"><%=arr_menu_desc(6)%></a></li>
									<li><a href="https://www.sti.co.th/th/hospital.php"><%=arr_menu_desc(7)%></a></li>
									<li><a href="https://www.sti.co.th/th/factory.php"><%=arr_menu_desc(8)%></a></li>
									<li><a href="https://www.sti.co.th/th/hotel.php"><%=arr_menu_desc(9)%></a></li>
									<li><a href="https://www.sti.co.th/th/housing.php"><%=arr_menu_desc(10)%></a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);"><%=arr_menu_desc(11)%></a>
								<ul class="dropdown-content">
									<li><a href="https://www.sti.co.th/th/blog-cat.php?cid=1"><%=arr_menu_desc(12)%></a></li>
									<li><a href="https://www.sti.co.th/th/blog-cat.php?cid=8"><%=arr_menu_desc(13)%></a></li>
									<li><a href="https://www.sti.co.th/th/blog-cat.php?cid=9"><%=arr_menu_desc(14)%></a></li>
								</ul>
							</li>
							<li class="scroll_btn"><a href="https://www.sti.co.th/th/career.php"><%=arr_menu_desc(15)%></a></li>
							<li class="scroll_btn last"><a href="https://www.sti.co.th/th/index.php#contacts"><%=arr_menu_desc(16)%></a></li>
							<li class="scroll_btn"><a href="https://www.sti.co.th/th/complaint.php"><%=arr_menu_desc(17)%></a></li>
						</ul>
					</div>
				</nav>
			</div>
			<!-- //MENU -->
		</div>
		<!-- //MENU BLOCK -->
	</div>
	<!-- //CONTAINER -->
</header>
<!-- //HEADER -->