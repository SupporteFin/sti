﻿<!DOCTYPE html>

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->
<!--#include file = "../../i_connews.asp" -->
<!--#include file = "../../i_conir.asp" -->
<!--#include file = "../../function_asp2007.asp" -->
<!--#include file = "../../i_conreference.asp" -->
<%
session("cur_page")="IR " &  listed_share & " IR Home"
session("page_asp")="home.asp"
page_name="IR Home"
%>

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<TITLE><%=message_title%> :: <%=page_name%></TITLE>
	<META name="Title" content="<%=message_metatitle%>">
	<META name="Description" content="<%=message_description%>">
	<META name="Keywords" content="<%=message_keyword%>">	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="author" content="">
	<link rel="shortcut icon" href="<%=img_icon%>">
	<link href="style_listed.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../function2007_utf8.js"></script>
	<!-- CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/flexslider.css" rel="stylesheet" type="text/css">
	<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
	<!-- ADD CSS -->
	<link href="css/component.css" rel="stylesheet" type="text/css">
	<link href="css/custom.css" rel="stylesheet" type="text/css">
	<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">
	<!-- FONTS -->
	<link href="css/font_add.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- Mouse Hover CSS -->
	<script src="scripts/jquery.min.js" type="text/javascript"></script>
	<script src="scripts/jquery.nicescroll.min.js"></script>
	<!-- SCRIPTS -->
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<!--[if IE]><html class="ie" lang="en"> <![endif]-->
	<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
	<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
	<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
	<script src="scripts/animate.js" type="text/javascript"></script>
	<script src="scripts/myscript.js" type="text/javascript"></script>
	<script src="scripts/owl.carousel.js" type="text/javascript"></script>
	<!-- NEW -->
	<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
	<link href="css/site.css" rel="stylesheet" type="text/css" />
	<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
	<!-- scroll-hint -->
	<link rel="stylesheet" href="css/scroll-hint.css">
</head>

<body class="main">

	<!-- PRELOADER -->
	<img id="preloader" src="images/preloader.gif" alt="">
	<!-- //PRELOADER -->
	<div class="preloader_hide">
		<div id="page" class="single_page">

			<!--================================ Menu Top ==================================-->
				<!--#include file='i_menu_top.asp'-->
			<!--=================================== End ====================================-->

			<!--=============================== Menu Left ==================================-->
				<!--#include file='i_menu_left.asp'-->
			<!--================================== End =====================================-->

			<!--===================================================== Content ====================================================-->
			<section class="mbr-section article" style="padding: 35px 0px 40px 0px;">
				<div class="container">
				<div id="slider1_container">
					<!-- Loading Screen -->
					<div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
						<img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="../svg/loading/static-svg/spin.svg" />
					</div>

					<!-- Slides Container -->
					<div data-u="slides" style="position: absolute; left: 0px; top: 0px;width: 1200px;height: 385px;
					overflow: hidden;">
						<div>
							<img data-u="image" src="images/EVENT.jpg" style="width: 1170px;height: 370px"/>
						</div>
						<div>
							<img data-u="image" src="images/AGM.jpg" style="width: 1170px;height: 370px"/>
						</div>
					</div>
					
					<div data-u="navigator" class="jssorb031" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
						<div data-u="prototype" class="i" style="width:16px;height:16px;">
							<svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
								<circle class="b" cx="8000" cy="8000" r="5800"></circle>
							</svg>
						</div>
					</div>

					<div data-u="arrowleft" class="jssora051" style="width:55px;height:55px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
						<svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
							<polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
						</svg>
					</div>
					<div data-u="arrowright" class="jssora051" style="width:55px;height:55px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
						<svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
							<polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
						</svg>
					</div>
					<!--#endregion Arrow Navigator Skin End -->
				</div>
				<!-- Jssor Slider End -->
				</div>
			  </section>
			  
			  <section class="mbr-section article section-other">
				<div style="position: absolute;width: 100%;z-index: 1;">
				<!-- css position:absolute; เพื่อให้หน้าจอไม่เลื่อนตอนข่าวขยับ-->
				<div class="container">
					<div class="row">
						<div class="col-lg-4">
							<p class="headNewtext"><%if session("lang")="E" then%>NEWS<%else%>ห้องข่าว<%end if%></p>
							<!-- ======================= i_set_announcement.asp ======================= -->
							<!--#include file='i_set_announcement.asp'-->
							<!-- ======================= i_set_announcement.asp ======================= -->
						</div>
						<div class="col-lg-4">
							<!-- ======================= i_news_update.asp ======================= -->
							<!--#include file='i_news_update.asp'-->
							<!-- ======================= i_news_update.asp ======================= -->
						</div>
						<div class="col-lg-4">
							<p class="headNewtext headNewtextRE"><%if session("lang")="E" then%>INVESTOR CALENDAR<%else%>ปฏิทินนักลงทุน<%end if%></p>
							<!-- ======================= i_content_calendar.asp ======================= -->
							<!--#include file='i_content_calendar.asp'-->
							<!-- ======================= i_content_calendar.asp ======================= -->
						</div>
					</div> <!-- END row -->
				</div> <!-- END container -->
				</div>
			  </section>
			  
			  <div class="newHeightTip section-other"></div> 
			  <!-- // newHeightTip ตัวหลอกความสูงของ section ข่าว -->
			<%if session("lang")="E" then%>
				<section class="mbr-section article section-other">
					<div class="container">
						<div class="row">
							<div class="col-lg-6 paddingFinan1" style="padding-right: 0px;">
								<div class="borderFinan1">
									<div class="Big-coverFinancial">
										<p class="headFinancialText">FINANCIAL HIGHLIGHTS</p>
										<div style="margin: auto;display: table;">
										<div class="coverFinancial-1">
											<div class="coverFinancial-Padding">
								  <span class="Financial_text1">รายได้จาก<br>การให้บริการ</span>
								  <p class="Financial_text2">631.39</p>
												<p class="Financial_text3">Million baht</p>
											</div>
										</div>
										<div class="coverFinancial-2">
											<div class="coverFinancial-Padding">
								  <span class="Financial_text1">รวม<br>รายได้</span>
								  <p class="Financial_text2">635.10</p>
												<p class="Financial_text3">Million baht</p>
											</div>	
										</div>
										<div class="coverFinancial-3">
											<div class="coverFinancial-Padding">
												<span class="Financial_text1">รวมค่า<br>ใช้จ่าย</span>
								  <p class="Financial_text2">548.36</p>
												<p class="Financial_text3">Million baht</p>
											</div>
										</div>
										</div>
									</div>
									<div class="Big-coverFinancial2">
										<div style="margin-bottom: 107px;">
											<table style="width:100%">
												<tr class="Tr-Financial">
													<td width="40%" class="Td-FinancialText1">กำไรก่อนค่าใช้จ่ายภาษีเงินได้</td>
													<td width="35%" class="Td-FinancialText2">86.73</td>
													<td width="25%" class="Td-FinancialText3">(Million baht)</td>
												</tr>
												<tr class="Tr-Financial">
													<td width="40%" class="Td-FinancialText1">กำไรสำหรับปี / งวด</td>
													<td width="35%" class="Td-FinancialText2">73.03</td> 
													<td width="25%" class="Td-FinancialText3">(Baht: share)</td>
												</tr>
											</table>
										</div>
										<div>
											<div class="circle"></div>
											<span class="lastDayFinancial">Year 2017 | Latest data : 22 July 2018</span>
											<a href="finance_highlights.asp?lang=E" target="_blank" class="AllInfoFinancial">MORE DETAIL</a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-6 paddingFinan2" style="padding-left: 0px;">
								<div class="borderFinan2">
									<div class="Big-coverStockQuote">
										<div class="coverStockQuote1">
											<!-- ======================= i_stock_quote.asp ======================= -->
											<!--#include file='i_stock_quote.asp'-->
											<!-- ======================= i_stock_quote.asp ======================= -->
										</div>
										<div class="coverStockQuote2">
											<p class="headStockQuoteText">Stock Quote</p>
											<p class="coverStockQuoteSymbol">symbol : <span class="SQSymbol">STI</span></p>
											<p class="StockQuoteText1">ข้อมูลเปิดเผยในราคาหุ้นที่สำคัญของบริษัท สโตนเฮ้นจ์ อินเตอร์ จำกัด (มหาชน) และปริมาณข้อมูลซื้อ - ขาย และการเปลี่ยนแปลงราคาหุ้นราคาล่าสุดของ บริษัท สโตนเฮนจ์ อินเตอร์ จำกัด (มหาชน)</p>
										</div>
										<div class="coverStockQuote3">
											<a href="https://www.set.or.th/set/historicaltrading.do?symbol=STI&ssoPageId=2&language=en&country=EN">
											<button type="button" class="buttonStockQuote1">ราคาหลักทรัพย์ย้อนหลัง</button></a>
											<a href="calculator.asp?lang=E" target="_blank">
											<button type="button" class="buttonStockQuote2">เครื่องคำนวณการลงทุน</button></a>
										</div>
									</div>
								</div>
							</div>
						
						</div> <!-- END row -->
					</div> <!-- END container -->
				  </section>
				  
				  <section class="mbr-section article section-other" style="padding-bottom: 20px !important;">
					<div class="container">
						<div class="row">
					<!-------------------------------------------- Content ---------------------------------------------->
					  <div class="col-lg-12">
						<div class="row">
						  <div class="clearfix"></div>
							
							<div class="col-md-4">
							  <div class="annual-wrap-index1">
								
								<div class="cover-annual">
									<p class="font_home-index_irhead6">ANNUAL REPORT</p>
									<p class="font_text-index_irhome5">รายงานประจำปีนำเสนอผลการดำเนินงานตามแผนปฏิบัติการของบริษัท สโตนเฮ้นจ์ อินเตอร์ จำกัด (มหาชน) เพื่อให้นักลงทุนได้รับข้อมูลนโยบาย และภาพรวมของ ธุรกิจ และข้อมูลอื่นๆ</p>
									<p class="font_home-index_All"><a href="annual_report.asp?lang=E" target="_blank" class="">MORE DETAIL</a></p>
								</div>
								
							   <!--  <div class="cover">
								  <img src="images/an.jpg" alt="" class="shadow-index img-responsive">
								</div> -->

								<div class="button-wrapper">
								  <a href="pdf/annual_report_2018.pdf" target="_blank" class="a-btnIndex">
									<!-- <span class="a-btn-symbol"></span> -->
									<span class="a-btn-slide-text">Files Download (Lastest)</span>
									<span class="a-btn-slide-icon"></span>
								  </a>
								</div>
							  </div>
							</div>
							  
							<div class="col-md-4">
							  <div class="annual-wrap-index2">

								<div class="cover-annual">
									<p class="font_home-index_irhead6">Dividend Payment</p>
									<p class="font_text-index_irhome5" style="margin-bottom: 35px;">ข้อมูลเพื่อชี้แจงรายละเอียดของนโยบายการจ่ายเงินปันผล และการจ่ายเงินปันผลให้กับนักลงทุนของบริษัท สโตนเฮ้นจ์ อินเตอร์ จำกัด (มหาชน)</p>
								</div>

									<div class="spaceAN"></div>
									
								<div class="button-wrapper">
								  <a href="dividend.asp?lang=E" target="_blank" class="a-btnIndex">
									<!-- <span class="a-btn-symbol"></span> -->
									<span class="a-btn-slide-text">MORE DETAIL</span>
									<span class="a-btn-slide-icon"></span>
								  </a>
								</div>
							  </div>
							</div>
							  
							<div class="col-md-4">
							  <div class="annual-wrap-index3">

								<div class="cover-annual">
									<p class="font_home-index_irhead6">General Meeting</p>
									<p class="font_text-index_irhome5">หลักเกณฑ์และแบบฟอร์มการเข้าร่วมประชุมล่วงหน้า ก่อนการประชุมสามัญผู้ถือหุ้นการสรรหาวาระการประชุม และเอกสารแจ้งอื่นๆ ของบริษัท สโตนเฮ้นจ์ อินเตอร์ จำกัด (มหาชน)</p>
									<a href="major.asp?lang=E" target="_blank">
									<div class="coverSM">
										<img src="images/icon-SM.png" class="SMimg">
										<p class="smText">Major Shareholder</p>
										<div class="fa fa-chevron-right" style="line-height: 0px;"></div>
									</div></a>
								</div>

									
									
								<div class="button-wrapper">
								  <a href="general_meeting.asp?lang=E" target="_blank" class="a-btnIndex">
									<!-- <span class="a-btn-symbol"></span> -->
									<span class="a-btn-slide-text">MORE DETAIL</span>
									<span class="a-btn-slide-icon"></span>
								  </a>
								</div>
							  </div>
							</div>
							  
						</div>
					  </div>
					<!----------------------------------------- End Content --------------------------------------------->
						</div> <!-- END row -->
					</div> <!-- END container -->
				  </section>

				  <section class="mbr-section article section-other" style="padding-bottom: 110px !important;">
					<div class="container">
						<div class="coverEmail">
							<div class="col-lg-4 col-md-4" style="text-align: center;">
								<img src="images/email.png">
							</div>
							<div class="col-lg-5 col-md-5">
								<div class="coverEmailText">
									<h1 class="emailfollowText">กดติดตามรึยัง?</h1>
									<p class="emailfollowText-sub">ต้องการรับการแจ้งเตือนเมื่อข่าวของเราเผยแพร่หรือเปล่า? <br> สมัครลงทะเบียนสิเพื่อเป็นคนแรกที่รู้</p>
								</div>
							</div>
							<div class="col-lg-3 col-md-3">
								<div class="coverEmailButton">
									<a href="request_alerts.asp?lang=E" target="_blank">
										<button type="button" class="buttonReEmail">Subscribe now</button>
									</a>
								</div>
							</div>
						</div>
					</div> <!-- END container -->
				  </section>
			<%else%>
				<section class="mbr-section article section-other">
					<div class="container">
						<div class="row">
							<div class="col-lg-6 paddingFinan1" style="padding-right: 0px;">
								<div class="borderFinan1">
									<div class="Big-coverFinancial">
										<p class="headFinancialText">ข้อมูลสำคัญทางการเงิน</p>
										<div style="margin: auto;display: table;">
										<div class="coverFinancial-1">
											<div class="coverFinancial-Padding">
												<span class="Financial_text1">รายได้จาก<br>การให้บริการ</span>
												<p class="Financial_text2">631.39</p>
												<p class="Financial_text3">ล้านบาท</p>
											</div>	
										</div>
										<div class="coverFinancial-2">
											<div class="coverFinancial-Padding">
												<span class="Financial_text1">รวม<br>รายได้</span>
												<p class="Financial_text2">635.10</p>
												<p class="Financial_text3">ล้านบาท</p>
											</div>		
										</div>
										<div class="coverFinancial-3">
											<div class="coverFinancial-Padding">
												<span class="Financial_text1">รวมค่า<br>ใช้จ่าย</span>
												<p class="Financial_text2">548.36</p>
												<p class="Financial_text3">ล้านบาท</p>
											</div>	
										</div>
										</div>
									</div>
									<div class="Big-coverFinancial2">
										<div style="margin-bottom: 107px;">
											<table style="width:100%">
												<tr class="Tr-Financial">
													<td width="40%" class="Td-FinancialText1">กำไรก่อนค่าใช้จ่ายภาษีเงินได้</td>
													<td width="40%" class="Td-FinancialText2">86.73</td>
													<td width="20%" class="Td-FinancialText3">(ล้านบาท)</td>
												</tr>
												<tr class="Tr-Financial" style="background-color: #e8e8e8;">
													<td width="40%" class="Td-FinancialText1">กำไรสำหรับปี / งวด</td>
													<td width="40%" class="Td-FinancialText2">73.03</td> 
													<td width="20%" class="Td-FinancialText3">(ล้านบาท)</td>
												</tr>
											</table>
										</div>
										<div>
											<div class="circle"></div>
											<span class="lastDayFinancial">ประจำปี 2560 | ข้อมูลล่าสุด : 22 ก.ค. 2561</span>
											<a href="finance_highlights.asp" target="_blank" class="AllInfoFinancial">ข้อมูลทั้งหมด</a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-6 paddingFinan2" style="padding-left: 0px;">
								<div class="borderFinan2">
									<div class="Big-coverStockQuote">
										<div class="coverStockQuote1">
											<!-- ======================= i_stock_quote.asp ======================= -->
											<!--#include file='i_stock_quote.asp'-->
											<!-- ======================= i_stock_quote.asp ======================= -->
										</div>
										<div class="coverStockQuote2">
											<p class="headStockQuoteText">ข้อมูลราคาหลักทรัพย์</p>
											<p class="coverStockQuoteSymbol">สัญลักษณ์ย่อ : <span class="SQSymbol">STI</span></p>
											<p class="StockQuoteText1">ข้อมูลเปิดเผยในราคาหุ้นที่สำคัญของบริษัท สโตนเฮ้นจ์ อินเตอร์ จำกัด (มหาชน) และปริมาณข้อมูลซื้อ - ขาย และการเปลี่ยนแปลงราคาหุ้นราคาล่าสุดของ บริษัท สโตนเฮนจ์ อินเตอร์ จำกัด (มหาชน)</p>
										</div>
										<div class="coverStockQuote3">
											<a href="https://www.set.or.th/set/historicaltrading.do?symbol=STI&ssoPageId=2&language=th&country=TH">
											<button type="button" class="buttonStockQuote1">ราคาหลักทรัพย์ย้อนหลัง</button></a>
											<a href="calculator.asp" target="_blank">
											<button type="button" class="buttonStockQuote2">เครื่องคำนวณการลงทุน</button></a>
										</div>
									</div>
								</div>
							</div>
						
						</div> <!-- END row -->
					</div> <!-- END container -->
				  </section>
				  
				  <section class="mbr-section article section-other" style="padding-bottom: 20px !important;">
					<div class="container">
						<div class="row">
					<!-------------------------------------------- Content ---------------------------------------------->
					  <div class="col-lg-12">
						<div class="row">
						  <div class="clearfix"></div>
							
							<div class="col-md-4">
							  <div class="annual-wrap-index1">
								
								<div class="cover-annual">
									<p class="font_home-index_irhead6">รายงานประจำปี</p>
									<p class="font_text-index_irhome5">รายงานประจำปีนำเสนอผลการดำเนินงานตามแผนปฏิบัติการของ <span>บริษัท สโตนเฮ้นจ์ อินเตอร์ จำกัด (มหาชน)</span> เพื่อให้นักลงทุนได้รับข้อมูลนโยบาย และภาพรวมของ ธุรกิจ และข้อมูลอื่นๆ</p>
									<p class="font_home-index_All"><a href="annual_report.asp" target="_blank" class="">ดูทั้งหมด</a></p>
										 </div>
								
								<!-- <div class="cover">
								  <img src="images/an.jpg" alt="" class="shadow-index img-responsive">
								</div> -->

								<div class="button-wrapper">
								  <a href="pdf/annual_report_2018.pdf" target="_blank" class="a-btnIndex">
									<!-- <span class="a-btn-symbol"></span> -->
									<span class="a-btn-slide-text">ดาวน์โหลด (ฉบับล่าสุด)</span>
									<span class="a-btn-slide-icon"></span>
								  </a>
								</div>
							  </div>
							</div>
							  
							<div class="col-md-4">
							  <div class="annual-wrap-index2">

								<div class="cover-annual">
									<p class="font_home-index_irhead6">นโยบายจ่ายเงินปันผล</p>
									<p class="font_text-index_irhome5" style="margin-bottom: 35px;">ข้อมูลเพื่อชี้แจงรายละเอียดของนโยบายการจ่ายเงินปันผล และการจ่ายเงินปันผลให้กับนักลงทุนของ <span>บริษัท สโตนเฮ้นจ์ อินเตอร์ จำกัด (มหาชน)</span></p>
								</div>

									<div class="spaceAN"></div>
									
								<div class="button-wrapper">
								  <a href="dividend.asp" target="_blank" class="a-btnIndex">
									<!-- <span class="a-btn-symbol"></span> -->
									<span class="a-btn-slide-text">เพิ่มเติม</span>
									<span class="a-btn-slide-icon"></span>
								  </a>
								</div>
							  </div>
							</div>
							  
							<div class="col-md-4">
							  <div class="annual-wrap-index3">

								<div class="cover-annual">
									<p class="font_home-index_irhead6">การประชุมผู้ถือหุ้น</p>
									<p class="font_text-index_irhome5">หลักเกณฑ์และแบบฟอร์มการเข้าร่วมประชุมล่วงหน้า ก่อนการประชุมสามัญผู้ถือหุ้นการสรรหาวาระการประชุม และเอกสารแจ้งอื่นๆ ของ <span>บริษัท สโตนเฮ้นจ์ อินเตอร์ จำกัด (มหาชน)</span></p>
									<a href="major.asp" target="_blank">
									<div class="coverSM">
										<img src="images/icon-SM.png" class="SMimg">
										<p class="smText">โครงสร้างผู้ถือหุ้นรายใหญ่</p>
										<div class="fa fa-chevron-right" style="line-height: 0px;"></div>
									</div></a>
								</div>

								<div class="button-wrapper">
								  <a href="general_meeting.asp" target="_blank" class="a-btnIndex">
									<!-- <span class="a-btn-symbol"></span> -->
									<span class="a-btn-slide-text">เพิ่มเติม</span>
									<span class="a-btn-slide-icon"></span>
								  </a>
								</div>
							  </div>
							</div>
							  
						</div>
					  </div>
					<!----------------------------------------- End Content --------------------------------------------->
						</div> <!-- END row -->
					</div> <!-- END container -->
				  </section>

				  <section class="mbr-section article section-other" style="padding-bottom: 110px !important;">
					<div class="container">
						<div class="coverEmail">
							<div class="col-lg-4 col-md-4" style="text-align: center;">
								<img src="images/email.png">
							</div>
							<div class="col-lg-5 col-md-5">
								<div class="coverEmailText">
									<h1 class="emailfollowText">กดติดตามรึยัง?</h1>
									<p class="emailfollowText-sub">ต้องการรับการแจ้งเตือนเมื่อข่าวของเราเผยแพร่หรือเปล่า? <br> สมัครลงทะเบียนสิเพื่อเป็นคนแรกที่รู้</p>
								</div>
							</div>
							<div class="col-lg-3 col-md-3">
								<div class="coverEmailButton">
									<a href="request_alerts.asp" target="_blank">
										<button type="button" class="buttonReEmail">สมัครเลย</button>
									</a>
								</div>
							</div>
						</div>
					</div> <!-- END container -->
				  </section>
			<%end if%>
			  
			</div> 	<!-- END id="page" -->

			<!-- FOOTER -->
			<!-- CONTACTS -->
			<section id="contacts">
			</section><!-- //CONTACTS -->
			<!--======================================================= End ======================================================-->
			
		</div>

		<!--====================================== Footer ======================================-->
			<!--#include file='i_footer.asp'-->
		<!--======================================== End =======================================-->

		<script type="text/javascript" src="scripts/modernizr.custom.js"></script>
		<%if session("lang")="E" then%><script type="text/javascript" src="scripts/jquery.dlmenu-en.js"></script><%else%><script type="text/javascript" src="scripts/jquery.dlmenu.js"></script><%end if%>
		<script>
			$(function() {
				$('#dl-menu').dlmenu();
			});
			
			// For Demo purposes only (show hover effect on mobile devices)
			  [].slice.call( document.querySelectorAll('a[href="#"') ).forEach( function(el) {
				el.addEventListener( 'click', function(ev) { ev.preventDefault(); } );
			  } );
		</script>
		
		<script type="text/javascript">
			$(function () {
				$(".demo1").bootstrapNews({
					newsPerPage: 5,
					autoplay: false,
						  pauseOnHover:true,
					direction: 'up',
					navigation: true,
					newsTickerInterval: 4000,
					onToDo: function () {
						console.log(this);
					}
				});
				
				$(".demo2").bootstrapNews({
					newsPerPage: 5,
					autoplay: false,
						  pauseOnHover:true,
					direction: 'up',
					navigation: true,
					newsTickerInterval: 4000,
					onToDo: function () {
						//console.log(this);
					}
				});
			});
		</script>
		
		<!-- jssor slider scripts-->
		<script src="scripts/docs.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="scripts/ie10-viewport-bug-workaround.js"></script>
		<script type="text/javascript" src="scripts/jssor.slider.min.js"></script>
		<script>
			jQuery(document).ready(function ($) {
				var options = {
					$AutoPlay: 1,                                       //[Optional] Auto play or not, to enable slideshow, this option must be set to greater than 0. Default value is 0. 0: no auto play, 1: continuously, 2: stop at last slide, 4: stop on click, 8: stop on user navigation (by arrow/bullet/thumbnail/drag/arrow key navigation)
					$AutoPlaySteps: 1,                                  //[Optional] Steps to go for each navigation request (this options applys only when slideshow disabled), the default value is 1
					$Idle: 4000,                                        //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
					$PauseOnHover: 1,                                   //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1

					$ArrowKeyNavigation: 1,                         //[Optional] Steps to go for each navigation request by pressing arrow key, default value is 1.
					$SlideEasing: $Jease$.$OutQuint,                    //[Optional] Specifies easing for right to left animation, default value is $Jease$.$OutQuad
					$SlideDuration: 3000,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
					$MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide, default value is 20
					//$SlideWidth: 600,                                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
					//$SlideHeight: 300,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
					$SlideSpacing: 0,                           //[Optional] Space between each slide in pixels, default value is 0
					$UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
					$PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
					$DragOrientation: 1,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $Cols is greater than 1, or parking position is not 0)

					$ArrowNavigatorOptions: {                           //[Optional] Options to specify and enable arrow navigator or not
						$Class: $JssorArrowNavigator$,                  //[Requried] Class to create arrow navigator instance
						$ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
						$Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
					},

					$BulletNavigatorOptions: {                          //[Optional] Options to specify and enable navigator or not
						$Class: $JssorBulletNavigator$,                 //[Required] Class to create navigator instance
						$ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
						$Steps: 1,                                      //[Optional] Steps to go for each navigation request, default value is 1
						$SpacingX: 12,                                  //[Optional] Horizontal space between each item in pixel, default value is 0
						$Orientation: 1                                 //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
					}
				};

				var jssor_slider1 = new $JssorSlider$("slider1_container", options);

				//responsive code begin
				//you can remove responsive code if you don't want the slider scales while window resizing
				function ScaleSlider() {
					var parentWidth = jssor_slider1.$Elmt.parentNode.clientWidth;
					if (parentWidth) {
						jssor_slider1.$ScaleWidth(parentWidth - 30);
					}
					else
						window.setTimeout(ScaleSlider, 30);
				}
				ScaleSlider();

				$(window).bind("load", ScaleSlider);
				$(window).bind("resize", ScaleSlider);
				$(window).bind("orientationchange", ScaleSlider);
				//responsive code end
			});
		</script>
		<!-- END jssor slider scripts-->
	
	</div>
	 <!--#include file="i_googleAnalytics.asp"-->	  
</body>
</html>