﻿<!DOCTYPE html>

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->
 <!--#include file = "../../i_conir.asp" -->	
 <!--#include file = "i_description_menu.asp" -->
  <!-- #include file = "../../function_asp2007.asp" -->

<%
pagetitle = title
title=""
title= arr_menu_desc(60)

session("cur_page")="IR " & listed_share &" Public Relation Detail" 
page_name="Public Relation Detail" 
%>

<html>
<head>
<title><%=message_title%> :: <%=page_name%></title>
<meta content="<%=message_keyword%>" name="keywords">
<meta content="<%=message_description%>" name="description">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="">

<link href="style_listed.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../function2007_utf8.js"></script>
<!-- CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/flexslider.css" rel="stylesheet" type="text/css">
<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
<!-- ADD CSS -->
<link href="css/component.css" rel="stylesheet" type="text/css">
<link href="css/custom.css" rel="stylesheet" type="text/css">
<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">
<!-- FONTS -->
<link href="css/font_add.css" rel="stylesheet" type="text/css">
<link href="css/font-awesome.css" rel="stylesheet">
<!-- Mouse Hover CSS -->
<script src="scripts/jquery.min.js" type="text/javascript"></script>
<script src="scripts/jquery.nicescroll.min.js"></script>
<!-- SCRIPTS -->
<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if IE]><html class="ie" lang="en"> <![endif]-->
<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
<script src="scripts/animate.js" type="text/javascript"></script>
<script src="scripts/myscript.js" type="text/javascript"></script>
<script src="scripts/owl.carousel.js" type="text/javascript"></script>
<!-- NEW -->
<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="css/site.css" rel="stylesheet" type="text/css" />
<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
<!-- scroll-hint -->
<link rel="stylesheet" href="css/scroll-hint.css">

</head>
<body>
	<main class="cd-main-content">
		<div class="row">
			<section id="main">
				<!-- ########################### menu top ################################ -->
				<!--#include file="i_top_detail.asp"-->
				<!-- ########################### menu top ################################ -->
			</section>
		</div>
		<div class="row">
			<!-- ########################### content ################################ -->
			
			<!-- Main Content -->
			<div class="container paddingmain" style="padding-top:80px;">
				<div class="row">
					<div class="col-lg-12" style="padding-bottom: 50px;">
						<div class="row">
							<!-- ########################### content details ################################ -->
							<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
								<div class="row">
									<div class="col-lg-12">
										<p style="color:#324695;font-size: 34px;font-weight:normal;padding-bottom:12px;font-weight:bold;"><%=title%></p>
										<div style="background-color:#00ccff;width:120px;height:2px;"></div><br><br>
									</div>
								</div>
								
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="left" valign="top">
												<div style="width: 100%;color: #252525;line-height:20px;">
												<!-- .............................................................................. Content .............................................................................. -->	
													<table cellSpacing=0 cellPadding=0 width="100%" border=0 align="center">
													   <TR>
													   <TD >
														 <table cellSpacing=0 cellPadding=0 width="100%" border=0 align="right">
															<tr><td>
																<%
																strsql = sql_public_detail_listed(request("id"),session("lang"))	 
																
																if strsql <> "" then
																	set rs=conir.execute(strsql)
																	 
																	if not rs.eof then
																		if instr(LCASE(rs("name")),".pdf") > 0 then
																		%>
																		<p align="justify">&nbsp;&nbsp;&nbsp;<font style="FONT-FAMILY:Tahoma;color:#145BA2;font-weight: bold;FONT-SIZE:14px"><strong><%=rs("title")%></strong></FONT></p>
																		<P align="justify">&nbsp;&nbsp;&nbsp;<FONT style="FONT-FAMILY:Tahoma;color:#767676;FONT-SIZE:13px;font-weight: normal;" size=3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=rs("detail")%></FONT></P>
																		<%if trim(rs("name")) <> "" and  trim(rs("name")) <>"-" then
																					str_name=rs("name")
																		%>
																	   <FONT style="FONT-FAMILY:Tahoma;color:#767676;font-weight: bold;FONT-SIZE:12px;font-weight: normal;" size=3><b><%=rs("msgdetail")%><b><br></FONT>
																  <%end if%>
																  <%
																		rs.close
																		conir.close
																		set rs=nothing
																		set conir=nothing											  
																  %>
																  
																	<%if trim(str_name) <>"" then%>
														  
																	<IFRAME id="detail" src="public_relation/<%= trim(str_name) %>" frameBorder="0" width="100%" height="1200" scrolling="yes"></IFRAME>
									
																	<%end if%>																				
																		
																		<%
																		elseif instr(LCASE(rs("name")),".asp") > 0 then
																		%>
																		<IFRAME id="detail" src="public_relation/File/<%=trim(rs("name"))%>" frameBorder="0" width="100%" scrolling="yes" height="1200"></IFRAME>      
																		<%     
																		end if
																	end if
																end if
														
														%>                          
															  </td></tr>
															  </table>
															</TD>
														 </TR>      
												  </table>  	

												 <!-- .............................................................................. End Content .............................................................................. -->	</div>													  
											</td>
										</tr>
										<tr>
										  <td align="left">&nbsp;</td>
										</tr>
										<tr>
										  <td></td>
										</tr>
							  </table>
								
							</div>
							<!-- ########################### content details ################################ -->
							
						</div>
					</div>
				</div>
			</div>
			<!-- Main Content -->
			
			<!-- ########################### content ################################ -->
		</div>
		<div class="row">
			<!-- ########################### footer ################################ -->
			<!--include file="i_footer_detail.asp"-->
			<!-- ########################### footer ################################ -->
		</div>
	</main>

<!--#include file="i_googleAnalytics.asp"-->	
</body>
</html>