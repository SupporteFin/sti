﻿<style>
	input[type=text], textarea, select {
		border-radius: 0px;
		border: 1px solid #006c9d;
		line-height: 20px;
		font-size: 13px;
		font-weight: normal;
		margin-bottom: 4px;
		height: 30px !important;
	}
	input[type=text]:focus, textarea:focus {
		border: 1px solid #006c9d;
	}
</style>

<div style="width: 100%;color: #252525;line-height:20px;">
	<table width="100%"  border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>&nbsp;</td>
			<td>
				<table width="100%"  border="0" cellspacing="0" cellpadding="0">
					<%if session("email_member") = "" then%>
					<tr class="bgcolor_InquiryTap">
						<td id="tap1" onclick="showTap(1);" width="50%" class="bgcolor_InquiryTap2">
						<%if session("lang")="E" then%>Register<%else%>ลงทะเบียน<%end if%>
						<td>
						<td  id="tap2" onclick="showTap(2);" width="50%" class="bgcolor_InquiryTap3">
							<%if session("lang")="E" then%>Member<%else%>สมาชิก<%end if%>
						<td>
					</tr>
					<%end if%>
					<tr>
						<td colspan="3">
								<table width="100%"  border="0" cellspacing="5" cellpadding="0">
									<tr>
											<td>
													<table width="100%"  border="0" cellspacing="0" cellpadding="0">
														<%if session("email_member") = "" then%>
														<tr>
															<td style="padding-top:10px;padding-left:10px;padding-right:10px;">
																	<div id="displayTap1" style="DISPLAY: block">
																				<table width="100%"  border="0" cellspacing="0" cellpadding="5" id="displayTap1" style="DISPLAY: block">
																					<tr>
																						<td align="left" >
																								<%if session("lang")="E" then%>
																										<strong>If this is the first time you are using the e-mail alert services, then you will need to register.</strong>
																							  <%else%>
																										<strong>กรณีที่ท่านยังไม่ได้สมัครสมาชิก IR Services</strong>
																							  <%end if%>
																						</td>
																					</tr>
																					<tr>
																						<td>
																								<!--#include file="i_content_sign_up.asp"-->	
																						</td>
																					</tr>
																				</table>
																	</div>
																	
																	<div id="displayTap2" style="DISPLAY: none">
																			<table width="100%"  border="0" cellpadding="5" cellspacing="0" align="left">
																					<tr>
																								<td align="left" valign="top"><br>
																								<%if session("lang")="E" then%>			
																									Enter your E-Mail Address in the space provided, Log in IR Services
																								<%else%>
																								   กรอกอีเมลที่ช่องด้านล่าง เพื่อเข้าสู่ระบบ IR Services
																								<%end if%>		
																								</td>
																						</tr>
																						<tr valign="middle" style="height:32px;">
																								<td align="left" valign="top">
																									<table  border="0" cellspacing="0" cellpadding="3">
																												<tr align="left" valign="top" >
																														<td valign="top" style="padding-left: 26px;">	
																															<table  border="0" cellspacing="0" cellpadding="3">
																																<tr  valign="top">                 
																																		<td width="102"><font color="red"><b>*</b></font>&nbsp;<%if session("lang")="T" or session("lang")=""  then%>อีเมล<%else%>E-mail<%end if%> : </td>
																																		<td><input name="e_mail_member" id="e_mail_member"  type="text" size="30" maxlength="40"  class="style_textbox" style="width: 240px;" >
																																		</td>
																																</tr>
																															</table>
																														</td>
																												</tr>
																									</table>
																								</td>
																						</tr>
																				</table>
																	</div>
															</td>
														</tr>
														<%end if%>
														<tr valign="middle" style="height:32px;">
															<td align="left" style="padding-left: 26px;"><%if session("email_member") <> "" then%><br><%end if%>
																	<div id="displayQuestion" >
																		<table  border="0" cellspacing="0" cellpadding="0">
																			<tr  valign="top" align="left">                 
																					<td width="120">&nbsp;&nbsp;<font color="red"><b>*</b></font>&nbsp;<% if session("lang")="T" or session("lang")=""  then%>คำถาม<%else%>Questions<%end if%> :  </td>
																					<td style="padding-right:10px;"><textarea name="questions" id="questions"  type="textarea" cols="30" rows="5" class="style_text_area" style="width: 240px;" WRAP="virtual" ></textarea></td>
																			</tr>
																		</table>
																	</div>
															</td>
														</tr>
														<tr>
															<td align="left">&nbsp;</td>
														</tr>
													</table>
											</td>
									</tr>
									
								</table>
						<td>
					</tr>
				</table>	
			</td>
		</tr>
	  <tr>
			<td>&nbsp;</td>
			<td align="left" valign="middle"><br>
					   <input type="hidden" name="flag"   id="flag"   value="<%=flag%>">
						<input type="button" name="ok" <%if session("lang")="T" or session("lang")=""  then %>value="ตกลง"<%else%>value="Submit" <%end if%>  class="style_button complainbutton_request" style="padding: 5px 15px; background: #006c9d; color: #FFFFFF; text-align: center;" onclick="javascript: return chk_Signin_InquiryForm();">
						<input type="button" name="reset" <%if session("lang")="T" or session("lang")=""  then %>value="ยกเลิก"<%else%>value="Reset" <%end if%>   class="style_button2 complainbutton_request" style="padding: 5px 15px; background: #757677; color: #FFFFFF; text-align: center;" onclick="javascript:document.frm1.submit();">
						<%if session("email_member") <>"" then%><input type="button" name="logout" <%if session("lang")="T" or session("lang")=""  then %>value="ออกจากระบบ"<%else%>value="Logout" <%end if%>  class="style_button2 complainbutton_request" style="padding: 5px 15px; background: #757677; color: #FFFFFF; text-align: center;" onclick="javascript:Logout_IRService('<%=session("page_asp")%>');"><%end if%>
						<input type="hidden" name="action" id="action" >
						<input type="hidden" name="resultCaptcha " id="resultCaptcha" value="<%=lblResult%>">
						<input type="hidden" name="listed_com_id " id="listed_com_id" value="<%=listed_com_id%>">
						<input type="hidden" name="language" id="language" value="<%=session("lang")%>">
						<input type="hidden" name="hid_email_member " id="hid_email_member" value="<%=session("email_member")%>">
			</td>
	  </tr>
		<tr>		
			 <td>&nbsp;</td>
				<td align="left" valign="top"><br>
				  <%if session("lang")="T" or session("lang")=""  then%>						
				   <strong>หมายเหตุ !</strong> ถ้าท่านต้องการฝากคำถามถึงบริษัทฯ กรุณากรอกข้อมูลเพื่อฝากคำถาม  เราจะตอบคำถามของท่านโดยเร็วที่สุด
				  <%else%>
					<strong>Remark !</strong> If you have any inquiry,please provide your information and fill out your questions
					  on the following items. We will respond you shortly.
				 <%end if%>
				 </td>
			 </td>
		</tr>	
	</table>
</div>

	<script language="javascript">
		if (document.getElementById("hid_email_member").value == ''){
			if ('<%=request("action")%>' == '2'){
				showTap(2);
			}else{
				showTap(1);
			}
		}
	   function showTap(id){ 
			var maxcount=2;
			for (var i=1;i<=maxcount;i++){
				if (i==id){
					document.getElementById("tap"+i).className ='bgcolor_InquiryTap2';
					document.getElementById("displayTap"+i).style.display='block';
				}else{
					 document.getElementById("tap"+i).className ='bgcolor_InquiryTap3';
					document.getElementById("displayTap"+i).style.display='none';
					document.getElementById("tap"+i).style.cursor = 'hand';
				}
			}
			// id = 1 คือ ลงทะเบียน , id = 2 คือ สมาชิก 
			document.getElementById("action").value=id;

	   }
		
	</script >




