﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->
<!--#include file = "../../i_conreference.asp" -->
<!--#include file = "../../function_asp2007.asp" -->	

<%
session("cur_page")="IR " &  listed_share & " Download Financial Statement"
page_name="Download Financial Statement"

pagesize=15
%>
<!--#include file="../../constpage2007.asp"-->

<html>
<head>
	<!-- Fix Default -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><%=message_title%> :: <%=page_name%></title>
	<meta content="<%=message_keyword%>" name="keywords">
	<meta content="<%=message_description%>" name="description">
	<link rel="shortcut icon" href="<%=shortcut_icon%>" type="image/x-icon">
	<link rel="icon" href="<%=shortcut_icon%>" type="image/x-icon">
	<link href="style_listed.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../function2007_utf8.js"></script>
	<!-- CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/flexslider.css" rel="stylesheet" type="text/css">
	<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
	<!-- ADD CSS -->
	<link href="css/component.css" rel="stylesheet" type="text/css">
	<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">
	<!-- FONTS -->
	<link href="css/font_add.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- Mouse Hover CSS -->
	<script src="scripts/jquery.min.js" type="text/javascript"></script>
	<script src="scripts/jquery.nicescroll.min.js"></script>
	<!-- SCRIPTS -->
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<!--[if IE]><html class="ie" lang="en"> <![endif]-->
	<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
	<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
	<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
	<script src="scripts/animate.js" type="text/javascript"></script>
	<script src="scripts/myscript.js" type="text/javascript"></script>
	<script src="scripts/owl.carousel.js" type="text/javascript"></script>
	<!-- NEW -->
	<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
	<link href="css/site.css" rel="stylesheet" type="text/css" />
	<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
	<!-- scroll-hint -->
	<link rel="stylesheet" href="css/scroll-hint.css">
	
	<style>
		.font_header_title {
			font-size: 24px;
			font-weight: 600;
		}
		
		.font_th_header3{
			font-size: 14px;
			text-align: center;
			background-color: #006c9d;
			color: #ffffff;
			padding: 10px;
		}
	</style>
</head>
<body>

<main class="cd-main-content">
	<section id="main">
		<!--#include file="i_top_detail.asp"-->
	</section>
	
	<div class="container paddingmain" style="padding-top:80px;">
		<div class="row">
			<div class="col-lg-12" style="padding-bottom: 50px;">
				<div class="row">
					<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
						<div class="row">
							<div class="col-lg-12" style="padding-bottom: 10px;padding-top: 10px;">		
								<p class="font_header_title"><%if session("lang")="E" then %>Download Financial Statements <%else%>ดาวน์โหลดงบการเงิน <%end if%></p>							
								<div class="hr_header_title"></div>	
							</div>  
							<br>
						</div>
						
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" valign="top">
									<div class="main_text">
									<!-- .............................................................................. Content .............................................................................. -->	
										<form name="frm1"   METHOD="POST" ACTION="download_financial.asp">								
												<table width="1000"  border="1" cellpadding="4" cellspacing="0">
													 <tr align="center" valign="middle" class="">
														 <th  valign="middle" width="75%"  align="center" class="font_th_header3">
															 <font>
																<%if session("lang")="" or session("lang")="T"  then %>
																  งบการเงิน
																<%else%>
																  Financial Statements
																<%end if%>
															 </font>
														 </th>
														 <th  valign="middle" width="18%" align="center" class="font_th_header3">
															 <font>
																<%if session("lang")="" or session("lang")="T"  then %>
																  วันที่
																<%else%>
																  Date
																<%end if%>
															 </font>
														 </th>
													   </tr>
													<!--#include file="../../select_page2007.asp"-->		
													<%
													dim totalrec
													totalrec=0
													
													strsql=""
													strsql="SELECT count(*) as  rec_count  FROM news_financial  " 
													strsql=strsql& " where security='" &  listed_share & "' and lang='" & session("lang") &  "' order by post_date DESC"													
				   
													set rs = conreference.execute(strsql)
													if not rs.eof and not rs.bof then
													   totalrec=rs("rec_count")
													end if
													rs.close												
													session("num_count")=totalrec
													%><!--#include file="../../calculate_page2007.asp"--><%
													strsql=""
													strsql="SELECT * FROM news_financial  where security='" & listed_share & "' and lang='" & session("lang") 
													strsql=strsql & "' order by post_date DESC  limit " & ((session("pageno")-1)*session("pagesize")) & "," & session("pagesize") 
													'response.write strsql
													set rs=conreference.execute(strsql)
												  
													if not rs.eof and not rs.bof then
														 i=1							
														   do while not rs.eof
															 ' +++ For Check Show News!! +++			
															 if session("pageno") = 1 then 
																  if last_update = "" then
																	last_update = cdate(left(rs("post_date"),8))
																	session("last_update") = last_update
																  end if		
															 else
																  last_update = session("last_update")
															 end if
															 if i mod 2 <>0 then
																%>
															  <tr valign="middle" style="height: 35px;">
															  <%else%>
																<tr valign="middle" class='even' style="height: 35px;">
															 <%end if%>	
																	<td  valign="middle" align="left" style="padding-left: 12px;">	
																		   <%=bullet_news%>
																		   <a  class="web" href="<%=pathnews%>news/<%=trim(left(formatdate(rs("post_date")),4))%>/zip/<%=rs("lang")%>/<%=trim(rs("news_id"))%>.zip">
																		   <%=rs("title")%>	  </a>		
																		   <%	if last_update = cdate(left(rs("post_date"),8)) then%>
																			   <img src="images/update.gif" >
																		   <%end if%> 																												   
																													
																	</td>
																	<td  valign="middle"  align="center">	
																		  <span class="font_datetime4">  
																			<%=DisplayDate(left(formatdateAm_Pm(rs("post_date")),10))%>		
																		  </span>
																	</td>
																  </tr>
																  <%
															   rs.movenext	
															 i=i+1
															 loop
															 
													else%>
													<tr style="height: 35px;">
													   <td  valign="middle" colspan="2" align="center" class="bgcolor_no_information">											   
														   <font color="#ff0000" class="no_information">
														  <%
																   if  session("lang")="E"  then 
																	response.write "No Information Now !! "
																   else																				
																	response.write "ไม่มีข้อมูล ณ ขณะนี้ "
																   end if
																   %>
														   </font>										
													   </td>
													</tr>
													<%	end if
													rs.close
													conreference.close
													set rs=nothing
													set conreference=nothing
													
													%>		
												  </table>	
												 
												 <br><font style="font-size:12px;"><!--#include file="page_button.asp"--></font>
										</form>
									 <!-- .............................................................................. End Content .............................................................................. -->	
									</div>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- ..................... footer ..................... -->
	<!--include file="i_footer_detail.asp"-->
	<!-- ..................... footer ..................... -->

</main>
 <!--#include file="i_googleAnalytics.asp"-->	
</body>
</html>