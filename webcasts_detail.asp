﻿<!DOCTYPE html>

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->
<!--#include file = "../../i_conir.asp" -->	
<!--#include file = "i_description_menu.asp" -->

<%
pagetitle = title
title=""
title=arr_menu_desc(51)

session("cur_page")="IR " &  listed_share & " Webcast & Presentation Detail"
page_name="Webcast & Presentation Detail"
%>

<html>
<head>
<title><%=message_title%> :: <%=page_name%></title>
<meta content="<%=message_keyword%>" name="keywords">
<meta content="<%=message_description%>" name="description">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="">

<link href="style_listed.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../function2007_utf8.js"></script>
<!-- CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/flexslider.css" rel="stylesheet" type="text/css">
<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
<!-- ADD CSS -->
<link href="css/component.css" rel="stylesheet" type="text/css">
<link href="css/custom.css" rel="stylesheet" type="text/css">
<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">
<!-- FONTS -->
<link href="css/font_add.css" rel="stylesheet" type="text/css">
<link href="css/font-awesome.css" rel="stylesheet">
<!-- Mouse Hover CSS -->
<script src="scripts/jquery.min.js" type="text/javascript"></script>
<script src="scripts/jquery.nicescroll.min.js"></script>
<!-- SCRIPTS -->
<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if IE]><html class="ie" lang="en"> <![endif]-->
<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
<script src="scripts/animate.js" type="text/javascript"></script>
<script src="scripts/myscript.js" type="text/javascript"></script>
<script src="scripts/owl.carousel.js" type="text/javascript"></script>
<!-- NEW -->
<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="css/site.css" rel="stylesheet" type="text/css" />
<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
<!-- scroll-hint -->
<link rel="stylesheet" href="css/scroll-hint.css">

</head>
<body>
	<main class="cd-main-content">
		<div class="row">
			<section id="main">
				<!-- ########################### menu top ################################ -->
				<!--#include file="i_top_detail.asp"-->
				<!-- ########################### menu top ################################ -->
			</section>
		</div>
		<div class="row">
			<!-- ########################### content ################################ -->
			
			<!-- Main Content -->
			<div class="container paddingmain" style="padding-top:80px;">
				<div class="row">
					<div class="col-lg-12" style="padding-bottom: 50px;">
						<div class="row">
							<!-- ########################### content details ################################ -->
							<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
								<div class="row">
									<div class="col-lg-12">
										<p style="color:#324695;font-size: 34px;font-weight:normal;padding-bottom:12px;font-weight:bold;"><%=title%></p>
										<div style="background-color:#00ccff;width:120px;height:2px;"></div><br><br>
									</div>
								</div>
								
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="left" valign="top">
												<div style="width: 100%;color: #252525;line-height:20px;">
												<!-- .............................................................................. Content .............................................................................. -->	
													<table cellSpacing=0 cellPadding=0 width="100%" border=0 align="center">
														<TR>
															 <TD align="left">
															 <%
															 strsql="select * from calendar_event where id=" & request("id")
															 
															 set rs=conir.execute(strsql)
															 if not rs.eof then
															   file_name=rs("name")
															   if request("type")="present" then
																 file_total=rs("presentation")
															   else
																 file_total=rs("transcript")
															   end if
															 end if
															 rs.close
															 conir.close
															 set rs=nothing
															 set conir=nothing	
														%>
														<%
															 path_pdf=""
															 
															 if request("seq")="" then
															   file_seq=1
															 else
															   file_seq=request("seq")
															 end if
															 
															 if file_total > 1 then
															   dim i
															   for i=1 to cint(file_total)
																 if cint(file_seq)=i then
																	if i = 1 then
																	  path_pdf=file_name & ".pdf"
																	else 
																	  path_pdf=file_name &"_" & i-1 & ".pdf"
																	end if
																 
																 %>
																 <strong class="font_wp_active"><%=file_seq%></strong>&nbsp;&nbsp;
																 <%
																 else
																 %>
																 <a class="page" href="webcasts_detail.asp?type=<%=request("type")%>&share=<%=request("share")%>&id=<%=request("id")%>&seq=<%=i%>"><%=i%></A>&nbsp;&nbsp;
																 <%
																 end if
															   next
															 else
															   path_pdf=file_name & ".pdf"
															   %><br><%
															 end if
															 %>  	
																</TD>
															 </TR>															 
															  <TR>
																 <TD align="left" valign="top"> <br>              
																		   <%if request("type")="present" then%>
																		  <IFRAME id="detail" src="calendar/presentation/<%=path_pdf%>" frameBorder="0" width="100%" height="1200" scrolling="yes"></IFRAME>
																		  <%else%>
																		  <IFRAME id="detail" src="calendar/transcripts/<%=path_pdf%>" frameBorder="0" width="100%" height="1200" scrolling="yes"></IFRAME>
																		  <%end if%>                                                                                                                                                 
																 </TD>
															 </TR>           
														</table>
												 <!-- .............................................................................. End Content .............................................................................. -->	</div>														  
											</td>
										</tr>
										<tr>
										  <td align="left">&nbsp;</td>
										</tr>
										<tr>
										  <td></td>
										</tr>
							  </table>
								
							</div>
							<!-- ########################### content details ################################ -->
							
						</div>
					</div>
				</div>
			</div>
			<!-- Main Content -->
			
			<!-- ########################### content ################################ -->
		</div>
		<div class="row">
			<!-- ########################### footer ################################ -->
			<!--include file="i_footer_detail.asp"-->
			<!-- ########################### footer ################################ -->
		</div>
	</main>
<!--#include file="i_googleAnalytics.asp"-->	
</body>
</html>