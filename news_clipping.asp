﻿<!DOCTYPE html>

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->
<!--#include file = "../../function_asp2007.asp" -->
<!--#include file = "../../i_conirauthen.asp" --> 
<!--#include file = "../../i_conreference.asp" --> 
<%
session("cur_page")="IR " &  listed_share & " News Clippings"
session("page_asp")="news_clipping.asp"
page_name="News Clippings"

pagesize=15
%>
<!--#include file="../../constpage2007.asp"-->

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<TITLE><%=message_title%> :: <%=page_name%></TITLE>
	<META name="Title" content="<%=message_metatitle%>">
	<META name="Description" content="<%=message_description%>">
	<META name="Keywords" content="<%=message_keyword%>">	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="author" content="">
	<link rel="shortcut icon" href="<%=img_icon%>">
	<link href="style_listed.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../function2007_utf8.js"></script>
	<!-- CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/flexslider.css" rel="stylesheet" type="text/css">
	<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
	<!-- ADD CSS -->
	<link href="css/component.css" rel="stylesheet" type="text/css">
	<link href="css/custom.css" rel="stylesheet" type="text/css">
	<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">
	<!-- FONTS -->
	<link href="css/font_add.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- Mouse Hover CSS -->
	
	
	<!-- NEW -->
	<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
	<link href="css/site.css" rel="stylesheet" type="text/css" />
	
	<!-- scroll-hint -->
	<link rel="stylesheet" href="css/scroll-hint.css">
</head>

<body class="main">

	<!-- PRELOADER -->
	<img id="preloader" src="images/preloader.gif" alt="">
	<!-- //PRELOADER -->
	<div class="preloader_hide">
		<div id="page" class="single_page">

			<!--================================ Menu Top ==================================-->
				<!--#include file='i_menu_top.asp'-->
			<!--=================================== End ====================================-->

			<!--=============================== Menu Left ==================================-->
				<!--#include file='i_menu_left.asp'-->
			<!--================================== End =====================================-->

			<!--============================ Title Menu =============================-->
			<section class="mbr-section article">
				<div class="container">
					<div class="bigTopic">
						<h1 class="bigTopicText"><%=arr_menu_desc(61)%></h1>
					</div>
				</div>
			</section>
			<!--============================== End ==================================-->

			<section class="mbr-section article sectionSub">
			<!--===================================================== Content ====================================================-->
				<div class="container">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td align="left" valign="top">
								<table border="0" cellspacing="0" cellpadding="0" width="100%">
									<form name="frm1" METHOD="POST" ACTION="<%=session("page_asp")%>">
										<!-- ............................ Search ............................ -->		
										<!--#include file = "i_search_info.asp" -->
										<!-- .......................... End Search .......................... -->
										<br><br>
										 <tr>
											<td align="left" valign="top">
												<div class="table-data js-scrollable scroll-hint" style="position: relative; overflow: auto;">
													<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table-width table-MajorShareholder">
														<tbody style="font-size: 15.5px;">
														<!--#include file="../../select_page2007.asp"-->
														<%
															dim totalrec
															totalrec=0
															
															strsql=sql_clipping_search(listed_com_id,strdate,ptdate,0)
																				   
															set rs=conirauthen.execute(strsql)
															if not rs.eof and not  rs.bof then 
															   totalrec=rs("num_count")
															end if
														
															session("num_count")=totalrec
														  %>
														  <!--#include file="../../calculate_page2007.asp"-->
															  <tr class="Head-table-MajorShareholder">
																 <th width="15%" align="center" class="topicTeble" style="background-color: #006c9d; color: #FFFFFF;"><%if session("lang")="E" then%>Date<%else%>วันที่<%end if%></th> 
																 <th width="60%" align="center" class="topicTeble" style="background-color: #006c9d; color: #FFFFFF;"><%if session("lang")="E" then%>Title<%else%>หัวข้อข่าว<%end if%></th>
																 <th width="25%" align="center" class="topicTeble" style="background-color: #006c9d; color: #FFFFFF;"><%if session("lang")="E" then%>Source<%else%>แหล่งข่าว<%end if%></th>
															</tr>
															<%
															strsql=sql_clipping_search(listed_com_id,strdate,ptdate,((session("pageno")-1)*session("pagesize")) & "," & session("pagesize"))		
															'response.write strsql
															set rs_clip=conirauthen.execute(strsql)
															if not rs_clip.eof and not  rs_clip.bof then 
															   i=0
															   last_update = ""
															   do while not rs_clip.eof
															   ' +++ For Check Show News!! +++			
																   if session("pageno") = 1 then                                              
																	 if last_update = "" then
																	   last_update = cdate(left(rs_clip("timestamp"),8))
																	   session("last_update") = last_update
																	 end if
																   else
																	 last_update =   session("last_update")
																   end if
																   %>
																   <%if i mod 2 = 0 then%>
																   <tr align="left" valign="top" class="even" style="padding: 12px; background-color: #f3f3f3;">
																   <%else%>
																   <tr align="left" valign="top" class="even" style="padding: 12px; background-color: #ffffff;">
																   <%end if%> 
																	<td align="center" valign="top" width="25%" align="center" >
																		 <font>
																			<%=DisplayDate(rs_clip("timestamp")) %>
																		</font>
																	 </td>
																	 <td align="left" valign="top" width="48%">
																		 <a class="web" href="<%=pathfileserver%>/Listed/<%=listed_share%>/read_detail.asp?topic=clipping&name=<%=replace(rs_clip("file_name"),"&","@")%>" target="_blank"><%= rs_clip("title")%></a> 
																																	 
																		 <%	if last_update = cdate(left(rs_clip("timestamp"),8)) then%>
																			   &nbsp;<img src="images/update.gif" border=0 align='absmiddle'>
																		 <%end if%> 
																	 </td>
																	<% 
																	   strsource=sql_source(rs_clip("source_id"))
																	   set rs_source=conreference.execute(strsource)
																	   if not rs_source.eof and not  rs_source.bof then 
																		 if trim(rs_source("picture"))<>"" then																				
																	%>
																	<td align="center" valign="middle" width="27%">
																		<img src="../../images2007/source/<%=rs_source("picture")%>" border="0" align='absmiddle'>
																	</td>
																	<%else%>
																	<td align="center" valign="top" width="30%">-</td>	
																	 <%
																	 end if
																 else%>
																	<td align="center" valign="top" width="30%">-</td>	
																<%end if
																   rs_source.close
																%>
																</tr>
																<%
																 rs_clip.movenext
																 i=i+1
															   loop
														else
														%>
															<tr style="height: 35px;">
																   <td align="center" colspan="3"  valign="middle">
																	  <font color="#ff0000" class="no_information">
																		 <%
																		 if  session("lang")="E"  then 
																			response.write "No Information Now !! "
																		   else																				
																			response.write "ไม่มีข้อมูล ณ ขณะนี้ "
																		   end if
																		 %>
																	 </font>
																</td>
															</tr>
														<%
														end if
														rs_clip.close
														conreference.close
														set conreference=nothing
														%>
														</tbody>
													</table>
												</div>
											</td>
										</tr>
										<tr>
											<td align="left">&nbsp;</td>
										</tr>
										<tr>
											<td align="right" valign="top">
												<!--#include file="page_button.asp"-->
											</td>
										</tr>
										<tr>
											<td align="left">&nbsp;</td>
										</tr>
										<tr>
											<td>													
												<div align="left">
													<h2>Disclaimer</h2>
												</div>
												<br>
												<table width="100%"  border="0" cellpadding="10" cellspacing="1" class="bgcolor_disclaimer1">		
													<tr>
														<td class="bgcolor_disclaimer2"><br>
															<!--#include file="i_disclaimer.asp"-->  	
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</form>
								</table>
							</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td></td>
						</tr>
					</table>
				</div>
			<!--======================================================= End ======================================================-->
			</section>
			
		</div>

		<!--====================================== Footer ======================================-->
			<!--#include file='i_footer.asp'-->
		<!--======================================== End =======================================-->

		<script src="scripts/jquery.min.js" type="text/javascript"></script>
		<script type="text/javascript" src="scripts/modernizr.custom.js"></script>
		<%if session("lang")="E" then%><script type="text/javascript" src="scripts/jquery.dlmenu-en.js"></script><%else%><script type="text/javascript" src="scripts/jquery.dlmenu.js"></script><%end if%>
		<script src="scripts/jquery.nicescroll.min.js"></script>
		<!-- SCRIPTS -->
		<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
		<!--[if IE]><html class="ie" lang="en"> <![endif]-->
		<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
		<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
		<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
		<script src="scripts/animate.js" type="text/javascript"></script>
		<script src="scripts/myscript.js" type="text/javascript"></script>
		<script src="scripts/owl.carousel.js" type="text/javascript"></script>
		<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
		
		<script>
			$(function() {
				$('#dl-menu').dlmenu();
			});
		</script>
		<!-- js/scroll-hint -->
		<script src="scripts/scroll-hint.js"></script>
		<!-- js/scroll-hint -->
		<script>
			new ScrollHint('.js-scrollable', {
			});
		</script>
	</div>
	 <!--#include file="i_googleAnalytics.asp"-->	  
</body>
</html>