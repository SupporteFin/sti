﻿<!DOCTYPE html>

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->
<%
session("cur_page")="IR " &  listed_share & " IR Contact"
session("page_asp")="ir_contact.asp"
page_name="IR Contact"
%>

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<TITLE><%=message_title%> :: <%=page_name%></TITLE>
	<META name="Title" content="<%=message_metatitle%>">
	<META name="Description" content="<%=message_description%>">
	<META name="Keywords" content="<%=message_keyword%>">	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="author" content="">
	<link rel="shortcut icon" href="<%=img_icon%>">
	<link href="style_listed.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../function2007_utf8.js"></script>
	<!-- CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/flexslider.css" rel="stylesheet" type="text/css">
	<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
	<!-- ADD CSS -->
	<link href="css/component.css" rel="stylesheet" type="text/css">
	<link href="css/custom.css" rel="stylesheet" type="text/css">
	<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">
	<!-- FONTS -->
	<link href="css/font_add.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- Mouse Hover CSS -->
	<script src="scripts/jquery.min.js" type="text/javascript"></script>
	<script src="scripts/jquery.nicescroll.min.js"></script>
	<!-- SCRIPTS -->
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<!--[if IE]><html class="ie" lang="en"> <![endif]-->
	<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
	<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
	<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
	<script src="scripts/animate.js" type="text/javascript"></script>
	<script src="scripts/myscript.js" type="text/javascript"></script>
	<script src="scripts/owl.carousel.js" type="text/javascript"></script>
	<!-- NEW -->
	<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
	<link href="css/site.css" rel="stylesheet" type="text/css" />
	<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
	<!-- scroll-hint -->
	<link rel="stylesheet" href="css/scroll-hint.css">
</head>

<body class="main">

	<!-- PRELOADER -->
	<img id="preloader" src="images/preloader.gif" alt="">
	<!-- //PRELOADER -->
	<div class="preloader_hide">
		<div id="page" class="single_page">

			<!--================================ Menu Top ==================================-->
				<!--#include file='i_menu_top.asp'-->
			<!--=================================== End ====================================-->

			<!--=============================== Menu Left ==================================-->
				<!--#include file='i_menu_left.asp'-->
			<!--================================== End =====================================-->

			<!--============================ Title Menu =============================-->
			<section class="mbr-section article">
				<div class="container">
					<div class="bigTopic">
						<h1 class="bigTopicText"><%=arr_menu_desc(67)%></h1>
					</div>
				</div>
			</section>
			<!--============================== End ==================================-->

			<section class="mbr-section article sectionSub">
			<!--===================================================== Content ====================================================-->
			<%if session("lang")="E" then%>
				<div class="container">
					<p style="font-size: 22px;font-weight: 500;margin-right: 15px;margin-top: 3px;color: #006c9d;text-align: left;margin-bottom: 40px;">นักลงทุนสัมพันธ์ :</p>
					<div style="float: left;">
					<p class="ContactHeadText" style="margin-right: 75px;">Name :</p>
					<p class="bigTopicSubText">Miss Jurairat Maipranet </p>
					<p class="ContactHeadText" style="margin-bottom: 80px;margin-right: 66px;">Address  :</p>
					<p class="bigTopicSubText">Stonehenge Inter Public Company Limited <br>163 Soi Chokchai Ruamitr (Ratchada 19), Dindaeng Sub-district, Dindaeng District, Bangkok 10400 </p>
					<p class="ContactHeadText" style="margin-right: 35px;">Tel.  :</p>
					<p class="bigTopicSubText">0-2690-7462 ext. 126</p>
					<p class="ContactHeadText" style="margin-right: 62px;">Email   :</p>
					<p class="bigTopicSubText" style="margin-bottom: 35px;"><a href="mailto:ir@sti.co.th" class="">ir@sti.co.th</a></p>
					</div>

					<!-- <ul style="padding-left: 110px;margin-bottom: 50px;">
						  <li class="text-note" style="margin: 0 0 13px;">
							  <p class="margin0">น.ส. จุไรรัตน์ ใหม่พระเนตร</p>
						  </li>
						  <li class="text-note" style="margin: 0 0 13px;">
							  <p class="margin0">บริษัท สโตนเฮ้นจ์ อินเตอร์ จำกัด (มหาชน) <br>
							  เลขที่ 163 ซอยโชคชัยร่วมมิตร (รัชดา 19) ถนนรัชดาภิเษก แขวงดินแดง เขตดินแดง กรุงเทพฯ 10400</p>
						  </li>
						  <li class="text-note" style="margin: 0 0 13px;">
							  <p class="margin0">โทรศัพท์ 0-2690-7462 ต่อ 126</p>
						  </li>
						  <li class="text-note" style="margin: 0 0 13px;">
							  <p class="margin0"><a href="mailto:ir@sti.co.th" class="">ir@sti.co.th</a></p>
						  </li>
					</ul> -->

					<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15498.663413203036!2d100.5736489!3d13.7990059!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x133607c057623116!2sStonehenge+Inter+Public+Company+Limited!5e0!3m2!1sth!2sth!4v1545913412700" height="430" frameborder="0" style="border:0;width: 100%;" allowfullscreen></iframe>

					<!-- <div style="height: 430px;overflow: hidden;border: 2px solid #dddddd;"><img src="images/ct.jpg"></div> -->

					</div>
			<%else%>
				<div class="container">
					<p style="font-size: 22px;font-weight: 500;margin-right: 15px;margin-top: 3px;color: #006c9d;text-align: left;margin-bottom: 40px;">นักลงทุนสัมพันธ์ :</p>
					<div style="float: left;">
					<p class="ContactHeadText" style="margin-right: 75px;">ชื่อ :</p>
					<p class="bigTopicSubText">น.ส. จุไรรัตน์ ใหม่พระเนตร</p>
					<p class="ContactHeadText" style="margin-bottom: 80px;margin-right: 66px;">ที่อยู่  :</p>
					<p class="bigTopicSubText">บริษัท สโตนเฮ้นจ์ อินเตอร์ จำกัด (มหาชน) <br> เลขที่ 163 ซอยโชคชัยร่วมมิตร (รัชดา 19) ถนนรัชดาภิเษก แขวงดินแดง เขตดินแดง กรุงเทพฯ 10400</p>
					<p class="ContactHeadText" style="margin-right: 35px;">โทรศัพท์ :</p>
					<p class="bigTopicSubText">โทรศัพท์ 0-2690-7462 ต่อ 126</p>
					<p class="ContactHeadText" style="margin-right: 62px;">อีเมล  :</p>
					<p class="bigTopicSubText" style="margin-bottom: 35px;"><a href="mailto:ir@sti.co.th" class="">ir@sti.co.th</a></p>
					</div>

					<!-- <ul style="padding-left: 110px;margin-bottom: 50px;">
						  <li class="text-note" style="margin: 0 0 13px;">
							  <p class="margin0">น.ส. จุไรรัตน์ ใหม่พระเนตร</p>
						  </li>
						  <li class="text-note" style="margin: 0 0 13px;">
							  <p class="margin0">บริษัท สโตนเฮ้นจ์ อินเตอร์ จำกัด (มหาชน) <br>
							  เลขที่ 163 ซอยโชคชัยร่วมมิตร (รัชดา 19) ถนนรัชดาภิเษก แขวงดินแดง เขตดินแดง กรุงเทพฯ 10400</p>
						  </li>
						  <li class="text-note" style="margin: 0 0 13px;">
							  <p class="margin0">0-2690-7462 ต่อ 126</p>
						  </li>
						  <li class="text-note" style="margin: 0 0 13px;">
							  <p class="margin0"><a href="mailto:ir@sti.co.th" class="">ir@sti.co.th</a></p>
						  </li>
					</ul> -->

					<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15498.663413203036!2d100.5736489!3d13.7990059!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x133607c057623116!2sStonehenge+Inter+Public+Company+Limited!5e0!3m2!1sth!2sth!4v1545913412700" height="430" frameborder="0" style="border:0;width: 100%;" allowfullscreen></iframe>

					<!-- <div style="height: 430px;overflow: hidden;border: 2px solid #dddddd;"><img src="images/ct.jpg"></div> -->

					</div>
			<%end if%>
			<!--======================================================= End ======================================================-->
			</section>
			
		</div>

		<!--====================================== Footer ======================================-->
			<!--#include file='i_footer.asp'-->
		<!--======================================== End =======================================-->

		<script type="text/javascript" src="scripts/modernizr.custom.js"></script>
		<%if session("lang")="E" then%><script type="text/javascript" src="scripts/jquery.dlmenu-en.js"></script><%else%><script type="text/javascript" src="scripts/jquery.dlmenu.js"></script><%end if%>
		<script>
			$(function() {
				$('#dl-menu').dlmenu();
			});
		</script>
		<!-- js/scroll-hint -->
		<script src="scripts/scroll-hint.js"></script>
		<!-- js/scroll-hint -->
		<script>
			new ScrollHint('.js-scrollable', {
			});
		</script>
	</div>
	 <!--#include file="i_googleAnalytics.asp"-->	  
</body>
</html>