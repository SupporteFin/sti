﻿<!DOCTYPE html>

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->
<%
session("cur_page")="IR " &  listed_share & " Stock Quote"
session("page_asp")="stock_quote.asp"
page_name="Stock Quote"
%>

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<TITLE><%=message_title%> :: <%=page_name%></TITLE>
	<META name="Title" content="<%=message_metatitle%>">
	<META name="Description" content="<%=message_description%>">
	<META name="Keywords" content="<%=message_keyword%>">	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="author" content="">
	<link rel="shortcut icon" href="<%=img_icon%>">
	<link href="style_listed.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../function2007_utf8.js"></script>
	<!-- CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/flexslider.css" rel="stylesheet" type="text/css">
	<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
	<!-- ADD CSS -->
	<link href="css/component.css" rel="stylesheet" type="text/css">
	<link href="css/custom.css" rel="stylesheet" type="text/css">
	<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">
	<!-- FONTS -->
	<link href="css/font_add.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- Mouse Hover CSS -->
	<script src="scripts/jquery.min.js" type="text/javascript"></script>
	<script src="scripts/jquery.nicescroll.min.js"></script>
	<!-- SCRIPTS -->
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<!--[if IE]><html class="ie" lang="en"> <![endif]-->
	<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
	<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
	<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
	<script src="scripts/animate.js" type="text/javascript"></script>
	<script src="scripts/myscript.js" type="text/javascript"></script>
	<script src="scripts/owl.carousel.js" type="text/javascript"></script>
	<!-- NEW -->
	<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
	<link href="css/site.css" rel="stylesheet" type="text/css" />
	<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
	<!-- scroll-hint -->
	<link rel="stylesheet" href="css/scroll-hint.css">
</head>

<body class="main">

	<!-- PRELOADER -->
	<img id="preloader" src="images/preloader.gif" alt="">
	<!-- //PRELOADER -->
	<div class="preloader_hide">
		<div id="page" class="single_page">

			<!--================================ Menu Top ==================================-->
				<!--#include file='i_menu_top.asp'-->
			<!--=================================== End ====================================-->

			<!--=============================== Menu Left ==================================-->
				<!--#include file='i_menu_left.asp'-->
			<!--================================== End =====================================-->

			<!--============================ Title Menu =============================-->
			<section class="mbr-section article">
				<div class="container">
					<div class="bigTopic">
						<h1 class="bigTopicText"><%=arr_menu_desc(54)%></h1>
					</div>
				</div>
			</section>
			<!--============================== End ==================================-->

			<section class="mbr-section article sectionSub">
			<!--===================================================== Content ====================================================-->
				<div class="container" style="min-height: 260px;">
					<% if url_stock_quote <> "" and url_stock_graph <> ""  then%>
					<table border="0" cellspacing="0" cellpadding="0" align="center" width="100%">
						<tr>
							<td align="right" valign="top">
							  <% if url_stock_quote <> ""  then%>
								<table width="205" border="0" cellspacing="3" cellpadding="5" style="BORDER-TOP: #EAEAEA 1px solid;BORDER-BOTTOM: #EAEAEA 1px solid;BORDER-LEFT: #EAEAEA1px solid;BORDER-RIGHT: #EAEAEA 1px solid;BACKGROUND: #FFFFFF; ">
									<tr bgcolor="#F6F6F6">
										<td>
											<strong><font color="#1F76B8">Stock Price</font></strong>
										</td>
									</tr>
									<tr>
										<td>
											 <iframe src="<%=url_stock_quote%>"  frameborder=0 scrolling=no width="203" height="215"  framepadding=0 framespacing=0 class="border_stock"></iframe>				
										</td>
									</tr>
								</table>
							  <% end if%>						
							</td>
							<td width="10%">&nbsp;</td>
							<td align="left" valign="top">
							  <% if url_stock_graph <> ""  then%>
								<table width="205" border="0" cellspacing="1" cellpadding="5" style="BORDER-TOP: #EAEAEA 1px solid;BORDER-BOTTOM: #EAEAEA 1px solid;BORDER-LEFT: #EAEAEA 1px solid;BORDER-RIGHT: #EAEAEA 1px solid;BACKGROUND: #FFFFFF; ">
									<tr bgcolor="#F6F6F6">
										<td>
											<strong><font color="#1F76B8">SET Index</font></strong>
										</td>
									</tr>
									<tr>
										<td>
											<iframe src="<%=url_stock_graph%>"  frameborder=0 scrolling=no width="203" height="260"  framepadding=0 framespacing=0 class="border_stock"></iframe>		
										</td>
									</tr>
								</table>
							  <% end if%>
							</td>								
						</tr>
						</table>
					<%end if%>
				</div>
			<!--======================================================= End ======================================================-->
			</section>
			
		</div>

		<!--====================================== Footer ======================================-->
			<!--#include file='i_footer.asp'-->
		<!--======================================== End =======================================-->

		<script type="text/javascript" src="scripts/modernizr.custom.js"></script>
		<%if session("lang")="E" then%><script type="text/javascript" src="scripts/jquery.dlmenu-en.js"></script><%else%><script type="text/javascript" src="scripts/jquery.dlmenu.js"></script><%end if%>
		<script>
			$(function() {
				$('#dl-menu').dlmenu();
			});
		</script>
		<!-- js/scroll-hint -->
		<script src="scripts/scroll-hint.js"></script>
		<!-- js/scroll-hint -->
		<script>
			new ScrollHint('.js-scrollable', {
			});
		</script>
	</div>
	 <!--#include file="i_googleAnalytics.asp"-->	  
</body>
</html>