﻿<!DOCTYPE html>

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->
<%
session("cur_page")="IR " &  listed_share & " Board Charter"
session("page_asp")="boardcharter.asp"
page_name="Board Charter"
%>

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<TITLE><%=message_title%> :: <%=page_name%></TITLE>
	<META name="Title" content="<%=message_metatitle%>">
	<META name="Description" content="<%=message_description%>">
	<META name="Keywords" content="<%=message_keyword%>">	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="author" content="">
	<link rel="shortcut icon" href="<%=img_icon%>">
	<link href="style_listed.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../function2007_utf8.js"></script>
	<!-- CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/flexslider.css" rel="stylesheet" type="text/css">
	<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
	<!-- ADD CSS -->
	<link href="css/component.css" rel="stylesheet" type="text/css">
	<link href="css/custom.css" rel="stylesheet" type="text/css">
	<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">
	<!-- FONTS -->
	<link href="css/font_add.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- Mouse Hover CSS -->
	<script src="scripts/jquery.min.js" type="text/javascript"></script>
	<script src="scripts/jquery.nicescroll.min.js"></script>
	<!-- SCRIPTS -->
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<!--[if IE]><html class="ie" lang="en"> <![endif]-->
	<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
	<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
	<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
	<script src="scripts/animate.js" type="text/javascript"></script>
	<script src="scripts/myscript.js" type="text/javascript"></script>
	<script src="scripts/owl.carousel.js" type="text/javascript"></script>
	<!-- NEW -->
	<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
	<link href="css/site.css" rel="stylesheet" type="text/css" />
	<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
	<!-- scroll-hint -->
	<link rel="stylesheet" href="css/scroll-hint.css">
</head>

<body class="main">

	<!-- PRELOADER -->
	<img id="preloader" src="images/preloader.gif" alt="">
	<!-- //PRELOADER -->
	<div class="preloader_hide">
		<div id="page" class="single_page">

			<!--================================ Menu Top ==================================-->
				<!--#include file='i_menu_top.asp'-->
			<!--=================================== End ====================================-->

			<!--=============================== Menu Left ==================================-->
				<!--#include file='i_menu_left.asp'-->
			<!--================================== End =====================================-->

			<!--============================ Title Menu =============================-->
			<section class="mbr-section article">
				<div class="container">
					<div class="bigTopic">
						<h1 class="bigTopicText"><%=arr_menu_desc(49)%></h1>
					</div>
				</div>
			</section>
			<!--============================== End ==================================-->

			<section class="mbr-section article sectionSub">
			<!--===================================================== Content ====================================================-->
			<%if session("lang")="E" then%>
				<div class="container">
					<p style="font-size: 22px;font-weight: 500;margin-right: 15px;margin-top: 3px;color: #006c9d;text-align: left;">Board Charter</p>

					  <div class="table-data js-scrollable scroll-hint" style="position: relative; overflow: auto;">
						<table border="0" cellspacing="0" cellpadding="0" class="table-width">
						  <tbody style="font-size: 15.5px;">
							<tr>
							  <th width="5%" align="center" class="topicTeble" style=""></th> 
							  <th width="55%" align="center" class="topicTeble" style="">Document name</th>   
							  <th width="15%" align="center" class="topicTeble">File size</th>
							  <th width="15%" align="center" class="topicTeble">File type</th>
							  <th width="10%" align="center" class="topicTeble">Download</th>
							</tr>
							<!-- Table Header -->
							<tr class="even">
							  <td align="center" class="topicTeble2">1</td>
							  <td align="left" class="topicTeble2">กฎบัตรของคณะกรรมการบริษัท บริษัท สโตนเฮ้นจ์ อินเตอร์ จำกัด (มหาชน)</td>
							  <td align="center" class="topicTeble2">276 KB</td>
							  <td align="center" class="topicTeble2">PDF</td>
							  <td align="center" class="topicTeble2">
								<a href="pdf/5-5_1_Charter_BOD_101060.pdf" target="_blank" download>
								<img src="images/dw.png"></a>
							  </td>
							</tr>
							<tr class="even">
							  <td align="center" class="topicTeble2">2</td>
							  <td align="left" class="topicTeble2">กฎบัตรของคณะกรรมการตรวจสอบ บริษัท สโตนเฮ้นจ์ อินเตอร์ จำกัด (มหาชน)</td>
							  <td align="center" class="topicTeble2">376 KB</td>
							  <td align="center" class="topicTeble2">PDF</td>
							  <td align="center" class="topicTeble2">
								<a href="pdf/5-5_2_Charter_AC_101060.pdf" target="_blank" download>
								<img src="images/dw.png"></a>
							  </td>
							</tr>
							<tr class="even">
							  <td align="center" class="topicTeble2">3</td>
							  <td align="left" class="topicTeble2">กฎบัตรของคณะกรรมการบริหาร บริษัท สโตนเฮ้นจ์ อินเตอร์ จำกัด (มหาชน)</td>
							  <td align="center" class="topicTeble2">101 KB</td>
							  <td align="center" class="topicTeble2">PDF</td>
							  <td align="center" class="topicTeble2">
								<a href="pdf/5-5_3_Charter_ExeCom_101060.pdf" target="_blank" download>
								<img src="images/dw.png"></a>
							  </td>
							</tr>
							<tr class="even">
							  <td align="center" class="topicTeble2">4</td>
							  <td align="left" class="topicTeble2">กฎบัตรของคณะกรรมการสรรหาและพิจารณาค่าตอบแทน บริษัท สโตนเฮ้นจ์ อินเตอร์ จำกัด (มหาชน)</td>
							  <td align="center" class="topicTeble2">111 KB</td>
							  <td align="center" class="topicTeble2">PDF</td>
							  <td align="center" class="topicTeble2">
								<a href="pdf/5-5_4_Charter_NRC_101060.pdf" target="_blank" download>
								<img src="images/dw.png"></a>
							  </td>
							</tr>
							<tr class="even">
							  <td align="center" class="topicTeble2">5</td>
							  <td align="left" class="topicTeble2">กฎบัตรของคณะกรรมการบริหารความเสี่ยง บริษัท สโตนเฮ้นจ์ อินเตอร์ จำกัด (มหาชน)</td>
							  <td align="center" class="topicTeble2">82 KB</td>
							  <td align="center" class="topicTeble2">PDF</td>
							  <td align="center" class="topicTeble2">
								<a href="pdf/5-5_5_Charter_RMC_101060.pdf" target="_blank" download>
								<img src="images/dw.png"></a>
							  </td>
							</tr>
							<tr class="even">
							  <td align="center" class="topicTeble2">6</td>
							  <td align="left" class="topicTeble2">กฎบัตรของประธานเจ้าหน้าที่บริหาร บริษัท สโตนเฮ้นจ์ อินเตอร์ จำกัด (มหาชน)</td>
							  <td align="center" class="topicTeble2">102 KB</td>
							  <td align="center" class="topicTeble2">PDF</td>
							  <td align="center" class="topicTeble2">
								<a href="pdf/5-5_6_Charter_CEO_101060.pdf" target="_blank" download>
								<img src="images/dw.png"></a>
							  </td>
							</tr>
						   

						  </tbody>
						</table>
					  </div>
				  
					</div>
			<%else%>
				<div class="container">
					<p style="font-size: 22px;font-weight: 500;margin-right: 15px;margin-top: 3px;color: #006c9d;text-align: left;">กฏบัตรคณะกรรมการ</p>

					  <div class="table-data js-scrollable scroll-hint" style="position: relative; overflow: auto;">
						<table border="0" cellspacing="0" cellpadding="0" class="table-width">
						  <tbody style="font-size: 15.5px;">
							<tr>
							  <th width="5%" align="center" class="topicTeble" style=""></th> 
							  <th width="55%" align="center" class="topicTeble" style="">ชื่อเอกสาร</th>   
							  <th width="15%" align="center" class="topicTeble">ขนาดไฟล์</th>
							  <th width="15%" align="center" class="topicTeble">ชนิดไฟล์</th>
							  <th width="10%" align="center" class="topicTeble">ดาวน์โหลด</th>
							</tr>
							<!-- Table Header -->
							<tr class="even">
							  <td align="center" class="topicTeble2">1</td>
							  <td align="left" class="topicTeble2">กฎบัตรของคณะกรรมการบริษัท บริษัท สโตนเฮ้นจ์ อินเตอร์ จำกัด (มหาชน)</td>
							  <td align="center" class="topicTeble2">276 KB</td>
							  <td align="center" class="topicTeble2">PDF</td>
							  <td align="center" class="topicTeble2">
								<a href="pdf/5-5_1_Charter_BOD_101060.pdf" target="_blank" download>
								<img src="images/dw.png"></a>
							  </td>
							</tr>
							<tr class="even">
							  <td align="center" class="topicTeble2">2</td>
							  <td align="left" class="topicTeble2">กฎบัตรของคณะกรรมการตรวจสอบ บริษัท สโตนเฮ้นจ์ อินเตอร์ จำกัด (มหาชน)</td>
							  <td align="center" class="topicTeble2">376 KB</td>
							  <td align="center" class="topicTeble2">PDF</td>
							  <td align="center" class="topicTeble2">
								<a href="pdf/5-5_2_Charter_AC_101060.pdf" target="_blank" download>
								<img src="images/dw.png"></a>
							  </td>
							</tr>
							<tr class="even">
							  <td align="center" class="topicTeble2">3</td>
							  <td align="left" class="topicTeble2">กฎบัตรของคณะกรรมการบริหาร บริษัท สโตนเฮ้นจ์ อินเตอร์ จำกัด (มหาชน)</td>
							  <td align="center" class="topicTeble2">101 KB</td>
							  <td align="center" class="topicTeble2">PDF</td>
							  <td align="center" class="topicTeble2">
								<a href="pdf/5-5_3_Charter_ExeCom_101060.pdf" target="_blank" download>
								<img src="images/dw.png"></a>
							  </td>
							</tr>
							<tr class="even">
							  <td align="center" class="topicTeble2">4</td>
							  <td align="left" class="topicTeble2">กฎบัตรของคณะกรรมการสรรหาและพิจารณาค่าตอบแทน บริษัท สโตนเฮ้นจ์ อินเตอร์ จำกัด (มหาชน)</td>
							  <td align="center" class="topicTeble2">111 KB</td>
							  <td align="center" class="topicTeble2">PDF</td>
							  <td align="center" class="topicTeble2">
								<a href="pdf/5-5_4_Charter_NRC_101060.pdf" target="_blank" download>
								<img src="images/dw.png"></a>
							  </td>
							</tr>
							<tr class="even">
							  <td align="center" class="topicTeble2">5</td>
							  <td align="left" class="topicTeble2">กฎบัตรของคณะกรรมการบริหารความเสี่ยง บริษัท สโตนเฮ้นจ์ อินเตอร์ จำกัด (มหาชน)</td>
							  <td align="center" class="topicTeble2">82 KB</td>
							  <td align="center" class="topicTeble2">PDF</td>
							  <td align="center" class="topicTeble2">
								<a href="pdf/5-5_5_Charter_RMC_101060.pdf" target="_blank" download>
								<img src="images/dw.png"></a>
							  </td>
							</tr>
							<tr class="even">
							  <td align="center" class="topicTeble2">6</td>
							  <td align="left" class="topicTeble2">กฎบัตรของประธานเจ้าหน้าที่บริหาร บริษัท สโตนเฮ้นจ์ อินเตอร์ จำกัด (มหาชน)</td>
							  <td align="center" class="topicTeble2">102 KB</td>
							  <td align="center" class="topicTeble2">PDF</td>
							  <td align="center" class="topicTeble2">
								<a href="pdf/5-5_6_Charter_CEO_101060.pdf" target="_blank" download>
								<img src="images/dw.png"></a>
							  </td>
							</tr>

						  </tbody>
						</table>
					  </div>
				  
					</div>
			<%end if%>
			<!--======================================================= End ======================================================-->
			</section>
			
		</div>

		<!--====================================== Footer ======================================-->
			<!--#include file='i_footer.asp'-->
		<!--======================================== End =======================================-->

		<script type="text/javascript" src="scripts/modernizr.custom.js"></script>
		<%if session("lang")="E" then%><script type="text/javascript" src="scripts/jquery.dlmenu-en.js"></script><%else%><script type="text/javascript" src="scripts/jquery.dlmenu.js"></script><%end if%>
		<script>
			$(function() {
				$('#dl-menu').dlmenu();
			});
		</script>
		<!-- js/scroll-hint -->
		<script src="scripts/scroll-hint.js"></script>
		<!-- js/scroll-hint -->
		<script>
			new ScrollHint('.js-scrollable', {
			});
		</script>
	</div>
	 <!--#include file="i_googleAnalytics.asp"-->	  
</body>
</html>