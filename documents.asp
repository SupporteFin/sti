﻿<!DOCTYPE html>

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->
<%
session("cur_page")="IR " &  listed_share & " documents"
session("page_asp")="documents.asp"
page_name="documents"
%>

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<TITLE><%=message_title%> :: <%=page_name%></TITLE>
	<META name="Title" content="<%=message_metatitle%>">
	<META name="Description" content="<%=message_description%>">
	<META name="Keywords" content="<%=message_keyword%>">	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="author" content="">
	<link rel="shortcut icon" href="<%=img_icon%>">
	<link href="style_listed.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../function2007_utf8.js"></script>
	<!-- CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/flexslider.css" rel="stylesheet" type="text/css">
	<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
	<!-- ADD CSS -->
	<link href="css/component.css" rel="stylesheet" type="text/css">
	<link href="css/custom.css" rel="stylesheet" type="text/css">
	<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">
	<!-- FONTS -->
	<link href="css/font_add.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- Mouse Hover CSS -->
	<script src="scripts/jquery.min.js" type="text/javascript"></script>
	<script src="scripts/jquery.nicescroll.min.js"></script>
	<!-- SCRIPTS -->
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<!--[if IE]><html class="ie" lang="en"> <![endif]-->
	<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
	<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
	<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
	<script src="scripts/animate.js" type="text/javascript"></script>
	<script src="scripts/myscript.js" type="text/javascript"></script>
	<script src="scripts/owl.carousel.js" type="text/javascript"></script>
	<!-- NEW -->
	<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
	<link href="css/site.css" rel="stylesheet" type="text/css" />
	<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
	<!-- scroll-hint -->
	<link rel="stylesheet" href="css/scroll-hint.css">
	
   <!-- css timeliner -->
  <!-- <link href="css/Timeline.css" rel="stylesheet" type="text/css"> -->
  <link rel="stylesheet" href="css/timeliner.css" type="text/css" media="screen">
  <link rel="stylesheet" href="css/colorbox.css" type="text/css" media="screen">
  <!-- end Timeline -->
  
  <!-- js timeliner -->
  <script type="text/javascript" src="scripts/colorbox.js"></script>
  <script type="text/javascript" src="scripts/timeliner.js"></script>
</head>

<body class="main">

	<!-- PRELOADER -->
	<img id="preloader" src="images/preloader.gif" alt="">
	<!-- //PRELOADER -->
	<div class="preloader_hide">
		<div id="page" class="single_page">

			<!--================================ Menu Top ==================================-->
				<!--#include file='i_menu_top.asp'-->
			<!--=================================== End ====================================-->

			<!--=============================== Menu Left ==================================-->
				<!--#include file='i_menu_left.asp'-->
			<!--================================== End =====================================-->

			<!--============================ Title Menu =============================-->
			<section class="mbr-section article">
				<div class="container">
					<div class="bigTopic">
						<h1 class="bigTopicText"><%=arr_menu_desc(28)%></h1>
					</div>
				</div>
			</section>
			<!--============================== End ==================================-->
			<section class="mbr-section article sectionSub">
			<!--===================================================== Content ====================================================-->
				<div class="container" style="min-height: 260px;">
					<table width="100%">
					<%if session("lang")="E" then%>
						<tbody>
							<tr>
								<th class="topicTeble" align="center">File</th>  
								<th class="topicTeble" align="center">Download</th>
							</tr>
							<!-- Table Header -->

							<tr class="even">
								<td align="left">Company Regulation</td>
								<td align="center"><a href="pdf/CompanyRegulation_EN.pdf" target="_blank"> <img src="images/icon_download.png"></a></td>
							</tr>
							<tr class="even">
								<td align="left">Memorandum</td>
								<td align="center"><a href="pdf/Memorandum.pdf" target="_blank"> <img src="images/icon_download.png"></a></td>
							</tr>
						</tbody>
					<%else%>
						<tbody>
							<tr>
								<th class="topicTeble" align="center">เอกสาร</th>  
								<th class="topicTeble" align="center">ดาวน์โหลด</th>
							</tr>
							<!-- Table Header -->

							<tr class="even">
								<td align="left">ข้อบังคับบริษัท</td>
								<td align="center"><a href="pdf/CompanyRegulation.pdf" target="_blank"> <img src="images/icon_download.png"></a></td>
							</tr>
							<tr class="even">
								<td align="left">หนังสือบริคณฑ์สนธิ </td>
								<td align="center"><a href="pdf/Memorandum.pdf" target="_blank"> <img src="images/icon_download.png"></a></td>
							</tr>
						</tbody>
					<%end if%>
					</table>
			<!--======================================================= End ======================================================-->
				</div>
			</section>
		</div>
		
		<!--====================================== Footer ======================================-->
			<!--#include file='i_footer.asp'-->
		<!--======================================== End =======================================-->

		<script type="text/javascript" src="scripts/modernizr.custom.js"></script>
		<%if session("lang")="E" then%><script type="text/javascript" src="scripts/jquery.dlmenu-en.js"></script><%else%><script type="text/javascript" src="scripts/jquery.dlmenu.js"></script><%end if%>
		
		<script>
			$(function() {
				$('#dl-menu').dlmenu();
			});
		</script>
		<!-- js/scroll-hint -->
		<script src="scripts/scroll-hint.js"></script>
		<!-- js/scroll-hint -->
		<script>
			new ScrollHint('.js-scrollable', {
			});
		</script>
		
		<script type="text/javascript">
			$(function () {
				$(".demo1").bootstrapNews({
					newsPerPage: 7,
					autoplay: false,
					pauseOnHover:true,
					direction: 'up',
					newsTickerInterval: 4000,
					onToDo: function () {
						//console.log(this);
					}
				});
				
				$(".demo2").bootstrapNews({
					newsPerPage: 7,
					autoplay: false,
					pauseOnHover:true,
					direction: 'up',
					newsTickerInterval: 4000,
					onToDo: function () {
						//console.log(this);
					}
				});
			});

			// timeliner
			$(document).ready(function() {
			  $.timeliner({});
			  $.timeliner({
				timelineContainer: '#timeline-js',
				timelineSectionMarker: '.milestone',
				oneOpen: true,
				startState: 'flat',
				expandAllText: '+ Show All',
				collapseAllText: '- Hide All'
			  });
			  // Colorbox Modal
			  $(".CBmodal").colorbox({inline:true, initialWidth:100, maxWidth:682, initialHeight:100, transition:"elastic",speed:750});
			});
		</script>
	</div>
	 <!--#include file="i_googleAnalytics.asp"-->	  
</body>
</html>