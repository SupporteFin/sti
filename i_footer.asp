<style>
	.styletext-textarea {
		width:100% !important; 
		margin:0 0 10px !important;
		padding: 10px 8px !important;
		text-transform:none !important;
		font-weight:400 !important;
		line-height:20px !important;
		color: #333 !important;
		font-style:normal !important;
		border-radius: 0 !important;
		background: none !important;
		border: 2px solid #242424 !important;
		box-shadow: none !important;
		transition: border-color 0.3s ease-in-out !important;
		-webkit-transition: border-color 0.3s ease-in-out !important;
	}
	.styletext-textarea:focus {
	outline: 0;
	box-shadow: none;
	color:#666;
	border-color:#ccc;
}
</style>

<section id="contacts"></section>
<footer style="opacity: 0;font-family: 'promptextralight';">
	<div class="container">
		<div class="row animated fadeInUp" data-appear-top-offset="-200" data-animated="fadeInUp">
			<div class="col-lg-5 col-md-5 col-sm-8 padbot30">
				<a href="images/map.jpg" target="_blank"><img src="images/map.jpg" data-toggle="tooltip" data-placement="top" title="คลิกที่รูปภาพเพื่อดูรูปใหญ่"></a>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6 padbot30 foot_about_block">
				<p class="b"><b><font color="#32abca">Stonehenge Inter Public Company Limited</font></b></p>
				<p><font size="3" color="#ffffff">163 ซอยโชคชัยร่วมมิตร (รัชดา 19)<br>แขวงดินแดง เขตดินแดง กรุงเทพมหานคร 10400</font></p>
				<p><font size="3" color="#ffffff">อีเมล : service@sti.co.th <br>เบอร์โทรศัพท์ : 66(0)-2690-7462 <br>เบอร์แฟกซ์ : 66(0)-2690-7463</font></p>
				<table>
					<tbody>
						<tr>
							<td>
								<ul class="social">
									<li><a href="https://www.facebook.com/StonehengeInter/" target="_blank"><i class="fa fa-facebook"></i></a></li>
									<li><a href="javascript:void(0);"><i class="map_show fa fa-map-marker"></i></a></li>
								</ul>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="respond_clear"></div>
			<div class="col-lg-3 col-md-3 padbot30">
				<p class="b"><b>Contacts</b> Us</p>
				<div class="span9 contact_form">
					<div id="note"></div>
					<div id="fields">
						<form action="include/process.php" method="POST" id="contact-form-face" class="clearfix">
							<input type="text" name="name" required="" placeholder="Name" class="styletext-textarea" disabled="disabled">
							<textarea name="message" required="" placeholder="Message" class="styletext-textarea" disabled="disabled"></textarea>
							<input class="contact_btn" name="contact" type="submit" value="Send message" disabled="disabled">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>

<!-- MAP -->
<div id="map">
	<a class="map_hide" href="javascript:void(0);"><i class="fa fa-angle-right"></i><i class="fa fa-angle-left"></i></a>
	<iframe src="https://www.google.com/maps/embed?wmode=transparent&amp;pb=!1m18!1m12!1m3!1d1937.3374137676474!2d100.57312982686487!3d13.798465573037554!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29c3524e401eb%3A0x8bd1c5e15dfc962d!2z4Lia4Lij4Li04Lip4Lix4LiXIOC4quC5guC4leC4meC5gOC4ruC5ieC4meC4iOC5jCDguK3guLTguJnguYDguJXguK3guKPguYwg4LiI4Liz4LiB4Lix4LiU!5e0!3m2!1sth!2sth!4v1509173239394"></iframe>
</div>
<!-- //MAP -->