﻿<div style="width: 100%;color: #252525;line-height:20px;">
	<table  width="100%" border="0" cellpadding="1" cellspacing="0">
			<tr>
				<td width="20px">&nbsp;</td>
				<td valign="top">
					<%if session("lang") = "E" then%>
						<table  width="100%" border="0" cellpadding="1" cellspacing="0">
							<tr>
									<td colspan="2"><b><font size="4px">Stonehenge Inter Public Company Limited</font></b><br><br></td>
							</tr>
							<tr>
									<td valign="top">Address :</td>
									<td valign="top">163 Chokechairuammitr (Ratchada19), Ratchadaphisek Rd. Dindaeng District<br />
																 Dindaeng Bangkok 10400</td>
							</tr>
							<tr>
									<td width="80px" valign="top">Tel. :</td>
									<td valign="top">0-2690-7462 ext. 126
									</td>
							</tr>
							<tr>
									<td valign="top">Fax :</td>
									<td valign="top">0-2690-7463
									</td>
							</tr>
							<tr>
									<td valign="top">Email :</td>
									<td valign="top"><a href="mailto:ir@sti.co.th"  target="_blank">ir@sti.co.th</a></p>
									</td>
							</tr>
						</table>	
					<%else%>
					<table  width="100%" border="0" cellpadding="1" cellspacing="0">
							<tr>
									<td colspan="2"><b><font size="4px">บริษัท สโตนเฮ้นจ์ อินเตอร์ จำกัด (มหาชน) </font></b><br><br></td>
							</tr>
							<tr>
									<td valign="top">สถานที่ตั้ง :</td>
									<td valign="top">เลขที่ 163 ซอยโชคชัยร่วมมิตร (รัชดา 19) ถนนรัชดาภิเษก แขวงดินแดง เขตดินแดง<br />
																กรุงเทพฯ 10400</td>
							</tr>
							<tr>
									<td width="80px" valign="top">โทรศัพท์ :</td>
									<td valign="top">0-2690-7462 ต่อ 126
									</td>
							</tr>
							<tr>
									<td valign="top">โทรสาร :</td>
									<td valign="top">0-2690-7463
									</td>
							</tr>
							<tr>
									<td valign="top">อีเมล :</td>
									<td valign="top"><a href="mailto:ir@sti.co.th"  target="_blank">ir@sti.co.th</a></p>
									</td>
							</tr>
						</table>		
					<%end if%>
				</td>
			</tr>
	</table>
	<hr>
</div>
<div class="line_dashed"><div>
