<%
menu_description=""

if session("lang")="E"   then
'menu top
	menu_desc="หน้าแรก#ข้อมูลบริษัท#ข้อมูลนักลงทุนสัมพันธ์"								'0-2
	menu_desc=menu_desc & "#ผลงาน#Residential Building#Commercial &amp; Office#Education Building#Hospital &amp; Clinic#Factory &amp; Warehouse#Hotel &amp; Resort#Housing" 		'3-10
	menu_desc=menu_desc & "#ข่าวสารและกิจกรรม#ข่าวสาร#กิจกรรม#กิจกรรมเพื่อสังคม"				'11-14
	menu_desc=menu_desc & "#ร่วมงานกับเรา#ติดต่อเรา#ช่องทางการร้องเรียน"					'15-17			
	'menu left
	menu_desc=menu_desc & "#IR MENU#IR Home#Corporate Information#History#Vision#Chairman’s Statement#Board of Director#Organization Chart#Shareholder Structure#Awards and Certificates#Company Documents"				'18-28	
	menu_desc=menu_desc & "#Financial Information#Financial Statement#Financial Highlight#MD&A"				'29-32	
	menu_desc=menu_desc & "#Shareholder Information#General Meeting#Major Shareholder#Dividend Payment#IR Calendar#Company Snapshot#Fact Sheet#Filing"			'33-40		
	menu_desc=menu_desc & "#Annual Report / Form 56-1#Annual Report#Form 56-1"					'41-43
	menu_desc=menu_desc & "#Sustainability#Corporate Governance#Anti-corruption#Business Ethics#CSR & Activities#Board Charter#Company policy"		'44-50		
	menu_desc=menu_desc & "#Webcast & Presentation#Analyst Report#Stock Information#Stock Price#Historical Price#Investment Calculator"				'51-56	
	menu_desc=menu_desc & "#News Room#SET Announcement#News Update#Public Relation#News Clipping#Press Release"					'57-62
	menu_desc=menu_desc & "#Information Request System#Inquiry From#Complaint Channel#E-mail Alert#IR Contact"					'63-67
	menu_desc=menu_desc & "#IR CALENDAR#Multimedia"			'68-69

		 
else   'menu top
	menu_desc="หน้าแรก#ข้อมูลบริษัท#ข้อมูลนักลงทุนสัมพันธ์"								'0-2
	menu_desc=menu_desc & "#ผลงาน#Residential Building#Commercial &amp; Office#Education Building#Hospital &amp; Clinic#Factory &amp; Warehouse#Hotel &amp; Resort#Housing" 		'3-10
	menu_desc=menu_desc & "#ข่าวสารและกิจกรรม#ข่าวสาร#กิจกรรม#กิจกรรมเพื่อสังคม"				'11-14
	menu_desc=menu_desc & "#ร่วมงานกับเรา#ติดต่อเรา#ช่องทางการร้องเรียน"					'15-17		
	'menu left
	menu_desc=menu_desc & "#เมนูนักลงทุนสัมพันธ์#หน้าแรก#ข้อมูลบริษัท#ประวัติบริษัท#วิสัยทัศน์#สารจากประธานกรรมการบริษัท#คณะกรรมการบริษัท#โครงสร้างองค์กร#โครงสร้างการถือหุ้นของกลุ่มบริษัท#รางวัลและมาตรฐาน#เอกสารสำคัญของบริษัท "			'18-28			
	menu_desc=menu_desc & "#ข้อมูลทางการเงิน#งบการเงิน#ข้อมูลสำคัญทางการเงิน#คำอธิบายและการวิเคราะห์"				'29-32		
	menu_desc=menu_desc & "#ข้อมูลผู้ถือหุ้น#การประชุมผู้ถือหุ้น#โครงสร้างผู้ถือหุ้น#นโยบายและการจ่ายเงินปันผล#ปฏิทินกิจกรรมนักลงทุนสัมพันธ์#Company Snapshot#สรุปข้อสนเทศ#หนังสือชี้ชวน"					'33-40	
	menu_desc=menu_desc & "#รายงานประจำปี / แบบ 56-1#รายงานประจำปี#แบบ 56-1"					'41-43
	menu_desc=menu_desc & "#การพัฒนาสู่ความยั่งยืน#การกำกับดูแลกิจการ#การต่อต้านทุจริตและคอร์รัปชั่น#จริยธรรมในการดำเนินธุรกิจ#ความรับผิดชอบต่อสังคมและกิจกรรมต่างๆ#กฏบัตรคณะกรรมการ#นโยบายบริษัท"					'44-50		
	menu_desc=menu_desc & "#กิจกรรมและเอกสารนำเสนอ#บทวิเคราะห์#ข้อมูลราคาหลักทรัพย์#ราคาหลักทรัพย์ล่าสุด#ราคาหลักทรัพย์ย้อนหลัง#เครื่องคำนวณการลงทุน"		'51-56
	menu_desc=menu_desc & "#ห้องข่าวประชาสัมพันธ์# ข่าวแจ้งตลาดหลักทรัพย์#ข่าวสารล่าสุด#ห้องข่าวประชาสัมพันธ์#ข่าวจากหนังสือพิมพ์#ข่าวเผยแพร่"			'57-62
	menu_desc=menu_desc & "#สอบถามข้อมูล#สอบถามข้อมูล#ช่องทางรับเรื่องร้องเรียน#รับข่าวสารทางอีเมล#ติดต่อนักลงทุนสัมพันธ์"					'63-67
	menu_desc=menu_desc & "#ปฏิทินนักลงทุน#Multimedia"			'68-69
	
		
end if

'after "#" is Count Frist ex "hello(0)#world(1)#summer(2)"
'number zero is not "#" 

arr_menu_desc=split(menu_desc,"#")

%>
