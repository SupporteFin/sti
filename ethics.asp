﻿<!DOCTYPE html>

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->
<%
session("cur_page")="IR " &  listed_share & " Business Ethics"
session("page_asp")="ethics.asp"
page_name="Business Ethics"
%>

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<TITLE><%=message_title%> :: <%=page_name%></TITLE>
	<META name="Title" content="<%=message_metatitle%>">
	<META name="Description" content="<%=message_description%>">
	<META name="Keywords" content="<%=message_keyword%>">	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="author" content="">
	<link rel="shortcut icon" href="<%=img_icon%>">
	<link href="style_listed.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../function2007_utf8.js"></script>
	<!-- CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/flexslider.css" rel="stylesheet" type="text/css">
	<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
	<!-- ADD CSS -->
	<link href="css/component.css" rel="stylesheet" type="text/css">
	<link href="css/custom.css" rel="stylesheet" type="text/css">
	<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">
	<!-- FONTS -->
	<link href="css/font_add.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- Mouse Hover CSS -->
	<script src="scripts/jquery.min.js" type="text/javascript"></script>
	<script src="scripts/jquery.nicescroll.min.js"></script>
	<!-- SCRIPTS -->
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<!--[if IE]><html class="ie" lang="en"> <![endif]-->
	<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
	<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
	<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
	<script src="scripts/animate.js" type="text/javascript"></script>
	<script src="scripts/myscript.js" type="text/javascript"></script>
	<script src="scripts/owl.carousel.js" type="text/javascript"></script>
	<!-- NEW -->
	<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
	<link href="css/site.css" rel="stylesheet" type="text/css" />
	<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
	<!-- scroll-hint -->
	<link rel="stylesheet" href="css/scroll-hint.css">
</head>

<body class="main">

	<!-- PRELOADER -->
	<img id="preloader" src="images/preloader.gif" alt="">
	<!-- //PRELOADER -->
	<div class="preloader_hide">
		<div id="page" class="single_page">

			<!--================================ Menu Top ==================================-->
				<!--#include file='i_menu_top.asp'-->
			<!--=================================== End ====================================-->

			<!--=============================== Menu Left ==================================-->
				<!--#include file='i_menu_left.asp'-->
			<!--================================== End =====================================-->

			<!--============================ Title Menu =============================-->
			<section class="mbr-section article">
				<div class="container">
					<div class="bigTopic">
						<h1 class="bigTopicText"><%=arr_menu_desc(47)%></h1>
					</div>
				</div>
			</section>
			<!--============================== End ==================================-->

			<section class="mbr-section article sectionSub">
			<!--===================================================== Content ====================================================-->
			<%if session("lang")="E" then%>
				<div class="container">
					<p style="font-size: 22px;font-weight: 500;margin-right: 15px;margin-top: 3px;color: #006c9d;text-align: left;">Code of Conduct and Business Practices</p>
					<p class="bigTopicSubText" style="margin-bottom: 35px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					The Company’s Board of Directors has policy to support its Corporate Governance by aiming to operate business on the principles of social responsibility which covers needs of all stakeholders
					and aligns with way to foster balance between business, society, and environment which will lead to a sustainable organization. In this regard, the Board of Directors has prepared 
					“Code of Conduct and Business Practices” to be used as guidelines for directors, executives, and employees of companies within Stonehenge Inter Public Company Limited. 
					The Company operates its businesses on the basis of corporate governance and responsibility to related individuals of both internal and external and conducts regular review and 
					monitoring on compliance with this Code of Conduct and Business Practices.
					</p>

					<p class="bigTopicSubText">This Code of Conduct has been approved by the meeting of the Board of Directors No. 2/2018 on March 27, 2018 and has been effective since March 28, 2018 onwards.</p>


					  <div class="table-data js-scrollable scroll-hint" style="position: relative; overflow: auto;">
						<table border="0" cellspacing="0" cellpadding="0" class="table-width">
						  <tbody style="font-size: 15.5px;">
							<tr>
							  <th width="60%" align="center" class="topicTeble" style="">Document name</th>   
							  <th width="15%" align="center" class="topicTeble">File size</th>
							  <th width="15%" align="center" class="topicTeble">File type</th>
							  <th width="10%" align="center" class="topicTeble">Download</th>
							</tr>
							<!-- Table Header -->
							<tr class="even">
							  <td align="center" class="topicTeble2">Code of Conduct Business Practices</td>
							  <td align="center" class="topicTeble2">249 KB</td>
							  <td align="center" class="topicTeble2">PDF</td>
							  <td align="center" class="topicTeble2">
								<a href="pdf/5-3_BusinessEthics_270361_EN.pdf" target="_blank" download>
								<img src="images/dw.png"></a>
							  </td>
							</tr>

						  </tbody>
						</table>
					  </div>
				   
					  <div style="height: 75px;"></div>

					</div>
			<%else%>
				<div class="container">
					<p style="font-size: 22px;font-weight: 500;margin-right: 15px;margin-top: 3px;color: #006c9d;text-align: left;">หลักการดำเนินธุรกิจ</p>
					<p class="bigTopicSubText" style="margin-bottom: 35px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;คณะกรรมการบริษัทฯ มีนโนบายที่จะส่งเสริมธรรมาภิบาลขององค์กร โดยมุ่งเน้นการดำเนินธุรกิจตามหลักการความรับผิดชอบต่อสังคม โดยครอบคลุม ความต้องการของกลุ่มผู้มีส่วนได้ส่วนเสียทุกด้าน และสอดคล้องกับวิถีแห่งการสร้าง ความสมดุลระหว่างธุรกิจ สังคมและสิ่งแวดล้อม ซึ่งจะนำไปสู่ความเป็นองค์กรแห่งความยั่งยืน ทั้งนี้คณะกรรมการบริษัทฯ ได้จัดให้มีคู่มือ "จริยธรรมทางธุรกิจ และข้อพึงปฏิบัติในการทำงาน" เพื่อใช้เป็นแนวทางปฏิบัติที่ดี ของกรรมการผู้บริหาร และพนักงาน ของบริษัทในกลุ่มบริษัท สโตนเฮ้นจ์ อินเตอร์ จำกัด (มหาชน) โดยดำเนินธุรกิจบนพื้นฐานแห่งธรรมาภิบาลและความรับผิดชอบต่อผู้เกี่ยวข้องทั้งภายใน และภายนอกองค์กร และกำหนดให้มีการทบทวน ติดตามผลการปฏิบัติงานตามคู่มือจริยธรรมทางธุรกิจและข้อพึงปฏิบัติในการทำงาน อย่างต่อเนื่อง</p>

					<p class="bigTopicSubText">จริยธรรมธุรกิจฉบับนี้ ได้รับการอนุมัติจากที่ประชุมคณะกรรมการบริษัท ครั้งที่ 2/2561 เมื่อวันที่ 27 มีนาคม 2561 โดยมีผลบังคับใช้ตั้งแต่วันที่ 28 มีนาคม 2561</p>


					  <div class="table-data js-scrollable scroll-hint" style="position: relative; overflow: auto;">
						<table border="0" cellspacing="0" cellpadding="0" class="table-width">
						  <tbody style="font-size: 15.5px;">
							<tr>
							  <th width="60%" align="center" class="topicTeble" style="">ชื่อเอกสาร</th>   
							  <th width="15%" align="center" class="topicTeble">ขนาดไฟล์</th>
							  <th width="15%" align="center" class="topicTeble">ชนิดไฟล์</th>
							  <th width="10%" align="center" class="topicTeble">ดาวน์โหลด</th>
							</tr>
							<!-- Table Header -->
							<tr class="even">
							  <td align="center" class="topicTeble2">จริยธรรมธุรกิจ ข้อพึงปฏิบัติในการทำงาน</td>
							  <td align="center" class="topicTeble2">416 KB</td>
							  <td align="center" class="topicTeble2">PDF</td>
							  <td align="center" class="topicTeble2">
								<a href="pdf/5-3_BusinessEthics_270361_THAI.pdf" target="_blank" download>
								<img src="images/dw.png"></a>
							  </td>
							</tr>

						  </tbody>
						</table>
					  </div>
				   
					  <div style="height: 75px;"></div>

					</div>
			<%end if%>
			<!--======================================================= End ======================================================-->
			</section>
			
		</div>

		<!--====================================== Footer ======================================-->
			<!--#include file='i_footer.asp'-->
		<!--======================================== End =======================================-->

		<script type="text/javascript" src="scripts/modernizr.custom.js"></script>
		<%if session("lang")="E" then%><script type="text/javascript" src="scripts/jquery.dlmenu-en.js"></script><%else%><script type="text/javascript" src="scripts/jquery.dlmenu.js"></script><%end if%>
		<script>
			$(function() {
				$('#dl-menu').dlmenu();
			});
		</script>
		<!-- js/scroll-hint -->
		<script src="scripts/scroll-hint.js"></script>
		<!-- js/scroll-hint -->
		<script>
			new ScrollHint('.js-scrollable', {
			});
		</script>
	</div>
	 <!--#include file="i_googleAnalytics.asp"-->	  
</body>
</html>