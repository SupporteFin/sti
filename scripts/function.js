function ir_calculateCommission(value)
{
	var min_comm = document.calculator.min_commission_enter.value;

	var comm = value * (document.calculator.commission_enter.value / 100);
	if (comm < min_comm) { comm = min_comm; }

	return comm;
}

function ir_calculateAmount()
{
	// Get values for Shares Held, Price Purchase and Price Sold
	document.calculator.shares_held.value = si_formatNumber(document.calculator.shares_held_enter.value, "###,###,###,###,###,###,###");
	document.calculator.price_purchase.value = si_formatNumber(document.calculator.price_purchase_enter.value, "###,###,###,###,###,###,###");
	document.calculator.price_sold.value = si_formatNumber(document.calculator.price_sold_enter.value, "###,###,###,###,###,###,###");

	if (document.calculator.shares_held_enter.value && document.calculator.price_purchase_enter.value && document.calculator.price_sold_enter.value)
	{
  	// Calculate Profit
  	var buy_value=document.calculator.shares_held_enter.value*document.calculator.price_purchase_enter.value;
  	var sell_value=document.calculator.shares_held_enter.value*document.calculator.price_sold_enter.value;
  	var profit=sell_value-buy_value;

  	document.calculator.profit.value = si_formatNumber(profit.toFixed(2), "###,###,###,###,###,###,###");

  	var commission=parseFloat(ir_calculateCommission(buy_value))+parseFloat(ir_calculateCommission(sell_value));

  	var total_fee=parseFloat(commission);
  	var vat=total_fee*(document.calculator.vat_enter.value/100);
  	var comm_vat=parseFloat(total_fee)+parseFloat(vat);

  	document.calculator.commission.value = si_formatNumber(commission.toFixed(2), "###,###,###,###,###,###,###");
  	document.calculator.vat.value = si_formatNumber(vat.toFixed(2), "###,###,###,###,###,###,###");

  	var net_profit=parseFloat(profit)-comm_vat;
  	document.calculator.net_profit.value = si_formatNumber(net_profit.toFixed(2), "###,###,###,###,###,###,###");

  	var buy_comm=parseFloat(ir_calculateCommission(buy_value));
  	    var buy_vat=parseFloat(buy_comm * (document.calculator.vat_enter.value/100));
  	    var invest_change_percent=(net_profit/(parseFloat(buy_value)+parseFloat(buy_comm)+parseFloat(buy_vat)))*100;

  	document.calculator.invest_change_percent.value = si_formatNumber(invest_change_percent.toFixed(3), "###,###,###,###,###,###,###");
  }
  else
  {
    document.calculator.profit.value='';
    document.calculator.commission.value='';
    document.calculator.vat.value='';
    document.calculator.net_profit.value='';
    document.calculator.invest_change_percent.value='';
  }

  // Calculate Dividend
	if (document.calculator.shares_held_enter.value && (document.calculator.dividend_taxed_enter.value || document.calculator.dividend_tax_exempt_enter.value))
	{
	  var dividend_taxed = document.calculator.dividend_taxed_enter.value ? document.calculator.dividend_taxed_enter.value : 0;
	  var dividend_tax_exempt = document.calculator.dividend_tax_exempt_enter.value ? document.calculator.dividend_tax_exempt_enter.value : 0;

	  var net_dividend=document.calculator.shares_held_enter.value * (parseFloat(dividend_taxed * 0.9) + parseFloat(dividend_tax_exempt));
	  document.calculator.net_dividend.value = si_formatNumber(net_dividend.toFixed(2), "###,###,###,###,###,###,###");
	}
	else
	{
	  document.calculator.net_dividend.value='';
	}
}

/* ..................................................................................  ADD For Format Number 23-12-2009 BY Bowl .................................................................................. */
 var si_si_g=navigator.userAgent.toLowerCase();var _si_gecko=(si_si_g.indexOf("gecko")!= -1);var _si_IE=document.all?true:false;var _si_mouseX=0;var _si_mouseY=0;document.onmousemove=si_si_b;function si_selectDropDownRemoveAllOptions(si_si_x){var i;for(i=si_si_x.options.length-1;i>=0;i--){si_si_x.remove(i);}};function si_selectDropDownAddOption(si_si_x,value,text){var si_si_M=document.createElement("option");si_si_M.text=text;si_si_M.value=value;si_si_x.options.add(si_si_M);};function si_si_b(e){if(!e)e=window.event;if(!e)return;if(typeof(e.pageX)=='number'){_si_mouseX=e.pageX;_si_mouseY=e.pageY;}else if(typeof(e.clientX)=='number'){_si_mouseX=e.clientX;_si_mouseY=e.clientY;if(document.body&&(document.body.scrollLeft||document.body.scrollTop)){_si_mouseX+=document.body.scrollLeft;_si_mouseY+=document.body.scrollTop;}else if(document.documentElement&&(document.documentElement.scrollLeft||document.documentElement.scrollTop)){_si_mouseX+=document.documentElement.scrollLeft;_si_mouseY+=document.documentElement.scrollTop;}}if(_si_mouseX<0)_si_mouseX=0;if(_si_mouseY<0)_si_mouseY=0;return;};function si_getObject(si_si_F){if(document.getElementById&&document.getElementById(si_si_F))return document.getElementById(si_si_F);else if(document.all&&document.all(si_si_F))return document.all(si_si_F);else return false;};function si_changeObjectStyle(si_si_F,si_si_z,si_si_y){var si_si_E=si_getObject(si_si_F).style;if(si_si_E){if(si_si_z=='display')si_si_E.display=si_si_y;if(si_si_z=='visibility')si_si_E.visibility=si_si_y;if(si_si_z=='left')si_si_E.left=si_si_y;if(si_si_z=='top')si_si_E.top=si_si_y;if(si_si_z=='zIndex')si_si_E.zIndex=si_si_y;return true;}else return false;};function si_toggleObjectDisplay(si_si_F){var si_si_n;var si_si_D=si_getObject(si_si_F);if(si_si_D){if(window.getComputedStyle)si_si_n=window.getComputedStyle(si_si_D,null).getPropertyValue('display');else if(si_si_D.currentStyle)si_si_n=eval('si_si_D.currentStyle.display');if(si_si_n=='none')if(si_si_D=='[object HTMLTableRowElement]')si_si_D.style.display='table-row';else si_si_D.style.display='block';else if(si_si_n=='table-row')si_si_D.style.display='none';else if(si_si_n=='block')si_si_D.style.display='none';}return true;};function si_setCookie(name,value,si_si_l){var expires;if(si_si_l){var si_si_k=new Date();si_si_k.setTime(si_si_k.getTime()+(si_si_l*24*60*60*1000));expires="; expires="+si_si_k.toGMTString();}else expires="";document.cookie=name+"="+value+expires+"; path=/";};function si_getCookie(name){var si_si_K=name+"=";var si_si_h=document.cookie.split(';');var n;for(n=0;n<si_si_h.length;n++){var c=si_si_h[n];while(c.charAt(0)==' ')c=c.substring(1,c.length);if(c.indexOf(si_si_K)==0)return c.substring(si_si_K.length,c.length);}return null;};function si_openWindow(si_si_v,si_si_t,si_si_p){window.open(si_si_v,si_si_t,si_si_p);};var si_si_e,si_si_d='',si_si_c,si_si_f;function si_dropDownSearchPopulate(si_si_w){var si_si_o=(si_si_w)?((si_si_w.target)?si_si_w.target:si_si_w.srcElement):window.event.srcElement;if(si_si_f!=si_si_o){si_si_c=new Array();for(var i=0;i<si_si_o.options.length;i++)si_si_c[i]=si_si_o.options[i].text.toLowerCase();si_si_f=si_si_o;}};function si_dropDownSearchSetSelection(si_si_w){var si_si_I=(si_si_w)?si_si_w:window.event;var si_si_o=(si_si_I.target)?si_si_I.target:si_si_I.srcElement;var keyCode=si_si_I.keyCode;if((keyCode>47&&keyCode<58)||(keyCode>64&&keyCode<91||keyCode==32));else if(keyCode>95&&keyCode<106)keyCode-=48;else if(keyCode>105&&keyCode<112)keyCode-=64;else if(keyCode>187&&keyCode<192)keyCode-=144;else if(keyCode>218&&keyCode<222)keyCode-=128;else{switch(keyCode){case 187:keyCode=61;break;case 222:keyCode=39;break;case 192:keyCode=96;break;case 186:keyCode=59;break;default:return;}}var si_si_i=String.fromCharCode(keyCode).toLowerCase();var si_si_q,si_si_j=si_si_o.selectedIndex,si_si_u=false;var si_si_L=new Date().getTime();if(si_si_e!=null&&si_si_L-si_si_e<1000){si_si_d+=si_si_i;si_si_q=si_si_a();if(si_si_q== -1)return;}else{si_si_d=si_si_i;si_si_q=si_si_j+1;if(si_si_q>=si_si_c.length||si_si_c[si_si_q].length==0||si_si_c[si_si_q].charAt(0)!=si_si_d)si_si_q=si_si_a();}if(si_si_q>=0){si_si_o.options[si_si_j].selected=false;var si_si_B=new RegExp('^'+si_si_d.charAt(0)+'+$',"i");if(_si_gecko&&si_si_B.test(si_si_d)&&si_si_q>0)si_si_o.options[si_si_q-1].selected=true;else si_si_o.options[si_si_q].selected=true;}si_si_e=si_si_L;};function si_si_a(){var si_si_O=si_si_d.length;for(var i=0;i<si_si_c.length;i++)if(si_si_c[i].length>=si_si_O&&si_si_c[i].substring(0,si_si_O)==si_si_d)return i;return-1;};function si_formatNumber(si_si_N,si_si_B,si_si_J){var result='';var si_si_H=0;si_si_G=si_si_N.split('.');si_si_r=si_si_G[0];si_si_N+='';if(si_si_N<0)si_si_H=1;si_si_s=si_si_r.length-1;si_si_A=si_si_B.length-1;while((si_si_s>=0)&&(si_si_A>=0)){var si_si_m=si_si_r.charAt(si_si_s);si_si_s--;if((si_si_m<'0')||(si_si_m>'9'))continue;while(si_si_A>=0){var si_si_C=si_si_B.charAt(si_si_A);si_si_A--;if(si_si_C=='#'){result=si_si_m+result;break;}else{result=si_si_C+result;}}}if(si_si_G[1])result=result+'.'+si_si_G[1];if(si_si_H){if(si_si_J=='()')result='('+result+')';else result='-'+result;}return result;}
 
 /* .................................................................................. For News room 05-01-2010 BY Bowl .................................................................................. */
    function BannerKey(e) {
      var key = (e.which) ? e.which : event.keyCode;
          
      if ((key==44) || (key==46) || (key > 47 && key < 58)) {
         return true;
      }
         return false;
   } 
   
       function IsNumeric(sID,sMessage,sValue)
{

   var ValidChars = "0123456789.,";
   var IsNumber=true;
   var Char;
   //document.calculator.price_purchase_enter.value
   var sText = sValue;
   /*
  element(sID).className = 'Correct';
  element(sMessage).style.display = 'none';
  element(sMessage).className = 'noMessageError';*/
   for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {
       /*  element(sID).className = 'noCorrect';
         element(sMessage).style.display = 'block';
         element(sMessage).className = 'messageError';*/
         IsNumber = false;
         }
      }
   return IsNumber;
   }
   
   
 function DefaultStyleMessageError() {
//ราคาซื้อ 
document.calculator.price_purchase_enter.className = 'Correct'; 
document.getElementById("message_price_purchase").style.display='none';
document.getElementById("message_price_purchase").className='noMessageError';
//จำนวนหุ้นที่ถือ
document.calculator.shares_held_enter.className = 'Correct'; 
document.getElementById("message_shares_held").style.display='none';
document.getElementById("message_shares_held").className='noMessageError';
//ค่าคอมมิสชั่น
document.calculator.commission_enter.className = 'Correct'; 
document.getElementById("message_commission").style.display='none';
document.getElementById("message_commission").className='noMessageError';
//คอมมิสชั่นขั้นต่ำ
document.calculator.min_commission_enter.className = 'Correct'; 
document.getElementById("message_min_commission").style.display='none';
document.getElementById("message_min_commission").className='noMessageError';
//ภาษีมูลค่าเพิ่ม 
document.calculator.vat_enter.className = 'Correct'; 
document.getElementById("message_vat").style.display='none';
document.getElementById("message_vat").className='noMessageError';
//ราคาขาย
 document.calculator.price_sold_enter.className = 'Correct'; 
document.getElementById("message_price_sold").style.display='none';
document.getElementById("message_price_sold").className='noMessageError';
 //หักภาษี 
 document.calculator.dividend_taxed_enter.className = 'Correct'; 
document.getElementById("message_dividend_taxed").style.display='none';
document.getElementById("message_dividend_taxed").className='noMessageError';
 //ยกเว้นภาษี 
 document.calculator.dividend_tax_exempt_enter.className = 'Correct'; 
document.getElementById("message_dividend_tax_exempt").style.display='none';
document.getElementById("message_dividend_tax_exempt").className='noMessageError';
   }
   
   
   
   //  add new function 19/06/2013
   
 function CheckTextInput()
 {	
 //alert(document.calculator.price_purchase_enter.value);
 DefaultStyleMessageError();
 
var checking_text_input;

			if (document.calculator.price_purchase_enter.value != ''){
				if (IsNumeric("price_purchase_enter","message_price_purchase",document.calculator.price_purchase_enter.value)== false )
				{ //ราคาซื้อ 
					 document.calculator.price_purchase_enter.className = 'noCorrect';
					  document.getElementById("message_price_purchase").style.display='block';
					  document.getElementById("message_price_purchase").className='messageError';
					//checking_text_input = "false";
				} 
			}

			if (document.calculator.shares_held_enter.value != ''){
				if (IsNumeric("shares_held_enter","message_shares_held",document.calculator.shares_held_enter.value)== false )
				{ //จำนวนหุ้นที่ถือ
						document.calculator.shares_held_enter.className = 'noCorrect';
						document.getElementById("message_shares_held").style.display='block';
						document.getElementById("message_shares_held").className='messageError';
					//checking_text_input = "false";
				}
			} 
		
			if (document.calculator.commission_enter.value != ''){
				if (IsNumeric("commission_enter","message_commission",document.calculator.commission_enter.value)== false )
				{ //ค่าคอมมิสชั่น
						document.calculator.commission_enter.className = 'noCorrect';
						document.getElementById("message_commission").style.display='block';
						document.getElementById("message_commission").className='messageError';
					//checking_text_input = "false";
				}
			}
	
			
			if (document.calculator.min_commission_enter.value != ''){
				if (IsNumeric("min_commission_enter","message_min_commission",document.calculator.min_commission_enter.value)== false )
				{ //คอมมิสชั่นขั้นต่ำ
						document.calculator.min_commission_enter.className = 'noCorrect';
						document.getElementById("message_min_commission").style.display='block';
						document.getElementById("message_min_commission").className='messageError';
					checking_text_input = "false";
				} 
			}
			
		if (document.calculator.vat_enter.value != ''){
				if (IsNumeric("vat_enter","message_vat",document.calculator.vat_enter.value)== false )
				{ //ภาษีมูลค่าเพิ่ม 
						document.calculator.vat_enter.className = 'noCorrect';
						document.getElementById("message_vat").style.display='block';
						document.getElementById("message_vat").className='messageError';
					//checking_text_input = "false";
				} 
			} 
			
		if (document.calculator.price_sold_enter.value != ''){
				if (IsNumeric("price_sold_enter","message_price_sold",document.calculator.price_sold_enter.value)== false )
				{ //ราคาขาย
						document.calculator.price_sold_enter.className = 'noCorrect';
						document.getElementById("message_price_sold").style.display='block';
						document.getElementById("message_price_sold").className='messageError';
					//checking_text_input = "false";
				} 
			}
			
			if (document.calculator.dividend_taxed_enter.value != ''){
				if (IsNumeric("dividend_taxed_enter","message_dividend_taxed",document.calculator.dividend_taxed_enter.value)== false )
				{ //หักภาษี 
						document.calculator.dividend_taxed_enter.className = 'noCorrect';
						document.getElementById("message_dividend_taxed").style.display='block';
						document.getElementById("message_dividend_taxed").className='messageError';
					//checking_text_input = "false";
				} 
			}
			
			if (document.calculator.dividend_tax_exempt_enter.value != ''){
				if (IsNumeric("dividend_tax_exempt_enter","message_dividend_tax_exempt",document.calculator.dividend_tax_exempt_enter.value)== false )
				{ //ยกเว้นภาษี 
						document.calculator.dividend_tax_exempt_enter.className = 'noCorrect';
						document.getElementById("message_dividend_tax_exempt").style.display='block';
						document.getElementById("message_dividend_tax_exempt").className='messageError';
					//checking_text_input = "false";
				} 
			}
			
	

	//if (checking_text_input=="true") 
	{ 
	ir_calculateAmount();     // เข้า ir_calculateAmount เพื่อทำการคำนวณ
	}		

     }

	 function MM_goToURL(url) { //v3.0
		window.location.assign(url);
	}