if($.cookie('popup') != 'on'){
  $('#popup-item').show();
}

$('.close-notification').click(function(){
  $(this).parents('#popup-item').slideUp();
  $.cookie('popup', 'off', { expires: 1, path: '/' });
});