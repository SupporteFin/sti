﻿<!DOCTYPE html>

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->
<%
session("cur_page")="IR " &  listed_share & " Shareholder Structure"
session("page_asp")="structure.asp"
page_name="Shareholder Structure"
%>

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<TITLE><%=message_title%> :: <%=page_name%></TITLE>
	<META name="Title" content="<%=message_metatitle%>">
	<META name="Description" content="<%=message_description%>">
	<META name="Keywords" content="<%=message_keyword%>">	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="author" content="">
	<link rel="shortcut icon" href="<%=img_icon%>">
	<link href="style_listed.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../function2007_utf8.js"></script>
	<!-- CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/flexslider.css" rel="stylesheet" type="text/css">
	<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
	<!-- ADD CSS -->
	<link href="css/component.css" rel="stylesheet" type="text/css">
	<link href="css/custom.css" rel="stylesheet" type="text/css">
	<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">
	<!-- FONTS -->
	<link href="css/font_add.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- Mouse Hover CSS -->
	<script src="scripts/jquery.min.js" type="text/javascript"></script>
	<script src="scripts/jquery.nicescroll.min.js"></script>
	<!-- SCRIPTS -->
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<!--[if IE]><html class="ie" lang="en"> <![endif]-->
	<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
	<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
	<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
	<script src="scripts/animate.js" type="text/javascript"></script>
	<script src="scripts/myscript.js" type="text/javascript"></script>
	<script src="scripts/owl.carousel.js" type="text/javascript"></script>
	<!-- NEW -->
	<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
	<link href="css/site.css" rel="stylesheet" type="text/css" />
	<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
	<!-- scroll-hint -->
	<link rel="stylesheet" href="css/scroll-hint.css">
</head>

<body class="main">

	<!-- PRELOADER -->
	<img id="preloader" src="images/preloader.gif" alt="">
	<!-- //PRELOADER -->
	<div class="preloader_hide">
		<div id="page" class="single_page">

			<!--================================ Menu Top ==================================-->
				<!--#include file='i_menu_top.asp'-->
			<!--=================================== End ====================================-->

			<!--=============================== Menu Left ==================================-->
				<!--#include file='i_menu_left.asp'-->
			<!--================================== End =====================================-->

			<!--============================ Title Menu =============================-->
			<section class="mbr-section article">
				<div class="container">
					<div class="bigTopic">
						<h1 class="bigTopicText"><%=arr_menu_desc(26)%></h1>
					</div>
				</div>
			</section>
			<!--============================== End ==================================-->

			<section class="mbr-section article sectionSub">
			<!--===================================================== Content ====================================================-->
			<%if session("lang")="E" then%>
				<div class="container" style="padding-right: 15px;padding-left: 15px;">
					<!-- <p class="ShareholderStructureText">โครงสร้างการถือหุ้นของกลุ่มบริษัท ยูนิเวนเจอร์ จำกัด (มหาชน) ณ วันที่ 30 มิถุนายน 2561</p> -->

					<div><img src="images/ShareholderStructure_E.png"></div>
					<div style="text-align: center;margin-top: 15px;"><img src="images/HistoryBG-1.jpg"></div>
					  <!-- <p class="text-sub1" style="margin-bottom: 15px;">หมายเหตุ </p>
						<ul style="padding-left: 105px;">
						  <li class="text-note">
							  <p>* ผู้ถือหุ้นใหญ่ที่ถือหุ้นเกินกว่าร้อยละ 10.00 ของ UV ณ วันที่ 30 มิถุนายน 2561 ได้แก่ บริษัท อเดลฟอส จำกัด (“อเดลฟอส”) โดยถือหุ้น UV จำนวน 1,262,010,305 หุ้น หรือคิดเป็นร้อยละ 66.01 กรรมการของ อเดลฟอส ได้แก่ (1) นายฐาปน สิริวัฒนภักดี (2) นายปณต สิริวัฒนภักดี (3) นางปภัชญา สิริวัฒนภักดี (4) ม.ล.ตรีนุช สิริวัฒนภักดี (5) นายสิทธิชัย ชัยเกรียงไกร และ (6) นางแววมณี โสภณพินิจ โดยผู้ถือหุ้นใหญ่ของอเดลฟอส ได้แก่ (1) นายฐาปน สิริวัฒนภักดี และ (2) นายปณต สิริวัฒนภักดี โดยแต่ละฝ่ายถือหุ้นในสัดส่วนร้อยละ 50.00 (ที่มาข้อมูลจาก  www.bol.co.th ณ วันที่ 28 ตุลาคม 2561) ทั้งนี้ สามารถดูข้อมูลเพิ่มเติมของ UV ได้จากเว็บไซต์ตลาดหลักทรัพย์ฯ (www.set.or.th)</p>
					</li>
					<li class="text-note">
					  <p>** บริษัท แผ่นดินทอง พร็อพเพอร์ตี้ ดีเวลลอปเมนท์ จำกัด (มหาชน) (“GOLD”) เป็นบริษัทจดทะเบียนในตลาดหลักทรัพย์ฯ โดยสามารถข้อมูลเพิ่มเติมของ GOLD ได้จากเว็บไซต์ตลาดหลักทรัพย์ฯ www.set.or.th</p>
					</li>   
					<li class="text-note">
					  <p>*** หยุดประกอบการ </p>
					</li>   
				  </ul> -->
					</div><!-- END container -->
			<%else%>
				<div class="container" style="padding-right: 15px;padding-left: 15px;">
					<!-- <p class="ShareholderStructureText">โครงสร้างการถือหุ้นของกลุ่มบริษัท ยูนิเวนเจอร์ จำกัด (มหาชน) ณ วันที่ 30 มิถุนายน 2561</p> -->

					<div><img src="images/ShareholderStructure.png"></div>
					<div style="text-align: center;margin-top: 15px;"><img src="images/HistoryBG-1.jpg"></div>
					  <!-- <p class="text-sub1" style="margin-bottom: 15px;">หมายเหตุ </p>
						<ul style="padding-left: 105px;">
						  <li class="text-note">
							  <p>* ผู้ถือหุ้นใหญ่ที่ถือหุ้นเกินกว่าร้อยละ 10.00 ของ UV ณ วันที่ 30 มิถุนายน 2561 ได้แก่ บริษัท อเดลฟอส จำกัด (“อเดลฟอส”) โดยถือหุ้น UV จำนวน 1,262,010,305 หุ้น หรือคิดเป็นร้อยละ 66.01 กรรมการของ อเดลฟอส ได้แก่ (1) นายฐาปน สิริวัฒนภักดี (2) นายปณต สิริวัฒนภักดี (3) นางปภัชญา สิริวัฒนภักดี (4) ม.ล.ตรีนุช สิริวัฒนภักดี (5) นายสิทธิชัย ชัยเกรียงไกร และ (6) นางแววมณี โสภณพินิจ โดยผู้ถือหุ้นใหญ่ของอเดลฟอส ได้แก่ (1) นายฐาปน สิริวัฒนภักดี และ (2) นายปณต สิริวัฒนภักดี โดยแต่ละฝ่ายถือหุ้นในสัดส่วนร้อยละ 50.00 (ที่มาข้อมูลจาก  www.bol.co.th ณ วันที่ 28 ตุลาคม 2561) ทั้งนี้ สามารถดูข้อมูลเพิ่มเติมของ UV ได้จากเว็บไซต์ตลาดหลักทรัพย์ฯ (www.set.or.th)</p>
					</li>
					<li class="text-note">
					  <p>** บริษัท แผ่นดินทอง พร็อพเพอร์ตี้ ดีเวลลอปเมนท์ จำกัด (มหาชน) (“GOLD”) เป็นบริษัทจดทะเบียนในตลาดหลักทรัพย์ฯ โดยสามารถข้อมูลเพิ่มเติมของ GOLD ได้จากเว็บไซต์ตลาดหลักทรัพย์ฯ www.set.or.th</p>
					</li>   
					<li class="text-note">
					  <p>*** หยุดประกอบการ </p>
					</li>   
				  </ul> -->
					</div><!-- END container -->
			<%end if%>
			<!--======================================================= End ======================================================-->
			</section>
			
		</div>

		<!--====================================== Footer ======================================-->
			<!--#include file='i_footer.asp'-->
		<!--======================================== End =======================================-->

		<script type="text/javascript" src="scripts/modernizr.custom.js"></script>
		<%if session("lang")="E" then%><script type="text/javascript" src="scripts/jquery.dlmenu-en.js"></script><%else%><script type="text/javascript" src="scripts/jquery.dlmenu.js"></script><%end if%>
		<script>
			$(function() {
				$('#dl-menu').dlmenu();
			});
		</script>
		<!-- js/scroll-hint -->
		<script src="scripts/scroll-hint.js"></script>
		<!-- js/scroll-hint -->
		<script>
			new ScrollHint('.js-scrollable', {
			});
		</script>
	</div>
	 <!--#include file="i_googleAnalytics.asp"-->	  
</body>
</html>