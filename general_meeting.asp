﻿<!DOCTYPE html>

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->
<!--#include file = "../../i_conir.asp" -->	
<!--#include file="../../function_asp2007.asp"-->
<!--#include file="clsTreeView.asp"-->	
<%
session("cur_page")="IR " &  listed_share & " General Meeting of  Shareholders"
session("page_asp")="general_meeting.asp"
page_name="General Meeting of  Shareholders"
%>

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<TITLE><%=message_title%> :: <%=page_name%></TITLE>
	<META name="Title" content="<%=message_metatitle%>">
	<META name="Description" content="<%=message_description%>">
	<META name="Keywords" content="<%=message_keyword%>">	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="author" content="">
	<link rel="shortcut icon" href="<%=img_icon%>">
	<link href="style_listed.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../function2007_utf8.js"></script>
	<!-- CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/flexslider.css" rel="stylesheet" type="text/css">
	<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
	<!-- ADD CSS -->
	<link href="css/component.css" rel="stylesheet" type="text/css">
	<link href="css/custom.css" rel="stylesheet" type="text/css">
	<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">
	<!-- FONTS -->
	<link href="css/font_add.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- Mouse Hover CSS -->
	<script src="scripts/jquery.min.js" type="text/javascript"></script>
	<script src="scripts/jquery.nicescroll.min.js"></script>
	<!-- SCRIPTS -->
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<!--[if IE]><html class="ie" lang="en"> <![endif]-->
	<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
	<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
	<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
	<script src="scripts/animate.js" type="text/javascript"></script>
	<script src="scripts/myscript.js" type="text/javascript"></script>
	<script src="scripts/owl.carousel.js" type="text/javascript"></script>
	<!-- NEW -->
	<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
	<link href="css/site.css" rel="stylesheet" type="text/css" />
	<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
	<!-- scroll-hint -->
	<link rel="stylesheet" href="css/scroll-hint.css">
	
	<style>
		td{
			padding-top: 4px;
			padding-right: 2px;
			padding-bottom: 4px;
			padding-left: 2px;
		}
		.color_gm_head_1 {
			background-color: #006c9d;
			color: #FFFFFF;
		}
		.fontsubhead_meeting{
			font-family: 'Sarabun';
			font-weight: 300;
			font-size: 13px!important;
			line-height: 20px!important;
			color: #000!important;
		}
	</style>
</head>

<body class="main">

	<!-- PRELOADER -->
	<img id="preloader" src="images/preloader.gif" alt="">
	<!-- //PRELOADER -->
	<div class="preloader_hide">
		<div id="page" class="single_page">

			<!--================================ Menu Top ==================================-->
				<!--#include file='i_menu_top.asp'-->
			<!--=================================== End ====================================-->

			<!--=============================== Menu Left ==================================-->
				<!--#include file='i_menu_left.asp'-->
			<!--================================== End =====================================-->

			<!--============================ Title Menu =============================-->
			<section class="mbr-section article">
				<div class="container">
					<div class="bigTopic">
						<h1 class="bigTopicText"><%=arr_menu_desc(34)%></h1>
					</div>
				</div>
			</section>
			<!--============================== End ==================================-->

			<section class="mbr-section article sectionSub">
			<!--===================================================== Content ====================================================-->
				<div class="container">
					<table id="ani_box1" width="85%" cellpadding="0" cellspacing="0" border="0" style="opacity:1!important;">
						<tr valign="top">
							<td>
						<%
							sql_count_record=sql_count_general_meeting_info(listed_com_id,session("lang"),1)
							
							set RS_count =  conir.execute(sql_count_record)
							
							num_count=0
							
							If Not RS_count.EOF and Not RS_count.BOF Then
								num_count= RS_count("num_count")
							end if
							RS_count.close
							
							if num_count > 0 then
								sql =  sql_general_meeting_info(listed_com_id,session("lang"),1)
								
								set RS= conir.execute(sql)
							
								Set objTV = New TreeView
								If Not RS.EOF Then
									Do While Not RS.EOF
										  
											if RS("title") <> "" then
													parent_id=RS("parent_id")
													
													Dim child				 
													Set child = new Node
													child.Target="_blank"
													
													if RS("filename")<>"" then
														strURL=pathfileserver & "/" & "Listed/" & listed_share & "/" & "general_meeting/" & RS("filename")
													else
														strURL=""
													end if
													
													If parent_id=0 then
															if IsDate(RS("release_timestamp")) then
															   Call child.Init("&nbsp;&nbsp;"&RS("title") ,strURL,"",RS("menu_id"),RS("highlight"),RS("parent_id"),RS("id"),status)
															else
															   Call child.Init("&nbsp;&nbsp;"&RS("title"),strURL,"",RS("menu_id"),RS("highlight"),RS("parent_id"),RS("id"),status)
															end if
																
															child.ID =RS("id")
																
															objTV.Nodes.Add(child)
													Else
															Call child.Init(RS("title"),strURL,"",RS("menu_id"),RS("highlight"),RS("parent_id"),RS("id"),status)
															child.ID =RS("id")
														
															Set parentNode = objTV.FindNode(objTV.Nodes,parent_id)
															If Not (parentNode is Nothing) Then
																parentNode.Add(child)
															End If
													End If
												end if 
									
										RS.MoveNext		
									Loop
								end if
								
								RS.Close	

								'display Treeview
								objTV.Display
								
								'clear memory
								Set objTV = Nothing	
								Set RS = Nothing				
							else
								%>
								<center><font class="no_information"><br><br><br><br><br><br>
								<%if session("lang")="E" then %><%=Message_Info_E%><%else%><%=Message_Info_T%><%end if%>
								<br><br><br><br><br><br><br><br></font></center>		
								<%
							end if
							
							Set RS_count = Nothing				
							%>
						</td>
					</tr>					

				</table> 
				</div>
			<!--======================================================= End ======================================================-->
			</section>
			
		</div>

		<!--====================================== Footer ======================================-->
			<!--#include file='i_footer.asp'-->
		<!--======================================== End =======================================-->

		<script type="text/javascript" src="scripts/modernizr.custom.js"></script>
		<%if session("lang")="E" then%><script type="text/javascript" src="scripts/jquery.dlmenu-en.js"></script><%else%><script type="text/javascript" src="scripts/jquery.dlmenu.js"></script><%end if%>
		<script>
			$(function() {
				$('#dl-menu').dlmenu();
			});
		</script>
		<!-- js/scroll-hint -->
		<script src="scripts/scroll-hint.js"></script>
		<!-- js/scroll-hint -->
		<script>
			new ScrollHint('.js-scrollable', {
			});
		</script>
		
		<script type="text/javascript">
			$(function(){
				$("#nav-sub-meeting a.here").parent().parent().show().siblings().addClass('sub-display').parent().addClass('sub-active');
				$("li.sub-level-meeting div").click(function(e){
					$("ul",$(this).toggleClass('sub-display').parent()).slideToggle();}
				);
				var i=0;
				$("li.sub-level-meeting div.Hignlight").each(function(){
					/*if(i==0) */
					$("ul",$(this).toggleClass('sub-display').parent()).slideToggle();
					i++;
				});
			});     
		</script>
	</div>
	 <!--#include file="i_googleAnalytics.asp"-->	  
</body>
</html>