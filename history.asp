﻿<!DOCTYPE html>

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->
<%
session("cur_page")="IR " &  listed_share & " History"
session("page_asp")="history.asp"
page_name="History"
%>

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<TITLE><%=message_title%> :: <%=page_name%></TITLE>
	<META name="Title" content="<%=message_metatitle%>">
	<META name="Description" content="<%=message_description%>">
	<META name="Keywords" content="<%=message_keyword%>">	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="author" content="">
	<link rel="shortcut icon" href="<%=img_icon%>">
	<link href="style_listed.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../function2007_utf8.js"></script>
	<!-- CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/flexslider.css" rel="stylesheet" type="text/css">
	<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
	<!-- ADD CSS -->
	<link href="css/component.css" rel="stylesheet" type="text/css">
	<link href="css/custom.css" rel="stylesheet" type="text/css">
	<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">
	<!-- FONTS -->
	<link href="css/font_add.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- Mouse Hover CSS -->
	<script src="scripts/jquery.min.js" type="text/javascript"></script>
	<script src="scripts/jquery.nicescroll.min.js"></script>
	<!-- SCRIPTS -->
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<!--[if IE]><html class="ie" lang="en"> <![endif]-->
	<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
	<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
	<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
	<script src="scripts/animate.js" type="text/javascript"></script>
	<script src="scripts/myscript.js" type="text/javascript"></script>
	<script src="scripts/owl.carousel.js" type="text/javascript"></script>
	<!-- NEW -->
	<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
	<link href="css/site.css" rel="stylesheet" type="text/css" />
	<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
	<!-- scroll-hint -->
	<link rel="stylesheet" href="css/scroll-hint.css">
	
   <!-- css timeliner -->
  <!-- <link href="css/Timeline.css" rel="stylesheet" type="text/css"> -->
  <link rel="stylesheet" href="css/timeliner.css" type="text/css" media="screen">
  <link rel="stylesheet" href="css/colorbox.css" type="text/css" media="screen">
  <!-- end Timeline -->
  
  <!-- js timeliner -->
  <script type="text/javascript" src="scripts/colorbox.js"></script>
  <script type="text/javascript" src="scripts/timeliner.js"></script>
</head>

<body class="main">

	<!-- PRELOADER -->
	<img id="preloader" src="images/preloader.gif" alt="">
	<!-- //PRELOADER -->
	<div class="preloader_hide">
		<div id="page" class="single_page">

			<!--================================ Menu Top ==================================-->
				<!--#include file='i_menu_top.asp'-->
			<!--=================================== End ====================================-->

			<!--=============================== Menu Left ==================================-->
				<!--#include file='i_menu_left.asp'-->
			<!--================================== End =====================================-->

			<!--============================ Title Menu =============================-->
			<section class="mbr-section article">
				<div class="container">
					<div class="bigTopic">
						<h1 class="bigTopicText"><%=arr_menu_desc(21)%></h1>
					</div>
				</div>
			</section>
			<!--============================== End ==================================-->

			<!--===================================================== Content ====================================================-->
			<%if session("lang")="E" then%>
				<section class="mbr-section article">
				  <div class="container" style="padding-right: 15px;padding-left: 15px;">
					<div class="coverHistoryText">
					  <!-- <div class="HistoryBG"></div> -->
						<img src="images/HistoryBG-1.jpg" style="margin-bottom: 30px;">
						<p style="color: #006c9d;line-height: 1.7;font-size: 26px;font-weight: 400;margin-bottom: 54px;">Company History</p>
						  <p class="HistoryText">Stonehenge Inter Public Company Limited (The Company or STI) was founded by shareholders and executives who are experienced engineers with best 
						  expertise in the engineering industry with the objective of providing construction Project Management and construction management, inspection and certification for handover the
						  completion of project to customers. In addition, the services of the Company also include architectural and engineering design, interior design, and historic preservation operated by 
						  Stonehenge Company Limited (STH), a subsidiary of the Company. The Company is committed to providing services to customers efficiently with quality of work that meets international
						  professional standards under conditions of time frame and budget of the construction project according to the customer's plan in order to create the highest satisfaction of customer
						  which employs services of the Company. </p>
						  
						  <!-- <p class="HistoryText">บริษัทฯ จัดตั้งขึ้นเป็นบริษัทจำกัด เมื่อวันที่ 13 ตุลาคม 2547 ด้วยทุนจดทะเบียนเริ่มแรก จำนวน 1.00 ล้านบาท มีวัตถุประสงค์การจัดตั้งเพื่อดำเนินธุรกิจที่ปรึกษาบริหารและควบคุมงานก่อสร้าง รวมถึงรองรับการขยายตัวในการรับงานด้านวิศวกรที่ปรึกษา โดยเฉพาะธุรกิจที่ปรึกษาบริหารและควบคุมงานก่อสร้างที่ยังมีแนวโน้มขยายตัวอย่างต่อเนื่องตามธุรกิจอสังหาริมทรัพย์และอุตสาหกรรมก่อสร้างในช่วงเวลาขณะนั้นโดยผู้ประกอบการกลุ่มดังกล่าวมีความต้องการวิศวกรที่ปรึกษาที่มีความเชี่ยวชาญและประสบการณ์เป็นผู้ควบคุมบริหารโครงการก่อสร้างให้ดำเนินการไปอย่างมีประสิทธิภาพ เพื่อให้งานก่อสร้างมีคุณภาพได้มาตรฐาน แล้วเสร็จภายในระยะเวลาและงบประมาณที่วางไว้ตามแผนงาน </p>
						  <p class="HistoryText">ตั้งแต่ปี 2550 เป็นต้นมา ผู้ถือหุ้นของบริษัทฯ ขยายการให้บริการงานด้านวิศวกรที่ปรึกษาในแขนงต่างๆ และธุรกิจเกี่ยวเนื่อง เช่น งานสำรวจ งานประมาณราคา เว็บไซต์ข้อมูลข่าวสารในแวดวงวิศวกรรม รวมไปถึงขยายการให้บริการงานด้านที่ปรึกษาและบริหารโครงการก่อสร้างไปยังกลุ่มลูกค้าเป้าหมายในภูมิภาคต่างๆ ของประเทศ อันได้แก่ ภาคตะวันออกเฉียงเหนือ ภาคเหนือ และภาคตะวันออก โดยผู้ถือหุ้นของบริษัทฯ ได้จัดตั้งบริษัทขึ้นใหม่ร่วมกับพันธมิตรทางธุรกิจ เพื่อรองรับการขยายธุรกิจต่างๆ ดังที่กล่าวไว้ข้างต้น และในปี 2552 บริษัทฯ ได้เพิ่มทุนจดทะเบียนบริษัทจาก 1.00 ล้านบาท เป็น 5.00 ล้านบาท</p>
						  <p class="HistoryText">ต่อมาในเดือนมิถุนายน 2559 STI เพิ่มทุนจดทะเบียนจาก 5.00 ล้านบาทเป็น 100.00 ล้านบาท โดยออกและเสนอขายหุ้นสามัญเพิ่มทุนให้แก่ผู้ถือหุ้นเดิมตามสัดส่วนการถือหุ้น และในเดือนสิงหาคม 2559 บริษัท ยูนิเวนเจอร์ แคปปิตอล จำกัด (“UVCAP”) ซึ่งเป็นบริษัทย่อยของบริษัท ยูนิเวนเจอร์ จำกัด (มหาชน) (“UV”) ได้เข้าซื้อหุ้นจากผู้ถือหุ้นเดิมของบริษัทฯ จำนวน 350,000 หุ้น ซึ่งคิดเป็นร้อยละ 35.00 ของทุนจดทะเบียนและชำระแล้วทั้งหมดของบริษัทฯ ในขณะนั้น ส่งผลให้ภายหลังการเข้าทำรายการดังกล่าว บริษัทฯ มีสถานะเป็นบริษัทร่วมทางอ้อมของ UV นอกจากนี้ ในปี 2559 บริษัทฯ ได้เริ่มดำเนินการจัดโครงสร้างกลุ่มบริษัทฯ เพื่อเพิ่มประสิทธิภาพในการบริหารจัดการและเตรียมตัวเข้าจดทะเบียนเป็นบริษัทจดทะเบียนในตลาดหลักทรัพย์ฯ  
						  <p class="HistoryText" style="margin-bottom: 15px;">โดยบริษัทฯ ได้เข้าลงทุนใน STH ด้วยการซื้อหุ้นร้อยละ 99.99 จากผู้ถือหุ้นเดิมของ STH รวมถึงดำเนินการรวมบริษัทที่ประกอบธุรกิจให้บริการด้านวิศวกรที่ปรึกษา และ/หรือบริษัทที่ดำเนินธุรกิจเกี่ยวเนื่องหรือสนับสนุนการประกอบธุรกิจของกลุ่มบริษัทฯ เข้าเป็นฝ่ายงานภายในกลุ่มบริษัทฯ และดำเนินการจดทะเบียนเลิกบริษัทและชำระบัญชีแล้ว ได้แก่ </p>
						  <ul class="cover-ul">
							<li>(1) บริษัท สำนักงาน อินทิเกรท ดีไซน์ จำกัด</li>
							<li>(2) บริษัท สโตนเฮ้นจ์ นอร์ท อีสท์ จำกัด</li>
							<li>(3) บริษัท สโตนเฮ้นจ์ นอร์ท จำกัด</li>
							<li>(4) บริษัท สโตนเฮ้นจ์ อีสท์ จำกัด</li>
						  </ul>  
						  <p class="HistoryText" style="margin-bottom: 15px;padding-left: 10px;">และอยู่ระหว่างบัญชี ได้แก่ </p>
						  <ul class="cover-ul">
							<li>(1) บริษัท สโตนเฮ้นจ์ โฮลดิ้ง จำกัด</li>
							<li>(2) บริษัท สโตนเฮ้นจ์ แอสเสท จำกัด</li>
						  </ul>
						  <p class="HistoryText">ต่อมาในการประชุมสามัญผู้ถือหุ้นประจำปี 2561 ของบริษัทฯ เมื่อวันที่ 26 เมษายน 2561 ได้มีมติแปรสภาพบริษัทฯ เป็นบริษัทมหาชน เพื่อการเสนอขายหุ้นต่อประชาชนครั้งแรก และนำหุ้นสามัญของบริษัทเข้าจดทะเบียนเป็นหลักทรัพย์จดทะเบียนต่อตลาดหลักทรัพย์ เอ็ม เอ ไอ (MAI) </p> -->
					</div>
				  </div> <!-- END container -->
				</section>

				<section id="timeline">
				  <div class="container" style="min-height: 2450px;">
					<h1 class="blue-text lighten-1 header" style="padding-right: 15px;padding-left: 15px;text-align: left;">Key Changes and Developments </h1>
					  <div id="timeline" class="timeline-container">
						<button class="timeline-toggle"></button>
				  <br class="clear">

				  <div class="timeline-wrapper">
					<h2 class="timeline-time"><span>1992</span></h2>
					<dl class="timeline-series">
					  <dt id="19540517" class="start-open"></dt>
					  <dd class="timeline-event-content" id="19540517EX">
						<p class="HistoryText">
						  Stonehenge Company Limited (STH) was founded with registered capital of THB 1.00 million to provide services of architectural and engineering design, interior design, 
						  historic preservation, as well as construction management. 
						</p>

						  <br class="clear">
					  </dd>
					</dl>
				  </div>

				  <div class="timeline-wrapper">
					<h2 class="timeline-time"><span>2004</span></h2>
					<dl class="timeline-series">
					  <dt class="start-open" id="19540517"></dt>
					  <dd class="timeline-event-content" id="19540517EX">
						<p class="HistoryText">
						  Stonehenge Inter Public Company Limited (STI or the Company) was founded with registered capital of THB 1.00 million to provide construction management services.
						</p>

						  <br class="clear">
					  </dd>
					</dl>
				  </div>

				  <div class="timeline-wrapper">
					<h2 class="timeline-time"><span>2016</span></h2>
					<dl class="timeline-series">
					  <dt class="start-open" id="19540517"></dt>
					  <dd class="timeline-event-content" id="19540517EX">
						<p class="HistoryText">
						 STI Group reorganized its organizational structure in order to increase management efficiency and prepare itself for registration into the Stock Exchange of Thailand. 
						 STI invested in ordinary shares of STH by 100% and increased registered capital from THB 5.00 million to THB 100.00 million. In addition, there was a merge of companies that 
						 operated engineering consultancy services and/or companies that operated related or supporting businesses to become business units under STI Group. 
						</p>
						<p style="font-weight: 600;margin-bottom: 5px;">August</p>
						<p class="HistoryText">- Univenture Capital Company Limited (“UVCAP”), a subsidiary of Univenture Public Company Limited (“UV”) acquired 350,000 shares or 35.00% of issued and paid-up shares of STI.</p>

						  <br class="clear">
					  </dd>
					</dl>
				  </div>

				  <div class="timeline-wrapper">
					<h2 class="timeline-time"><span>2017</span></h2>
					<dl class="timeline-series">
					  <dt class="start-open" id="19540517"></dt>
					  <dd class="timeline-event-content" id="19540517EX">
						<p class="HistoryText">
						  The Company received certiciation of ISO 9001 : 2015 quality standards related to Consultant and Pre-Construction Management from TÜV NORD Institution which is an institution that issues certification of system quality standards for both domestic and international related to quality, safety, health, environment, and social responsibility. 
						</p>

						  <br class="clear">
					  </dd>
					</dl>
				  </div>

				  <div class="timeline-wrapper">
					<h2 class="timeline-time"><span>2018</span></h2>
					<dl class="timeline-series">
					  <dt class="start-open" id="19540517"></dt>
					  <dd class="timeline-event-content" id="19540517EX">
						<p style="font-weight: 600;margin-bottom: 5px;">April </p>
						<p class="HistoryText">- The Company’s shareholders had a resolution to change the Company’s par value from THB 100.00 per share to THB 0.50 per share which resulted in the 
						change of number of STI shares from 1,000,000 shares to 200,000,000 shares. There was a resolution to increase registered capital from THB 100.00 million to THB 134.00 million by 
						issuing new ordinary shares for capital increase of 68,000,000 shares at par value of THB 0.50 per share. In addition, there was a resolution to transform the Company into a 
						public company by initial public offering in which the Company proceeded with registration with the Ministry of Commerce to transform into a public company, changed the par value
						of its share, and issued new ordinary shares for capital increase on May 7, 2018.</p>
						<p style="font-weight: 600;margin-bottom: 5px;">July </p>
						<p class="HistoryText">- The Company filed the application for an offer for sale of shares and draft prospectus to the Office of the Securities and Exchange Commission (SEC) on July 19, 2018.</p>
						<p style="font-weight: 600;margin-bottom: 5px;">December </p>
						<p class="HistoryText" style="margin-bottom: 0px;">- The application for an offer for sale of shares and draft prospectus was effective on December 4, 2018.</p>
						<p class="HistoryText" style="margin-bottom: 0px;">- The Company had its initial public offering (IPO) for its ordinary shares of 68 million shares with par value of THB 0.50 per share and selling price of THB 6.30 per share. KTB Securities (Thailand) Public Company Limited was responsible as the underwriter of the Company’s newly issued ordinary shares on December 6-7 and 11, 2018.</p>
						<p class="HistoryText" style="margin-bottom: 0px;">- The Company organized 1st Trading Day event at the Stock Exchange of Thailand Building and entered to trade securities on SET on December 19, 2018.</p>

							<br class="clear">
					  </dd>
					</dl>
				  </div>

				  <br class="clear">
					  </div><!-- /#timelineContainer -->

				  </div>
				</section>
			<%else%>
				<section class="mbr-section article">
				  <div class="container" style="padding-right: 15px;padding-left: 15px;">
					  <div class="coverHistoryText">
					  <!-- <div class="HistoryBG"></div> -->
						<img src="images/HistoryBG-1.jpg" style="margin-bottom: 30px;">
						<p style="color: #006c9d;line-height: 1.7;font-size: 26px;font-weight: 400;margin-bottom: 54px;">บริษัท สโตนเฮ้นจ์ อินเตอร์ จำกัด (มหาชน) (“STI”)</p>
						  <p class="HistoryText">จัดตั้งขึ้นโดยผู้ถือหุ้นและผู้บริหารซึ่งเป็นวิศวกรผู้มีประสบการณ์ความเชี่ยวชาญในแวดวงวิศวกรรม โดยมีวัตถุประสงค์เพื่อดำเนินธุรกิจที่ปรึกษาบริหารและควบคุมงานก่อสร้าง ตั้งแต่การศึกษาสำรวจ ให้คำปรึกษา ข้อเสนอแนะ บริหาร และควบคุมงานก่อสร้าง รวมไปถึงการตรวจสอบรับรองงาน เพื่อให้โครงการแล้วเสร็จและสามารถเปิดดำเนินการได้ตามแผนงานของผู้ว่าจ้าง </p>
						  <p class="HistoryText">นอกจากนี้ การให้บริการของกลุ่มบริษัทฯ ยังครอบคลุมไปถึงการให้บริการออกแบบงานด้านสถาปัตยกรรมและวิศวกรรม งานตกแต่งภายใน และงานอนุรักษ์โบราณสถาน ซึ่งดำเนินการโดยบริษัท สโตนเฮ้นจ์ จำกัด (“STH”) ที่เป็นบริษัทย่อยของบริษัทฯ โดยกลุ่มบริษัทฯ มีความมุ่งมั่นที่จะให้บริการแก่ผู้ว่าจ้างอย่างมีประสิทธิภาพ ทั้งในด้านคุณภาพงานที่ได้มาตรฐานวิชาชีพระดับสากล ภายใต้การควบคุมกรอบระยะเวลาและงบประมาณของโครงการก่อสร้างให้เป็นไปตามแผนงานผู้ว่าจ้างที่กำหนดไว้ เพื่อสร้างความพึงพอใจสูงสุดให้แก่ผู้ว่าจ้างที่รับบริการของบริษัทฯ</p>
						  <!-- <p class="HistoryText">บริษัทฯ จัดตั้งขึ้นเป็นบริษัทจำกัด เมื่อวันที่ 13 ตุลาคม 2547 ด้วยทุนจดทะเบียนเริ่มแรก จำนวน 1.00 ล้านบาท มีวัตถุประสงค์การจัดตั้งเพื่อดำเนินธุรกิจที่ปรึกษาบริหารและควบคุมงานก่อสร้าง รวมถึงรองรับการขยายตัวในการรับงานด้านวิศวกรที่ปรึกษา โดยเฉพาะธุรกิจที่ปรึกษาบริหารและควบคุมงานก่อสร้างที่ยังมีแนวโน้มขยายตัวอย่างต่อเนื่องตามธุรกิจอสังหาริมทรัพย์และอุตสาหกรรมก่อสร้างในช่วงเวลาขณะนั้นโดยผู้ประกอบการกลุ่มดังกล่าวมีความต้องการวิศวกรที่ปรึกษาที่มีความเชี่ยวชาญและประสบการณ์เป็นผู้ควบคุมบริหารโครงการก่อสร้างให้ดำเนินการไปอย่างมีประสิทธิภาพ เพื่อให้งานก่อสร้างมีคุณภาพได้มาตรฐาน แล้วเสร็จภายในระยะเวลาและงบประมาณที่วางไว้ตามแผนงาน </p>
								  <p class="HistoryText">ตั้งแต่ปี 2550 เป็นต้นมา ผู้ถือหุ้นของบริษัทฯ ขยายการให้บริการงานด้านวิศวกรที่ปรึกษาในแขนงต่างๆ และธุรกิจเกี่ยวเนื่อง เช่น งานสำรวจ งานประมาณราคา เว็บไซต์ข้อมูลข่าวสารในแวดวงวิศวกรรม รวมไปถึงขยายการให้บริการงานด้านที่ปรึกษาและบริหารโครงการก่อสร้างไปยังกลุ่มลูกค้าเป้าหมายในภูมิภาคต่างๆ ของประเทศ อันได้แก่ ภาคตะวันออกเฉียงเหนือ ภาคเหนือ และภาคตะวันออก โดยผู้ถือหุ้นของบริษัทฯ ได้จัดตั้งบริษัทขึ้นใหม่ร่วมกับพันธมิตรทางธุรกิจ เพื่อรองรับการขยายธุรกิจต่างๆ ดังที่กล่าวไว้ข้างต้น และในปี 2552 บริษัทฯ ได้เพิ่มทุนจดทะเบียนบริษัทจาก 1.00 ล้านบาท เป็น 5.00 ล้านบาท</p>
								  <p class="HistoryText">ต่อมาในเดือนมิถุนายน 2559 STI เพิ่มทุนจดทะเบียนจาก 5.00 ล้านบาทเป็น 100.00 ล้านบาท โดยออกและเสนอขายหุ้นสามัญเพิ่มทุนให้แก่ผู้ถือหุ้นเดิมตามสัดส่วนการถือหุ้น และในเดือนสิงหาคม 2559 บริษัท ยูนิเวนเจอร์ แคปปิตอล จำกัด (“UVCAP”) ซึ่งเป็นบริษัทย่อยของบริษัท ยูนิเวนเจอร์ จำกัด (มหาชน) (“UV”) ได้เข้าซื้อหุ้นจากผู้ถือหุ้นเดิมของบริษัทฯ จำนวน 350,000 หุ้น ซึ่งคิดเป็นร้อยละ 35.00 ของทุนจดทะเบียนและชำระแล้วทั้งหมดของบริษัทฯ ในขณะนั้น ส่งผลให้ภายหลังการเข้าทำรายการดังกล่าว บริษัทฯ มีสถานะเป็นบริษัทร่วมทางอ้อมของ UV นอกจากนี้ ในปี 2559 บริษัทฯ ได้เริ่มดำเนินการจัดโครงสร้างกลุ่มบริษัทฯ เพื่อเพิ่มประสิทธิภาพในการบริหารจัดการและเตรียมตัวเข้าจดทะเบียนเป็นบริษัทจดทะเบียนในตลาดหลักทรัพย์ฯ  
						  <p class="HistoryText" style="margin-bottom: 15px;">โดยบริษัทฯ ได้เข้าลงทุนใน STH ด้วยการซื้อหุ้นร้อยละ 99.99 จากผู้ถือหุ้นเดิมของ STH รวมถึงดำเนินการรวมบริษัทที่ประกอบธุรกิจให้บริการด้านวิศวกรที่ปรึกษา และ/หรือบริษัทที่ดำเนินธุรกิจเกี่ยวเนื่องหรือสนับสนุนการประกอบธุรกิจของกลุ่มบริษัทฯ เข้าเป็นฝ่ายงานภายในกลุ่มบริษัทฯ และดำเนินการจดทะเบียนเลิกบริษัทและชำระบัญชีแล้ว ได้แก่ </p>
						  <ul class="cover-ul">
							<li>(1) บริษัท สำนักงาน อินทิเกรท ดีไซน์ จำกัด</li>
							<li>(2) บริษัท สโตนเฮ้นจ์ นอร์ท อีสท์ จำกัด</li>
							<li>(3) บริษัท สโตนเฮ้นจ์ นอร์ท จำกัด</li>
							<li>(4) บริษัท สโตนเฮ้นจ์ อีสท์ จำกัด</li>
						  </ul>  
						  <p class="HistoryText" style="margin-bottom: 15px;padding-left: 10px;">และอยู่ระหว่างบัญชี ได้แก่ </p>
						  <ul class="cover-ul">
							<li>(1) บริษัท สโตนเฮ้นจ์ โฮลดิ้ง จำกัด</li>
							<li>(2) บริษัท สโตนเฮ้นจ์ แอสเสท จำกัด</li>
						  </ul>
								  <p class="HistoryText">ต่อมาในการประชุมสามัญผู้ถือหุ้นประจำปี 2561 ของบริษัทฯ เมื่อวันที่ 26 เมษายน 2561 ได้มีมติแปรสภาพบริษัทฯ เป็นบริษัทมหาชน เพื่อการเสนอขายหุ้นต่อประชาชนครั้งแรก และนำหุ้นสามัญของบริษัทเข้าจดทะเบียนเป็นหลักทรัพย์จดทะเบียนต่อตลาดหลักทรัพย์ เอ็ม เอ ไอ (MAI) </p> -->
						  </div>
				  </div> <!-- END container -->
				</section>

				<section id="timeline">
				  <div class="container" style="min-height: 2450px;">
					<h1 class="blue-text lighten-1 header" style="padding-right: 15px;padding-left: 15px;text-align: left;">สรุปการเปลี่ยนแปลงและพัฒนาการที่สำคัญของกลุ่มบริษัทฯ</h1>
					  <div id="timeline" class="timeline-container">
						<button class="timeline-toggle"></button>
				  <br class="clear">

				  <div class="timeline-wrapper">
					<h2 class="timeline-time"><span>ปี 2535</span></h2>
					<dl class="timeline-series">
					  <dt id="19540517" class="start-open"></dt>
					  <dd class="timeline-event-content" id="19540517EX">
						<p class="HistoryText">
						  บริษัท สโตนเฮ้นจ์ จำกัด (“STH”) จัดตั้งด้วยทุนจดทะเบียนเริ่มต้น 1.00 ล้านบาท เพื่อดำเนินธุรกิจออกแบบงานด้านสถาปัตยกรรมและวิศวกรรม งานตกแต่งภายใน และงานอนุรักษ์โบราณสถาน รวมถึงธุรกิจที่ปรึกษาบริหารและควบคุมงานก่อสร้าง 
						</p>

						  <br class="clear">
					  </dd>
					</dl>
				  </div>

				  <div class="timeline-wrapper">
					<h2 class="timeline-time"><span>ปี 2547</span></h2>
					<dl class="timeline-series">
					  <dt class="start-open" id="19540517"></dt>
					  <dd class="timeline-event-content" id="19540517EX">
						<p class="HistoryText">
						  บริษัท สโตนเฮ้นจ์ อินเตอร์ จำกัด (มหาชน) (“STI” หรือ บริษัทฯ) จัดตั้งด้วยทุนจดทะเบียนเริ่มต้น 1.00 ล้านบาท เพื่อดำเนินธุรกิจที่ปรึกษาบริหารและควบคุมงานก่อสร้าง 
						</p>

						  <br class="clear">
					  </dd>
					</dl>
				  </div>

				  <div class="timeline-wrapper">
					<h2 class="timeline-time"><span>ปี 2559</span></h2>
					<dl class="timeline-series">
					  <dt class="start-open" id="19540517"></dt>
					  <dd class="timeline-event-content" id="19540517EX">
						<p class="HistoryText">
						  ดำเนินการจัดโครงสร้างกลุ่มบริษัทฯ เพื่อเพิ่มประสิทธิภาพในการบริหารจัดการและเตรียมตัวจดทะเบียนเข้าตลาดหลักทรัพย์ฯ โดย STI เข้าลงทุนในหุ้นสามัญร้อยละ 100.00 ของ STH และเพิ่มทุนจดทะเบียนจาก 5.00 ล้านบาท เป็น 100.00 ล้านบาท รวมถึงดำเนินการรวมบริษัทที่ประกอบธุรกิจให้บริการด้านวิศวกรที่ปรึกษา และ/หรือบริษัทที่ดำเนินธุรกิจเกี่ยวเนื่องหรือสนับสนุนการประกอบธุรกิจของกลุ่มบริษัทฯ เข้าเป็นฝ่ายงานภายในกลุ่มบริษัทฯ 
						</p>
						<p style="font-weight: 600;margin-bottom: 5px;">เดือน สิงหาคม</p>
						<p class="HistoryText">- บริษัท ยูนิเวนเจอร์ แคปปิตอล จำกัด (“UVCAP”) ซึ่งเป็นบริษัทย่อยของบริษัท ยูนิเวนเจอร์ จำกัด (มหาชน) (“UV”) เข้าลงทุนในบริษัทฯ ด้วยการ ซื้อหุ้นบริษัทฯ จำนวน 350,000 หุ้น หรือคิดเป็น ร้อยละ 35.00 ของจำนวนหุ้นที่ออกและชำระแล้ว ทั้งหมดของบริษัทฯ ณ ขณะนั้น (1,000,000 หุ้น) ส่งผลให้บริษัทฯ มีสถานะเป็นบริษัทร่วมทางอ้อม ของ UV</p>

						  <br class="clear">
					  </dd>
					</dl>
				  </div>

				  <div class="timeline-wrapper">
					<h2 class="timeline-time"><span>ปี 2560</span></h2>
					<dl class="timeline-series">
					  <dt class="start-open" id="19540517"></dt>
					  <dd class="timeline-event-content" id="19540517EX">
						<p class="HistoryText">
						  บริษัทฯ ได้ผ่านการรับรองตามมาตรฐานคุณภาพ ISO 9001 : 2015 ในด้านที่ปรึกษาและบริหารจัดการการก่อสร้าง (Consultant and Pre-Construction Management) จากสถาบัน TÜV NORD ซึ่งเป็นสถาบันด้านการรับรองและออกใบรับรองตามมาตรฐานระบบทั้งใน และนอกประเทศ อันเกี่ยวกับคุณภาพ ความปลอดภัย สุขภาพ สิ่งแวดล้อม และความรับผิดชอบต่อสังคม
						</p>

						  <br class="clear">
					  </dd>
					</dl>
				  </div>

				  <div class="timeline-wrapper">
					<h2 class="timeline-time"><span>ปี 2561</span></h2>
					<dl class="timeline-series">
					  <dt class="start-open" id="19540517"></dt>
					  <dd class="timeline-event-content" id="19540517EX">
						<p style="font-weight: 600;margin-bottom: 5px;">เดือน เมษายน</p>
						<p class="HistoryText">- ผู้ถือหุ้นบริษัทฯ มีมติเปลี่ยนแปลงมูลค่าที่ตราไว้ของบริษัทฯ จาก 100.00 บาทต่อหุ้น เป็น 0.50 บาทต่อหุ้น ส่งผลให้จำนวนหุ้น STI มีการเปลี่ยนแปลงจาก 1,000,000 หุ้น เป็น 200,000,000 หุ้น และมีมติเพิ่มทุนจดทะเบียนจาก 100.00 ล้านบาท เป็น 134.00 ล้านบาท โดยการออกหุ้นสามัญเพิ่มทุนใหม่ จำนวน 68,000,000 หุ้น มูลค่าที่ตราไว้หุ้นละ 0.50 บาท และมีมติแปรสภาพเป็นบริษัทมหาชน เพื่อการเสนอขายหุ้นต่อประชาชนครั้งแรก โดยบริษัทฯ ได้ดำเนินการจดทะเบียนกับกระทรวงพาณิชย์เพื่อแปรสภาพเป็นบริษัทมหาชน เปลี่ยนแปลงมูลค่าหุ้นที่ตราไว้ และออกหุ้นสามัญเพิ่มทุนใหม่ เมื่อวันที่ 7 พฤษภาคม 2561</p>
						<p style="font-weight: 600;margin-bottom: 5px;">เดือน กรกฎาคม</p>
						<p class="HistoryText">- บริษัทฯได้ยื่นแบบแสดงรายการข้อมูลการเสนอขายหลักทรัพย์และร่างหนังสือชี้ชวน ต่อสำนักงานคณะกรรมการกำกับหลักทรัพย์และตลาดหลักทรัพย์ (ก.ล.ต.) เมื่อวันที่ 19 กรกฎาคม 2561</p>
						<p style="font-weight: 600;margin-bottom: 5px;">เดือน ธันวาคม</p>
						<p class="HistoryText" style="margin-bottom: 0px;">- แบบแสดงรายการข้อมูลการเสนอขายหลักทรัพย์และร่างหนังสือชี้ชวนของบริษัทฯ มีผลบังคับใช้ เมื่อวันที่ 4 ธันวาคม 2561</p>
						<p class="HistoryText" style="margin-bottom: 0px;">- บริษัทฯเปิดระยะเวลาการจองซื้อหุ้นสามัญเพิ่มทุนให้กับประชาชนทั่วไปเป็นครั้งแรก (IPO) จำนวน 68 ล้านหุ้น มูลค่าที่ตราไว้ (พาร์) หุ้นละ 0.50 บาท ในราคา 6.30 บาทต่อหุ้น โดยมี บริษัทหลักทรัพย์ เคทีบี (ประเทศไทย) จำกัด (มหาชน) เป็นผู้จัดการการจัดจำหน่ายและรับประกันการจำหน่ายหุ้นสามัญเพิ่มทุนของบริษัทฯ เมื่อวันที่ 6-7 และ 11 ธันวาคม 2561</p>
						<p class="HistoryText" style="margin-bottom: 0px;">- บริษัทฯจัดงาน 1st Trading day ณ อาคารตลาดหลักทรัพย์แห่งประเทศไทย และได้เริ่มเข้าซื้อขายหลักทรัพย์ ใน SET เป็นวันแรก เมื่อวันที่ 19 ธันวาคม 2561</p>

							<br class="clear">
					  </dd>
					</dl>
				  </div>

				  <br class="clear">
					  </div><!-- /#timelineContainer -->

				  </div>
				</section>
			<%end if%>
			<!--======================================================= End ======================================================-->
			
		</div>
		
		<!--====================================== Footer ======================================-->
			<!--#include file='i_footer.asp'-->
		<!--======================================== End =======================================-->

		<script type="text/javascript" src="scripts/modernizr.custom.js"></script>
		<%if session("lang")="E" then%><script type="text/javascript" src="scripts/jquery.dlmenu-en.js"></script><%else%><script type="text/javascript" src="scripts/jquery.dlmenu.js"></script><%end if%>
		
		<script>
			$(function() {
				$('#dl-menu').dlmenu();
			});
		</script>
		<!-- js/scroll-hint -->
		<script src="scripts/scroll-hint.js"></script>
		<!-- js/scroll-hint -->
		<script>
			new ScrollHint('.js-scrollable', {
			});
		</script>
		
		<script type="text/javascript">
			$(function () {
				$(".demo1").bootstrapNews({
					newsPerPage: 7,
					autoplay: false,
					pauseOnHover:true,
					direction: 'up',
					newsTickerInterval: 4000,
					onToDo: function () {
						//console.log(this);
					}
				});
				
				$(".demo2").bootstrapNews({
					newsPerPage: 7,
					autoplay: false,
					pauseOnHover:true,
					direction: 'up',
					newsTickerInterval: 4000,
					onToDo: function () {
						//console.log(this);
					}
				});
			});

			// timeliner
			$(document).ready(function() {
			  $.timeliner({});
			  $.timeliner({
				timelineContainer: '#timeline-js',
				timelineSectionMarker: '.milestone',
				oneOpen: true,
				startState: 'flat',
				expandAllText: '+ Show All',
				collapseAllText: '- Hide All'
			  });
			  // Colorbox Modal
			  $(".CBmodal").colorbox({inline:true, initialWidth:100, maxWidth:682, initialHeight:100, transition:"elastic",speed:750});
			});
		</script>
	</div>
	 <!--#include file="i_googleAnalytics.asp"-->	  
</body>
</html>