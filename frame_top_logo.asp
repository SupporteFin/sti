﻿<!DOCTYPE html>

<!--#include file="../../i_constant.asp"-->
<!--#include file="i_constant_listed.asp"-->
<!--#include file = "i_description_menu.asp" -->

<% 
if request("title")=1 then             'Lastest news
         subtitle=  arr_menu_desc(59)
elseif request("title")=2 then      'Set Announcement
         subtitle=  arr_menu_desc(58)
end if
%>
<HTML xmlns="http://www.w3.org/1999/xhtml">
	<HEAD>
		<META content="text/html; charset=utf-8" http-equiv="Content-Type">
		<title><%=message_title%> :: <%=page_name%></title>
		<meta name="keywords" content="<%=message_keyword%>">
		<meta name="description" content="<%=message_description%>"> 
		<META name="GENERATOR" content="MSHTML 8.00.7600.16385">
		<LINK rel="shortcut icon" href="images/favicon.ico"></LINK>
		
		<link href="style_listed.css" rel="stylesheet" type="text/css">
		<script language="javascript" src="../../function2007_utf8.js"></script>
		<!-- CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="css/flexslider.css" rel="stylesheet" type="text/css">
		<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
		<link href="css/style.css" rel="stylesheet" type="text/css">
		<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
		<!-- ADD CSS -->
		<link href="css/component.css" rel="stylesheet" type="text/css">
		<link href="css/custom.css" rel="stylesheet" type="text/css">
		<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">
		<!-- FONTS -->
		<link href="css/font_add.css" rel="stylesheet" type="text/css">
		<link href="css/font-awesome.css" rel="stylesheet">
		<!-- Mouse Hover CSS -->
		<script src="scripts/jquery.min.js" type="text/javascript"></script>
		<script src="scripts/jquery.nicescroll.min.js"></script>
		<!-- SCRIPTS -->
		<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
		<!--[if IE]><html class="ie" lang="en"> <![endif]-->
		<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
		<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
		<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
		<script src="scripts/animate.js" type="text/javascript"></script>
		<script src="scripts/myscript.js" type="text/javascript"></script>
		<script src="scripts/owl.carousel.js" type="text/javascript"></script>
		<!-- NEW -->
		<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
		<link href="css/site.css" rel="stylesheet" type="text/css" />
		<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
		<!-- scroll-hint -->
		<link rel="stylesheet" href="css/scroll-hint.css">
	</HEAD>
	<body>
<div class="header-wrap">
	<!-- ............................................ Top ............................................ -->	
		<!--#include file = "i_top_detail.asp" -->
	<!-- ...................................... End Top ......................................... -->		
</div>	

	 <table width="980px" border="0" cellspacing="0" cellpadding="0" align="center" >		
			<tr height="10px"><td></td></tr>
			<tr>
				<td align="left" valign="top" class="normal_4 td_title">
					<div id="head_topic"><%=subtitle%></div>
				</td>
			</tr>
	 </table>	
	</body>
</HTML>
