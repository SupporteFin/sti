﻿<!DOCTYPE html>

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->
<!--#include file = "../../i_conir.asp" -->	
<!--#include file = "../../function_asp2007.asp" -->
<!--#include file = "i_description_menu.asp" -->

<%
session("cur_page")="IR " & listed_share &" Webcasts Detail" 
page_name="Webcasts Detail"

title=   arr_menu_desc(51) 
%>

<html>
<head>
<title><%=message_title%> :: <%=page_name%></title>
<meta content="<%=message_keyword%>" name="keywords">
<meta content="<%=message_description%>" name="description">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="">

<link href="style_listed.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../function2007_utf8.js"></script>
<!-- CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/flexslider.css" rel="stylesheet" type="text/css">
<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
<!-- ADD CSS -->
<link href="css/component.css" rel="stylesheet" type="text/css">
<link href="css/custom.css" rel="stylesheet" type="text/css">
<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">
<!-- FONTS -->
<link href="css/font_add.css" rel="stylesheet" type="text/css">
<link href="css/font-awesome.css" rel="stylesheet">
<!-- Mouse Hover CSS -->
<script src="scripts/jquery.min.js" type="text/javascript"></script>
<script src="scripts/jquery.nicescroll.min.js"></script>
<!-- SCRIPTS -->
<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if IE]><html class="ie" lang="en"> <![endif]-->
<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
<script src="scripts/animate.js" type="text/javascript"></script>
<script src="scripts/myscript.js" type="text/javascript"></script>
<script src="scripts/owl.carousel.js" type="text/javascript"></script>
<!-- NEW -->
<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="css/site.css" rel="stylesheet" type="text/css" />
<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
<!-- scroll-hint -->
<link rel="stylesheet" href="css/scroll-hint.css">

</head>
<body>
<%
		dim file_name 
		file_name = request("id")
		
		if file_name <> "" then
			strsql = sql_calendar_event_player("" & listed_share &"",session("lang"),file_name)					'=========== Revise Support TH/EN  
			'response.write strsql & "<br>"
			 set rs = conir.execute(strsql)
			 
			 if not  rs.eof and not  rs.bof then 
				 if session("lang") = "T" or session("lang")= "" then
					title_player_th = rs("detail_th")
				else
					title_player_en = rs("detail")
				end if
				share = rs("share")
				name = rs("name")
				file_name = share+"_"+name
				
			 end if
		 else 
			response.redirect(error_page_path)
		 end if
		
	%>
	<main class="cd-main-content">
		<div class="row">
			<section id="main">
				<!-- ########################### menu top ################################ -->
				<!--#include file="i_top_detail.asp"-->
				<!-- ########################### menu top ################################ -->
			</section>
		</div>
		<div class="row">
			<!-- ########################### content ################################ -->
			
			<!-- Main Content -->
			<div class="container paddingmain" style="padding-top:80px;">
				<div class="row">
					<div class="col-lg-12" style="padding-bottom: 50px;">
						<div class="row">
							<!-- ########################### content details ################################ -->
							<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
								<div class="row">
									<div class="col-lg-12">
										<p style="color:#324695;font-size: 34px;font-weight:normal;padding-bottom:12px;font-weight:bold;"><%=title%></p>
										<div style="background-color:#00ccff;width:120px;height:2px;"></div><br><br>
									</div>
								</div>
								
								<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="left" valign="top">
												<div style="width: 100%;color: #252525;line-height:20px;">
												<!-- .............................................................................. Content .............................................................................. -->	
											
													<table border="0" cellspacing="0" cellpadding="0" class="tb_maincontent">
														<tr>
															<td align="left">
																<p>
																	<br>
																	<br>
																	<strong><%if session("lang")="E" then%><%=title_player_en%><%else%><%=title_player_th%><%end if%></strong><br>
																	<br>
																</p>
															</td>
														</tr>
														 <tr>
															<td style="padding-right:207px;">
																<table width="100%" cellpadding="0" cellspacing="0" border="0" id="video_flash">
																	<tr>
																		<td>
																		<link href="css/video_js_5.19.css" rel="stylesheet">
																		<script src="scripts/video.js"></script>
																		<script src="scripts/videojs_contrib_hls.js"></script>
																		<video id="my_video_1" class="video-js" controls autoplay preload="auto" width="640" height="350" data-setup='{}'>
																			<source id="source2" src="<%=ip_webcasts_player%>/IR/Multimedia/<%=file_name%>/playlist.m3u8" type="application/x-mpegURL" />
																		</video>						
																		</td>
																	</tr>
																</table>
																
															</td>
														</tr>  
														<tr>
															<td>&nbsp;</td>
														</tr>
													</table>

												 <!-- .............................................................................. End Content .............................................................................. -->	</div>													  
											</td>
										</tr>
										<tr>
										  <td align="left">&nbsp;</td>
										</tr>
										<tr>
										  <td></td>
										</tr>
							  </table>
								
							</div>
							<!-- ########################### content details ################################ -->
							
						</div>
					</div>
				</div>
			</div>
			<!-- Main Content -->
			
			<!-- ########################### content ################################ -->
		</div>
		<div class="row">
			<!-- ########################### footer ################################ -->
			<!--include file="i_footer_detail.asp"-->
			<!-- ########################### footer ################################ -->
		</div>
	</main>

<!--#include file="i_googleAnalytics.asp"-->	
</body>
</html>