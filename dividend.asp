﻿<!DOCTYPE html>

<!--#include file = "../../i_constant.asp" -->
<!--#include file = "i_constant_listed.asp" -->
<!--#include file = "function.asp" -->
<%
session("cur_page")="IR " &  listed_share & " Dividend Payment"
session("page_asp")="dividend.asp"
page_name="Dividend Payment"
%>

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<TITLE><%=message_title%> :: <%=page_name%></TITLE>
	<META name="Title" content="<%=message_metatitle%>">
	<META name="Description" content="<%=message_description%>">
	<META name="Keywords" content="<%=message_keyword%>">	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="author" content="">
	<link rel="shortcut icon" href="<%=img_icon%>">
	<link href="style_listed.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="../../function2007_utf8.js"></script>
	<!-- CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/flexslider.css" rel="stylesheet" type="text/css">
	<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
	<!-- ADD CSS -->
	<link href="css/component.css" rel="stylesheet" type="text/css">
	<link href="css/custom.css" rel="stylesheet" type="text/css">
	<link href="css/mbr-additional.css" rel="stylesheet" type="text/css">
	<!-- FONTS -->
	<link href="css/font_add.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- Mouse Hover CSS -->
	<script src="scripts/jquery.min.js" type="text/javascript"></script>
	<script src="scripts/jquery.nicescroll.min.js"></script>
	<!-- SCRIPTS -->
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<!--[if IE]><html class="ie" lang="en"> <![endif]-->
	<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
	<script src="scripts/smooth-scroll.js" type="text/javascript"></script>
	<script src="scripts/jquery.flexslider-min.js" type="text/javascript"></script>
	<script src="scripts/animate.js" type="text/javascript"></script>
	<script src="scripts/myscript.js" type="text/javascript"></script>
	<script src="scripts/owl.carousel.js" type="text/javascript"></script>
	<!-- NEW -->
	<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
	<link href="css/site.css" rel="stylesheet" type="text/css" />
	<script src="scripts/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
	<!-- scroll-hint -->
	<link rel="stylesheet" href="css/scroll-hint.css">
</head>

<body class="main">

	<!-- PRELOADER -->
	<img id="preloader" src="images/preloader.gif" alt="">
	<!-- //PRELOADER -->
	<div class="preloader_hide">
		<div id="page" class="single_page">

			<!--================================ Menu Top ==================================-->
				<!--#include file='i_menu_top.asp'-->
			<!--=================================== End ====================================-->

			<!--=============================== Menu Left ==================================-->
				<!--#include file='i_menu_left.asp'-->
			<!--================================== End =====================================-->

			<!--============================ Title Menu =============================-->
			<section class="mbr-section article">
				<div class="container">
					<div class="bigTopic">
						<h1 class="bigTopicText"><%=arr_menu_desc(36)%></h1>
					</div>
				</div>
			</section>
			<!--============================== End ==================================-->

			<section class="mbr-section article sectionSub">
			<!--===================================================== Content ====================================================-->
			<%if session("lang")="E" then%>
			<div class="container">
				<p class="bigTopicSubText" style="text-indent: 0;">The Company and its subsidiaries have the policy to pay dividends to shareholders with criteria and conditions of payment as follows:</p>
				<p class="bigTopicSubText">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- In the absence of unforeseen circumstances, the Company has a policy to pay dividend no less than 
				50 percent of net profit after corporate income tax and legal reserves. This dividend policy is subjected to change depending on economic conditions, liquidity, investment plans and legal 
				conditions. The Company will consider the necessity and appropriateness of other factors in the future. The payment of dividends will not have a significant impact on the Company’s normal 
				operations. Resolution of Board of Directors on dividend payment must be submitted to the shareholders' meeting for approval. The Board of Directors has the authority to pay an interim 
				dividend if it deems appropriate and will not affect the business operations. The Board of Directors is required to report the dividend payments to the shareholders at its next 
				shareholders’ meeting.</p>
				<p class="bigTopicSubText">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- The subsidiary company has a policy to pay dividends each year at a rate of not less than 50 percent 
				of the net income after tax and after deduction of all reserves as required by law. Dividend payment will depend on appropriateness, investment plan, liquidity and financial condition of the 
				subsidiary.</p>
				
				  <div class="table-data js-scrollable scroll-hint" style="position: relative; overflow: auto;padding-top: 19px;padding-bottom: 30px;">
					<table border="0" cellspacing="0" cellpadding="0" class="table-width table-MajorShareholder">
					  <tbody style="font-size: 15.5px;">
						<tr>
						  <th align="center" class="topicTeble">Approved Date</th>
						  <th align="center" class="topicTeble">X-Date</th>
						  <th align="center" class="topicTeble">Payment Date</th>
						  <th align="center" class="topicTeble">Dividend Type</th>
						  <th align="center" class="topicTeble">Dividend<br>(Baht/Share)</th>
						  <th align="center" class="topicTeble">Operation Period</th>
						</tr>
						<tr class="even">
						  <td align="center">23/04/2019</td>
						  <td align="center">29/04/2019</td>
						  <td align="center">22/05/2562</td>
						  <td align="center">Cash</td>
						  <td align="center">0.10</td>
						  <td align="center">01/10/18-31/12/618 </td>
						</tr>
					  </tbody>
					</table>
				  </div>
				  
				<div style="text-align: center;margin-top: 30px;"><img src="images/DividendPayment.png"></div>
				<!-- <div style="height: auto;overflow: hidden;"><img src="images/dp.jpg"></div> -->
			   
				</div>
			<%else%>
			<div class="container">
				<p class="bigTopicSubText" style="text-indent: 0;">บริษัทฯ และบริษัทย่อยมีนโยบายจ่ายเงินปันผลให้แก่ผู้ถือหุ้น โดยมีหลักเกณฑ์และเงื่อนไขการจ่ายเงินปันผล ดังนี้</p>
				<p class="bigTopicSubText">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- บริษัทฯ มีนโยบายจ่ายเงินปันผลในแต่ละปีในอัตราไม่น้อยกว่าร้อยละ 50.00 ของกำไรสุทธิที่เหลือหลังจากหักสำรองต่างๆ ทุกประเภทตามที่กฎหมายกำหนด อย่างไรก็ตามการจ่ายเงินปันผลดังกล่าวอาจมีการเปลี่ยนแปลง ขึ้นอยู่กับสภาวะเศรษฐกิจกระแสเงินสด แผนการลงทุน เงื่อนไขทางกฎหมายโดยบริษัทจะคำนึงถึงความจำเป็นและเหมาะสมของปัจจัยอื่นๆ ในอนาคต และการจ่ายเงินปันผลนั้นจะไม่มีผลกระทบต่อการดำเนินงานปกติของบริษัทอย่างมีนัยสำคัญ
				<br>ทั้งนี้มติที่ประชุมคณะกรรมการของบริษัทที่อนุมัติให้จ่ายเงินปันผลจะต้องนำเสนอต่อที่ประชุมผู้ถือหุ้นเพื่อขออนุมัติ อนึ่งคณะกรรมการบริษัทมีอำนาจในการพิจารณาจ่ายเงินปันผลระหว่างกาลได้หากเห็นควรว่ามีความเหมาะสมและไม่กระทบต่อการดำเนินงานของบริษัท ทั้งนี้จะต้องรายงานให้ที่ประชุมผู้ถือหุ้นทราบในการประชุมคราวถัดไป</p>
				<p class="bigTopicSubText">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- บริษัทย่อยมีนโยบายจ่ายเงินปันผลในแต่ละปีในอัตราไม่น้อยกว่าร้อยละ 50.00 ของกำไรสุทธิของงบการเงิน หลังหักภาษีเงินได้นิติบุคคล และหักสำรองตามกฎหมายต่างๆ ตามที่กฎหมายกำหนด และขึ้นอยู่กับความเหมาะสม แผนการลงทุน รวมทั้งจะคำนึงถึงกระแสเงินสดและฐานะทางการเงินของบริษัทย่อยเป็นสำคัญ</p>
				
				  <div class="table-data js-scrollable scroll-hint" style="position: relative; overflow: auto;padding-top: 19px;padding-bottom: 30px;">
					<table border="0" cellspacing="0" cellpadding="0" class="table-width table-MajorShareholder">
					  <tbody style="font-size: 15.5px;">
						<tr>
						  <th align="center" class="topicTeble">วันที่ <br> คณะกรรมการมีมติ</th>
						  <th align="center" class="topicTeble">วันที่ <br> ขึ้นเครื่องหมาย</th>
						  <th align="center" class="topicTeble">วันที่ <br> จ่ายเงินปันผล</th>
						  <th align="center" class="topicTeble">ประเภท <br> เงินปันผล</th>
						  <th align="center" class="topicTeble">จำนวนเงิน <br> ปันผลต่อหุ้น</th>
						  <th align="center" class="topicTeble">วันผล <br> ประกอบการ</th>
						</tr>
						<tr class="even">
						  <td align="center">23/04/2562</td>
						  <td align="center">29/04/2562</td>
						  <td align="center">22/05/2562</td>
						  <td align="center">เงินสด</td>
						  <td align="center">0.10</td>
						  <td align="center">01/10/61-31/12/61 </td>
						</tr>
					  </tbody>
					</table>
				  </div>
				  
				<div style="text-align: center;margin-top: 30px;"><img src="images/DividendPayment.png"></div>
				<!-- <div style="height: auto;overflow: hidden;"><img src="images/dp.jpg"></div> -->
			   
				</div>
				
				<%end if%>
			<!--======================================================= End ======================================================-->
			</section>
			
		</div>

		<!--====================================== Footer ======================================-->
			<!--#include file='i_footer.asp'-->
		<!--======================================== End =======================================-->

		<script type="text/javascript" src="scripts/modernizr.custom.js"></script>
		<%if session("lang")="E" then%><script type="text/javascript" src="scripts/jquery.dlmenu-en.js"></script><%else%><script type="text/javascript" src="scripts/jquery.dlmenu.js"></script><%end if%>
		<script>
			$(function() {
				$('#dl-menu').dlmenu();
			});
		</script>
		<!-- js/scroll-hint -->
		<script src="scripts/scroll-hint.js"></script>
		<!-- js/scroll-hint -->
		<script>
			new ScrollHint('.js-scrollable', {
			});
		</script>
	</div>
	 <!--#include file="i_googleAnalytics.asp"-->	  
</body>
</html>