<style>
	input[type=text], textarea, select {
		border-radius: 0px;
		border: 1px solid #006c9d;
		line-height: 20px;
		font-size: 13px;
		font-weight: normal;
		margin-bottom: 4px;
		height: 30px !important;
	}
	input[type=text]:focus, textarea:focus {
		border: 1px solid #006c9d;
	}
</style>

<form action="<%=session("page_asp")%>" method='post' name="frm1">	
	<table cellSpacing="0" cellPadding="0" border="0" width="100%" align="center">
		<tr>
			<td align="center">
				<table width="100%"  border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table width="100%"  border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td align="left">
										<div style="width: 100%;color: #252525;line-height:20px;">
											<%if session("lang")="T" or session("lang")=""  then%>
											เมื่อท่านไม่ได้รับความเป็นธรรมในการลงทุน หรือพบเห็นการกระทำผิด การกระทำอันไม่เป็นธรรมเกี่ยวกับบริษัท และ/หรือ ผู้ลงทุน โดยคณะกรรมการตรวจสอบ และ/หรือ คณะกรรมการบริษัท จะดำเนินการตรวจสอบเรื่องร้องเรียนและเบาะแสที่ท่านแจ้งมา เพื่อให้มีความยุติธรรมแก่ผู้ลงทุนและบริษัทโดยเร็วที่สุด<br><br>
											
											กรุณากรอกข้อมูลให้ครบถ้วนโดยข้อมูลนี้จะถูกส่งไปยัง คณะกรรมการตรวจสอบ และ/หรือ คณะกรรมการบริษัท ที่ได้รับการแต่งตั้งจาก CEO ข้อมูลของท่านจะถูกปกปิดเป็นความลับสูงสุด<br><br><br>
											<%else%>
											If you did not receive a fair investment, or found guilty of Acts of unfair competition on the company and investors, The Audit Committee and / or Board of Directors will performed with a complaint and clues that you inform us as soon as possible to be fair to investors<br><br>
											
											Please complete the form below, your information will be submitted to the Board / Audit committee who was appointed by CEO. Your information will be protected as confidential.<br><br><br>
											<%end if%>
										</div>
									</td>
								</tr>
								<tr>
									<td style="background-color: #f9f9f9; border-radius: 5px;">		
										<div style="width: 100%;color: #252525;padding-top: 30px;padding-bottom: 20px;line-height:20px;">									
											<table cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
												<tbody>
												
												<tr>
												  <td style="padding: 0;"><!-- ++++++++++++++++++++++++  content ++++++++++++++++++++++++-->
													
													<div class="webcasts">
													  <form action="complaints.asp" method="post" name="frm1">
														<table cellspacing="0" cellpadding="0" border="0" width="100%" align="center" class="bg_email_alert5">
														  <tbody>
														  
														  <tr>
															<td style="padding: 0;" align="center">
																<table width="100%" border="0" cellspacing="0" cellpadding="0">
																	<tbody>
																		<tr>
																		  <td style="background-color: #f9f9f9; border-radius: 5px; ">
																			<div class="capcha-wrap" style="display:inline-block;"> <br>
																			  <img src="img/captcha.png" style="width:101%; border:0px solid #ccc; border-radius:4px;" class="hidden animated"> </div>
																			<br><br>
																				<table width="98%" border="0" cellspacing="0" cellpadding="10">
																					<tbody style="font-size:  14px;">
																						<tr style="font-size:  14px; color: #324695;">
																							  <td class="align-complaint display-complaint" width="35%" align="right" valign="top"><font color="#ec5140">*</font> <%if session("lang")="T" or session("lang")=""  then%>ประเภทการรับเรื่องร้องเรียน<%else%>Category<%end if%></td>
																							  <td class="display-complaint text-center" align="left" width="2%" valign="top">:</td>

																							  <td class="display-complaint" align="left" valign="top" style="padding-bottom:10px;">
																								  <select name="catagory" id="catagory" size="1" style="cursor:hand; height: 35px;" class="style_combo">
																									<option selected value="0"><%if session("lang")="T" or session("lang")=""  then%>กรุณาเลือกหัวข้อการร้องเรียน*<%else%>Please Specify*<%end if%></option>
																									<%	
																										strsql=""	
																										strsql=sql_complaint_type(session("lang"))		
																										
																										set rs=conir.execute(strsql)
																										if not rs.eof and not rs.bof then
																											do while not rs.eof %>
																									
																									<option value="<%=rs("cp_id")%>" <%if catagory=trim(rs("cp_id")) then%> selected<%end if%>><%=rs("catagory")%></option>
																									
																									<% rs.movenext	
																											loop		
																										end if
																										rs.close
																										set rs=nothing
																										conir.close
																										set conir=nothing																	
																									%>
																								</select>
																							</td>
																						</tr>
																						<tr style="color: #324695;">
																							  <td class="align-complaint display-complaint" align="right" valign="top">
																								<font color="#ec5140">*</font> <%if session("lang")="T" or session("lang")=""  then%>รายละเอียด<%else%>Description<%end if%>
																							</td>
																							  <td class="display-complaint text-center" align="left" valign="top">:</td>
																							  <td class="display-complaint" align="left" valign="top">
																								<textarea name="description" id="description" type="textarea" cols="43" rows="3" class="style_text_area" wrap="virtual" value="<%=description%>"></textarea>
																								<br>
																								<%if session("lang")="T" or session("lang")=""  then%>กรุณาใส่ข้อความไม่เกิน 3000 ตัวอักษร<%else%>Detail character limit to 3,000<%end if%> </td>
																						</tr>
																						
																						 <tr style="color: #324695;">
																							<td class="align-complaint display-complaint" align="right" valign="top" >
																								<%if session("lang")="T" or session("lang")=""  then%>เอกสารแนบ<%else%>Attach File<%end if%>
																							</td>
																							<td align="left" valign="top">:</td>
																							<td align="left" valign="top">
																								<!----------------------------------------  Upload File ---------------------------------------->	
																									<IFRAME name="frameUpload" id="frameUpload" src="../../upload_file/default.asp" frameBorder="0" width="100%" height="auto" scrolling="no"></IFRAME>
																								<!-----------------------------------------  End Upload File ---------------------------------->	
																								
																								<%if session("lang")="T" or session("lang")=""  then%>
																									รองรับไฟล์ .doc .xls .pdf .jpeg .tif (ขนาดไม่เกิน 2MB)<br>
																									และไม่อนุญาติให้อัพโหลดไฟล์ .exe
																								<%else%>
																									File Support .doc .xls .pdf .jpeg .tif (File Should not over 2MB)<br>
																									and .exe does not allow.
																								<%end if%>
																							</td>
																						</tr>
																						
																						<tr style="color: #324695;">
																							<td class="display-complaint" align="center" valign="top" colspan="3"><h3 class="text-orange"></h3>
																								<p style="padding-top:30px; padding-bottom:30px; color:#324695; font-size:24px;">
																									<%if session("lang")="T" or session("lang")=""  then%>ช่องทางติดต่อกลับผู้ร้องเรียน<%else%>Reply to complainant<%end if%>
																								</p>
																							</td>
																						</tr>
																						<tr style="color: #324695;">
																							  <td class="align-complaint display-complaint" align="right" valign="top" style="padding-bottom:10px;">
																								<font color="#ec5140">*</font> <%if session("lang")="T" or session("lang")=""  then%>ชื่อ<%else%>Name<%end if%>
																							</td>
																							  <td class="display-complaint text-center" align="left" valign="top">:</td>
																							  <td class="display-complaint" align="left" valign="top">
																								<input name="name" id="name" type="text" size="35" maxlength="50" class="style_textbox" value="<%=name%>">
																							</td>
																						</tr>
																						<tr style="color: #324695;">
																						  <td class="align-complaint display-complaint" align="right" valign="top" style="color: #324695; padding-bottom:10px;">
																							<%if session("lang")="T" or session("lang")=""  then%>เบอร์โทรศัพท์<%else%>Telephone<%end if%>
																						  </td>
																						  <td class="display-complaint text-center" align="left" valign="top">:</td>
																						  <td class="display-complaint" align="left" valign="top">
																							<input name="telphone" id="telphone" type="text" size="35" maxlength="50" class="style_textbox" value="<%=telphone%>">
																						  </td>
																						</tr>
																						<tr style="color: #324695;">
																							  <td class="align-complaint display-complaint" align="right" valign="top" style="color: #324695; padding-bottom:10px;">
																								<font color="#ec5140">*</font> <%if session("lang")="T" or session("lang")=""  then%>อีเมล์<%else%>E-mail<%end if%>
																							  </td>
																							  <td class="display-complaint text-center" align="right" valign="top">:</td>
																							  <td class="display-complaint" align="left" valign="top">
																								<input name="email" id="email" type="text" size="35" maxlength="100" class="style_textbox" value="<%=email%>">
																							</td>
																						</tr>
																						<tr style="color: #324695;">
																							  <td class="align-complaint display-complaint" align="right" valign="top">
																								<%if session("lang")="T" or session("lang")=""  then%>ช่องทางอื่นๆ<%else%>Other<%end if%>
																							</td>
																							  <td class="display-complaint text-center" align="left" valign="top">:</td>
																							  <td class="display-complaint" align="left" valign="top" style="padding-bottom:10px;">
																								<textarea name="other" id="other" type="textarea" cols="43" rows="4" class="style_text_area" wrap="virtual" value="<%=other%>"></textarea>
																							  </td>
																						</tr>
																						<tr class="form-group" style="color: #324695;">
																							  <td class="align-complaint display-complaint" align="right" valign="top" style="padding-bottom:10px;">
																								<font color="#ec5140">*</font> <%if session("lang")="E" then%>Please Enter the Code<%else%>กรุณากรอกรหัสที่ท่านเห็น<%end if%>
																							</td>
																							  <td class="display-complaint text-center" align="left" valign="top">:</td>
																							  <td class="display-complaint" align="left" valign="top">
																								<input name="resultCaptcha" id="resultCaptcha" placeholder="<%if session("lang")="E" then%>Please enter code<%else%>โปรดกรอกรหัส<%end if%>" class="form-control radius-edit"  type="text">	
																							  </td>
																						</tr>
																						
																						<tr class="form-group" height="50px">
																							  <td></td>
																							  <td class="display-complaint text-center" align="left" valign="top"></td>
																							  <td>
																								<iframe id="iframe_content_captcha" name="iframe_content_captcha" src="i_content_captcha.asp" frameborder="0" framespacing="0" framepadding="0"  scrolling="no" style="width:200px;height: 70px;padding-bottom:10px;"></iframe>
																							  </td>
																						</tr>
																						<tr class="form-group">
																							<td></td>
																							<td class="display-complaint text-center" align="left" valign="top"></td>
																							<td>									  
																								<div class="news col-lg-4 col-md-6 col-sm-6 col-xs-6 complainbutton" style="padding: 12px 15px; background: #006c9d; color: #FFFFFF; text-align: center; width: 150px;"> 
																									<a href="#0" onclick="javascript: return chk_complaints_info();">
																										<font color="#ffffff"><%if session("lang")="T" or session("lang")=""  then%>ส่งคำร้อง<%else%>Submit<%end if%></font>
																										<br>
																									</a>
																								</div>
																							</td>									
																						</tr>
																						<tr>
																							<td class="display-complaint hidden-sm hidden-xs">&nbsp;</td>
																							<td class="display-complaint hidden-sm hidden-xs">&nbsp;</td>
																							<td class="display-complaint" align="left" valign="top"><br>
																								<p style="color:red;"><%if session("lang")="T" or session("lang")=""  then%>* หมายเหตุ กรุณากรอกข้อมูลที่จำเป็น<%else%>* Note: All fields are required.<%end if%></p><br>
																								<br>
																							</td>
																						</tr>
																					</tbody> 
																				</table>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</td>
														  </tr>
														  </tbody>
														  
														</table>
													  </form>
													</div>
													
													<!-- ++++++++++++++++++++++ end content +++++++++++++++++++++++--></td>
												</tr>
												</tbody>
												
											  </table>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</form>