﻿<script src="scripts/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="scripts/calendar_scripts/js/daterangepicker.jQuery.js"></script>
<style>
	::-webkit-inner-spin-button { 
		 -webkit-appearance: none;
	}
</style>
<link rel="stylesheet" type="text/css" href="scripts/calendar_scripts/css/ui.daterangepicker.css">
 <link title="ui-theme" rel="stylesheet" type="text/css" href="scripts/calendar_scripts/css/jquery-ui-1.7.1.custom.css">
 <link rel="Stylesheet" type="text/css" href="scripts/calendar_scripts/css/demoPages.css" media="screen">
<link href="css/style.css" rel="stylesheet" type="text/css">
<style>
	.style_button {
		padding: 10px 25px; 
		background: #006c9d; 
		color: #FFFFFF;
	}
</style>

		<%if session("page_asp")="news_clipping.asp" then
				intpastyear = 1%>
			<script language="javascript" >
				var dateStartDefault = "-1y";
			</script>		
		<%end if%>
		
			 <script language="javascript" >
				 jQuery.noConflict();
			</script>
	<%
						
		Start_Date = DisplayDateNow(now,1,intpastyear)
		End_Date = DisplayDateNow(now,2,intpastyear)
		
		if request("hidStartDate") = "" then
			strdate = Start_Date
		else
			strdate = request("hidStartDate") 
		end if
		strdate = DateToString(strdate,"YYYY-MM-DD")
		strdatevalue = DateToString(strdate,"DD/MM/YYYY")
		
		if request("hidEndDate") = "" then
			ptdate = End_Date
		else
			ptdate = request("hidEndDate") 		
		end if
		ptdate = DateToString(ptdate,"YYYY-MM-DD")
		ptdatevalue = DateToString(ptdate,"DD/MM/YYYY")
		
		DateRangeValue = request("hidDateRangeValue")
	%>
	<br><br>
	<div class="col-lg-2"></div>
	<div class="col-lg-8">
		<div class="row">
			<div class="col-lg-4 col-md-4">
				<div style="padding-bottom:10px;"><span class="nomal_clipping"><%if session("lang")="E" then%>Start Date<%else%>ตั้งแต่วันที่<%end if%>&nbsp;&nbsp;
				<input id="StartDate" type="date" value="<%=strdate%>" required="required"></span>
				</div>
				
			</div>
			<div class="col-lg-1 "></div>
			<div class="col-lg-4 col-md-4">
				<div style="padding-bottom:10px;"><span class="nomal_clipping"><%if session("lang")="E" then%>End Date<%else%>ถึงวันที่<%end if%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input id="EndDate" type="date" value="<%=ptdate%>" required="required"><span>
				</div>
			</div>
			<div class="col-lg-1 "></div>
			<div class="col-lg-2 col-md-4 btn_search">
				<a href="javascript:void(0);" class="style_button" onclick="javascript: return calendar_search('frm1','<%=session("lang")%>');"><%if session("lang")="E" then%>Search<%else%>ค้นหา<%end if%></a>
			</div>
		</div>
	</div>
	<div class="col-lg-2"></div>

	<input type="hidden" name="hidDateRange" id="hidDateRange"  value="<%=DateRangeValue%>"> 
	<input type="hidden" name="hidStartDate" value="<%=strdate%>">
	<input type="hidden" name="hidEndDate" value="<%=ptdate%>">
	<input type="hidden" name="hidDateRangeValue" id="hidDateRangeValue" value="<%=DateRangeValue%>"> 

		