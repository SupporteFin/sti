﻿<style>
	input[type=text], textarea, select {
		border-radius: 0px;
		border: 1px solid #006c9d;
		line-height: 20px;
		font-size: 13px;
		font-weight: normal;
		margin-bottom: 4px;
		height: 30px !important;
	}
	input[type=text]:focus, textarea:focus {
		border: 1px solid #006c9d;
	}
</style>

<table cellSpacing=0 cellPadding=0 width="100%" border=0 align="center">
	<%if session("email_member") ="" then%>
	<tr>
	   <td align="left" valign="top">
			<table cellSpacing=0 cellPadding=3 width="100%" border=0 align="center">
				<tr>
					   <td align="left" valign="top">&nbsp;</td>
					</tr>	
					<tr>
						<td align="left" valign="top">	
							<%if session("lang")="E" then%>						
								Email alerts are messages that are conveniently delivered to your email
								box whenever certain new company information is posted to this site.
							<%else%>
								บริษัทจะส่งข้อมูลข่าวสารอัพเดท ผ่านระบบแจ้งเตือนข่าวสารทางอีเมลไปยังนักลงทุนและบุคคลที่สนใจ
							<%end if%>
							 </td>
					</tr>	
						<tr>
							<td align="left" valign="top">	
								<%if session("lang")="E" then%>								
							   To automatically receive <%if listed_share="" then%>listed company <%else response.write listed_share end if%> e-mail Alerts…
								<%else%>
								 ขั้นตอนการสมัครระบบแจ้งเตือนข่าวสารทางอีเมล
								<%end if%>
							 </td>
					</tr>
					<tr>																				   
						 <td align="left" valign="top" >	
							 <%if session("lang")="E" then%>			
								1. Enter your information in the space provided. 		
							<%else%>
							   1. กรอกรายละเอียดที่ช่องด้านล่าง
							<%end if%>
						 </td>
					</tr>	
					<tr>																				   
						 <td align="left" valign="top" >	
							 <%if session("lang")="E" then%>	
							  2. Click on the "<strong><font color="#4B6683">OK</font></strong>" button.
								<%else%>
								2. คลิกที่ปุ่ม  "<strong><font color="#4B6683">ตกลง</font></strong>" เพื่อใช้บริการ
								<%end if%>
						 </td>
					</tr>	
				</table>
	   </td>
	</tr>
	<tr>
	   <td align="left" valign="top">&nbsp;</td>
	</tr>		
	<%end if%>
	<tr>
		<td>
				<table width="100%"  border="0" cellpadding="15" cellspacing="5">
						<tr>
							<td align="center" valign="top">
									<table width="100%"  border="0" cellspacing="0" cellpadding="1">
										<%if session("email_member") ="" then%>
											<tr align="left" valign="top" >
													<td>
															<table width="100%"  border="0" cellspacing="0" cellpadding="5">
																<tr>
																	<td>
																		<%if session("lang")="E" then%>
																				<strong>Log in IR Member Service.</strong>
																	  <%else%>
																				<strong>เข้าสู่ระบบสมาชิก IR Services</strong>
																	  <%end if%>
																	</td>
																</tr>
																<tr align="left" valign="top" >
																		<td ><br>
																			<table width="100%"  border="0" cellpadding="5" cellspacing="0" align="left">
																					<tr>
																								<td align="left" valign="top">
																								<%if session("lang")="E" then%>			
																									Enter your E-Mail Address in the space provided, Log in IR Services
																								<%else%>
																								   กรอกอีเมลที่ช่องด้านล่าง เพื่อเข้าสู่ระบบ IR Services
																								<%end if%>	
																								</td>
																						</tr>
																						<tr>
																								<td align="left" valign="top">
																									<table  border="0" cellspacing="0" cellpadding="3">
																											<tr  valign="top">                 
																													<td><strong><font color="red"><b>*</b></font>&nbsp;<%if session("lang")="T" or session("lang")=""  then%>อีเมล<%else%>E-mail<%end if%> : </strong></td>
																											</tr>    
																											<tr  valign="top">                 
																													<td><input name="e_mail_member" id="e_mail_member"  type="text" size="40" maxlength="40"  class="style_textbox"  value=""></td>
																											</tr>      
																											
																									</table>
																								</td>
																						</tr>
																				</table>
																		</td>																										
																</tr>
														</table>
													</td>
													<td></td>
													<td  width="55%" >
															<table width="100%"  border="0" cellspacing="0" cellpadding="5">
																<tr>
																	<td align="left" valign="top" width="10"></td>
																	<td>
																			<%if session("lang")="E" then%>
																					<strong>If this is the first time you are using the e-mail alert services, then you will need to register.</strong>
																		  <%else%>
																					<strong>กรณีที่ท่านยังไม่ได้สมัครสมาชิก IR Services</strong>
																		  <%end if%>
																	</td>
																</tr>
																<tr>
																	<td>&nbsp;</td>
																	<td>
																			<!--#include file="i_content_sign_up.asp"-->	
																	</td>
																</tr>
															</table>
													</td>
										</tr>
									 <%end if%>
										<tr>
											<td colspan="3">
													<!--#include file="i_content_email_alert.asp"-->	
											</td>
										</tr>		
								</table>
							</td>
						</tr>
						 <tr>
							<td align="left" valign="top"><br>
									   <input type="hidden" name="flag" value="<%=flag%>">
										<!--input type="Submit" name="ok" value="Submit" style="FONT-FAMILY: Ms sans serif;FONT-SIZE:10px;cursor:hand;" onclick="javascript:return chksubmit();"-->
										<!--input type="Submit" name="ok" value="Reset" style="FONT-FAMILY: Ms sans serif;FONT-SIZE:10px;cursor:hand;" onclick="javascript:return chkreset('<%'=strconfirm%>');"-->
											<input type="button" name="ok" <%if session("lang")="T" or session("lang")=""  then %>value="ตกลง"<%else%>value="Submit" <%end if%>  class="style_button complainbutton_request"  onclick="javascript: return chk_Signin_emailalert();" style="padding: 5px 15px; background: #006c9d; color: #FFFFFF; text-align: center;">
											<input type="button" name="reset" <%if session("lang")="T" or session("lang")=""  then %>value="ยกเลิก"<%else%>value="Reset" <%end if%>   class="style_button2 complainbutton_request" onclick="javascript:document.frm1.submit();" style="padding: 5px 15px; background: #757677; color: #FFFFFF; text-align: center;">
											<input type="button" name="h_alert"  <%if session("lang")="T" or session("lang")=""  then %>value="ดูรายละเอียดทั้งหมด"<%else%> value="View all your e-mail alerts." <%end if%>    class="style_button2 complainbutton_request" onclick="javascript:window.open('../../IRPlus/EmailAlert.asp');" style="width: 180px; padding: 5px 15px; background: #006c9d; color: #FFFFFF; text-align: center;" >
											<%if session("email_member") <>"" then%><input type="button" name="logout" <%if session("lang")="T" or session("lang")=""  then %>value="ออกจากระบบ"<%else%>value="Logout" <%end if%>  class="style_button2 complainbutton_request" style="padding: 5px 15px; background: #757677; color: #FFFFFF; text-align: center;"  onclick="javascript:Logout_IRService('<%=session("page_asp")%>');"><%end if%>
											<input type="hidden" name="listed_com_id " id="listed_com_id" value="<%=listed_com_id%>">
											<input type="hidden" name="language" id="language" value="<%=session("lang")%>">
											<input type="hidden" name="hid_email_member " id="hid_email_member" value="<%=session("email_member")%>">
											<input type="hidden" name="action" id="action">
							</td>
					  </tr>
					  
					  <%if session("email_member") <>"" then%>
						<tr>
							<td align="left" valign="top"><br>	
								<%if session("lang")="T" or session("lang")=""  then
										 response.write "<strong>หมายเหตุ:</strong> ถ้าท่านต้องการยกเลิกการเป็นสมาชิก IR Services  กรุณา <a class='web' href='"&pathfileserver&"/IRPlus/EmailAlert.asp' target='_blank'><font class='style12'><strong>คลิกที่นี่</strong></font></a> เพื่อทำการยกเลิก"
									else
											response.write "<strong>Note:</strong> To unsubscribe from  " & listed_share & "  E-mail Alerts, please <a  class='web'  href='"&pathfileserver&"/IRPlus/EmailAlert.asp' target='_blank'><font class='style12'><strong>click here</strong></font></a>"
									end if 
								%>
							</td>
						</tr>
						<%end if%>
					</table>
			</td>
	  </tr>
</table>