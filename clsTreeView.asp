<%
'Option Explicit
' ======================================================================================
' Desc:		This code show how to create a simple treeview class using ASP and Cascading Stylesheets.
'			Greate for programmers who want to learn how to create simple ASP controls.
'
' Author:	Tanwani Anyangwe (tanwani@aspwebsolution.com)
'
' Requires: ASP 2.1 +
'
' Copyright � 2001 Tanwani Anyangwe for AspWebSolution.com
' --------------------------------------------------------------------------------------
' Visit AspWebSolution - free source code for ASP & AS.NET programmers
' http://aspwebsolution.com
' --------------------------------------------------------------------------------------
'
' Please ensure you visit the site and read their free source licensing
' information and requirements before using their code in your own
' application.
'
' ======================================================================================



Class Collection
	Private m_next,m_len
	Private m_dic	
	
	Public Sub Add(Item)
		m_dic.Add "K" & m_next,Item
		m_next = m_next+1		
		m_len = m_len+1		
	End Sub
	
	Public Sub Clear
		m_dic.RemoveAll 
	End Sub
	
	Public Function Length
		Length=m_len
	End Function
	
	Public Default Function Item(Index)
		Dim tempItem,i
		For Each tempItem In m_dic.Items 
			If i=Index Then
				Set Item=tempItem
				Exit Function
			End If
			i=i+1
		Next	
	End Function
	
	Public Sub Remove(ByVal Index)
		Dim Item,i
		For Each Item In m_dic.Items 
			If i=Index Then
				m_dic.Remove(Item)
				m_len=m_len-1
				Exit Sub
			End If
			i=i+1
		Next			
	End Sub
	
	Private Sub Class_Initialize
		m_len=0
		Set m_dic = Server.CreateObject("Scripting.Dictionary")				
	End Sub
	
	Private Sub Class_Terminate
		Set m_dic = Nothing				
	End Sub
End Class

Class Node	
	'Public Parent
	Public Text
	Public Href
	Public Target
	Public ToolTipText
	Public ChildNodes
	Public ImageUrl
	Public ID
	Public Menu
	Public Hignlight
	Public SubID
	Public MainID
	Public SortParent
	
	Public Sub Init(strText,strHref,strToolTipText,strMenu,strHignlight,strParentID,strID,strSortParent)
		Text=strText
		Href=strHref
		ToolTipText=strToolTipText
		Menu = strMenu
		Hignlight = strHignlight
		SubID = strParentID
		MainID = strID
		SortParent = strSortParent
		
	End Sub
	
	Public Sub Add(objNode)
		ChildNodes.Add(objNode)
	End Sub
	
	Private Sub Class_Initialize
		Set ChildNodes = New Collection				
	End Sub
	
	Private Sub Class_Terminate
		Set ChildNodes = Nothing				
	End Sub
End Class

Class TreeView
	
	Private m_folder
	Public Color	
	Public Nodes
	Public DefaultTarget
	Public ID
	
	Public Property Let ImagesFolder(strFolder)
		m_folder=strFolder
	End Property
	Public Property Get ImagesFolder()
		ImagesFolder=m_folder	
	End Property
	
	Private Sub Class_Initialize
		Set Nodes = New Collection	
		Color="Navy"
		m_folder="images"					
	End Sub
	
	Private Sub Class_Terminate
		Set Nodes = Nothing				
	End Sub
	
	Public Function AddNode(Text)
		Dim tn 
		Set tn = new Node
		tn.Text=Text
		Nodes.Add(tn)
	End Function
	
	Public Function CreateNode(Text,Href,ToolTipText)
		Dim tn 
		Set tn = new Node
		Call tn.Init(Text,Href,ToolTipText,strMenu,strHignlight)
		Set CreateNode=tn
	End Function
	
	Public Function CreateSimpleNode(Text)
		Dim tn 
		Set tn = new Node
		tn.Text = Text
		Set CreateSimpleNode=tn
	End Function

	
	Private Sub LoopThru(NodeList,Parent)	
		Dim i,j,Node,blnHasChild,strStyle	
		if len(Parent) > 3 then
			Out("<ul id='nav-sub-meeting' groupname='childmenu'>")		
		else
			Out("<ul id='nav-sub-meeting'>")		
		end if
		
		Out("<table width='100%' border='0' cellspacing='0' cellpadding='0' class='color_gm_head'>")
		For i=0 To NodeList.Length-1
			Out("<tr valign='top'>")
			
			Set Node = NodeList(i)		
			If (Node.ChildNodes.Length>0) Then 
				blnHasChild=True	
			Else
				blnHasChild=False
			End If
			If Node.ImageUrl="" Then
				strStyle="<img src='images/plus.png' border='0' align='absmiddle' style='padding-left: 10px;'> "
			Else
				'strStyle="style='list-style-image: url("& Node.ImageUrl &");'"
				strStyle="<img src='" & Node.ImageUrl &"' border='0' align='absmiddle'> "
			End If				

			If Node.Target="" Then
				Node.Target=DefaultTarget
			End If
			
			parentId = ""
			seqId = ""
			seqText=""
			seqId=i+1
			if parent<>"0" then
				arr_parent = split(parent,"_")
				
				for loops = 2 to ubound(arr_parent)
					parentid=parentid & (cint(arr_parent(loops))+1) & "."
				next
				
				if parentid = "" then
					seqText= seqId & ". "
				else
					seqText=parentid & seqId & " "
				end if
				if len(Parent) = 3 then
					seqText = "<font class='fontsubhead_meeting'>"&seqText& "</font>"
				end if
				'Out("<td width='10' align='right'>&nbsp;</td>")
				'Out("<td width='3' align='right'>" & seqText & "</td>")
			end if
			
			
			Out("<td>")
			If Parent = "0" Then
				'Out("<br>")    
			end if
			
			Out("<li class='sub-level-meeting'>")
			'response.write Node.SortParent & "<br>"
			'response.write Node.MainID &"&nbsp;&nbsp;&nbsp;"& Node.subID
			If Parent<>"0" Then
				if Node.Hignlight = "Y" then
					Out("<div class='Hignlight' name='Hignlight_sub_Header'><table border='0' width='100%' cellspacing='1' cellpadding='5' ><tr class='color_gm'><td><input name='H_subID' type='hidden' value='"& Node.SubID &"'/>" )
				else
					Out("<div name='unlight'><table border='0' width='100%' cellspacing='1' cellpadding='5' ><tr class='color_gm'><td><input name='subID' type='hidden' value='"& Node.SubID &"'/>" )
				end if				
			else
				if Node.Hignlight = "Y" then
					Out("<div class='Hignlight' name='Hignlight_Header'>")
				else
					Out("<div name='unlight'>")
				end if
				if Node.Menu="" then
					Out("<table border='0' width='100%' cellspacing='0' cellpadding='0'><tr><td class='color_gm_head_1'><input type='hidden' value='"& Node.MainID &"'/>")
				else
					Out("<table border='0' width='100%' cellspacing='0' cellpadding='0'><tr><td  class='color_gm_head_1'><input name='MainID' type='hidden' value='"& Node.MainID &"'/>")
				end if
			end if
			
			if Node.Href="" then
				If Parent<>"0" Then
					if len(Parent) = 3 then
						Out("<font class='fontsubhead_meeting'>" & Node.Text & "</font>"  )
					else
						Out("<span style='display: inline-block;padding-left: 20px;'><font class='fontnolinkmeeting'>" & Node.Text & "</font>"  )
					end if
				else
					if Node.Menu="" then
						Out(strStyle  & " <font class='fontsubhead'>" & Node.Text & "</font>"  )
					else
						Out(strStyle  & " <font class='fonttreeview'>" & Node.Text & "</font>"  )
					end if
				end if
			else
				If Parent<>"0" Then
					' ��������
					if len(Parent) = 3 then
						Out("<span ><a class='treeview' href=""" & Node.Href & """ target=""" & Node.Target & """  title=""" & Node.ToolTipText & """><font class='fontsubhead_meeting'>" & Node.Text & "</font></a></span>")
					else 'Sub �ͧ��������
						Out("<span style='display: inline-block;padding-left: 20px;'><a class='treeview' href=""" & Node.Href & """ target=""" & Node.Target & """  title=""" & Node.ToolTipText & """>" & Node.Text & "</a></span>")
					end if
				else
					' ������ѡ
					if Node.Menu="" then
						Out(strStyle  & "<a class='treeviehead' href=""" & Node.Href & """ target=""" & Node.Target & """  title=""" & Node.ToolTipText & """><font class='fontsubhead'>" & Node.Text & "</font></a></span>")
					else
						Out(strStyle  & "<a class='treeviehead' href=""" & Node.Href & """ target=""" & Node.Target & """  title=""" & Node.ToolTipText & """>" & Node.Text & "</a></span>")
						
					end if
				end if
			end if
			
			Out("</td>" )
			If Parent<>"0" Then
				   Out("<td width='50px' align='center'>" )			
				   	if Node.Href<>"" then
						if len(Parent) = 3 then
							Out("<a class='treeview' href=""" & Node.Href & """ target=""" & Node.Target & """  title=""" & Node.ToolTipText & """><font class='fontsubhead_meeting'><img src='images/pdf.png' width='20px' align='absmiddle' border='0'></font></a></span>")
						else 'Sub �ͧ��������
							Out("<a class='treeview' href=""" & Node.Href & """ target=""" & Node.Target & """  title=""" & Node.ToolTipText & """><img src='images/pdf.png' width='20px' align='absmiddle' border='0'></a></span>")
						end if
					else
						if len(Parent) >= 3 then
							'response.write len(Parent)
							'blnHasChild
							if blnHasChild = "True" then
								Out("&#9660;") 'Symbol �١��
							end if
							
						end if						
					end if
					Out("</td>" )				
			end if
			Out("</tr></table></div>" )
			'Out("</td></tr></table></div>" )
			'End If
				
			If blnHasChild Then					
				Call LoopThru(Node.ChildNodes,Parent & "_" & i)
			End If						
				
			Out("</li>")	
			Out("</td>")
			Out("</tr>")
			
			If Parent = "0" Then
			Out("<tr><td>&nbsp;</td></tr>" )
			end if
			'Out ("</li>")
			
		Next
		Out("</table>")
		
		Out("</ul>")
		'Out ("</ol>")
	End Sub

	Private Sub Out(s)
		Response.Write(s)
	End Sub
	
	Public Sub Display
		'Out("<script>function toggle"& id &"(id,p){var myChild = document.getElementById(id);if(myChild.style.display!='block'){myChild.style.display='block';document.getElementById(p).className='folderOpen';}else{myChild.style.display='none';document.getElementById(p).className='folder';}}</script>")
		'Out("<style>ul.tree{display:none;margin-left:17px;}li.folder{list-style-image: url("& ImagesFolder &"/plus.gif);}li.folderOpen{list-style-image: url("& ImagesFolder &"/minus.gif);}li.file{list-style-image: url("& ImagesFolder &"/dot.gif);}a.treeview{color:"& Color &";font-family:verdana;font-size:8pt;}a.treeview:link {text-decoration:none;}a.treeview:visited{text-decoration:none;}a.treeview:hover {text-decoration:underline;}</style>")
		Call LoopThru(Nodes,0)		
	End Sub
	
	Public Sub LoadFromDB(strConn,strMenuTable)
		Dim Conn 
		Set Conn = Server.CreateObject("ADODB.Connection")
		Conn.Open strConn
		
		Dim RS,node,parentid,parentNode
		Set RS = Conn.Execute("SELECT * FROM " & strMenuTable & " ORDER BY MenuID,ParentID")
		
		If Not RS.EOF Then
			Do While Not RS.EOF
				parentid=RS("ParentID")
				
				Dim child				 
				Set child = new Node
				Call child.Init(RS("Text"),RS("URL"),RS("ToolTip"))
				child.ID =RS("MenuID")
				
				If parentid=0 then
					Nodes.Add(child)
				Else
					Set parentNode = FindNode(Nodes,ParentID)
					If Not (parentNode is Nothing) Then
						parentNode.Add(child)
					End If
				End If
				RS.MoveNext		
			Loop
			RS.Close
		End If
		Set RS = Nothing
		Conn.Close 
		Set Conn = Nothing		
	End Sub
	
	Public Function FindNode (nodes,ID)
		dim i,tempNode		
		For i=0 To nodes.Length-1
			Set tempNode = nodes(i)
			if tempNode.Id=ID then
				Set FindNode=tempNode
				Exit Function
			Else
				If tempNode.ChildNodes.length>0 Then
					Set tempNode = FindNode(tempNode.ChildNodes,ID)
					If Not (tempNode is Nothing) Then
						Set FindNode=tempNode
						Exit Function
					End If
				end if
			End If				
		Next
		Set FindNode = Nothing			
	End Function
	
	Public Sub DisplayFolderContents(ByVal strFolderPath)
		Out("<script>function toggle"& id &"(id,p){var myChild = document.getElementById(id);if(myChild.style.display!='block'){myChild.style.display='block';document.getElementById(p).className='folderOpen';}else{myChild.style.display='none';document.getElementById(p).className='folder';}}</script>")
		Out("<style>ul.tree{display:none;margin-left:17px;}li.folder{list-style-image: url("& ImagesFolder &"/plus.gif);}li.folderOpen{list-style-image: url("& ImagesFolder &"/minus.gif);}li.file{list-style-image: url("& ImagesFolder &"/dot.gif);}a.treeview{color:"& Color &";font-family:verdana;font-size:8pt;}a.treeview:link {text-decoration:none;}a.treeview:visited{text-decoration:none;}a.treeview:hover {text-decoration:underline;}</style>")

		Dim fso 
		Set fso = Server.CreateObject("Scripting.FileSystemObject")
		If fso.FolderExists(strFolderPath) Then			
			Call ListFolderContents(fso.GetFolder(strFolderPath),0)			
		Else
			Out "<font color=red>Folder <b>'" & strFolder & "'</b> does not exist</font>"
		End If
		Set fso = Nothing
	End Sub
	
	Private Sub ListFolderContents(objFolder,Parent)
		Dim objSubFolder, objFile	
		If Parent<>"0" Then
			Out ("<ul class=tree id=""N" & Parent & """>")
		Else
			Out ("<ul xstyle='margin-left:20px;' id=""N" & Parent & """>")
		End If	
		
		dim i
		For Each objSubFolder In objFolder.SubFolders
			Out("<li class=folder id=""P" & Parent & i & """><a class=treeview href=""javascript:toggle"& id &"('N" & Parent & "_" & i & "','P" & Parent & i & "')"">")
			Out objSubFolder.Name & "</a>"			
			Call ListFolderContents(objSubFolder,Parent & "_" & i)
			Out "</li>"
			i=i+1
		Next
		
		For Each objFile In objFolder.Files
			Out "<li class=file>" & objFile.Name & "</li>"
		Next
		
		Out "</ul>"
		
		Set objFile = Nothing
		Set objSubFolder = Nothing
	End Sub
	
	' Add Silde Menu For Listed THANA By Bowl 2011-03-22
	Public Sub DisplaySlideMenu
		Call LoopThruSlideMenu(Nodes,0)		
	End Sub
	
	Private Sub LoopThruSlideMenu(NodeList,Parent)	
	
		Dim i,j,Node,blnHasChild,strStyle
		
		Out("<ul id='nav-sub-meeting'>")
		
		For i=0 To NodeList.Length-1
			
			Set Node = NodeList(i)		
			If (Node.ChildNodes.Length>0) Then 
				blnHasChild=True	
			Else
				blnHasChild=False
			End If
			If Node.ImageUrl="" Then
				strStyle=""
			Else
				strStyle="style='list-style-image: url("& Node.ImageUrl &");'"
			End If		
			
			If Node.Target="" Then
				Node.Target=DefaultTarget
			End If
			
			parentId = ""
			seqId = ""
			seqText=""
			seqId=i+1
			if parent<>"0" then
				arr_parent = split(parent,"_")
				
				for loops = 2 to ubound(arr_parent)
					parentid=parentid & (cint(arr_parent(loops))+1) & "."
				next
				
				if parentid = "" then
					seqText= seqId & ". "
				else
					seqText=parentid & seqId & " "
				end if
				Out(seqText)
				
			end if
			
			Out("<li class='sub-level-meeting'>")
				Out("<div><table border='0' width='100%' cellpadding='5' cellspacing='0'><tr><td>" )
				if Node.Href="" then
					Out("<font class='fonttreeview'>" & Node.Text & "</font>" )
				else
					Out("<a class=treeview1 href=""" & Node.Href & """ target=""" & Node.Target & """  title=""" & Node.ToolTipText & """>" & Node.Text & "</a>")
				end if
				Out("</td></tr></table></div>" )
				Out("<ul style='display: none;'>")
					Out("<li>")			
						If blnHasChild Then	
							Call LoopThruSubMenu(Node.ChildNodes,Parent & "_" & i)
						End If
					Out("</li>")
				Out("</ul>")
			Out("</li>")			
		Next
		Out("</ul>")
	End Sub
	
	
End Class




%>

